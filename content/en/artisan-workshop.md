---
description: >-
  Laurel Parker Book is artisanal bookbinding workshop dedicated to artistic
  creation.
images:
  - /media/atelier-d-artisan-romainville-2024-site.jpg
  - /media/_40B9194.jpg
  - /media/atelier-laurel-parker-book-3.jpg
  - /media/vue_atelier_lpb_04.jpg
  - /media/atelier-laurel-parker-book-2.jpg
layout: page
menu:
  aside:
    weight: 2
slug: artisan-workshop
title: Artisan workshop
translationKey: atelier-artisan
___mb_schema: /.mattrbld/schemas/page.json
draft: null
---
Since 2008, Laurel Parker Book has been an artisanal bookbinding workshop dedicated to artistic creation. Each project is handcrafted in the workshop, tailor-made with particular attention to detail. Working with paper, the knowledge of the arts, of techniques, of the evolution of forms and of historical structures— all this allows coherent and lasting creations. The fields of Contemporary Art, of archives and collections, luxury goods, photography and publishing provide innovative projects and contribute to the specificity of the studio.

Projects by appointment. Please send us[ an email ](mailto:laurelparkerbook@gmail.com)
