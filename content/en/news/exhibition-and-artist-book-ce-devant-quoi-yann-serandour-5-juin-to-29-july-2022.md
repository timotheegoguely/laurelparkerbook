---
title: >-
  Exhibition and artist book : Ce devant quoi, Yann Sérandour, 5 Juin to 29 July
  2022
translationKey: cedevantquoi
description: null
categories:
  - Reliure
  - Livre d’artiste
  - Expositions
cover: /media/serandour_expo_laurel_parker_book_web1.jpg
date: 2022-06-04T00:00:00.000Z
___mb_schema: /.mattrbld/schemas/news.json
---
![](/media/expo_serandour_01_w_copitet.jpg)

![](/media/expo_serandour_02_w_copitet.jpg)

This leporello reproduces 66 covers of the same book by Jean-Paul Sartre, Esquisse d’une théorie des émotions, published by the Editions Hermann, in the form of a long, colored frieze in accordion binding. The shades of the reproduced covers have been arranged in the chronological order of the various reprints of the book between 1960 and 1995. Curiously turning from blue to green, the sequence reveals the progressive degradation of Adrian Frutiger’s original design and retraces the reception of Sartre’s book over time by means of its signs of wear. The cover of the piece features a drawing by Geneviève Asse, known for her very singular use of shades of blue in paintings that constitute relentless forays into space, light and transparency. Although the source of the «Asse blue» remains unknown, the shades produced by the covers of the books printed between 1960 and 1965 evoke its palette in a completely fortuitous way.

Published for the first time in 1939, Sartre’s text advocates a phenomenological approach to emotions. It is the opening part of a phenomenological treatise on psychology, Le Psyche, which was never published. In this book, Sartre opposes the idea that emotions are mere manifestations triggered by our body, which completely escape our will. Emotion is a transformation of the world. Faced with the impossibility of transforming the world, the consciousness alters the image of it in order to transform an unbearable reality. “During emotion, it is the body which, directed by consciousness, changes its relationship with the world so that the world should change its qualities,” he writes.

In the guise of a colourful colour chart marked by the passage of time, this wall-mounted leporello connects many stories and figures from the world of art and publishing. The cover of the book published by Hermann was designed by the Swiss typographer Adrian Frutiger (1928–2015), the artistic director of the Hermann publishing house between 1957 and 1967, based on a still life drawing that Geneviève Asse (1923–2021) gave to Pierre Berès (1913–2008) to illustrate this text by Sartre. Considered one of the most important booksellers from the second half of the twentieth century, this collector and dealer of ancient books and literary manuscripts bought the Hermann scientific publishing house in 1956 and opened it up to the field of the arts by associating with the art historian André Chastel. He called on Frutiger to modernise the image of the house. Pursuing this objective, the collection “L’Esprit et la main,” in which this title by Sartre was reissued, republished philosophical or scientific texts and associated them with illustrations by contemporary artists.

Translated from French by Juliet Powys

Yann Sérandour  
Et puis le bleu tourna au vert  
Leporello, 66 pages, 25 × 19 cm (closed), 1254 cm open  
Laurel Parker Edition, Romainville, France, 2022  
Layout Marine Bigourie  
Printing Média Graphic, Rennes  
Edition limited to 15 copies + 4 HC signed and numbered

views of the space © Gregory Copitet

![](/media/serandour_bleu_03_w.jpg)

![](/media/serandour_bleu_02_w.jpg)
