---
categories:
- Boîte de présentation
cover: /media/delepine-boite-02.jpg
date: 2014-10-02
description: ""
draft: true
title: Boîte pour tirage grand format
---
Nathalie Delépine, photographe specialisée dans le portrait des chevaux, est venue vers nous pour fabriquer une boîte speciale. Un seul tirage de grand format avec un certificat d’authenticité, le tout dans un beau coffret en toile coton et lin. La touche spéciale : un marquage à chaud sur une étiquette incrustée.

![](/media/delepine-boite-01.jpg)

![](/media/delepine-boite-02.jpg)