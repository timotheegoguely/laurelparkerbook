---
categories:
- Album photo
- Reliure
- Restauration
cover: /media/royere-album_lacoste_01.jpg
date: 2017-07-14
description: ""
draft: true
title: Album photos des meubles de Jean Royère, pour la galerie Jacques Lacoste
---
En collaboration avec la restauratrice de livres et œuvres graphiques Coralie Barbe, nous avons fabriqué le fac-similé d’un album des années 1930, pour contenir des pages originales restaurées par son atelier parisien.

![](/media/royere-album_lacoste_01.jpg)

Le client, [la galerie Jacques Lacoste](http://www.jacqueslacoste.com/fr/galerie/#), spécialiste des arts décoratifs des années 20 à 60, possède des archives du designer Jean Royère. Les albums contenants les photographies de ses œuvres de mobilier – prises à l’époque – sont à la fois des outils exceptionnels de recherche et d’authentification ainsi que des archives de prestige pour la galerie.

L’atelier Coralie Barbe s’est occupé de la restauration des pages d’album. En parallèle, nous avons commencé notre travail de recherche pour choisir les matières et analyser la structure de l’album. C’est cette étude de l’album original en morceaux qui nous a permis de découvrir comment il a été construit, et surtout pourquoi il s’est cassé, pour ne pas faire la même erreur de fabrication.

![](/media/royere-album_lacoste_03.jpg)

Nous avons fait plusieurs recherches de matières pour remplacer le recouvrement original en raphia tissé main. Le raphia est une belle matière mais trop fragile pour les articulations d’un album lourd. Nous avons proposé des toiles de fibres plus solides comme le lin et le jute, mais il a été décidé de préserver le style original de l’album. Pour cela, nous avons teinté à la main une nouvelle toile de raphia tissé suite à des tests de teinture pour trouver une couleur proche à la patine du vieux raphia. Notre solution pour améliorer la structure a été de doubler toutes les articulations en toile enduite.

Pour créer un dos arrondi comme sur l’album original, nous avons fait appel à [Maonia](http://www.grandsateliersdefrance.com/fr/maonia-55.html), un atelier d’ébénisterie qui est membre des Grands Ateliers de France.

![](/media/royere-album-lacoste_02.jpg)

La dernière petite touche a été de récréer sur la couverture une peinture en pochoir comme sur l’album original – un cercle vert – qui identifie cet album dans une collection de sept pièces.

Un grand merci à Célia Matten, relieur diplômé de l’École Estienne, qui a travaillé au sein de notre atelier sur la confection des charnières de pages et de l’ensemble de l’album.