---
categories:
- Livre d’artiste
- Projet d’artiste
- Reliure
cover: /media/roy-passion_01.jpg
date: 2016-03-25
description: ""
draft: true
title: Passion de Sébastien Le Roy
---
Après [Abécédaire](http://laurelparkerbook-blog.com/?p=180), un livre pour lequel nous avions créé un coffret de présentation, les éditions Les Paroles Gelées nous ont proposés de travailler sur la reliure d’un nouveau projet.

![](/media/roy-passion_01.jpg)

D’après 44 gravures de Sébastien Le Roy, le livre _Passion_ se décline en plusieurs éditions : deux livres différents, des planches imprimées et une série d’estampes en coffret.

Nous avons proposé un papier à fort grammage pour l’utiliser comme couverture pour les livres et fabriquer une pochette rigide pour l’édition non reliée. Le système de reliure, qui date du Moyen-Âge, est sans colle et permet une très bonne ouverture du livre.

![](/media/roy-passion_02.jpg)

![](/media/roy-passion_03.jpg)