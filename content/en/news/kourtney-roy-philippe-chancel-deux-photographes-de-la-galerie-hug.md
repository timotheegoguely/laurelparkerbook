---
categories:
- Boîte d’éditeur
- Boîte de présentation
cover: /media/kourtney_roy_hug_04.jpg
date: 2018-04-24
description: ""
draft: true
title: Kourtney Roy, Philippe Chancel, deux photographes de la galerie Hug
---
Pour présenter le travail de photographes, nous réalisons de plus en plus de coffrets sur-mesure. Cet objet permet de manipuler les tirages un à un mais aussi de les conserver sans les abîmer. Le collectionneur peut ainsi choisir d’encadrer un tirage pour l’afficher et de garder précieusement la série ou bien d’exposer le coffret dans sa bibliothèque.

![](/media/kourtney_roy_hug_02.jpg)

C’est dans cette optique qu’André Hug de la galerie [**Catherine et André Hug**](http://www.galeriehug.com/?lang=en) est venu nous voir. Il souhaitait réaliser deux coffrets pour les photographes Kourtney Roy et Philippe Chancel.

Le travail de mise en scène de [**Kourtney Roy**](http://www.kourtneyroy.com/?do=home) est marquant par ses couleurs qui arrivent comme des blocs. En puisant dans ses photographies, nous avons tiré des teintes comme le bleu et le rouge orangé. Ces couleurs sont directement empruntées des photographies de l’artiste, issu du ciel, d’une peinture d’un bâtiment, ou encore d’un vêtement. Elles sont inspirés du paysage d’Amérique du Nord de son enfance : des motels, des « diners », des mini-golfs.

![](/media/kourtney_roy_hug_01.jpg)

Nous avons utilisé un papier bleu turquoise gaufré pour la boîte et un papier bleu rivière pour la cale, ces deux couleurs créent tout de suite un espace physique qui se retrouve dans les photographies. Le marquage à chaud en rouge orangé vient souligner le côté « peps » propre à Kourtney Roy.

Pour [**Philippe Chancel**](http://www.philippechancel.com/), nous avons pensé à un objet très sobre, qui s’efface presque pour laisser la place aux photographies. Son travail est très marqué par sa vision de journaliste. Il photographie comme on fait un reportage, en démontrant une situation, en analysant une vision du réel ou une action humaine qui peut être néfaste. Son projet ambitieux, _Datazone_ est le travail de nombreuses années de recherches qui vise à mettre en lumière des territoires peu couverts par les médias.

![](/media/chancel-boite-01.jpg)

Kourtney Roy et Philippe Chancel sont régulièrement exposés à la galerie Catherine et André Hug. Vous pouvez retrouver les premières photographies de Philippe Chancel à the Barbican jusqu’au 27 mai 2018 à Londres.

_Hope_, Kourtney Roy  
Coffret en papier gaufré, cale en papier gaufré, marquage à chaud rouge.

_Datazone_, Philippe Chancel  
Coffret en papier taupe pH neutre, marquage à chaud.

Design et fabrication : Laurel Parker Book, 2017