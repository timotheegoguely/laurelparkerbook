---
categories:
- Conférence
cover: /media/workshop_visiting-school_1.jpg
date: 2017-06-30
description: ""
draft: true
title: 'Conférence : AA School of Architecture, Visiting School, Paris'
---
En juillet 2017, nous avons accueilli une quinzaine d’étudiants du [Paris Visiting School de l’AA School of Architecture, Londres](https://paris.aaschool.ac.uk). Le thème de leur atelier d’étude était « Architecture + Écriture ». À l’invitation de [Caroline Rabourdin](https://aaessays.com), professeur d’histoire et théorie à l’AA School of Architecture et directrice de leur Paris Visiting School, Laurel Parker a présenté une cinquantaine de livres présent dans la bibliothèque de notre atelier pour présenter aux étudiants des formes atypiques pour utiliser du texte, et notamment l’essai, dans le livre.

![](/media/workshop_visiting-school_2.jpg)

Les livres présentés viennent de maisons d’éditions très diverses comme Roma Publications, RVB Books, Fw:Books, éditions P, Hyphen Press, Christophe Daviet-Thery et Le Point du Jour, ainsi que des livres auto-édités par des artistes comme Sara MacKillop et documentation céline duval.

![](/media/workshop_visiting-school_1.jpg)

Photos © Caroline Rabourdin.