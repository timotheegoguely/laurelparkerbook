---
categories:
- Presse
cover: /media/viva-villa-01.png
date: 2021-04-05
description: ""
draft: true
title: '¡Viva Villa! - Feedback on the 2020 edition '
translationKey: viva-villa
---
¡Viva Villa! - Feedback on the 2020 edition

Laurel Parker and Paul Chamard are Laureates of the Villa Kujoyama (Kyoto 2019) and they participate at the festival of artist residencies Viva Villa, in Avignon (october 24, 2020 -  march 14, 2021). In the exhibition Les vies Minuscules, they show Inside Outside #2 installation, project about their researches and their work as residents.

The Viva Villa Festival looks back on these artists in residencies (Casa de Velázquez, Villa Kujoyama and Villa Medicis) in a video with interviews and projects presentations in Les vies Minuscules exhibition.

_(Laurel Parker and Paul Chamard, 3:00 min)_

[viva-villa-festival.mp4](/media/viva-villa-festival.mp4 "viva-villa-festival.mp4")