---
categories:
- Projet d’artiste
cover: /media/reboud-climatesii-04.jpg
date: 2018-04-19
description: ""
draft: true
title: Lola Reboud, poursuite d’une édition
---
[**Lola Reboud**](https://lolareboud.com/works/climates-ii/) est venue nous voir dans l’idée de réaliser une édition limitée issue de sa parution _Les Climats II_ aux éditions Poursuite. Son travail sur ce livre explore la relation entre l’être humain et la géothermie, le lien et l’implication que cela peut avoir sur un mode de vie, un territoire comme celui de l’archipel, et l’énergie qui en découle.

![](/media/reboud-climatesii-04.jpg)

Le coffret contient le livre ainsi qu’un tirage limité agrémenté de feuille d’or. Nous avons trouvé une solution pour contenir ces objets un peu à la manière de [**_cristal automatique n°1_**](http://laurelparkerbook-blog.com/cristal-automatique-n1-de-babx/), de Babx. Sur l’extérieur du coffret, nous retrouvons un cercle en pointillés fait en découpe numérique, forme que Lola Reboud a beaucoup utilisée dans son ouvrage. Une fois déplié, nous avons le tirage et le livre. Nous avons préféré ajouter un rabat en papier sur le tirage pour protéger la feuille d’or.

Nous apprécions ces objets en papier épais, leur apparente simplicité cache de nombreuses contraintes techniques qui sont autant de défis à relever pour nous.

![](/media/reboud-climatesii-01.jpg)

![](/media/reboud-climatesii-03.jpg)

_Les Climats II_, Lola Reboud.  
Papier épais bleu nuit, découpe numérique, 2018.  
Design et fabrication : Laurel Parker Book.