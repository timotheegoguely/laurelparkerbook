---
categories:
- Papeterie
cover: /media/f93_carte_2019-02.jpg
date: 2017-12-31
description: ""
draft: true
title: F93 – Bonne année 2018
---
Après deux cartes de vœux en 2015 et 2017, nous avons réalisé une carte pour la nouvelle année 2018 commandée par la [**fondation F93**](http://www.f93.fr/) à Montreuil.

![](/media/f93_carte_2019-02.jpg)

Nous avons prolongé la période des calendriers de l’avant avec la carte de vœux, reprenant le principe des cases à ouvrir. Sous chaque case, nous découvrons des chiffres clés pour la fondation. Au verso de la case, nous avons joué avec le papier de correspondance de la fondation. Les créations originales par des artistes se transforment avec le découpage des cases pour créer un nouveau motif dynamique.

Les chiffres sont imprimés en sérigraphie jaune, une couleur qui fait référence au site internet de la fondation. Les cases du « calendrier » sont agencés à la manière du logo de la fondation.

Une fois affiché au mur, vous pouvez remarquer les jeux d’ombre créés par l’ouverture des cases, un autre petit détail que nous apprécions. Et comme les détails se mêlent à l’anecdote, en voici une, cela faisait longtemps que Laurel Parker voulait utiliser le principe du calendrier de l’avant, cette idée semblait idéal pour travailler sur cet objet.

Carte de vœux 2018 pour la fondation F93.  
Papier recyclé, impression numérique, sérigraphie noir et jaune, découpe à la forme, 2017.