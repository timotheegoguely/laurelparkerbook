---
categories:
- Boîte de conservation
cover: /media/cartier_boite-plume_01.jpg
date: 2019-02-14
description: ""
draft: true
title: Boîte à plumes pour Cartier
---
La boite de conservation « Plumes » est une commande spéciale des archives de Cartier. Elle a été conçue pour rassembler plusieurs éléments historiques : des plumes de martins-pêcheurs sous leur forme naturelle et transformée, conservées dans leurs boîtes d’origine.

![](/media/cartier_boite-plume_01.jpg)

Ces éléments sont une référence pour l’histoire de la maison Cartier. Pour leur archivage, nous avons réalisé un coffret de conservation avec des compartiments sur mesure. Des plateaux fins permettent de manipuler les éléments pour les sortir du coffret et les présenter sans les abîmer.

![](/media/cartier_boite-plume_02.jpg)