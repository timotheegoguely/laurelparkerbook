---
categories:
- Non classé
cover: /media/books-london2.jpg
date: 2021-11-02
description: article Books. Art, Craft & Community
title: Books. Art, Craft & Community
translationKey: London-book-arts
---
  
‘Books. Art, Craft & Community'. Dive into the world of papermakers, printers, bookbinders, artists, designers and publishers from round the world and discover all about their passion for the art of bookmaking. We are thrilled to be a part of this beautiful book published by London Book Arts and Ludion Publishers. Thanks to Ira & Simon at the London Centre for Book Arts for selecting us !!

![](/media/books-london.jpg)

![](/media/books-london2.jpg)

![](/media/book-art-and-craft.jpg)