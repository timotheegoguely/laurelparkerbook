---
categories:
- Projet d’artiste
- Reliure
cover: /media/matiere_isos5web.jpg
date: 2015-02-15
description: ""
draft: true
title: Au revoir à Sarah Mohisen, apprentie 2014
---
It is with great sadness that we say so long, but not goodbye, to our apprentice Sarah Mohisen.  
Sarah joined our studio in January 2014 for a year-long apprenticeship, thanks to a grant from the City of Paris : le Prix de perfectionnement aux métiers d’art. We did not know Sarah when we accepted this challenge – a year long apprenticeship ! Our fears were quickly dispelled and Sarah soon became one of the family.

![](/media/mohisen_report-stage_02.jpg)

Sarah was _chef du projet_ of the following projets : _1001 nuits + un jour_ de Yona Friedman (mfc Michèle Didier), the _Book Shoe_ (Laurel Parker Book), our line of leather boxes for our new [boutique](http://laurelparkerbook.bigcartel.com), and this very same blog. She also worked extensively on the projects _Inventario_ by Oscar Muñoz, _ISO’s_ for the Musée des Arts Décoratifs, and our end of the year _Carnet 2013._

Her end of the year report, pictured below, is beautiful example of her artistic development. Her design is both refined and functional : a Japanese binding for simplicity of fabrication and printing, a double-page layout with the fold at the foredge (à la japonaise), the mix of the two browns, dark and light, which bring out the simplicity of the white page and color photos.

Sarah came to us with diplomas from two bookbinding schools, and a diploma in art history and archeology. She leaves us with an intensive year-long experience in a small but functioning bookbinding studio. She participated in meetings with clients, worked with us designing projets, and learned how to do all things necessary to run a her own studio : accounting, billing, and inventory.

We hope very much to bring Sarah back to work with us on future projects. We have lost a wonderful apprentice but gained a very dear friend.

![](/media/matiere_isos5web.jpg)

![](/media/peri-fabrique-isos_03.jpg)

Sarah Mohisen working on _ISO’s_ for an exhibition at the Musée des Arts Décoratifs in May.

![](/media/livre_d-artiste_yona_friedman_4w-copie.jpg)

Sarah Mohisen working on the artist book _1001 nuits + un jour_ by Yona Friedman, in July.

Sarah Mohisen working on our new collection of papeterie, Gone Fishing, in December.