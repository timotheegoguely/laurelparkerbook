---
categories:
- Reliure
cover: /media/chaumet_reliure_bleu_01.jpg
date: 2017-03-14
description: ""
draft: true
title: Reliures de la nouvelle collection haute Joaillerie Chaumet
---
Après la [série de coffrets](http://laurelparkerbook-blog.com/?p=535) réalisée l’année dernière, notre nouvelle collaboration avec la maison Chaumet a pris la forme d’une reliure fabriquée en 51 exemplaires. Cet objet est un outil de marketing haut de gamme pour présenter une collection de haute joaillerie sous forme de dessins.

![](/media/chaumet_reliure_bleu_01.jpg)

La mise en valeur de la nouvelle collection s’est faite par une sélection pointu des matériaux. Nous avons travaillé en collaboration avec la directrice artistique du projet, [Hélène Nounou](http://www.helenenounou.com), pour nous aider avec ces choix. Les tirages sont montés un à un sur onglet en papier japonais mitsumata. Ce beau papier est fait main par Shinichiro Abe, le petit fils de Eishiro Abe, le premier artisan du papier à être nommé Trésor Vivant au Japon.

La reliure sur onglets permet une excellente ouverture du livre malgré le fort grammage des pages imprimées.

![](/media/chaumet_reliure_bleu_03.jpg)

Nous avons également pris en charge l’emballage individuel de chaque coffret pour une belle présentation et un transport international.

Un grand merci à Celia Matten et Léa Hussenot Desenonges, relieurs diplômées de l’école Estienne, qui ont fait partie de notre équipe pour la fabrication de cette collection d’albums.

![](/media/chaumet_reliure-01.jpg)

![](/media/chaumet_reliure-02.jpg)