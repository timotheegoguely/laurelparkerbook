---
categories:
- Boîte de conservation
- Coffret
cover: /media/cartier-boite_conservation_01.jpg
date: 2017-03-26
description: ""
draft: true
title: Boites de conservation pour Cartier et l’ENSBA de Paris
---
Nous fabriquons régulièrement des boites de conservation pour protéger des ouvrages rares.  
Voici nos dernières réalisations, elles suivent à la fois les standards de qualité archivage et l’esprit des collections pour lesquelles elles sont créées.

![](/media/ensba-boite-conservation-01-1.jpg)

**_Boites de conservation pour la maison Cartier_**

Les [archives de la maison Cartier](http://www.cartier.fr/fr/maison/patrimoine/archives-et-savoir-faire/les-archives.html) rassemblent registres, photographies, moulages et dessins. Nous avons fabriqué une dizaine de boîtes de conservation pour des albums qui ont été récemment restaurés. Les beaux albums de photographies de Jacques Cartier ont demandé un travail spécifique : des boîtes classeur avec des pochettes plastique sur mesure et des cales de compensation. Les boîtes contenants plusieurs albums très fins sont conçues avec des plateaux pour séparer et ôter les ouvrages. Toute la collection a été habillée en papier fait main au Moulin du Verger par Monsieur Jacques Brejoux.

![](/media/cartier-boite_conservation_01.jpg)

Un grand merci à Laure Fuhrmann, relieur spécialisée dans la préservation et l’entretien matériel des collections, qui a commencé ce projet avec la maison Cartier.

![](/media/cartier-boite_conservation_02.jpg)

**_Boites de conservation pour l’Ecole Nationale Supérieure des Beaux Arts de Paris_**

Le service des [collections](https://www.beauxartsparis.fr/fr/la-collection/livre) de l’École des beaux arts de Paris compte actuellement 65 000 ouvrages du XVE au XXe siècles, dont 3500 ouvrages des XVe et XVIe siècles provenant en majeure partie de la donation Masson.

Nous avons fabriqué 25 boîtes de conservation pour des livres de la collection Masson, dont plusieurs incunables : des livres imprimés avant 1501 au début de l’impression typographique à caractères mobiles. La plupart des volumes sont lourds, avec des plats en bois et des pièces métalliques qui nécessitaient une construction solide, résistante et protectrice.

![](/media/ensba-boite-conservation-04.jpg)

Nous avons travaillé avec le conservateur en charge des manuscrits et imprimés anciens pour choisir des matières qui s’accordaient bien avec leur collection.

Nous sommes très heureux d’avoir participé à la conservation de ces œuvres qui font partie du patrimoine national.

![](/media/ensba-boite-conservation-02.jpg)