---
categories:
- Expositions
cover: /media/galerie-playground-studies2-photo-gregory-copitet.jpg
date: 2021-08-07
description: Playground Studies, an exhibition curated by Vincent Romagny
title: Playground Studies, an exhibition curated by Vincent Romagny (10 September
  - 24 October)
translationKey: Playground expo
---
**Laurel Parker Book** gallery is thrilled to welcome you September 12 from 2pm to 6pm to the opening of **Playground Studies, an exhibition curated by Vincent Romagny**. The exhibition will take place from **September 10 to October 24, 2021**.

The exhibition includes works from **Arakawa, Pol Gallo, Ane Hjort Guttu, Palle Nielsen, Seith Price and Corin Sworn.**

![](/media/playground-studies-double.jpg)

![](/media/playground-1.jpg)

L’enfance fait un retour fort dans l’art contemporain. On lui dédie saison (1), œuvres et exposition (2). On invoque son étymologie sans son image (3). Dans ces occurrences, nulle résurgence du mythe fondateur de l’art moderne, l’Enfance de l’art, selon lequel l’enfant est immédiatement facteur d’art, puisqu’il « voit tout en nouveauté » (Charles Baudelaire, « Le Peintre de la vie moderne »). Ni même de sa contestation radicale à la fin du XXème siècle, avec son renversement et l’adolescence, résurgence de l’enfance comprise comme régression. D’autres formes dont on peine encore à saisir le sens global, mais que l’on peut comprendre par les difficultés du temps présent et l’incerti- tude du futur que nous lui préparons. Il aura fallu quelques décennies pour que l’image de l’enfance réapparaisse frontalement dans l’art contemporain. Loin de l’innocence et des promesses de maintien et renouvellement du monde, l’enfant inspira longtemps nostalgie et naïveté, auxquelles l’exigence « critique » de l’art contemporain s’accommodait bien peu. Il n’en avait pas moins été aveugle à des œuvres le mettant en scène qui étaient en fait autant de « tableaux de chasse (4) ». Ces questions dérangeantes ont été vite tues, pour être vite recouvertes d’autres représentations autrement plus satisfaisantes. N’en sont-elles pas pour autant trompeuses ? Heureusement, l’on se pose aussi la question de savoir comment et à quelles conditions il est possible d’accorder le statut de co-créateur à l’enfant, loin de toute instrumentalisation (5).

Peut-être fallait-il aussi que s’épuise l’idée de jeu dans l’art pour que, par le biais de la question de l’aire de jeux, on ne revienne timidement à l’enfance. On voudrait, chez Laurel Parker Book, par le biais de quelques œuvres vidéo présentant des aires de jeux réelles (Arakawa, Palle Nielsen, Seth Price) ou bien métaphoriques (Pol Gallo, Ane Hjort Guttu, Corin Sworn), revenir sur quelques mythes de l’enfance, que les aires de jeux manifestent pour autant qu’elles lui donnent des sens toujours nouveaux, sur lesquels elles nous renseignent : l’enfant comme origine perdue, l’enfant comme vulnérabilité, l’enfant comme modèle politique, l’enfance comme utopie. Peut-être ce sera là une des vertus de playground studies à venir, sortir d’« un long processus d’enfermement des enfants (6) », et ce d’abord dans nos représentations. Sans doute n’évoquera-t-on que quelques uns des mythes de l’enfance dans l’art. Au moins aura-t-on essayé de les montrer comme tels. Nul doute qu’il faille encore les mettre à nu pour espérer imaginer un rapport non biaisé à l’enfance, à égale distance d’un passé et d’un futur fantasmés qui instrumentalisent l’enfance au prétexte de l’art et empêchent de lui reconnaître un statut politique.

— Vincent Romagny

[visualiser le livret de l'expo](/media/playground-studies-expo-vincentromagny-2021.pdf)

Le livre _Politiser l’enfance_ accompagne l’exposition, une publication de Laurel Parker Book, éditée par Vincent Romagny et John D. Alamer, avec des textes de Tal Piterbraut-Merx, Pierre Zaoui et Vincent Romagny.

Remerciements : Air de Paris, galerie Chantal Crousel, Kendall Koppe gallery, Jeune Création, Frac Grand Large – Hauts de France, Arakawa + Gins Tokyo Office, MACBA (Barcelone), Parsons Paris.

1 « Enfance, Encore un jour banane pour le poisson-rêve », Palais de Tokyo, 2018  
2 Children’s Power au Plateau - Frac Ile de France, 2021.  
3 Isabelle Cornaro, Infans, Fondation Pernod Ricard, 2021, Fabien Giraud et Raphaël Siboni, Infantia (1894-7231), IAC, 2020.  
4 Matthieu Potte-Bonneville, « Points aveugles – à propos des soutiens apportés à Claude Lévêque » \[[http://mathieupottebonneville.fr/2021/02/25/points-aveugles/](http://mathieupottebonneville.fr/2021/02/25/points-aveugles/ "http://mathieupottebonneville.fr/2021/02/25/points-aveugles/")\]  
5 Marie PRESTON et Céline POULIN (dir.), Inventer l’école, penser la co-création, Nevers / Brétigny-sur-Orge, Tombolo Presses / CAC Brétigny, 2021.  
6 Philippe ARIES, L’enfant et la vie familiale sous l’Ancien Régime, Paris, Éditions du Seuil, 1975, p. 7.

crédit photos © Gregory Copitet

![](/media/playground-studies-double2.jpg)