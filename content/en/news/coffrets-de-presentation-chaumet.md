---
categories:
- Coffret de luxe
cover: /media/chaumet_coffret_bleu_02.jpg
date: 2016-06-08
description: ""
draft: true
title: Coffrets de présentation Chaumet
---
Nous réalisons régulièrement des coffrets de présentation pour les maisons de haute joaillerie. Ce type de coffret est un outil de marketing haut de gamme pour présenter une collection.  
Pour ce projet, la maison Chaumet nous a commandé une série de 44 coffrets classiques avec de hautes finitions. La structure du coffret est montée à la main et consolidée pour une utilisation régulière de l’objet. Les matières utilisées suivent la direction artistique de la collection et l’identité de la maison.

![](/media/chaumet_reliure_bleu_02.jpg)

Nous avons pris en charge l’emballage de chaque coffret, réalisé à partir d’un papier spécialement commandé pour ce projet.

![](/media/chaumet_coffret_bleu_02.jpg)

![](/media/chaumet_coffret_bleu_03.jpg)