---
categories:
- Restauration
- Coffret
- Boîte de conservation
cover: /media/desnos-les-sommeils_02.jpg
date: 2018-05-29
description: ""
draft: true
title: Les Sommeils de Robert Desnos
---
La Bibliothèque Littéraire Jacques Doucet est venue vers nous pour la réalisation d’un coffret de conservation sur mesure. Ce projet concerne un ensemble de notes et de dessins des _Sommeils_ de Robert Desnos.

![](/media/desnos-les-sommeils_03.jpg)

Un travail de restauration a d’abord été effectué par l’atelier Coralie Barbe sur les nombreux feuillets qui composent cette œuvre du poète surréaliste.

Ces écrits sont issus des séances des sommeils réalisées par les surréalistes à partir de 1922. Endormi, Desnos est soumis à des questions par une assistance. Il en naît des écrits et esquisses au caractère onirique et hypnotique. Ces amorces sont ici regroupées sous forme de différentes chemises.

Pour la structure et le choix des matériaux de l’objet, nous avons proposé de rester dans l’esprit du coffret _L’anglais Récréatif_ de Mallarmé que nous avions réalisé il y a quelques années pour la bibliothèque Doucet.

![](/media/desnos-les-sommeils_04.jpg)

Nous avons confectionné cinq boîtes de conservation pour accueillir les pochettes des _Sommeils_.  
Notre travail s’est poursuivi par la fabrication d’un coffret à étagères qui permet de garder grouper l’ensemble des œuvres.  
Pour pouvoir sortir facilement les boîtes, nous avons prévu un abattant sur le devant, qui est maintenu par deux fermoirs en os. Le titre du coffret est marqué à chaud sur une étiquette incrustée en cuvette.

Toute cette réalisation respecte les normes muséographiques avec l’emploi de matériaux pH neutre et de colle réversible.

![](/media/desnos-les-sommeils_01.jpg)