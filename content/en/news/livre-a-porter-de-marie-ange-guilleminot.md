---
categories:
- Livre d’artiste
- Projet d’artiste
- Reliure
cover: /media/guilleminot-toucher-voir_01.jpg
date: 2015-05-17
description: ""
draft: true
title: « Livre-à-porter » de Marie-Ange Guilleminot
---
Ce livre d’artiste fait partie du projet _Toucher-voir,_ qui a été confié à Marie-Ange Guilleminot par Olivier Saillard, directeur du _Palais Galliera_ – musée de la mode de la Ville de Paris. Le projet a été réalisé grâce au soutien de The Conny-Maeva Charitable Foundation et l’association « Le Livre de l’aveugle ».

![](/media/guilleminot-toucher-voir_01.jpg)

_Toucher-Voir_ est une installation, destinée à tous mais principalement aux aveugles et aux mal voyants, qui donne accès à une quarantaine de vêtements, accessoires et objets dans les collections du musée. Le _Livre-à-porter_ raconte l’histoire de la conception, de la fabrication et des matières de certaines de ces pièces. Les pages sont imprimées en gaufrage, avec des textes en braille et des images en relief. La maquette du livre a été conçue par Élise Gay et Kévin Donnot qui ont été très investis dans leur collaboration avec l’artiste et l’imprimeur.

Ce livre a été réalisé en deux exemplaires identiques mis à part leurs reliures : un livre est relié « en accordéon » et l’autre est relié « à la japonaise ».

![](/media/guilleminot-toucher-voir_02.jpg)

![](/media/guilleminot-toucher-voir_03.jpg)

_Livre-à-porter_ de Marie-Ange Guilleminot [(video de l’artiste qui explique le projet)  
](https://www.youtube.com/watch?v=TFSbF8WU5ac)Design Graphique et pilotage du projet : [Élise Gay et Kévin Donnot](http://www.e-k.fr/#index).  
Impressions : Le Livre de l’Aveugle  
Reliure : Laurel Parker Book