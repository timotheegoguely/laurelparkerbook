---
categories:
- Livre d’artiste
- Reliure
cover: /media/leblanc-dargile-04.jpg
date: 2016-11-08
description: ""
draft: true
title: D’argile, de Laurence Leblanc
---
_D’argile_, de Laurence Leblanc  
Lauréate du Prix Niépce, 2016  
Paris Photo  
**Présenté en avant-première à Paris Photo** : Jeudi 10 novembre 2016 à 12h sur le Balcon d’Honneur.

![](/media/leblanc-dargile-03.jpg)

Ce livre d’artiste de Laurence Leblanc a été réalisé avec Picto Foundation, Gens d’images et l’atelier Laurel Parker Book. Édition limitée de 15 exemplaires. Pour plus d’information, visiter le site de [Picto Foundation](http://www.picto.fr/2016/laurence-leblanc-prix-niepce-2016/?utm_source=emailing&utm_medium=cible-global&utm_campaign=foundation-02-11-16) ou le site de [Laurence Leblanc](http://laurenceleblancphotographe.fr/wordpress/).

Pour ce projet, nous avons adapté un prototype déjà conçu par l’artiste. Pour essayer de garder l’authenticité de l’objet originel, nous avons utilisé deux types de papiers. Cela donne un effet grisé sur les tranches, en rappel du scotch et de la colle utilisée par l’artiste.

Voici les photos du prototype de l’artiste :

![](/media/leblanc-dargile-02.jpg)![](/media/leblanc-dargile-01.jpg)