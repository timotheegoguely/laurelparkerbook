---
categories:
- Conférence
cover: ""
date: 2017-03-25
description: ""
draft: true
title: 'Table ronde : La vie secrète des livres de photographe'
---
Maison Doisneau de la photographie, Gentilly  
Dimanche 26 mars 2017 à 16h  
Réalisation – traitement d’image, papier, impression, reliure  
Laurel Parker / Laurel Parker Book, Alain Escourbiac / Escourbiac l’imprimeur, Eduardo Serafim + Benoît Vollmer / Après Midi Lab

« A chacun(e) son livre de photographe, tour à tour recueil, récit, document, espace d’inventivité et de consécration, objet d’art et d’artisanat, de recherches et de blogs, de partage et de collection, de commerce, voire de spéculation. Ce cycle de rencontres consacré à « La vie secrète des livres de photographe » propose à un large public non spécialiste de découvrir les différentes étapes de la création de ces livres à travers les regards des acteurs de toute la chaîne de l’édition. »

IMG

IMG

IMG

IMG