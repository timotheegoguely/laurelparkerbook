---
categories:
- Press book
cover: /media/janssens_pressbook_01.jpg
date: 2018-03-26
description: ""
draft: true
title: 'Pressbooks : Philippine Janssens au Bon Marché'
---
Nous sommes fiers de participer au développement de la boutique [**Philippine Janssens**](https://www.philippinejanssens.com/) depuis deux ans. Philippine Janssens est une boutique de pantalon féminin sur-mesure basée à Paris. Elle s’entoure exclusivement de fabricants français. C’est pour réaliser ses books de présentation de tissus qu’elle a fait appel à notre savoir-faire.

![](/media/janssens_pressbook_06.jpg)

Deux ensemble de pressbooks ont été réalisés, un pour la boutique rue du Faubourg Saint-Honoré, l’autre pour l’ouverture d’un espace au Bon Marché.

![](/media/janssens_pressbook_02.jpg)

Les matières utilisées pour les books sont liées à l’identité visuelle de la boutique : un cuir rose et une toile enduite couleur laiton, des rubans en tissu choisis parmis plusieurs propositions. L’ensemble doit être résistant pour une manipulation quotidienne par les vendeurs et leurs clients.

Voici un exemple de propositions pour le choix de la tirette attachée au dos du book : rubans et cordelettes de différentes matières, des intercalaires en toile doublée organisent les matières dans les books.

![](/media/janssens_pressbook_01.jpg)

![](/media/janssens_pressbook_04.jpg)