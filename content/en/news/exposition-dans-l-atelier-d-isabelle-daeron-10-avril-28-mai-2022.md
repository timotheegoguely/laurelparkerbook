---
categories:
- Livre d’artiste
- Expositions
cover: /media/atelier_daeron_expo_02.jpg
date: 2022-04-09
description: Exhibition In the Studio of Isabelle Daëron
title: 'Exhibition and book : In the Studio of Isabelle Daëron (10 April - 28 May
  2022)'
translationKey: expo-daeron
---
Laurel Parker Book is thrilled to present the work of designer Isabelle Daëron.

Displayed in the form of a galaxy, Isabelle Daëron gives us a part of her universe. Original drawings, prototypes of forms and materials, research plans and objects testify to her latest projects. The exhibition is produced in parallel with the Biennale of Design of St Etienne.

![](/media/atelier_daeron_expo_01.jpg)

A new limited edition book has been created for the occasion, showcasing the artist's drawings and the projects produced in situ.

L'existence est un ailleurs, artist book by Isabelle Daëron. 200 ex, Laurel Parker Edition, 2022.

![](/media/existence_daeron_ed_lpb_05_w.jpg)

[consulter le livret de l'exposition](/media/expo-daeron-livret-web.pdf)

[consulter le dossier de presse](/media/press_release_lpb_isabelle_daeron.pdf) 

Link to the website of the studio of Isabelle Daëron : [Studio Idaë](https://www.studioidae.com/en/)

![](/media/existence_daeron_ed_lpb_02_w.jpg)