---
categories:
- Press book
cover: /media/amankai_book_03.jpg
date: 2016-06-05
description: ""
draft: true
title: Nouveautés pressbook
---
Nous sommes ravis des ces deux press books, aussi originaux que le travail présenté dedans.

Le beau press book de la photographe [**Laetitia Hotte**](http://laetitiahotte.com/) suit une forme classique avec un mécanisme à vis cachées. Son originalité vient du choix des matières et du papier d’impression. Les tirages ont été imprimés sur un papier « dos bleu », un papier fin pas cher au verso de couleur bleue et conçu pour l’impression d’affiches publicitaires. Ils ont été pliés en gouttière comme dans une reliure à la japonaise pour avoir des pages recto/verso. La teinte bleue bicolore de la toile de couverture est un rappel du « dos bleu » des tirages qui est visible légèrement à la tête et à la queue du book. La toile de lin d’intérieur fait un beau contraste avec la toile bleue. Le tout donne très envie de regarder les magnifiques images de Laetitia Hotte.

![](/media/hotte-pressbook-01.jpg)

![](/media/hotte-pressbook-02.jpg)

Pour présenter son travail de graphisme, [**Amankai Araya**](http://www.amankai.net/) nous a commandé un press book utilisant un tirage de son logo en sérigraphie pour la couverture. Le papier gris vient du Japon, le mécanisme et le faux dos est en toile buckram blanche. Le petit format du book rend l’objet intime et spécial.

![](/media/amankai_book_03.jpg)

![](/media/amankai_book_02.jpg)

![](/media/amankai_book_01.jpg)