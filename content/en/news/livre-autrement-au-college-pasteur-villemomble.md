---
categories:
- Enseignement
- Livre d’artiste
- Workshop
cover: /media/atelier_villemomble_05.jpg
date: 2016-06-12
description: ""
draft: true
title: Livre Autrement au Collège Pasteur, Villemomble
---
Entre novembre 2015 et avril 2016 nous avons proposé à la classe ULIS du Collège Pasteur de Villemomble de découvrir notre métier. Le projet _Livre autrement_ était une démarche imaginée et coordonnée par la [F93](http://www.f93.fr), centre de culture scientifique, technique et industrielle de Seine-Saint-Denis.

Pour entrer dans l’univers des livres d’artiste, nous avons visité l’exposition _Anselm Kiefer, l’alchimie du livre_ à la BNF en novembre 2015.

![](/media/atelier_villemomble_02.jpg)

Les élèves sont ensuite venus à notre atelier à côté de la BNF. Ils ont découvert notre collection de livres d’artiste, notre espace de travail et nos outils.

Nous avons planifié plusieurs ateliers au collège avec les élèves : réalisation de 3 types de reliures différentes, recherches d’idées et création de trois projets de livre :

– la reliure d’une brochure constituée d’un cahier  
– la reliure d’un accordéon  
– la reliure d’un livre à la japonaise

![](/media/atelier_villemomble_01.jpg)

Le livre _Pour la paix dans le monde_ a été réalisé sur le modèle de la brochure. Il s’agit d’un unique cahier cousu dans lequel alternent des images, des symboles et des phrases. Les élèves ont sélectionné ces différents éléments pour parler des violences de notre monde et offrir un message positif.

![](/media/atelier_villemomble_06.jpg)

_Vide Plein_ est un accordéon présentant des photographies du collège à différents moments de la journée. Nous pouvons suivre d’un côté du livre l’espace vide, représenté en noir et blanc et de l’autre, l’espace en couleurs, rempli des activités quotidiennes des collégiens.est un accordéon présentant des photographies du collège à différents moments de la journée. Nous pouvons suivre d’un côté du livre l’espace vide, représenté en noir et blanc et de l’autre, l’espace en couleurs, rempli des activités quotidiennes des collégiens.

![](/media/atelier_villemomble_03.jpg)

Le livre _Tagus_, avec une reliure à la japonaise, est une exploration des tags. Les élèves ont recherché des tags sur internet et ont créé leurs propres images en couleur pour aborder les notions d’écriture et de trace. La couture, fait main par les élèves est à la fois fonctionnelle et décorative.

![](/media/atelier_villemomble_04.jpg)

Les trois projets ont été présentés dans une exposition à la Médiathèque de Villemomble. L’exposition, mise en place par Anna Mezey de la F93, montre toutes les étapes de la création des livres : apprendre la forme de la reliure, faire des recherches, créer des maquettes, créer un prototype, créer des fichiers pour l’impression et relier les feuilles imprimées.

Un grand merci à Anny Dauthieux, enseignante de la classe ULIS de Villemomble, à Anna Mezey de la F93 et à nos élèves du Collège Pasteur à Villemomble.

![](/media/atelier_villemomble_05.jpg)