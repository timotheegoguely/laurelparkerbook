---
categories:
- Papeterie
- Boîte d’éditeur
- Coffret
cover: /media/lpb-papeterie-boite_carte_cuir_1.jpg
date: 2015-02-14
description: ""
draft: true
title: E-shop online !
---
Nous sommes très heureux de présenter notre nouvelle e-boutique de nos propres créations de papeterie de luxe. Albums, carnets, et boîtes faites en petite série ou en pièce unique.  
Tout fait à la main et signé Laurel Parker Book.

![](/media/lpb-papeterie-boite_carte_cuir_1.jpg)

![](/media/lpb-papeterie-carnetvoeux2009-02.jpg)