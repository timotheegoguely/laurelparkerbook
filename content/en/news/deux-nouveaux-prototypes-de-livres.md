---
categories:
- Reliure
cover: /media/doppelt-pop-up-02.jpg
date: 2015-10-12
description: ""
draft: true
title: Deux nouveaux prototypes de livres
---
La commande d’un prototype est utile à la préparation d’une édition, il s’agit pour nos clients de réaliser une maquette au plus proche de l’objet final. Voici quelques exemples de projets récents.

Lash Vegas de Federico Berardi

Reliure à vis d’un bloc de tirages noir et blanc, coloration des tranches en noir et couverture en papier japonais marqué à chaud.

![](/media/berardi_lash_vegas_02.jpg)

![](/media/berardi_lash_vegas_01.jpg)

Inventions d’Axelle Doppelt

Un livre pop-up réalisé dans le cadre de son diplôme 2015 à l’ESAD Penninghen, en route pour être édité aux éditions L’AGRUME l’année prochaine !

![](/media/doppelt-pop-up-01.jpg)

![](/media/doppelt-pop-up-02.jpg)