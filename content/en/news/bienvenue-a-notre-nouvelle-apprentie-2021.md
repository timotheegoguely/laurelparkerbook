---
categories:
- Non classé
cover: /media/agathe-atelier-oiseaux.jpg
date: 2020-12-31
description: ""
draft: true
title: Welcome our new apprentice 2021
translationKey: agathe-apprentie
---
2021 is already starting off great !! We are thrilled to announce that Agathe Ruelland Rémy will be joining our team for 2021 as Graphic Designer and Studio Apprentice in our new studio at Komunuma.

![](/media/agathe-atelier-kimono.jpg)

![](/media/agathe-atelier-birthday.jpg)

Agathe is a Laureat of the Prix de perfectionnement aux métiers d’art, Savoir-faire en transmission ! Agathe earned her National Diploma of Art in graphic design (with photography option) and her Master’s degree in graphic design / book edition from the ESAM Caen-Cherbourg.

![](/media/agathe-atelier-oiseaux.jpg)