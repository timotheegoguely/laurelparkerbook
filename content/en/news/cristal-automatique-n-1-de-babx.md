---
categories:
- Coffret
- Projet d’artiste
cover: /media/babx-pochettecd-03.jpg
date: 2015-05-17
description: ""
draft: true
title: Cristal automatique n°1 de Babx
---
Créer une pochette de disque est un rêve de longue date pour Laurel Parker. _cristal automatique n°1_ est notre première édition de disque et, nous espèrons, pas la dernière…

Auto-produite par l’artiste Babx, cette édition est arrivée chez nous par le biais de notre ami Yoan De Roeck.

![](/media/babx-pochettecd-01.jpg)

Avec Yoan, nous avions travaillé sur un projet en commun il y a quelques années :[_Anastylose, Rome, XIII av. J._C.; 1942, farce archéologigue en deux actes et un aparté_](http://ludovicmichaux.com/PDFs/anastylose%20DP.pdf). Parmis les 4 artistes installés à la Villa Médicis, Yoan était le graphiste du livre, avec Ludovic Michaux pour la photographie. Ensuite, nous avions fabriqué un [coffret](http://www.laurelparkerbook.com/555-coffret-de-luxe) pour l’édition de tirages de Ludovic pour la BNF.

Les chansons du _cristal automatique n°1_ viennent de textes d’auteurs variés : Rimbaud, Baudelaire, Genet, Artaud, Kerouac, Waits, Miron et Césaire. Ces textes sont reproduits dans un livret relié à la main dans la pochette. L’artiste Laurent Allaire a réalisé 9 dessins autour des portraits des écrivains qui sont glissés dans le coffret.

Cette édition est un joli mélange d’images, de musique, et de textes.

![](/media/babx-pochettecd-04.jpg)

![](/media/babx-pochettecd-03.jpg)

_cristal automatique n°1_, de Babx  
350 exemplaires numérotés, avec un disque, un livret, et 9 dessins.  
En carte japonaise, avec un marquage à chaud.  
Conception graphique et pilotage du projet : [Yoan De Roeck](http://yoanderoeck.com/).  
Design et fabrication du coffret : Laurel Parker  
Dessins : Laurent Allaire (Allx)  
Impressions : Stipa, Montreuil