---
categories:
- Press book
- Projet de diplôme
cover: /media/jinling-pressbook_02.jpg
date: 2015-09-30
description: ""
draft: true
title: Nouveaux pressbooks
---
Les projets apportés dans notre atelier par les jeunes photographes et artistes sont souvent les plus originaux.

Le press book de Victoria Viennet, récemment diplômée de l’école Icart Photo, est un book à vis cachées en toile au format paysage. Il s’ouvre à la manière d’un calendrier, mettant en valeur ses séries de photographies. Une toile couleur chair et un marquage en cuivre sont un rappel aux images des corps féminins.

![](/media/viennet-pressbook-01.jpg)

![](/media/viennet-pressbook-03.jpg)

Pour le book de Jinling Xu, récemment diplômée des Gobelins, nous avons proposé deux toiles aux matières opposées qui s’accordent très bien ensemble : une toile d’aspect brut en coton pour l’extérieur et une toile vert clair, légèrement enduite pour l’intérieur (les gardes et le mécanisme). La délicatesse de ces couleurs choisies par l’artiste s’accordent parfaitement avec le style de son travail.

![](/media/jinling-pressbook_03.jpg)

![](/media/jinling-pressbook_02.jpg)

![](/media/jinling-pressbook_01.jpg)