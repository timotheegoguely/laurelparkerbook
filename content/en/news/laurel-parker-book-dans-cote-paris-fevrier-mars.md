---
categories:
- Presse
cover: /media/cote-paris-presse-01.jpg
date: 2015-02-15
description: ""
draft: true
title: Laurel Parker Book dans Côté Paris, février – mars
---
Nous avons un article de trois pages dans le nouveau numéro de [Côté Paris, février – mars, « Paris respire New York »](http://www.cotemaison.fr/chaine-d/deco-design/designer-et-relieuse-d-art-laurel-parker_24345.html). Beaucoup de belles images de nos projets d’éditions d’art. Mille mercis à Amandine Schira pour le bel article et à Nathalie Baetens pour les belles photos !

![](/media/cote-paris-presse-01.jpg)