---
categories:
- Livre d’artiste
- Reliure
cover: /media/blexbolex_fleur_oracle_04.jpg
date: 2016-11-07
description: ""
draft: true
title: La Fleur Oracle, de Blexbolex
---
Le nouvel album de Blexbolex édité par [l’Orbis Pictus Club](http://www.orbispictusclub.fr/) va sortir la semaine prochaine. Imprimé entièrement en sérigraphie par Frédéric Déjean à l’atelier Co-op. Ce livre est la suite de L’arrière-pays de 2012.

![](/media/blexbolex_fleur_oracle_04.jpg)

Dédicace-présentation en présence de Blexbolex :  
le 15 novembre à l’atelier CO-OP, à Maisons-Alfort  
le 16 novembre à Paris, chez Philippe le Libraire, 32 rue des Vinaigriers, Paris 10e