---
categories:
- Restauration
cover: /media/adler-armoire-doucet_01.jpg
date: 2016-06-09
description: ""
draft: true
title: Boîtes de Rose Adler
---
Durant plus de quatre ans nous avons réalisé des boîtes de conservation pour la bibliothèque littéraire Jacques Doucet à partir du travail de Rose Adler.  
Ce projet s’est découpé en deux parties. Il concerne la conservation de documents manuscrits et tapuscrits d’écrivains du début du XXème, regroupés autour du mouvement surréaliste et amis de Jacques Doucet.

![](/media/adler-boite-doucet_01.jpg)

Les réalisations de Rose Adler commandées par Jacques Doucet sont nombreuses. Dans le cas de l’armoire A, il s’agissait à l’époque de conserver les documents et pouvoir augmenter leur nombre dans chaque rangement. Rose Adler réalisa des cartons à dessins à rabats, en parchemin et papier bois japonais.  
Au fil du temps, ces rabats souples ont travaillé et vieilli, ne pouvant plus conserver les documents correctement.

En collaboration avec l’atelier Coralie Barbe, restauratrice de patrimoine, nous avons trouvé les solutions pour réaménager ces éléments en boîte de conservation tout en gardant les matières d’origine. Puis nous avons construits de nouvelles boites à l’identique, en préservant le plus possible l’esthétique de Rose Adler.

Ci-dessous, état de la chemise d’origine avec rabats avant restauration et état sous forme de boite après restauration : montants rigidifiés, pochettes et chemises de conservation sur-mesure pour les documents.

![](/media/adler-armoire-doucet_01.jpg)

Images tirées des boites réalisées à l’identiques. Le parchemin a été traité pour un rendu proche de celui d’origine. Nous avons utilisé la fin de notre stock de papier bois japonais, matière qui n’existe plus aujourd’hui.

![](/media/adler-boite-doucet_04.jpg)![](/media/adler-boite-doucet_02.jpg)