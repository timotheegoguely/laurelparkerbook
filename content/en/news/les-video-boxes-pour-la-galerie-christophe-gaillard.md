---
categories:
- Coffret
- Projet d’artiste
cover: /media/gaillard_videobox-01.jpg
date: 2016-03-26
description: ""
draft: true
title: Les Video Boxes pour la Galerie Christophe Gaillard
---
Nos récentes collaborations avec les artistes de la [Galerie Christophe Gaillard](http://galeriegaillard.com) nous ont permis de travailler sur un nouveau genre de coffret : la Video Box. Suite à la présentation de vidéos d’artistes à la galerie, nous proposons des designs originaux pour contenir un DVD et une clef USB en édition limitée.

Voici trois Video Boxes :

![](/media/gaillard_videobox-01.jpg)

_Soup_ de Rachel de Joode  
La matière qui compose ce coffret est un papier au toucher particulier, rappelant la peau qui est un élément important dans le travail de Rachel de Joode. Réalisé en découpe numérique, un jeu de pliages forme des cales de compensation pour entourer le DVD, ainsi qu’un emplacement pour la clef USB. Une pochette se situe dans le rabat de couverture pour y glisser un certificat.

![](/media/dejoode_videoboxe-livre_d-artiste_2.jpg)

![](/media/dejoode_videobox-livre_d-artiste_04.jpg)

_Prova Poetica_ de Véronique Boudier  
La première contrainte a été d’utiliser une couverture de survie pour habiller ce coffret – un vrai challenge. Les couleurs or et argent, ainsi que le carton noir brut, rappellent l’œuvre de l’artiste. Un mélange de techniques mécaniques – des découpages numériques – et manuelles – un ponçage délicat à la main – rendent cet objet unique.

![](/media/boudier_videobox-livre_artiste_02.jpg)

_Invisible Object_ de Michelle Lopez  
Comme le coffret pour Véronique Boudier, nous avons travaillé à partir de l’œuvre de l’artiste américaine Michelle Lopez pour nous inspirer en recherchant des matières et des formes. Le papier miroir et le plastique blanc opaque permettent ainsi d’entrer dans son univers dès qu’on ouvre le coffret. Un marquage à chaud sur le carton brut est visible à travers la transparence du plastique.

![](/media/lopez_videobox-livre_d-artiste_01.jpg)