---
categories:
- Livre d’artiste
cover: /media/inventario-drawing-franck.jpg
date: 2014-10-01
description: ""
draft: true
title: Dessin pour projet Inventario
---
Un de nos croquis pour le projet Inventario de Oscar Muñoz, éditions Toluca. Les chiffres en laitons seront décalés.

![](/media/inventario-drawing-franck.jpg)