---
categories:
- Boîte d’éditeur
- Boîte de présentation
cover: /media/lemaire_boite-presentation_02.jpg
date: 2018-03-26
description: ""
draft: true
title: Le bleu – une couleur choisie par deux photographes
---
Voici deux projets de boites de présentation utilisées par des photographes. Nous réalisons le design de chaque projet avec le client, en proposant des matières et des formes adaptées à son univers.

![](/media/lemaire_boite-presentation_02.jpg)

Avec le photographe [**Clément Lemaire**](http://clement-lemaire.com/), nous avons opté pour une boite à couvercle composée de deux plateaux distincts. Cela lui permet de présenter son portfolio composé de feuilles volantes. Les échancrures facilitent la manipulation des tirages et sont un élément de design visible une fois la boite fermée.

La matière choisie pour ce projet est une toile enduite résistante en deux coloris iridescents : un bleu « Martin-pêcheur » et un vert électrique. Les deux plateaux sont composés des deux couleurs interverties pour l’extérieur et l’intérieur.

![](/media/lemaire_boite-presentation_01.jpg)

La seconde boite est plus simple dans sa structure. Il s’agit d’une commande du photographe [**Joseph Melin**](https://www.josephmelin.com/) pour lequel nous avons déjà réalisé plusieurs pressbooks.

![](/media/melin_boite_presentation-01.jpg)

Une fine toile enduite grise recouvre l’extérieur. La couleur bleue apparaît sur les doublures internes et sur la charnière qui recouvre le dos. Ces teintes sont propres à l’identité visuelle, au travail et à la sensibilité de Joseph Melin.

![](/media/melin_boite_presentation-02.jpg)

![](/media/melin_boite_presentation-03.jpg)