---
categories:
- Livre d’artiste
- Album photo
cover: /media/lavoue_pampa_02.jpg
date: 2018-11-04
description: ""
draft: true
title: PAMPA de Stéphane Lavoué
---
PAMPA, de Stéphane Lavoué  Livre d’artiste réalisé dans le cadre du Prix Niépce 2018.

![](/media/lavoue_pampa_02.jpg)

Édition de 12 exemplaires  
Réalisation des tirages : Patrice Baron du laboratoire Picto  
Reliure : Laurel Parker Book  
Direction artistique : Laurel Parker  
Direction éditorial : Vincent Marcilhacy

Depuis 2016, Picto Foundation est mécène du du Prix Niépce – Gens d’images et récompense le lauréat par la conception et l’édition d’un objet d’artiste. Cette année, Stéphane Lavoué, Prix Niépce 2018, a imaginé « PAMPA » en collaboration avec Laurel Parker Book et avec le soutien d’Awagami et Taos Photographic. A découvrir à la Galerie Folia du 7 novembre au 15 décembre 2018 et sur le stand de Fisheye Gallery à Paris Photo du 8 au 11 novembre 2018.

![](/media/lavoue_pampa_01.jpg)