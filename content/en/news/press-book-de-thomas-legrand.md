---
categories:
- Projet d’artiste
- Reliure
- Press book
cover: /media/legrand_pressbook_01.jpg
date: 2018-04-24
description: ""
draft: true
title: press book de Thomas Legrand
---
[**Thomas Legrand**](https://thomas-legrand.com/) est venu vers nous pour la réalisation d’un pressbook grand format. Ce fut un plaisir de travailler à mettre en valeur ses photographies !

![](/media/legrand_pressbook_01.jpg)

Il s’agit d’un étui contenant un pressbook de taille A3+ à vis cachées, avec un marquage à chaud de couleur bronze. Nous en avons fabriqué deux exemplaires, l’un avec le texte en français, l’autre traduit en anglais.

![](/media/legrand_pressbook_02.jpg)

Nous avons particulièrement apprécié cette série personnelle de Thomas Legrand dont le sujet tourne autour de propositions de compositions florales. Il est vrai que le grand format des tirages met particulièrement en valeur ces bouquets. Thomas Legrand a photographié des natures mortes qui évoquent un autre temps, nous apprécions les détails qui se glissent dans les images, le choix des fleurs (toutes fraîches et de saison !), l’utilisation des vases et des objets comme les boîtes d’allumettes vintage ou les napperons en dentelle qui agrémentent les clichés.

![](/media/legrand_pressbook_03.jpg)