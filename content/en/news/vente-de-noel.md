---
categories:
- Expositions
- Portes ouvertes
cover: ""
date: 2014-12-11
description: ""
draft: true
title: Vente de Noël
---
Nous ouvrons les portes de notre atelier pour une vente de Noël ce vendredi, samedi et dimanche.

Mise en vente de petits carnets, d’albums photos, de mini-boîtes pour cartes de visite, etc. de notre collection de papeterie ainsi que des photos anonymes de notre chère amie Emmanuelle Fructus de Un livre-une image, et des livres des éditions Les Paroles Gelées.

Une dizaine d’ateliers de notre bâtiment qui participera à la vente de Noël…une belle occasion de trouver des cadeaux singuliers.

Au plaisir de vous voir !