---
categories:
- Expositions
cover: /media/galerie-lpb2-photo-gregory-copitet.jpg
date: 2021-06-14
description: Unfolding - expo du 29 mai au 31 juillet 2021
title: Exhibition Unfolding, group show (29 may - 31 july 2021)
translationKey: unfolding
---
La galerie **Laurel Parker Book** est ravie de vous accueillir dimanche **29 mai 2021** pour le **vernissage** de sa nouvelle exposition **Unfolding**. Elle se déroule **du 29 mai au 31 juillet 2021**.

Elle comprend des œuvres-multiples de **Mark Dion, Mateo López, Sara MacKillop, Françoise Pétrovitch et Yann Sérandour.**

![](/media/galerie-lpb-unfoldingphoto-gregory-copitet.jpg)![](/media/galerie-lpb-unfolding2-photo-gregory-copitet.jpg)

_Regarder les oiseaux, Françoise Pétrovitch_

Dans cette exposition, le livre qui se déploie transforme une œuvre graphique bidimensionnelle en sculpture tridimensionnelle. La vie d’un objet à déployer a plusieurs facettes : monté sur un mur comme un dessin, mis en forme sur un socle, ou dispersé sur une table comme un jeu de société ...

Dans l’exposition Unfolding, chaque œuvre utilise l’espace différemment en conservant le déploiement comme fil conducteur. Cette exposition présente les œuvres sous plusieurs angles afin d’explorer de diverses manières ces multiples d’artistes.

[Visualiser le pdf de présentation de l'exposition](/media/pdf-expo-unfolding.pdf "unfolding-pdf")

![](/media/galerie-lpb-photo-gregory-copitet.jpg)

![](/media/galerie-lpb2-photo-gregory-copitet.jpg)

![](/media/unfolding-serandour-gregory-copitet.jpg)

horaires galerie :

mardi - samedi

14h - 18h

Laurel Parker Book, membre de Komunuma

43 rue de la Commune de Paris

93230 Romainville