---
categories:
- Boîte de conservation
cover: /media/ensba_michair_conservation-02.jpg
date: 2018-02-19
description: ""
draft: true
title: Conditionnement des mini-livres incunables
---
À la demande de Monsieur Alexandre Leducq, conservateur en charge des manuscrits et imprimés anciens à l’école des Beaux-Arts, nous avons pensé à une solution qui permettrait de préserver les manuscrits de petite taille, leur rangement initial n’étant pas satisfaisant de par leur format.

![](/media/ensba_michair_conservation-01.jpg)

Nous avons proposé des boîtes dans un format standardisé comprenant la plus grande hauteur et l’épaisseur la plus large. Nous venons combler la boîte à l’aide de Plastazote pour créer un espace sur-mesure qui accueillera l’ouvrage et permettra sa conservation dans les meilleures conditions.

![](/media/ensba_michair_conservation-02.jpg)