---
categories:
- Boîte de présentation
- Coffret
cover: /media/vuitton-boite-polka_02.jpg
date: 2015-05-17
description: ""
title: Yves Marchand and Romain Meffre, box for Louis Vuitton Foundation
translationKey: blog-marchand-meffr
---
Louis Vuitton Foundation selected the artists Yves Marchand and Romain Meffre to photograph the new building designed by Frank Gehry. They collaborated with the Louis Vuitton Foundation and Polka Galerie to make the exhibition and book : _Fondation Louis Vuitton / Frank Gehry_, Skira Edition.

![](/media/vuitton-boite-polka_02.jpg)

The box contains 15 pigment prints produced for the event.

These artists are known for their work on the ruins of Detroit ruins and the abandoned island of Gunkanjima, both projects  published by Steidl Edition.

To appreciate this work of Yves Marchand and Romain Meffre, you can see book at the [Louis Vuitton Foundation](https://librairie.fondationlouisvuitton.fr/fr/editions-de-la-fondation/fondation-louis-vuitton-frank-gehry-version-bilingue-anglais-francais/18.html "Fondation Louis Vuitton") bookshop or visit their [website](http://www.marchandmeffre.com/ "marchand et meffre").

![](/media/vuitton-boite-polka_03.jpg)

![](/media/vuitton-boite-polka_04.jpg)

_Fondation Louis Vuitton / Frank Gehry_  
_Yves Marchand et Romain Meffre_  
Limited edition box of 12 copies  
white cloth, blue German mould-made paper and hologramme hot stamping.  
Client : Polka Galerie