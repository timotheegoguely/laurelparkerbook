---
categories:
- Livre d’artiste
- Reliure
cover: /media/freund_poussiere-blanche_02.jpg
date: 2017-06-30
description: ""
draft: true
title: Poussière blanche sur noir, de Pierre-Yves Freund
---
« [Pierre-Yves Freund](http://www.pyfreund.net/) travaille sur les notions de trace, d’empreinte, avec le hasard de lieux, de rencontres, l’appropriation et le prélèvement de matériaux. À travers ses photographies et ses sculptures, il est question de geste, de fragment, de recommencement, de réminiscence, d’affleurement. L’artiste introduit parfois la mise à l’épreuve du geste unique, l’équilibre possible – qui ne tient qu’à un fil – entre un volume de plâtre et le sol ; parfois encore il aligne des formes organisées en séries plus ou moins semblables issues d’une même matrice, ou perd ses mots dans la matière. Laissant souvent au temps l’action de son empreinte, il utilise également le thé, la lumière, la température comme éléments actifs de ses propositions.

![](/media/freund_poussiere-blanche_01.jpg)

Au cœur de sa pratique réside une prise en compte du temps qui induit des processus de réalisation et une syntaxe exigeants : la répétition d’un même geste (jamais à l’identique), l’économie de moyens, le prélèvement d’éléments de l’environnement proche. »

![](/media/freund_poussiere-blanche_02.jpg)

17 × 24 cm, 20 pages, français, impression 4 couleurs  
Design : [Juanma Gomez, GRAPHIC-LAB  
](http://www.graphique-lab.com/py_freund_pbns.html)Texte de François Bazzoli  
Photographies de Olivier Perrenoud et Pierre-Yves Freund  
Reliure : Laurel Parker Book