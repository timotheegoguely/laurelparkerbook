---
title: Folded By Hand
description: Japanese papiers fashionned by hand destined for interior design.
layout: folded-by-hand
menu:
  aside:
    weight: 5
slug: folded-by-hand
translationKey: folded-by-hand
images: null
___mb_schema: /.mattrbld/schemas/page.json
draft: null
---
Following their residency at the Villa Kujoyama in Kyoto, Laurel Parker Book has developed *Folded by Hand*: artworks and surfaces in washi (Japanese artisanal paper) using traditional treatments. These objects – a mixture of Japanese techniques and French savoir-faire – are intended for decoration and interior design.

Folded by Hand is represented by [Sinople Paris](https://sinople.paris/galerie/).
