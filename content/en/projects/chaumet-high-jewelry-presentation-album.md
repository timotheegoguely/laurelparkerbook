---
artist: ""
client: Chaumet
cover: /media/chaumet_reliure_bleu_02.jpg
date: 2017-11-11
description: |-
  Bookbinding with blue cloth, iridescent paper, mitsumata japanese paper, hot stamping. 51 copies, 2017.

  Chaumet uses this book as a tool to present their high-end jewellery collection in the form of bound drawings. The quality of the materials is our first priority, such as the paper tabs made from handmade japanese mitsumata fiber paper. This stunning paper, which is naturally shinny, is handmade by Shinichiro Abe, Eishiro Abe’s grand-son, the first paper maker in Japan nominated as a Living National Treasure.

  Artist director : Hélène Nounou.
  Hot stamping : Créanog.
  Fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/chaumet_reliure_bleu_02.jpg
- /media/chaumet_reliure_bleu_03.jpg
- /media/chaumet_reliure_bleu_01.jpg
- /media/chaumet_reliure_02.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Presentation album for Chaumet fine jewelry
translationKey: album-chaumet
---
