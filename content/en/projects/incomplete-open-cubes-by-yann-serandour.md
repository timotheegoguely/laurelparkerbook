---
artist: Yann Sérandour
client: '&: Christophe Daviet-Thery'
cover: /media/serandour-open-cube-01.jpg
date: 2010-11-10
description: This box presented in the form of a numbered open edition can contain
  up to twelve copies of Sol LeWitt’s book _Incomplete Open Cubes_ (The John Weber
  Gallery, New York, 1974). Its owner is invited to complete this edition by adding
  any copies of Sol LeWitt’s book that he may have collected. Instruction booklet
  printed by Les Compagnons du Sagittaire, Rennes. Box in archival museum board covered
  in coated paper.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images: []
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Incomplete Open Cubes, by Yann Sérandour
translationKey: incomplete-open-cubes
---
