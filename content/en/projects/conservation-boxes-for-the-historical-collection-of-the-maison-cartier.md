---
artist: ""
client: Cartier
cover: /media/cartier_boite-plume_01.jpg
date: 2018-05-13
description: |-
  Custom box with compartments and removable trays for a collection of kingfisher feathers in their original boxes. We are happy to collaborate often with the Cartier Archives, which houses and preserves an historical collection of photographs, drawings, and inventories for consultation. We use pH neutral raw materials of museum quality.

  Design and fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/cartier_boite-plume_01.jpg
- /media/cartier_boite-plume_02.jpg
secteurs:
- Archives
- Luxe
series:
- Unique
shop: false
title: Conservation boxes for the historical collection of the Maison Cartier
translationKey: boite-cartier
---
