---
artist: Clément Lemaire
client: Clément Lemaire
cover: /media/lemaire_boite-presentation_01.jpg
date: 2017-11-11
description: |-
  Many photographers come to our studio looking for custom-made presentation boxes. We are always happy to produce an object that marries well with their personal æsthetics.

  For Clément Lemaire, a custom handmade box with lid and openings for an easier handling of the photos. Two different shinny bookcloths, kingfisher blue and electric green, with hot stamping in mirrored film, 2017.

  Design and fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/lemaire_boite-presentation_01.jpg
- /media/lemaire_boite-presentation_02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Presentation box for Clément Lemaire
translationKey: clement-lemaire
---
