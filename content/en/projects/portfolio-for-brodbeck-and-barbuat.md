---
artist: Brodbeck & Barbuat
client: Brodbeck & Barbuat
cover: /media/brodbeck_barbuat_book_03.jpg
date: 2021-01-07
description: Set of three cloth-covered objects composed of a presentation box, a
  screw-post portfolio and a 2.0 box. The presentation box includes a booklet, held
  in place by two elastics, and a custom location to hold a computer and a selection
  of paper prints. The screw-post portfolio has a cloth-covered pocket to hold a booklet.
  The 2.0 box protects and brings a physical presence to the digital medium (here
  a flash drive). On each of the objects, the title has been hot stamped in our workshop.
  Design and fabrication by Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
images:
- /media/brodbeck_barbuat_book_03.jpg
- /media/brodbeck_barbuat_book_01.jpg
- /media/brodbeck_barbuat_book_02.jpg
- /media/brodbeck_barbuat_book_05.jpg
- /media/brodbeck_barbuat_book_04.jpg
secteurs: []
series:
- Unique
shop: false
title: Portfolio for Brodbeck and Barbuat
translationKey: portfolio-brodbeck-barbuat
---
