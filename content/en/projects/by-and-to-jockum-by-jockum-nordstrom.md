---
artist: Jockum Nordström
client: '&: Christophe Daviet-Thery'
cover: /media/nordstrom-to-jockum-01.jpg
date: 2011-11-10
description: 'Box containing 7 individual pop-up scenes by the artist Jockum Nordström.
  Published by & : Christophe Daviet-Thery and XN Éditions. Edition of 25 copies and
  6 AP. Hot stamping of the artist’s hand written colophon. Paper engineer, Marie-Victoria
  Garrido. Box : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/nordstrom-to-jockum-01.jpg
- /media/nordstrom-to-jockum-02.jpg
- /media/nordstrom-to-jockum-03.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: By and To Jockum, by Jockum Nordström
translationKey: jockum-nordstrom
---
