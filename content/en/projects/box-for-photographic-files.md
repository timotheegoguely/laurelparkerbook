---
artist: Sara White Wilson
client: ""
cover: /media/wilson-coffret-presentation-01.jpg
date: 2015-06-09
description: |-
  Box in black calfskin with 8 folders for photographs. Folders made of cloth backed with pH neutral paper. Titles in hot stamping. With a leather frame for a presentation card.

  Design and fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
- Coffret
images:
- /media/wilson-coffret-presentation-01.jpg
- /media/wilson-coffret-presentation-02.jpg
- /media/wilson-coffret-presentation-04.jpg
- /media/wilson-coffret-presentation-03.jpg
secteurs:
- Photo
series:
- Unique
shop: false
title: Portfolio for Sara White Wilson
translationKey: portfolio-white-wilson
---
