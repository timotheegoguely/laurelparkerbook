---
artist: Laurence Leblanc
client: Picto Foundation
cover: /media/leblanc-dargile-04.jpg
date: 2016-11-11
description: "_D'argile_, an artist's book produced with the support of the [Picto
  Foundation](https://www.pictofoundation.fr/prix-niepce/ \"Picto Foundation\"), sponsor
  of the Niépce Prize, as part of the prize awarded to Laurence Leblanc by the Gens
  dʻImages in 2016.\n\nThis artist's book was designed around the \"D’ARGILE\" series.
  The series is the result of meticulous photographic work, taking as its subject
  small clay figurines modeled for a film by Rithy Panh testifying to the atrocities
  of the Cambodian genocide.\n\nLaurence Leblanc uses the principle of reversal of
  scale and seriality to make these faces, smaller than the thumbnail of a hand, into
  real portraits. The 54 black and white prints found in the object strangely resonate
  with the history of anthropomorphic photography that the Khmer Rouge has terribly
  exploited for the purposes of identification and repression. However, to question
  the value of a unique and singular life in this atrocity of numbers, Laurence Leblanc
  does not give in to the systematic approach that characterizes mug shots and plays
  with a renewed photographic device. The appearance of a disproportionate human hand
  or the sight of two figurines seemingly supporting each other are all salient images
  that question differently. Still with Laurence Leblanc, this dissonance surfaces
  the meaning.\n\nFor this project, we have adapted a prototype already designed by
  the artist. To try to keep the authenticity of the original object, we have used
  two types of paper. This gives a gray effect on the edges, reminiscent of the tape
  and glue used by the artist.\n\n_D’argile_, by Laurence Leblanc, prize winner of
  the Prix Niepce, 2016, Paris. This artist book was made with Picto Foundation, Gens
  d’images and Laurel Parker Book. Limited edition of 15 copies.\n\nDesign : Laurence
  Leblanc with Laurel Parker  \nPrints : Picto  \nFabrication : Laurel Parker Book."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/leblanc-dargile-04.jpg
- /media/leblanc-dargile-03.jpg
- /media/leblanc-dargile-02.jpg
- /media/leblanc-dargile-01.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: D’argile, by Laurence Leblanc
translationKey: dargile
---
