---
title: Sirènes
translationKey: sirenes
description: >-
  Sirènes, artist book by Isidora Gajic, limited edition of 35 copies + 5 AP.
  Lithographic prints by&nbsp; Studio Bruno Robbe (Belgium). Accordion binding
  and slipcase by&nbsp; Laurel Parker Book. Published by L’éditeur du dimanche.
date: '2023-11-11'
client: l'éditeur du dimanche
artist: Isidora Gajic
series:
  - Multiple
secteurs:
  - Art contemporain
  - Photo
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
cover: /media/isidora-gajic-sirenes-artistbook-art-photo-edition-05-w.jpg
images:
  - /media/isidora-gajic-sirenes-artistbook-art-photo-edition-05-w.jpg
  - /media/isidora-gajic-sirenes-artistbook-art-photo-edition-02-w.jpg
  - /media/isidora-gajic-sirenes-artistbook-art-photo-edition-01-w.jpg
  - /media/isidora-gajic-sirenes-artistbook-art-photo-edition-03-w.jpg
  - /media/isidora-gajic-sirenes-artistbook-art-photo-edition-04-w.jpg
shop: null
foldedByHand: null
___mb_schema: /.mattrbld/schemas/projet.json
---

