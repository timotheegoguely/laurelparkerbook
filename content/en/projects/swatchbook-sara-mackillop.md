---
artist: Sara MacKillop
client: Laurel Parker Edition
cover: /media/mackillop_swatchbook_01.jpg
date: 2019-11-11T00:00:00.000Z
description: >-
  SWATCHBOOK is an artist book celebrating 10 years of self-publishing by
  British artist Sarah MacKillop. The samples of materials, images, and colors
  in SWATCHBOOK refer to favorite subjects of the artist, while also evoking
  "Presage: Paris Fashion Forecast", a leading quarterly in haute couture from
  1961 through 1987. This folder with 4 foldout screens can be viewed either as
  a book or taken apart to create an exhibition space. Silkscreen printing,
  digital printing, and hot stamping on Colorplan papers, with hand-made
  elements by the artist, stationery, pens, stickers, and pages from Ikea
  catalogues.  31 x 22 x 6 cm (closed) • Digital prints, silkscreen printing,
  hot stamping, mixed media • 15 ex + 3 AP • Laurel Parker Edition, 2018 •
  980,00 € VAT included
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
  - Livre
images:
  - /media/mackillop_swatchbook_01.jpg
  - /media/mackillop_swatchbook_03.jpg
  - /media/mackillop_swatchbook_02.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: SWATCHBOOK
translationKey: swatchbook
___mb_schema: /.mattrbld/schemas/projet.json
---

