---
artist: ""
client: Delpire & Co
cover: /media/delpire-multimasques-tigre.jpg
date: 2021-06-15
description: "Multimasques for the Delpire & Co bookshop.   \nHand mounting of tiger
  and zebra masks from the 1967 book from Delpire Editions.  \nOn exhibit at the opening
  of the Delpire & Co bookshop."
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Œuvre papier
images:
- /media/delpire-multimasques-tigre.jpg
- /media/delpire-multimasques-zebre.jpg
- /media/delpire-multimasques1.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: Multimasques for the Delpire & Co bookshop
translationKey: multimasques-delpire
---
