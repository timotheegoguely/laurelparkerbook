---
artist: Babx
client: Babx
cover: /media/babx-pochettecd-03.jpg
date: 2020-11-11
description: |-
  Cristal automatique n°1, by Babx
  Fold-out boxes, limited edition for CD, including a hand-bound booklet and 9 drawing that can be taken out and framed.
  Hot stamping, rubber stamping, offset printing, and hand-sewing
  Graphic design : Yoan De Roeck.
  Drawings : Laurent Allaire (Allx)
  Box design and fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Chemise
- Boîte 2.0
images:
- /media/babx-pochettecd-03.jpg
- /media/babx-pochettecd-02.jpg
- /media/babx-pochettecd-01.jpg
- /media/babx-pochettecd-05.jpg
- /media/babx-pochettecd-04.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Cristal automatique n°1, by Babx
translationKey: cristal-automatique
---
