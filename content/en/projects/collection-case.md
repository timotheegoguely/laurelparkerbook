---
artist: ""
client: Laurel Parker Book
cover: /media/collection-boxes-01.jpg
date: 2020-11-11
description: |-
  Developed by our studio, this box presents four types of conservation enclosures for use in archives and collections.

  Each model is designed for different documents :
  Demeter Box : for heavy and fragile books
  Daphne Box : for series of minuscules and small formats
  Artemis Portfolio : for loose leaves and thin documents
  Gaïa Slipcase : adapted to all formats

  Each conservation box is custom-made, composed of pH neutral materials, and developed to follow the logic of an archive.

  Box available for conservators of collections, contact us to benefit from this offer.
designs:
- Semi-sur mesure
fonctions:
- Archiver
formes:
- Coffret
- Chemise
images:
- /media/collection-boxes-01.jpg
secteurs:
- Archives
series:
- Multiple
shop: false
title: Collection Case
translationKey: collection-case
---
