---
artist: Stéphane Lavoué
client: ""
cover: /media/lavoue_pampa_02_w.jpg
date: 2018-11-11
description: 'Artist book produced for the Prix Niépce 2018. Digital prints on Japanese
  paper, Picto Laboratoire. Tab binding with "plats rapportés". Marbeled paper endsheets,
  italian cloth. Picto Foundation, sponsor of the Prix Niépce with the Gens d’images.
  Book design and graphic design : Laurel Parker'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lavoue_pampa_02.jpg
- /media/lavoue_pampa_01.jpg
- /media/lavoue_pampa_03.jpg
- /media/_40B9194.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Pampa de Stéphane Lavoué
translationKey: pampa
---
