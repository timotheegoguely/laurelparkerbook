---
artist: François Mallon
client: François Mallon
cover: /media/mallon-for-100-drawings_02.jpg
date: 2014-06-02
description: '100 portraits of the last days of the artist’s partner. Box in blue
  cloth with hot stamping in orange and a folder made of cloth backed with pH neutral
  paper.  Design : Antoine Delage de Luget. Fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Éditer
- Présenter
formes:
- Coffret
images:
- /media/mallon-for-100-drawings_02.jpg
- /media/mallon-for-100-drawings_03.jpg
- /media/mallon-for-100-drawings_01.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: In Memoriam...100 drawings, by François Mallon
translationKey: francois-mallon
---
