---
artist: ""
client: Grafikmente
cover: /media/ledoyen_menu_01.jpg
date: 2019-06-04
description: |-
  Menus developed for l’Abysse, new three-star restaurant at the Pavillon Ledoyen.
  Origami structure to hold - and change often - the menu. Cover in japanese paper transformed by itajime hand-folding.
  Art direction : Arancha Vega, Agence Grafikmente. Design : Laurel Parker.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Œuvre papier
images:
- /media/ledoyen_menu_01.jpg
- /media/ledoyen_menu_02.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Menus for the restaurant Abysse, Pavillon Ledoyen
translationKey: abysse-ledoyen
---
