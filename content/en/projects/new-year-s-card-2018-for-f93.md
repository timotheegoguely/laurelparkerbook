---
artist: ""
client: F93
cover: /media/f93_carte_2019-02.jpg
date: 2018-05-06
description: 'Since 2017 we have been asked to design and produce the New Year’s card
  for the F93 in Montreuil (Paris suburb). Each of the 3 projects have focused on
  the paper support, transforming it and playing with the F93 logo (designed by Gaël
  Hugo @onemorestudio). In 2018 the design was inspired by the advent calendar, which
  reveals the anniversary year of the F93. Silkscreen printing in black and yellow.
  Series of 100 copies. Design and fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Œuvre papier
images:
- /media/f93_carte_2019-02.jpg
- /media/f93_logo_web.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: New Year’s card 2018 for F93
translationKey: F93-2018
---
