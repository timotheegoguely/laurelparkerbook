---
artist: Etienne Chambaud, Vincent Normand
client: Paraguay Press
cover: /media/chambaud_contre-histoire-separation-01.jpg
date: 2016-06-07
description: |-
  Publication based on the eponymous film by Etienne Chambaud and Vincent Normand in which the authors develop the concept of the “Decapitated Museum.” The voice-over in the film has been made into a one-edition book, which was rephotographed according to the technique the authors used to make the film. The outcome is a photography book that is as much a text document than an imaginary scene.
  "Counter History of Separation (52 min, 2010) begins during the Reign of Terror, in 1793, which simultaneously witnesses the apparition of the guillotine on the revolutionary stage and the creation of the modern museum. It ends in 1977, when the last decapitation by guillotine occurred and when the Centre Pompidou, itself a new museum paradigm, was inaugurated. It explores the space opened by the optical and political limits of modernity, by tracking the dissolution of the functions of the guillotine and the modern museum in what the authors define as 'the Decapitated Museum.'" — Raphael Brunel, Les Cahiers de la Création Contemporaine
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/chambaud_contre-histoire-separation-01.jpg
- /media/chambaud_contre-histoire-separation-03.jpg
- /media/chambaud_contre-histoire-separation-02.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: Counter History of  Separation
translationKey: contre-histoire
---
