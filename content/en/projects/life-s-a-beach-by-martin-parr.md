---
artist: Martin Parr
client: Éditions Xavier Barral
cover: /media/parr-prototype-02.jpg
date: 2020-11-10
description: 'Prototypes for _Life’s a Beach_ by Martin Parr. Co-edition of the Aperture
  Foundation and the Éditions Xavier Barral. Photo album with a Japanese 4-hole binding.
  Design : Xavier Barral Graphic design : Aurélie Vitoux.  Prototyping, with fabrication
  design including videos for the industrial binding of 2000 copies : Laurel Parker
  Book.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/parr-prototype-02.jpg
- /media/parr-prototype-01.jpg
- /media/parr-prototype-03.jpg
- /media/parr-prototype-05.jpg
- /media/parr-prototype-04.jpg
secteurs:
- Photo
- Art contemporain
series:
- Prototype
shop: false
title: Life's a Beach, by Martin Parr
translationKey: lifes-a-beach
---
