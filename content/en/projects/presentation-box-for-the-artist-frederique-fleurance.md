---
artist: Frédérique Fleurance
client: Fréderique Fleurance
cover: /media/fleurance-boite-presentation-02.jpg
date: 2014-11-10
description: |-
  Presentation box for the artist Frédérique Fleurance.
  A4 box made with japanese paper, hot stamping, nickel-plated rivets, elastic, 2014.
  Design : Frédérique Fleurance.
  Fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/fleurance-boite-presentation-02.jpg
- /media/fleurance-boite-presentation-04.jpg
- /media/fleurance-boite-presentation-03.jpg
- /media/fleurance-boite-presentation-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Presentation box Frédérique Fleurance
translationKey: frederique-fleurence
---
