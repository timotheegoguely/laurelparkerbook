---
artist: ""
client: F93
cover: /media/f93_carte_2017_01.jpg
date: 2017-05-07
description: Since 2017 we have been asked to design and produce the New Year’s card
  for the F93 in Montreuil (Paris suburb). Each of the 3 projects have focused on
  the paper support, transforming it and playing with the F93 logo (designed by Gaël
  Hugo @onemorestudio). Japanese papier with hand folding and silkscreen printing
  by Frédéric Déjean of the Orbis Pictus Club. Series of 100 copies.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Œuvre papier
images:
- /media/f93_carte_2017_01.jpg
- /media/f93_carte_2017_02.jpg
- /media/f93_logo_web.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: New Year’s card 2017 for F93
translationKey: F93-2017
---
