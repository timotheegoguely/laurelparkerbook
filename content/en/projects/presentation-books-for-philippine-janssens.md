---
artist: ""
client: Philippine Janssens
cover: /media/janssens_-classeur-02.jpg
date: 2019-11-11
description: |-
  Presentation books for the boutique Philippine Janssens, Paris.
  Gilded cloth, gold-tinted ring mechanism, hot stamping, 2019.
  Fabrication and material research : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
images:
- /media/janssens_-classeur-02.jpg
- /media/janssens_-classeur-01.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Presentation books for Philippine Janssens
translationKey: janssens
---
