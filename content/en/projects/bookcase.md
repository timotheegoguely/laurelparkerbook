---
artist: ""
client: Laurel Parker Book
cover: /media/bookcase_02.jpg
date: 2019-02-04
description: 'Bookcase : a Tool for Designers. A box with 15 hand-sewn pamphlets with
  1, 2, or 3 sections. Created as a presentation tool for graphic designers and artistic
  directors, the Bookcase offers a look at hand-bindings to refine a publishing project
  with the client. A double slipcase transforms the bookcase into a transportation
  case for meetings.'
designs:
- Semi-sur mesure
fonctions:
- Présenter
- Archiver
formes:
- Chemise
- Livre
- Coffret
images:
- /media/bookcase.jpg
secteurs:
- Luxe
- Agence
- Archives
series:
- Multiple
shop: false
title: Bookcase
translationKey: bookcase
---
