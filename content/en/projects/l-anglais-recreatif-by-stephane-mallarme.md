---
artist: Stéphane Mallarmé
client: Bibliothèque Littéraire Jacques Doucet
cover: /media/mallarme-boite-conservation_02.jpg
date: 2013-06-11
description: 'Conservation box to hold _L’Anglais Récréatif,_ a game to learn English,
  by Stéphane Mallarmé. In the collection of the Jacques Doucet Literary Library,
  Paris. Restoration of Mallarmé’s board game by Atelier Coralie Barbe. Box with
  drawers, materials conforming to museum standards. Design and fabrication : Laurel
  Parker Book.'
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/mallarme-boite-conservation_02.jpg
- /media/mallarme-boite-conservation_01.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: L'Anglais Récréatif by Stéphane Mallarmé
translationKey: stephane-mallarme
---
