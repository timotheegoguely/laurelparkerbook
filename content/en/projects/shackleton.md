---
artist: Franck Hurley
client: Salto Ulbeek
cover: /media/hurley-endurance-01.jpg
date: 2021-01-06
description: Shackleton's exploration of Antarctica aboard the Endurance from 1914
  to 1917 was documented by photographer Frank Hurley. The negatives made during this
  odyssey have been restored, reissued and augmented with unpublished prints to offer
  this wonderful project initiated by Salto Ulbeek in collaboration with The Royal
  Geographical Society and The Scott Polar Research Institute, Cambridge. This box
  includes a book, two sewn booklets and 68 prints, all printed in the platinum-palladium
  process. These different elements are organized around a removable tray that structures
  the exploration of the box. Cotton cloth and ribbons forge links with sailors' clothing
  and the warm color of platinum prints. Edition and prints, Salto Ulbeek Box design,
  fabrication and hot stamping by Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/hurley-endurance-20.jpg
- /media/hurley-endurance-05.jpg
- /media/hurley-endurance-06.jpg
- /media/hurley-endurance-03.jpg
- /media/hurley-endurance-07.jpg
- /media/hurley-endurance-01.jpg
- /media/hurley-endurance-04.jpg
secteurs:
- Photo
series:
- Multiple
shop: false
title: Shackleton's Endurance
translationKey: shackleton
---
