---
artist: ""
client: Philippine Janssens
cover: /media/janssens_pressbook_03.jpg
date: 2018-11-11
description: |-
  Philippine Janssens has worked with us several times to produce portfolio books, which are used in her boutiques to present the selection of fabrics for her bespoke trousers. High quality hand work and an attention to detail are things that we have in common with Philippine Janssens.

  We made a series of portfolio books for both her boutique on the rue Faubourg Saint-Honoré and for her new boutique in the Bon Marché, Paris.

  Mat pink leather, coated champagne-colored cloth, hot stamping, pink and gold ribbons, cloth dividers with hot stamping, 2018.

  Design and fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portoflio
- Coffret
images:
- /media/janssens_pressbook_03.jpg
- /media/janssens_pressbook_01.jpg
- /media/janssens_pressbook_05.jpg
- /media/janssens_pressbook_02.jpg
- /media/janssens_pressbook_04.jpg
- /media/janssens_pressbook_06.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Portfolio book for Philippine Janssens
translationKey: portfolio-philippine-janssens
---
