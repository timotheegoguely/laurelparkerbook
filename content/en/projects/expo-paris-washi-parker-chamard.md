---
title: Inside Outside au Musée de la Chasse et de la Nature
translationKey: museenature
description: >-
  On the occasion of the 39th European Heritage Days and the Paris Design Week
  2022, the Musée de la Chasse et de la Nature and la Villa Kujoyama, partners
  since 2016, invited the public to come and discover the research project that
  Laurel Parker and Paul Chamard developed during their residency in Kyoto
  around Japanese paper - <em>washi</em> - listed by UNESCO as part of the world
  heritage. As a surprising trompe l’œil, each element of the « Inside Outside »
  installation -kimono, tatami, packaging etc., is made from <em>washi</em>
  transformed using traditional techniques. These interpretive objects invite us
  to question the contemporary use of <em>washi</em>, which has become one of
  the favorite materials of architects, craftsmen, artists and designers.
date: 2022-09-18T00:00:00.000Z
client: null
artist: Laurel Parker & Paul Chamard
series:
  - Unique
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Installation
  - Œuvre papier
cover: /media/parker-chamard-musee01-site.jpg
images:
  - /media/parker-chamard-musee01-site.jpg
  - /media/parkerchamardmusee01.jpg
  - /media/parker-chamard-musee02-site.jpg
  - /media/parker-chamard-musee03-site.jpg
  - /media/parker-chamard-musee05-site.jpg
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

