---
artist: ""
client: ENSBA-École de Beaux-Arts de Paris
cover: /media/ensba-boite-a-seaux-02.jpg
date: 2020-11-11
description: |-
  Conservation quality box covered in Italian buckram book cloth with plastazote compensation. Produced for the protection and presentation of the historical archive of the École des Beaux-Arts de Paris : founding ordinances of the Royal Academy of Painting and Sculpture, in parchment with wax seals.

  Alexandre Leducq, conservator in charge of ancient manuscripts and printed documents.
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/ensba-boite-a-seaux-01.jpg
- /media/ensba-boite-a-seaux-02.jpg
- /media/ensba-boite-a-seaux-03.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: Conservation box for parchment documents
translationKey: boîte-sceaux
---
