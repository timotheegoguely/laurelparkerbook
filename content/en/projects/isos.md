---
artist: ""
client: Undo-Redo
cover: /media/peri-fabrique-isos_02.jpg
date: 2014-11-10
description: _ISOs_, a creation for _Peri’Fabrique_ during the Design Days 2014, an
  exhibition at the Musée des Arts Décoratif, Paris. _ISOs_ is a woven poster, an
  innovative and experimental artwork from the collaboration of the designers Undo-Redo,
  the tapestry weavers Atelier 3, and the book binding techniques of Laurel Parker
  Book. Papiers by Takeo.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Œuvre papier
- Installation
images:
- /media/peri-fabrique-isos_02.jpg
- /media/peri-fabrique-isos_04.jpg
- /media/peri-fabrique-isos_06.jpg
- /media/peri-fabrique-isos_03.jpg
- /media/peri-fabrique-isos_01.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: ISOs
translationKey: iso
---
