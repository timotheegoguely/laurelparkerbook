---
title: sudare
translationKey: sudare
description: >-
  Le sudare, l’écran de fenêtre qui protège du soleil et de la pluie, est
  traditionnellement fabriqué de lattes de bois ou de bambou. Ici, une fine
  baguette de bois présente nos interprétations du sudare en washi. Les
  techniques utilisées sont le froissage, ou encore la formation de paille en
  papier cousu de fil d’or.
date: '2021-01-11'
client: null
artist: Laurel Parker
series:
  - Unique
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
cover: /media/sudare-1-site.jpg
images:
  - /media/sudare-1-site.jpg
  - /media/sudare-momigami-rose-1-site.jpg
  - /media/sudare-momigami-rose-3-site.jpg
  - /media/sudare-momigami-rose-2-site.jpg
  - /media/sudare-paille-1-site.jpg
  - /media/sudare-paille-2-site.jpg
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

