---
title: 51, by Emmanuelle Fructus
artist: Emmanuelle Fructus
client: ""
cover: /media/fructus-51-01.jpg
date: 2019-11-11
description: |-
  One-of-a-kind sculpture. Archival box which transforms into a exhibition podium. Buckram cloth, plastazote and hot stamping on the underneath of the box. Presented for the first time at the exhibition EXTRAordinaire : regards photographiques sur le quotidien, at the Institut pour la photographie, Lille.

  Artwork, design and creation of the sculptures : Emmanuelle Fructus. Box design and fabrication : Paul Chamard.
designs:
- Sur mesure
fonctions:
- Archiver
- Présenter
formes:
- Coffret
images:
- /media/fructus-51-01.jpg
- /media/fructus-51-02.jpg
- /media/fructus-51-03.jpg
secteurs:
- Archives
- Photo
- Art contemporain
series:
- Unique
shop: false
translationKey: "51"
---
