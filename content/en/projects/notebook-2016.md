---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-carnet-2016-01.jpg
date: 2016-11-11
description: 'Our end-of-the-year journal 2016 : hand sewn chain stitch, die cutting,
  silkscreen printing, and hot stamping with a hologram film. Limited edition of 150
  copies. Design and fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet-2016-01.jpg
- /media/lpb-carnet-2016-02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: End-of-the-year Notebook 2016
translationKey: notebook-2016
---
