---
artist: Mateo López
client: '&: Christophe Daviet-Thery'
cover: /media/lopez_flor_01.jpg
date: 2019-11-11
description: "“Flor (Flower) is part of a series of folding sculptures, I like to
  imagine art objects that deal with space, have a certain portability, expand and
  contract. I ́m also interested on the physical relation between our body and the
  book object. Flor is an invitation to engage, move, and activate, a game of abstraction
  that evokes blooming.\"—Mateo López\n\nArticulated pages in cardboard covered with
  buckram canvas\n25 ex + 5A.P. \nFabrication : Laurel Parker Book"
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lopez_flor_01.jpg
- /media/lopez_flor_02.jpg
- /media/lopez_flor_03.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Flor, by Mateo Lopez
translationKey: flor
---
