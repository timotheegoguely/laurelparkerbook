---
artist: Andreas Varnavides
client: Andreas Varnavides
cover: /media/andreas_camerasolitude_03.jpg
date: 2019-11-11
description: 'Camera Solitude, by Andreas Varnavides . Hinged box covered in cloth
  with digital UV printing. Edition of 30 copies + 5 AP. Digital pigment prints on
  Hahnemuehle fine art paper.'
designs:
- Semi-sur mesure
fonctions:
- Présenter
- Éditer
formes:
- Boîte d’éditeur
images:
- /media/andreas_camerasolitude_01.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Camera Solitude, by Andreas Varnavides
translationKey: camera-solitude
---
