---
artist: Sébastien Le Roy
client: Les Paroles Gelées
cover: /media/roy-passion_01.jpg
date: 2016-11-11
description: 'Artist book. The binding system, which dates from the middle ages, is
  a non-adhesive structure which allows for a very flat opening. Forty-four original
  linoleum etchings printed on a cylinder press. Design and fabrication : Laurel Parker
  Book.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/roy-passion_01.jpg
- /media/roy-passion_03.jpg
- /media/roy-passion_02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Passion by Sébastien Le Roy
translationKey: passion-sebastien-le-roy
---
