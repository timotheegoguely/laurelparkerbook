---
artist: ""
client: Louis Vuitton – Cabinet d’Écriture
cover: /media/vuitton-carnet-ecriture-02.jpg
date: 2013-01-31
description: 'A collector’s box of bindings which have marked the history of writing
  and of books : Japanese binding in lambskin backed with hand-dyed japanese paper,
  sewn with hand-dyed thread. Lyonnaise binding in goat skin backed with paper, hand
  sewn with hand-dyed thread. Coptic binding in lambskin, with goat suede linings,
  hand sewn with hand-dyed thread. Research of materials, product refinement, study
  for the production in a series, and creation of finished prototypes. Design and
  fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/vuitton-carnet-ecriture-02.jpg
- /media/vuitton-carnet-ecriture-03.jpg
- /media/vuitton-carnet-ecriture-01.jpg
- /media/vuitton-carnet-ecriture-05.jpg
- /media/vuitton-carnet-ecriture-04.jpg
secteurs:
- Luxe
series:
- Prototype
shop: false
title: Prototypes for the Cabinet d’Écriture, Louis Vuitton
translationKey: prototype-louis-vuitton
---
