---
artist: Kourtney Roy
client: Galerie Catherine et André Hug
cover: /media/kourtney_roy_hug_02.jpg
date: 2017-11-11
description: 'Box covered with embossed paper in two colors of blue, raised platform,
  red hot stamping, 2017. For the Catherine and André Hug Gallery, Paris. Design and
  fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/kourtney_roy_hug_02.jpg
- /media/kourtney_roy_hug_01.jpg
- /media/kourtney_roy_hug_04.jpg
- /media/kourtney_roy_hug_03.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Hope, by Kourtney Roy
translationKey: hope
---
