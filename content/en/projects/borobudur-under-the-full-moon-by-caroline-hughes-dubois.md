---
artist: Caroline & Hughes Dubois
client: Luna Light Agency
cover: /media/dubois-borobudur-03.jpg
date: 2017-06-06
description: Portfolio of 9 platinum-palladium prints. Letterpress printing by the
  Imprimerie Nationale. Design and fabrication of the deluxe box by Laurel Parker
  Book
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
- Portoflio
images:
- /media/dubois-borobudur-03.jpg
- /media/dubois-borobudur-01.jpg
- /media/dubois-borobudur-02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Borobudur Under the Full Moon, by Caroline & Hughes Dubois
translationKey: borobudur
---
