---
title: 'Folded by Hand : Gampi Rectangles'
translationKey: fbh gampi
description: >-
  <em>Folded by Hand : </em>surfaces in washi (Japanese artisanal paper) using
  traditional treatments. These objects – a mixture of Japanese techniques and
  French savoir-faire – are intended for decoration and interior design. Series
  Gampi Rectangles in colors matcha, sugi and natural.
date: '2024-05-12'
client: Laurel Parker Book
artist: Folded by Hand
series:
  - Multiple
secteurs:
  - Agency
designs:
  - Custom-made
fonctions:
  - Exhibit
formes:
  - Paper object
cover: /media/fbh-paper-design-gampi-matcha-2-web.jpg
images:
  - /media/fbh-paper-design-gampi-matcha-web.jpg
  - /media/fbh-paper-design-gampi-sugi-web.jpg
  - /media/fbh-paper-design-gampi-natural-web.jpg
  - null
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

