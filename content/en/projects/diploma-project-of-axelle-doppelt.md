---
title: Diploma project of Axelle Doppelt
artist: Axelle Doppelt
client: Axelle Doppelt
cover: /media/doppelt-pop-up-01.jpg
date: 2015-11-11
description: Diploma project of Axelle Doppelt, in graphic design, at ESAD Penninghen.
  Prototype for a pop-up book. Deesign, hand-cut pop-ups and plexiglas engraving by
  the student.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/doppelt-pop-up-01.jpg
- /media/doppelt-pop-up-02.jpg
- /media/doppelt-pop-up-03.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
translationKey: livre-axelle-doppelt
---
