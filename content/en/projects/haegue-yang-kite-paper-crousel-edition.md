---
title: Mesmerizing Kite – Acrobat in Butterfly-Bat Grip
translationKey: kite
description: >-
  Hanji, graph paper on traditional Korean kite, pine reel, kite string, maple
  components, 25 5/8 × 13 3/8 × 9 in. Edition of 50 + 5AP. Box with a cover, a
  removable tray which holds the kite, and a hidden compartment that hold the
  stand. Glazed cotton cloth and screenprinting. Design &amp; Fabrication of the
  box :&nbsp; @laurelparkerbook. Production by&nbsp; Galerie Chantal Crousel.
  Photos : Jiayun Deng — Galerie Chantal Crousel.
date: '2022-12-23'
client: Galerie Chantal Crousel
artist: Haegue Yang
series:
  - Multiple
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Coffret
cover: /media/haegue-yang-kite-paper-crousel-01.jpg
images:
  - /media/haegue-yang-kite-paper-crousel-01.jpg
  - /media/haegue-yang-kite-paper-crousel-04.jpg
  - /media/haegue-yang-kite-paper-crousel-02.jpg
  - /media/haegue-yang-kite-paper-crousel-05.jpg
  - /media/haegue-yang-kite-paper-crousel-03.jpg
shop: null
foldedByHand: null
___mb_schema: /.mattrbld/schemas/projet.json
---

