---
title: Atlas of the world RRHV
translationKey: atlas
description: >-
  A copy of Philips Atlas of the World is re-ordered and re-bound according to
  the patterns of movement of refugees from around the world. The book includes
  two texts by Vahid Davar : ‘Nassim’s Testament’, an epic poem about the
  oneiric asylum journey of two friends from Iran to the UK, and the essay ‘A
  Chimerical World with a Nomenclature of its Own’ which is a commentary on the
  poem. The texts are set into pages made from gold emergency rescue thermal
  blankets with separately the original Persian version of ‘Nassim’s Testament’
  in a cutout. Interspersed throughout are a series of prints on Japanese paper
  which provide an echo to the refugee story. Artist book, series of 10 copies
  with 4 artists' proofs. Produced by In Situ - Fabienne Leclerc
date: '2023-06-14'
client: null
artist: 'Ramin Haerizadeh, Rokni Haerizadeh, Hesam Rahmanian & Vahid Davar'
series:
  - Multiple
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
cover: /media/rrh-atlas-03-site-laurelparkerbook.jpg
images:
  - /media/rrh-atlas-03-site-laurelparkerbook.jpg
  - /media/rrh-atlas-07-web.jpg
  - /media/rrh-atlas-09-web.jpg
  - /media/rrh-atlas-11-site-laurelparkerbook.jpg
  - /media/rrh-atlas-13-web.jpg
  - /media/rrh-atlas-20-web.jpg
shop: false
___mb_schema: /.mattrbld/schemas/projet.json
---

