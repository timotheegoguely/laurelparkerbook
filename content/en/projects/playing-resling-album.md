---
artist: Emmanuelle Fructus
client: Un Livre Une Image
cover: /media/fructus_album-un-livre-une-image-02.jpg
date: 2012-06-04
description: |-
  Albums for a collection of found photos. Archival quality paper. Photographs inserted into corners cut into the page. Hot stamping on the removable spine. Slipcase.
  Publisher : Emmanuelle Fructus, Un Livre Une Image
  Fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Archiver
- Présenter
formes:
- Portfolio
- Livre
images:
- /media/fructus_album-un-livre-une-image-02.jpg
- /media/fructus_album-un-livre-une-image-01.jpg
secteurs:
- Archives
- Photo
series:
- Multiple
shop: false
title: Albums for a collection fo anonymous photography
translationKey: albums-fructus
---
