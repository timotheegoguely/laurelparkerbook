---
artist: Charline von Heyl
client: '&: Christophe Daviet-Thery'
cover: /media/von_heyl-sabotage-02-1.jpg
date: 2008-11-10
description: |-
  Artist book. 46 accordion pages and 23 free Mylar pages.

  "Rejecting both written language and illustration, Sabotage is a sort of image-text that gets straight to one of the book format’s most abstract possibilities: the material production of a sort of counterspace that exists beyond meaning. Interspersing transparent (Mylar) and opaque (paper) pages—a selection of the latter have been reconceived for publication here—Sabotage exploits the optical effects of superimposition while riveting the attention of its reader to the basic activity of turning pages. Isn’t this every book’s most intimate desire—to be ransacked and explored by fingers and eyes? Each turned page makes and unmakes the next, and the book remains in a state of constant optical transformation." — John Kelsey, Artforum

  Edition of 100 numbered copies. Silkscreen printing : Screen Group, Brussels Binding : Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/von_heyl-sabotage-02-1.jpg
- /media/von_heyl-sabotage-01.jpg
- /media/von_heyl-sabotage-03.jpg
secteurs:
- Art contemporain
series:
- Multiple
title: Sabotage
translationKey: sabotage
---
