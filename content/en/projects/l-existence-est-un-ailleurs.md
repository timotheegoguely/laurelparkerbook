---
artist: Isabelle Daëron
client: Laurel Parker Edition
cover: /media/existence_daeron_ed_lpb_01_w.jpg
date: 2022-04-26T00:00:00.000Z
description: >-
  The work of designer Isabelle Daëron begins with drawing - the sketch, the
  esquisse - which continues to live in the finished project - in the line
  formed by a cord, in the pattern of a painted watercolor, in the texture of
  colored granit. These places and objects, full of joy for life, owe their
  existence from an “elsewhere”, the imagination and plastic practice of the
  artist. 21 x 28 cm, 36 pages • Offset printing, Colorplan papers with
  corresponding hot stamping in 3 colors, hand sewn in green cotton cord • 200
  ex • Laurel Parker Edition, 2022 • 20 € VAT included
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/existence_daeron_ed_lpb_00_w.jpg
  - /media/existence_daeron_ed_lpb_03_w.jpg
  - /media/existence_daeron_ed_lpb_05_w.jpg
  - /media/existence_daeron_ed_lpb_02_w.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: L'existence est un ailleurs
translationKey: existence
___mb_schema: /.mattrbld/schemas/projet.json
---

