---
artist: J.D. Okhai Ojeikere
client: Toluca Editions
cover: /media/ojeikere-afrokhoi-sapiens_01.jpg
date: 2014-11-10T00:00:00.000Z
description: >-
  5 original photographs printed on resin coated paper in a deluxe boxed set.
  Text by Abdourahman Waberi. Box in ebony wood designed by Edward Barber and
  Jay Osgerby (not pictured). Graphic design by Olivier Andreotti. Folders in
  archival card stock with hot stamping, in a paper chemise, by Laurel Parker
  Book. Limited édition of 20 copies + 4 A.P.
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
  - Chemise
images:
  - /media/ojeikere-afrokhoi-sapiens_01.jpg
  - /media/ojeikere-afrokhoi-sapiens_02.jpg
  - /media/ojeikere-afrokhoi-sapiens_03.jpg
secteurs:
  - Art contemporain
  - Photo
series:
  - Multiple
shop: false
title: 'Afrokhoï Sapiens, by J.D. Okhai Ojeikere'
translationKey: afrokhoi-sapiens
___mb_schema: /.mattrbld/schemas/projet.json
---

