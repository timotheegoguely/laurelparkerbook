---
title: surfaces en washi
translationKey: matiere-papier
description: >-
  Artworks and surfaces details in washi (Japanese artisanal paper) using
  traditional treatments. These objects – a mixture of Japanese techniques and
  French savoir-faire – are intended for decoration and interior design.
date: '2023-01-11'
client: null
artist: Laurel Parker Book
series:
  - Prototype
secteurs:
  - Agence
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Œuvre papier
cover: /media/carre-rose-1-site.jpg
images:
  - /media/carre-rose-1-site.jpg
  - /media/carre-rose-6-site.jpg
  - /media/cire-rose-5-site.jpg
  - /media/cire-rose-4-site.jpg
  - /media/gampi-vert-3-site.jpg
  - /media/momigami-marron-3-site.jpg
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

