---
title: Miroir Noir 13/18 & 9/14
translationKey: miroirnoir1318
description: >-
  Un coffret&nbsp;en carton de qualité&nbsp;conservation recouvert
  de&nbsp;papier, avec un verre convexe float argenté noir de fumée. Miroir Noir
  au format 13x18, édition de 3ex. Miroir Noir au format 9x14, édition de 3ex.
date: '2023-06-10'
client: Emmanuelle Fructus
artist: Emmanuelle Fructus
series:
  - Multiple
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
  - Archiver
formes:
  - Coffret
cover: /media/miroir-noir9-14-03-carre.jpg
images:
  - /media/miroir-noir9-14-04-web.jpg
  - /media/miroir-noir9-14-05-web.jpg
  - /media/miroir-noir9-14-06-web.jpg
  - /media/miroir-noir9-14-10-web.jpg
  - /media/miroir-noir9-14-01-web.jpg
  - /media/miroir-noir9-14-02-web.jpg
shop: false
___mb_schema: /.mattrbld/schemas/projet.json
---

