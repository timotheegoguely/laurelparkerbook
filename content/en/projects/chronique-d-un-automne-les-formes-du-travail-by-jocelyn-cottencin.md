---
artist: Jocelyn Cottencin
client: Jocelyn Cottencin
cover: /media/cottencin_reliure_01.jpg
date: 2019-06-04
description: |-
  _Chronique d’un automne, les formes du travail_, a project by Jocelyn Cottencin. This edition is a re-reading of a video project produced at the IUT at Roubaix. Hand sewn case binding, silkscreen printing on book cloth.

  Artistic direction, graphic design and typographic design : Jocelyn Cottencin
  Printing by Media Graphic, Rennes
  Binding by Laurel Parker Book
designs: []
fonctions:
- Éditer
formes:
- Livre
images:
- /media/cottencin_reliure_03.jpg
- /media/cottencin_reliure_01.jpg
- /media/cottencin_reliure_02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Chronique d’un automne, les formes du travail, by Jocelyn Cottencin
translationKey: jocelyn-cottencin
---
