---
title: Work, Simone Shailes
artist: Simone Shailes
client: Simone Shailes
cover: /media/shailes-book-02.jpg
date: 2021-02-04
description: "Simone Shailes, fashion designer. Portfolio in the form of 10 booklets
  with assorted colored covers and stitching, in a custom slipcase. Presentation box
  for the designers swatches. Four color digital printing, hot stamping.\n\n  \nDesign
  : Simone Shailes.\n\nAdvice and fabrication : Laurel Parker Book"
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
images:
- /media/shailes-book-02.jpg
- /media/shailes-book-09.jpg
- /media/shailes-book-04.jpg
- /media/shailes-book-10.jpg
- /media/shailes-book-05.jpg
- /media/shailes-book-08.jpg
- /media/shailes-book-01.jpg
- /media/shailes-book-06.jpg
secteurs:
- Luxe
- Photo
series:
- Unique
shop: false
translationKey: work-shailes
---
