---
artist: Klima Intérieurs (Léa Bigot et Sarah Espeute)
client: Laurel Parker Edition
cover: /media/klima-pages2_01.jpg
date: 2018-09-17T00:00:00.000Z
description: >-
  Following the beautiful Riso printed Klima Pages #1...#2, Somptueuses
  Résidences. This _faux_ design magazine proposes four vacation villas located
  somewhere in the south of France. Format 30,5 × 23,5 cm, 90 pages. Digital
  prints by Media Graphic / Compagnons du Sagittaire, Rennes. Edition limited to
  150 numbered copies, 20 which comme with an original bas-relief sculpture key
  fob made by the Klima girls. Format 30,5 x 23,5 cm, 90 pages • Digital
  printing • 150 ex + 20 ex special series with key and key chain in bas-relief
  signed Klima • Laurel Parker Edition, 2018 • 35 € / 70 € VAT included
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/klima-pages2_01.jpg
  - /media/klima-pages2_03.jpg
  - /media/klima-pages2_04.jpg
  - /media/klima-pages2_02.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: 'Klima Pages #2'
translationKey: klima-pages
___mb_schema: /.mattrbld/schemas/projet.json
---

