---
title: 97 figurants, by Luc Delahaye
artist: Luc Delahaye
client: Luc Delahaye
cover: /media/delahaye-97-figurants-01.jpg
date: 2013-11-10
description: |-
  97 figurants. Artist book by Luc Delahaye. Paris, 2013. Digital printing.
  Design and fabrication : Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/delahaye-97-figurants-01.jpg
- /media/delahaye-97-figurants-02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
translationKey: 97-figurants
---
