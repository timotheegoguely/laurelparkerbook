---
artist: Amélie Ambroise
client: Amélie Ambroise
cover: /media/ambroise_portfolio_01.jpg
date: 2009-11-11
description: Diploma project of Amélie Ambroise, student at Icart Photo. Photographic
  prints in hand-cut paper mats, original drawings by the artist on the reverse, binding
  on accordion pleat, cotton gingham fabric, cloth-covered slipcase with stencil print
  and interior cushioned end pages. The design, stencil printing, drawings, cushions,
  and graphic design are all by the artist. Binding by Laurel Parker. Winner of best
  book of her class.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/ambroise_portfolio_01.jpg
- /media/ambroise_portfolio_02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Unique
shop: false
title: Diploma project of Amélie Ambroise
translationKey: livre-amelie-ambroise
---
