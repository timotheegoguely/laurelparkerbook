---
artist: Koenraad Dedobbeleer
client: '&: Christophe Daviet-Thery'
cover: /media/dedobbeleer-space-02.jpg
date: 2011-11-10
description: "Artist book which can be viewed either as a book (page by page) or upright
  to create a space with volume.\n\"The edition succinctly visualizes Koenraad Dedobbeleer's
  artistic approach: shapes taken from everyday objects, architectural details and
  sculptural forms. The images of these objects create an interlocking network that
  can time and again be combined, recombined and shifted around. The edition, which
  stood as an artwork exemplifies how such shifts are never done in a straightforward
  manner in his work but instead create a network in which everything is connected
  to each other.\"— les presses du réel\nLeporello.  Series of 200 copies.  \nOffset
  printing by Cultura, Belgium.\nBinding by Laurel Parker Book."
designs: []
fonctions:
- Éditer
formes:
- Livre
images:
- /media/dedobbeleer-space-02.jpg
- /media/dedobbeleer-space-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
title: Space Has No Meaning Outside of Time
translationKey: space-has-no-meaning-outside-of-time
---
