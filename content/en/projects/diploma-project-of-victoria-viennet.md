---
title: Diploma project of Victoria Viennet
artist: Victoria Viennet
client: ""
cover: /media/viennet-pressbook-03.jpg
date: 2015-11-11
description: Diploma project of Victoria Viennet, Icart Photo, 2015. Portfolio book
  in pale pink cloth and copper hot stamping.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
images:
- /media/viennet-pressbook-03.jpg
- /media/viennet-pressbook-02.jpg
- /media/viennet-pressbook-01.jpg
secteurs:
- Photo
series:
- Unique
shop: false
translationKey: pressbook-victoria-viennet
---
