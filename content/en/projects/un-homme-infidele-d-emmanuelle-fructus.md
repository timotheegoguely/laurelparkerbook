---
title: Un homme infidèle, d'Emmanuelle Fructus
artist: Emmanuelle Fructus
client: Un Livre Une Image
cover: /media/fructus-homme-infidele-02.jpg
date: 2008-11-09
description: 'Artist book made form original prints from the artist’s collections
  of anonymous photographs. Design : Emmanuelle Fructus and Laurel Parker.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/fructus-homme-infidele-01.jpg
- /media/fructus-homme-infidele-02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
translationKey: un-homme-infidele
---
