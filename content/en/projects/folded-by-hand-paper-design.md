---
title: Folded by Hand Swatch book
translationKey: fbh nuancier
description: >-
  <em>Folded by Hand</em> : surfaces in washi (Japanese artisanal paper) using
  traditional treatments. These objects – a mixture of Japanese techniques and
  French savoir-faire – are intended for decoration and interior design.
date: '2024-05-13'
client: Laurel Parker Book
artist: Folded by Hand
series: null
secteurs: null
designs:
  - Custom-made
fonctions:
  - Exhibit
formes:
  - Paper object
cover: /media/fbh-paper-design-02-web.jpg
images:
  - /media/fbh-paper-design-02-web.jpg
  - /media/fbh-paper-design-01-web.jpg
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

