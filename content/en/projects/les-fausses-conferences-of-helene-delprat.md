---
artist: Hélène Delprat
client: ""
cover: /media/delprat_fausses-conferences-04.jpg
date: 2017-06-05
description: |-
  Coinciding with Hélène Delprat’s retrospective exhibition at the Maison Rouge, I Did It My Way, we made a series of video boxes with the Galerie Christophe Gaillard. Graphic design by Camille Morin for the gallery. 5 copies + 1 A. P.

  Les (Fausses) Conférences. This video box has the same proportions as a 35mm slide, with transparent black and white images printed on mylar. museum board, and black hot stamping.
  -W.O.R.K.S & D.A.Y.S, Hélène Delprat imagined a personalized diary, with gilded edges and spiral bound. Digital printing on Munken Polar paper.
  -Hi-Han Song Remix, detail of a painting by the artist printed on embossed paper. Digital printing, black hot stamping on Chromolux paper, museum broad.
  -Comment j’ai inventé Versailles. Hand folded Japanese paper, museum board, blind stamping.
  -Les chants du guerrier couvert de cendres, An image of a face hidden behind a ski mask, drawn by shiny paper seen through perforations in black suede.
designs:
- Sur mesure
fonctions:
- Présenter
- Éditer
formes:
- Boîte 2.0
images:
- /media/delprat_fausses-conferences-04.jpg
- /media/delprat_fausses-conferences-02.jpg
- /media/delprat_dvd-box_01.jpg
- /media/delprat_fausses-conferences-01.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Video boxes for Hélène Delprat
translationKey: video-box-helene-delprat
---
