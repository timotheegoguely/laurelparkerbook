---
artist: Françoise Pétrovitch
client: Laurel Parker Edition
cover: /media/petrovitch_oiseaux-01.jpg
date: 2020-09-14T00:00:00.000Z
description: >-
  During the lockdown, we followed Françoise Pétrovitch on Instagram, where she
  posted daily drawings of birds that she saw from her window. We contacted
  Françoise and proposed making and edition with these birds – more than a
  hundred in all ! The result of our collaboration is a box that is both a
  specimen box when closed, and a mini-theater when opened. The birds in each
  box – seven original drawings – can be staged, like flowers in an ikebana, the
  Japanese art of floral arrangement. We look at this arrangement of living and
  dead birds through a piece of hand blown glass, which adds the irregularities
  of windows in old houses. The edition is limited to 17 unique sets of birds (A
  to Q). Each set includes a menagerie of 7 original drawings of birds grouped
  together by the artist, in a handmade windowed box. Laurel Parker Edition,
  2020 • Price on demande
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Coffret
  - Œuvre papier
images:
  - /media/petrovitch_oiseaux-01.jpg
  - /media/petrovitch_oiseaux-04.jpg
  - /media/petrovitch_oiseaux-08.jpg
  - /media/petrovitch_oiseaux-03.jpg
  - /media/petrovitch_oiseaux-05.jpg
  - /media/petrovitch_oiseaux-06.jpg
  - /media/petrovitch_oiseaux-02.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: Regarder les oiseaux
translationKey: regarder-les-oiseaux
___mb_schema: /.mattrbld/schemas/projet.json
---

