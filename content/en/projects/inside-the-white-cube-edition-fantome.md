---
artist: Yann Sérandour
client: '&: Christophe Daviet-Thery'
cover: /media/serandour_inside_1.jpg
date: 2009-01-04
description: |-
  "Phantom edition" limited edition box to hold the English and French versions of  [Inside the White Cube, Overprinted Edition]() by Yann Sérandour.  "Designed after Jack W. Stauffacher’s design for Brian O’Doherty _Inside the White Cube_, using Adrian Frutiger’s Méridien typeface. The inside faces of the case are made of a life size printed photograph of the cover of this book and of the cover of the issue of the magazine _Artforum_ in which the first part of Brian O’Doherty essay was first published, giving clues to the book’s layout."

  Design by Jérôme Saint-Loubert Bié. Box covered in Cialux cloth with hot stamping by  Laurel Parker Book. Offset printing by Media Graphic / Les Compagnons du Sagittaire, Rennes. Edition of 50 copies.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Boîte d’éditeur
images:
- /media/serandour_inside_1.jpg
- /media/serandour_inside_2.jpg
- /media/serandour_inside_3.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Inside the White Cube, Phantom edition
translationKey: inside-the-white-cube
---
