---
artist: Yann Sérandour
client: ""
cover: /media/serandour_how_01.jpg
date: 2020-11-23
description: We are proud to announce the book launch of the limited library edition
  of HOWOHWHWOOWHWHOWOH by Yann Sérandour. Published by & Christophe Daviet-Thery
  and Parc Saint Leger.
designs:
- Sur mesure
fonctions:
- Présenter
- Éditer
formes:
- Livre
images:
- /media/serandour_how_01.jpg
- /media/serandour_how_02.jpg
secteurs:
- Art contemporain
series:
- Multiple
title: HOWOHWHWOOWHWHOWOH by Yann Sérandour
---
