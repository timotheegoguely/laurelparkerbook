---
artist: Jasper Johns
client: Wildenstein Institute
cover: /media/johns-coffret-01.jpg
date: 2017-08-31
description: Five bindings in various materials (Scottish calfskin, Japanese Linen,
  Italian cloth). Handmade endpapers by Ruscombe Mill, Margaux. Letterpress on endpaper.
  Box in black bog oak made by Hervé Morin, Maonia, Paris. Design and Binding by Laurel
  Parker Book.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
- Coffret
images:
- /media/johns-coffret-01.jpg
secteurs:
- Art contemporain
series:
- Prototype
title: Prototype, limited edition Jasper Johns catalogue raisonné
translationKey: prototype-catalogue-raisonne-jasper-johns
---
