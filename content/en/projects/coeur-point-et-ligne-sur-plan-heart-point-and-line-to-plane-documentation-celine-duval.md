---
artist: documentation céline duval
client: documentation céline duval
cover: /media/celine-duval-sur-plan_03.jpg
date: 2016-11-11
description: |-
  _Cœur, point et ligne sur plan_ _(Heart, Point and Line to Plane)_, documentation céline duval. One-of-a-kind book created during a conference at the Pompidou Center in January, 2013. Spiral binding with postcards and a slipcase with hot stamping. Logo designed by the graphic designer Myriam Barchechat using a typeface created by Josef Albers at the Bauhaus in the 1920s. In her book, Céline Duval proposes a very personal interpretation of some family photos of her favorite painter Wassily Kandinsky, which she discovered in an album at the Pompidou Centre’s Kandinsky Library. This interpretation builds upon the explanations that the painter offers in his philosophical book Point and Line to Plane, which describes and defines the geometric elements found in painting. Paris, 2016 for the one-of-a-kind book, and 2013 for the edition of 1000 copies published by Éditions Semiose.

  To learn more about this project, pick up a copy of The Shelf Journal N°5.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
- Livre
images:
- /media/celine-duval-sur-plan_02.jpg
- /media/celine-duval-sur-plan_04.jpg
- /media/celine-duval-sur-plan_03.jpg
secteurs:
- Art contemporain
- Photo
series:
- Unique
shop: false
title: Cœur, point et ligne sur plan (Heart, Point and Line to Plane), documentation
  céline duval
translationKey: coeur-point
---
