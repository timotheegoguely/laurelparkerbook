---
artist: Dora Garcia
client: Rosascape
cover: /media/garcia-mad-marginal-02.jpg
date: 2011-11-10
description: |-
  Artist book with facsimilies of the archives of  project _Mad Marginal,_ presented at the Biennale de Venise, Spanish pavillon, 2011. Box with binder system, covered in paper with cloth joints. Colophon hot stamped of the cover in 4 different versions. Graphic design and design of folders : Jean-Claude Chianale. Series of 48. Box design and fabrication : Laurel Parker Book.

  "The artist's book _Mad Marginal_ is part of the eponymous project on which Dora García has been working since 2009. In the form of an archive box, this artist's book brings together 35 documents that fed into Dora García's research during these two years. These documents are reproduced here identically in facsimile form. Dealing with marginality as an artistic and political position, Dora García is interested in the different meanings that such a position can have as well as in the contradiction and the beauty of the artist as a marginal figure."
designs:
- Sur mesure
fonctions:
- Archiver
- Éditer
formes:
- Boîte d’éditeur
images:
- /media/garcia-mad-marginal-02.jpg
- /media/garcia-mad-marginal-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Mad Marginal, The archives, by Dora Garcia
translationKey: 'mad-marginal-dora-garcia '
---
