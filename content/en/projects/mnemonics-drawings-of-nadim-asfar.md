---
artist: Nadim Asfar
client: Nadim Asfar
cover: /media/asfar-livre-artistes-onglets6.jpg
date: 2021-09-29
description: "Prints mounted on Japanese paper tabs, visible sewing and covers « rapportés
  » , with a conservation slipcase. Prints on tracing paper, drawings by Antoine Atallah.
  Typewriter text on Japanese paper by Nadim Asfar.\n\nThis project is now on exhibit
  at the Tanit Gallery, Munich : Imaginary Cities: chapter II  \nBeyrouth as project,
  Nadim Asfar in collaboration with Antoine Atallah  \n24 sept - 20 nov 2021"
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/asfar-livre-artistes-onglets5.jpg
- /media/asfar-livre-artistes-onglets6.jpg
- /media/asfar-livre-artistes-onglets4.jpg
- /media/asfar-livre-artistes-onglets3.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Mnemonics drawings by Nadim Asfar
translationKey: Dessins-mnémoniques
---
