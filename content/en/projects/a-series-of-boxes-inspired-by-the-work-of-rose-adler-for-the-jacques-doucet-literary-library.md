---
title: Boxes in the style of Rose Adler
artist: ""
client: Bibliothèque Littéraire Jacques Doucet
cover: /media/adler-boite-doucet_01.jpg
date: 2014-06-03
description: |-
  A series of boxes inspired by the work of Rose Adler for the Jacques Doucet Literary Library. Rose Adler realized a series of portfolios with flaps covered in parchment and Japanese wood paper, but they were deeply damage by the time. The new conservation boxes respect the aesthetic of Rose Adler design and the original materials, as well as the conservation of the document.

  Project realized in partnership with the Atelier Coralie Barbe, restorer of national heritage. Fabrication : Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/adler-boite-doucet_01.jpg
- /media/adler-boite-doucet_04.jpg
- /media/adler-boite-doucet_02.jpg
secteurs:
- Archives
series:
- Multiple
shop: false
translationKey: facsimile-rose-adler
---
