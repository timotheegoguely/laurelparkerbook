---
title: Bag by Sara MacKillop
artist: Sara MacKillop
client: Laurel Parker Edition
cover: /media/mackillop-bag-01.jpg
date: 2018-10-18T00:00:00.000Z
description: >-
  We invited British artist Sara MacKillop to design a shopping bag for our
  stand at the Salon MAD book fair in September 2018. Following her artist’s
  books IKEA Kitchens, IKEA Baths, and IKEA Wardrobes, Sara MacKillop has
  created a bag on the same theme. Format 41 x 32 cm • Silkscreen printed 2
  colors recto verso • Laurel Parker Edition, 2018 • 9,99 € VAT included
designs:
  - Semi-sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
images:
  - /media/mackillop-bag-01.jpg
  - /media/mackillop-bag-02.jpg
  - /media/mackillop-bag-serigraphy.jpg
  - /media/mackillop-bag-serigraphy-02.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
translationKey: bag-sara-mackillop
___mb_schema: /.mattrbld/schemas/projet.json
---

