---
artist: ""
client: Louis Vuitton – Studio Graphique
cover: /media/vuitton-reliure-coptes_01.jpg
date: 2016-09-17
description: Coptic binding of archived documents, Louis Vuitton.
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Livre
images:
- /media/vuitton-reliure-coptes_01.jpg
secteurs:
- Luxe
- Archives
series:
- Multiple
shop: false
title: Bindings for the archives of Louis Vuitton
translationKey: archives-vuitton
---
