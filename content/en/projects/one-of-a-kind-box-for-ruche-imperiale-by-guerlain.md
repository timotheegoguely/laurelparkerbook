---
artist: ""
client: Créanog
cover: /media/guerlain_boite-ruche-03.jpg
date: 2013-06-04
description: |-
  One-of-a-kind box for the presentation of the perfume Ruche impériale by Guerlain.
  Design of the box and relief stamps : Créanog.
  Box fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/guerlain_boite-ruche-03.jpg
- /media/guerlain_boite-ruche-01.jpg
- /media/guerlain_boite-ruche-04.jpg
- /media/guerlain_boite-ruche-02.jpg
secteurs:
- Luxe
series:
- Unique
shop: false
title: Ruche impériale by Guerlain
translationKey: ruche-imperiale
---
