---
artist: Luminita Liboutet
client: Un Livre Une Image
cover: /media/liboutet-touch-me-02.jpg
date: 2009-08-31
description: 'A series of snapshot-sized photographs by the Romanian artist Luminita
  Liboutet. Published by Un Livre Une Image, Paris 2009.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Boîte d’éditeur
images:
- /media/liboutet-touch-me-01.jpg
- /media/liboutet-touch-me-02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
title: Touch Me, by Luminita Liboutet
translationKey: touch-me
---
