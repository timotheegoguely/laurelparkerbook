---
artist: Joël Alain Dervaux
client: Joël Alain Dervaux
cover: /media/dervaux_allonges_02.jpg
date: 2018-11-11
description: Allongés by Joël Alain Dervaux, album with 10 black and white silver
  prints in bound mats. Cloth binding with « plats rapportés », hot stamping on cover
  and colophon. All materials archival quality. Limited edition of 3 + 2 AP.
designs:
- Sur mesure
fonctions:
- Présenter
- Éditer
formes:
- Livre
images:
- /media/dervaux_allonges_01.jpg
- /media/dervaux_allonges_06.jpg
- /media/dervaux_allonges_03.jpg
- /media/dervaux_allonges_04.jpg
- /media/dervaux_allonges_02.jpg
- /media/dervaux_allonges_05.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Allongés by Joël Alain Dervaux
translationKey: allonges-joel-alain-dervaux
---
