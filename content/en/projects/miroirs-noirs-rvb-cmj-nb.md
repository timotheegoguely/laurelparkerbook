---
artist: Emmanuelle Fructus
client: Emmanuelle Fructus
cover: /media/fructus_miroir_noir_01_w.jpg
date: 2022-11-01
description: "Ensemble de 8 miroirs noirs.   \nVerre convexe float argenté noir de
  fumée en vis-à-vis de tissus et soies colorés. Étui en carton de conservation, recouvert
  de toile Buckram. "
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/fructus_miroir_noir_01_w.jpg
- /media/fructus_miroir_noir_04_w.jpg
- /media/fructus_miroir_noir_09_w.jpg
- /media/miroir_noir_at_copitet_02_w.jpg
- /media/miroir_noir_at_copitet_01_w.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Miroirs Noirs RVB CMJ NB
translationKey: miroirs-noirs
---
