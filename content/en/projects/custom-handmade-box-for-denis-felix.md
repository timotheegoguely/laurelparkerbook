---
artist: Denis Felix
client: Denis Félix
cover: /media/felix-portfolio-02.jpg
date: 2014-05-05
description: |-
  For Denis Felix, a custom handmade box with opening for an easier handling of the photos A2. Thin grey cloth with hot stamping on a label well.

  Design and fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/felix-portfolio-02.jpg
- /media/felix-portfolio-04.jpg
- /media/felix-portfolio-03.jpg
- /media/felix-portfolio-01.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Custom handmade box for Denis Felix
translationKey: denis-felix
---
