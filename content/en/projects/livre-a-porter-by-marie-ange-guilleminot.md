---
artist: Marie-Ange Guilleminot
client: Marie-Ange Guilleminot
cover: /media/guilleminot-toucher-voir_01.jpg
date: 2015-11-11
description: 'Artist’s book, two different copies : one an accordion book, the other
  a Japanese stab binding. From her project _Toucher-voir,_ entrusted to the artist
  by Olivier Saillard, director of the Paiais Galliera - the Paris Fashion Museum.
  This work, destined for all but especially for the blind and the visually impaired,
  was made possible by a grant from the The Conny-Maeva Charitable Foundation and
  the charitable organization _Le Livre de l’aveugle_. 2015 Graphic design and project
  management : Élise Gay et Kévin Donnot. Printing : Le Livre de l’Aveugle Binding
  : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/guilleminot-toucher-voir_01.jpg
- /media/guilleminot-toucher-voir_02.jpg
- /media/guilleminot-toucher-voir_03.jpg
- /media/guilleminot-toucher-voir_04.jpg
secteurs:
- Art contemporain
- Luxe
series:
- Multiple
shop: false
title: Livre-à-porter, by Marie-Ange Guilleminot
translationKey: livre-marie-ange-guilleminot
---
