---
artist: Tiane Doan na Champassak
client: Tiane Doan na Champassak
cover: /media/tiandoan-veilofmaya-01.jpg
date: 2018-11-11
description: Box for a book of 2000 pages. Series of 35 copies. "By sequencing recurring
  patterns from the streets of Kolkata, 'The Veil of Maya 1' materializes on paper
  the flayed skin of a city. Layers and layers of colorful posters and promising ads
  cover the walls like an aesthetic bacteria; it’s intermittence is almost compulsive.
  This 2000-page volume brings together an overflowing torrent of 'imaginary photographs'
  in an editorial ideal of copiousness." Custom-made box by Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/tiandoan-veilofmaya-01.jpg
- /media/tiandoan-veilofmaya-03.jpg
- /media/tiandoan-veilofmaya-04.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
title: 'The Veil of Maya 1 '
---
