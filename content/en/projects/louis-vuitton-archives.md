---
artist: ""
client: Louis Vuitton – Studio Graphique
cover: /media/vuitton-reliure-coptes_01.jpg
date: 2020-11-10
description: |-
  Bookbinding for the Louis Vuitton archives. Coptic sewing.
  For the Studio Graphique, Louis Vuitton.
  Fabrication : Laurel Parker Book
designs:
- Sur mesure
draft: true
fonctions:
- Archiver
formes:
- Livre
images: []
secteurs:
- Luxe
series:
- Multiple
title: Louis Vuitton archives
---
