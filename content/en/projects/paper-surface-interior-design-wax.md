---
title: 'Folded by Hand : series shoji'
translationKey: fbh shoji
description: >-
  <em>Folded by Hand :&nbsp;</em>surfaces in washi (Japanese artisanal paper)
  using traditional treatments. These objects – a mixture of Japanese techniques
  and French savoir-faire – are intended for decoration and interior design.
  Series <em>Shoji</em> in colors white, rose waxed or verdigris waxed.
date: '2024-05-11'
client: Laurel Parker Book
artist: Folded by Hand
series:
  - Multiple
secteurs:
  - Agency
designs:
  - Custom-made
fonctions:
  - Exhibit
formes:
  - Paper object
cover: /media/fbh-paper-design-shoji-rectangle-2-web.jpg
images:
  - /media/fbh-paper-design-shoji-rectangle-web.jpg
  - /media/fbh-paper-design-wax-rose-web.jpg
  - /media/fbh-paper-design-wax-verdigris-web.jpg
  - null
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

