---
artist: Nathalie Grenier
client: Nathalie Grenier
cover: /media/grenier_leporello_01_w.jpg
date: 2022-10-20
description: "One of a kind giant accordion book for the presentation of 9 original
  drawings (ink and etching).  \nPaper structure, title and colophon printing: UV
  inkjet."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/grenier_leporello_01_w.jpg
- /media/grenier_leporello_04_w.jpg
- /media/grenier_leporello_03_w.jpg
- /media/grenier_leporello_02_w.jpg
secteurs:
- Art contemporain
series: []
shop: false
title: Soulévements
translationKey: soulevements
---
