---
artist: Laurel Parker Book
client: ""
cover: /media/0088.jpg
date: 2021-03-08
description: "We are proud to present “Endurance”, our end of the year notebook, designed
  and fabricated by our studio. Each graphic element is linked to a client project
  from throughout the year. The jobs referenced are hot stamped on the colophon. On
  the cover, a little detachable figure which symbolizes this very particular year
  : stand tall in front of our recent troubles. \n\nInspirations : Emmanuelle Fructus
  and the photos of the Endurance voyage by Frank Hurley."
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/0088.jpg
- /media/0092.jpg
- /media/0053.jpg
- /media/0066.jpg
- /media/0067.jpg
- /media/0068.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Notebook 2020
translationKey: carnet-2020
---
