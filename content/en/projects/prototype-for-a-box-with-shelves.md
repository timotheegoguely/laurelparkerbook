---
artist: ""
client: Royal Mansour
cover: /media/royal-mansour-coffret-etageres-02.jpg
date: 2014-06-03
description: |-
  Prototype for a box with shelves for an edition of three books for the hotel Royal Mansour in Marrakech.
  Design and fabrication : Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/royal-mansour-coffret-etageres-02.jpg
- /media/royal-mansour-coffret-etageres-01.jpg
secteurs:
- Luxe
series:
- Prototype
shop: false
title: Prototype for a box with shelves
translationKey: coffret-royal-mansour
---
