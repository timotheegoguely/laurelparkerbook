---
artist: Ivars Gravlejs
client: Maya Mikelsone
cover: /media/gravlejs-nelson-goodman_01.jpg
date: 2010-11-10
description: '"My Newspaper" by artist Ivars Gravlejs in found hidden inside a re-edition
  of "L’Art in theory and in action" by Nelson Goodman.  Conception and design by
  Maya Mikelsone for her Master’s project at the école du Magasin- CNAC de Grenoble,
  2010. Graphic design : Thomas Berthou .'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/gravlejs-nelson-goodman_01.jpg
- /media/gravlejs-nelson-goodman_02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: My Newspaper, by Ivars Gravlejs
translationKey: my-newspaper
---
