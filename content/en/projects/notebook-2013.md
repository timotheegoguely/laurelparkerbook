---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-carnet-2013-04.jpg
date: 2013-11-11
description: 'Our end of the year journal 2013 : screw post binding, fold out pages
  in yellow Ingres paper, handmade paper covers, and hot stamping in midnight blue.
  Series of 150 copies. Design and fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet-2013-02.jpg
- /media/lpb-carnet-2013-01.jpg
- /media/lpb-carnet-2013-03.jpg
- /media/lpb-carnet-2013-04.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: End-of-the-year Notebook 2013
translationKey: notebook-2013
---
