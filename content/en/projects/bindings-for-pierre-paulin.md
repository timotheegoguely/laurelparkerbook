---
artist: Pierre Paulin
client: Pierre Paulin
cover: /media/paulin-white-01.jpg
date: 2019-07-11
description: |-
  3 bindings for the artist Pierre Paulin. White leather, blind stamping, 2019
  Binding by Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/paulin-white-01.jpg
- /media/paulin-white-04.jpg
- /media/paulin-white-02.jpg
- /media/paulin-white-03.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Look
translationKey: look-pierre-paulin
---
