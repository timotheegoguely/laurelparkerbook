---
artist: Caroline & Cyril Desroche
client: Delpire & Co
cover: /media/delpire-losangeles-standards5.jpg
date: 2021-11-04
description: "Each box with selection of 15 beautiful photographs by Carline & Cyril
  Desroche taken from their book Los Angeles Standards (Poursuite Edition) now in
  its 2nd printing ! We are so happy to be a small part of this wonderful project.
  Thank you Caroline, Cyril, Grégoire Pujade-Lauraine, and Anna Planas at Delpire.\n\nLos
  Angeles Standards, limited edition box set, 15 copies + 3 AP.\n\nPublishing by Delpire&Co
  \ \nCollaboration with Poursuite Edition  \nGraphic design by Grégoire Pujade-Lauraine
  \ \nFabrication by Laurel Parker Book"
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/delpire-losangeles-standards.jpg
- /media/delpire-losangeles-standards3.jpg
- /media/delpire-losangeles-standards5.jpg
- /media/delpire-losangeles-standards2.jpg
- /media/delpire-losangeles-standards4.jpg
secteurs:
- Photo
series:
- Multiple
shop: false
title: Los Angeles Standards (limited edition box) by Caroline & Cyril Desroche
translationKey: los-angeles-standards
---
