---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-book-shoe-02.jpg
date: 2014-11-10
description: 'Our travelling "book shoe" : a presentation case with 30 different sewings
  for pamphets of 1, 2, or 3 sections. Inspired by an American design for conservation
  box structure (the Book Shoe), this object showcases our technical and and creative
  skills. We added a cover to the book shoe for protection and transportation. Design
  and fabrication : Laurel Parker Book'
designs:
- Semi-sur mesure
fonctions:
- Éditer
- Présenter
formes:
- Coffret
- Livre
images:
- /media/lpb-book-shoe-02.jpg
- /media/lpb-book-shoe-04.jpg
- /media/lpb-book-shoe-05.jpg
- /media/lpb-book-shoe-03.jpg
- /media/lpb-book-shoe-01.jpg
secteurs:
- Agence
- Luxe
series:
- Unique
shop: false
title: Book shoe
translationKey: book-shoe
---
