---
artist: Laurel Parker Book
client: Laurel Parker Book
cover: /media/lpb-reliure_en_valise-04.jpg
date: 2018-08-14
description: 'An hommage to _Boîte-en-valise_ by Marcel Duchamp. Seven bindings in
  a deluxe box. _Reliure-en-valise_ highlights what is important to us : the exigence
  of technique, the careful choice of materials and the historical and cultural aspect
  of each binding. White calfskin, black calfskin, linen cloth, Japanese cardboard,
  German papers, waxed linen thread, mother-of-pearl button and bone clasps. An invitation
  of curator Éric Sébastien Faure-Lagorce for a traveling exhibition at the Institut
  Français. Design et fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Éditer
- Présenter
formes:
- Coffret
- Livre
images:
- /media/lpb-reliure_en_valise-04.jpg
- /media/lpb-reliure_en_valise-01.jpg
- /media/lpb-reliure_en_valise-02.jpg
- /media/lpb-reliure_en_valise-03.jpg
secteurs:
- Art contemporain
- Luxe
series:
- Unique
shop: false
title: Reliure en valise
translationKey: reliure-en-valise
---
