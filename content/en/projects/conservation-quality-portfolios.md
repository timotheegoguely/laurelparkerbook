---
artist: ""
client: ENSBA-École de Beaux-Arts de Paris
cover: /media/lpb_portfolios-conservation_02.jpg
date: 2020-11-11
description: |-
  Conservation quality portfolios in Italian buckram cloth with archival paper pockets.
  Produced for the protection of miniature and small format books and documents, Masson Collection, Ecole des Beaux-Arts of Paris.
designs:
- Semi-sur mesure
fonctions:
- Archiver
formes:
- Portfolio
images:
- /media/lpb_portfolios-conservation_02.jpg
- /media/lpb_portfolios-conservation_01.jpg
- /media/lpb_portfolios-conservation_03.jpg
- /media/lpb_portfolios-conservation_04.jpg
secteurs:
- Archives
series:
- Multiple
shop: false
title: Conservation quality portfolios
translationKey: portfolio-de-conservation
---
