---
title: Baobab from Elaine Ling
artist: Elaine Ling
client: Elaine Ling
cover: /media/baobab_ling_02.jpg
date: 2015-11-11T00:00:00.000Z
description: >-
  Custom box with 11 original prints 53 × 68 cm from the artist Elaine Ling.
  Silkscreen printed image on book cloth. Title, text and colophon silkscreen
  printed on handmade paper from Ruscombe Mill. Limited edition of 5. Paris,
  2015.


  Platine-palladium process photographic prints by Salto Ulbeek. Silkscreen
  printing by Orbis Pictus Club. Design and fabrication : Laurel Parker Book.
designs:
  - Sur mesure
fonctions:
  - Éditer
  - Présenter
formes:
  - Coffret
images:
  - /media/baobab_ling_02.jpg
  - /media/baobab_ling_03.jpg
  - /media/baobab_ling_04.jpg
  - /media/baobab_ling_01.jpg
secteurs:
  - Art contemporain
  - Photo
series:
  - Multiple
shop: false
translationKey: baobab-elaine-ling
___mb_schema: /.mattrbld/schemas/projet.json
---

