---
artist: Daido Moriyama
client: Le Bal
cover: /media/moriyama-ikebukuro-01.jpg
date: 2013-11-11
description: "Edition produced during the Print Show at Le Bal, Paris, July 9, 2013.
  Visitors were invited to make their own edition from a selection of 50 of Moriyama’s
  images. Pages were printed, gathered, then hand bound during the event. Edition
  limited to 200 copies.\n\nConception by Pierre Hourquet & Sébastian Hau  \nDesign
  by Antoine Seiter  \nRiso prints by Après Midi Lab  \nBinding by Laurel Parker Book"
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/moriyama-ikebukuro-01.jpg
- /media/moriyama-ikebukuro-02.jpg
- /media/moriyama-ikebukuro-03.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Ikebukuro, by Daido Moriyama
translationKey: ikebukuro
---
