---
artist: ""
client: Calligrane
cover: /media/lpb-carnet2018-05.jpg
date: 2015-06-10
description: 'Collection of luxury stationery :  hand made journals with covers made
  from our very own hand-dyed papers.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet2018-05.jpg
- /media/lpb-papeterie_motif_02.jpg
- /media/lpb-papeterie_motif_03.jpg
- /media/lpb-papeterie_motif_01.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: 'Luxury stationery '
translationKey: papeterie-itajime
---
