---
artist: ""
client: F93
cover: /media/f93_carte_2019_01.jpg
date: 2019-07-11
description: 'Since 2017 we have been asked to design and produce the New Year’s card
  for the F93 in Montreuil (Paris suburb). Each of the 3 projects have focused on
  the paper support, transforming it and playing with the F93 logo (designed by Gaël
  Hugo @onemorestudio). In 2019, the logo was both laser cut and hand embroidered.
  Series of 300 copies. Design and fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Œuvre papier
images:
- /media/f93_carte_2019_01.jpg
- /media/f93_carte_2019_03.jpg
- /media/f93_logo_web.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: New Year’s card 2019 for F93
translationKey: F93-2019
---
