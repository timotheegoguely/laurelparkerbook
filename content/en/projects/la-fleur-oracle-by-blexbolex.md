---
artist: Blexbolex
client: Orbis Pictus Club
cover: /media/blexbolex_fleur_oracle_04.jpg
date: 2016-11-11
description: 'Artist book with 32 silkscreen prints, 200 signed copies. Coptic sewing
  in waxed linen thread. Publisher : Orbis Pictus Club. Printer : Frédérique Déjean.
  Fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/blexbolex_fleur_oracle_04.jpg
- /media/blexbolex_fleur_oracle_02.jpg
- /media/blexbolex_fleur_oracle_03.jpg
- /media/blexbolex_fleur_oracle_01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: La fleur Oracle, by Blexbolex
translationKey: fleur-oracle
---
