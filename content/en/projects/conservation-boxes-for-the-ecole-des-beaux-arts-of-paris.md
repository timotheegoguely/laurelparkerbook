---
artist: ""
client: ENSBA-École de Beaux-Arts de Paris
cover: /media/ensba-boite-conservation-04.jpg
date: 2020-11-11
description: |-
  We collaborate with the conservator in charge of the rare book collection of ENSBA. These conservation boxes are of archival quality and are of very solid construction to hold heavy books, often _incunables_ with wooden covers and metallic elements. Cloth, museum board, pH neutral paper, plastazote.

  Design et fabrication : Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Portfolio
- Boîte d’éditeur
- Coffret
images:
- /media/ensba-boite-conservation-04.jpg
- /media/ensba-boite-conservation-01.jpg
- /media/ensba-boite-conservation-02.jpg
- /media/ensba-boite-conservation-03.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: Conservation boxes for the Ecole des Beaux-Arts of Paris
translationKey: boites-conservation-ensba
---
