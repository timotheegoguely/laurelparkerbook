---
artist: Nico Therin
client: ""
cover: /media/therin_book_02.jpg
date: 2019-06-04
description: Portfolio in buckram cloth, hot stamping. Hidden screw-post binding.
  Slipcase in brown cloth. For Nico Therin, a Los Angeles based French photographer.
designs:
- Semi-sur mesure
fonctions:
- Présenter
formes:
- Portoflio
images:
- /media/therin_book_02.jpg
- /media/therin_book_01.jpg
- /media/therin_book_03.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Portfolio book for Nico Therin
translationKey: pressbook-nico-therin
---
