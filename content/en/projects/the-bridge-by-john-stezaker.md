---
title: The Bridge
artist: John Stezaker
client: '&: Christophe Daviet-Thery'
cover: /media/stezaker-the-bridge-04.jpg
date: 2010-11-10
description: |-
  The Bridge, by John Stezaker. Published by Christophe Daviet-Thery, Paris.  Edition of 33 copies numbered and signed, and 7 artist’s proofs. Book with collages by the artist. Wrapper design by Jérôme Saint- Loubert Bié. Conservation slipcase by Laurel Parker.

  "The recent 'Bridge' works evolved from a series of topographical collages that began in the late '80's, involving combinations and inversions of the upper parts of cityscapes across a diagonal divide to create bridge-like spaces. These imaginary and gravity-defying spaces of ambiguity negotiate a void, above and below.

  The source images are tourist books of Prague and its castle from the 1940's to 50's, collected originally by Stezaker because of his interest in the world of Kafka's castle. The uncanny dream-like spaces evoked in these pictorial bridges have become more precarious in the most recent collages. They feel more imminently endangered – the atmosphere is more apocalyptic. They share with his better-known film portrait collages (also on view) a dark fascination with the fragility of the photographic illusion. — Friedrich Petzel Gallery , 2009
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/stezaker-the-bridge-04.jpg
- /media/stezaker-the-bridge-01.jpg
- /media/stezaker-the-bridge-02.jpg
- /media/stezaker-the-bridge-03.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
translationKey: the-bridge
---
