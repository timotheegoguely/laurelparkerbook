---
artist: Yann Sérandour
client: Laurel Parker Edition
cover: /media/serandour_bleu_08_w.jpg
date: 2022-09-30T00:00:00.000Z
description: >-
  This leporello reproduces 66 covers of the same book by Jean-Paul Sartre,
  Esquisse d’une théorie des émotions, published by the Editions Hermann, in the
  form of a long, colored frieze in accordion binding. The shades of the
  reproduced covers have been arranged in the chronological order of the various
  reprints of the book between 1960 and 1995. Curiously turning from blue to
  green, the sequence reveals the progressive degradation of Adrian Frutiger’s
  original design and retraces the reception of Sartre’s book over time by means
  of its signs of wear. Leporello, 66 pages, 25 × 19 cm (closed), 1254 cm open,
  graphic design Marine Bigourie, digital printing Média Graphic, Rennes • 15
  copies + 4 HC • Laurel Parker Edition, 2022 • 1254,00 € VAT included
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/serandour_bleu_03_w.jpg
  - /media/serandour_bleu_02_w.jpg
  - /media/serandour_bleu_08_w.jpg
  - /media/serandour_bleu_06_w.jpg
  - /media/serandour_bleu_02_atgregory_copitet.jpg
  - /media/serandour_bleu_11_w.jpg
  - /media/serandour_bleu_01_atgregory_copitet.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: Et puis le bleu tourna au vert
translationKey: yann-emotions
___mb_schema: /.mattrbld/schemas/projet.json
---

