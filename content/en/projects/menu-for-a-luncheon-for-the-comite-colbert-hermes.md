---
artist: ""
client: Hermès
cover: /media/hermes-colbert-menu_03.jpg
date: 2017-11-11
description: |-
  Menu for a special luncheon for the Comité Colbert, hosted by Hermès, 2017.
  Japanese paper, folded and dyed by hand, stitching in gold thread.
  This object uses the sense of touch to appreciate the appearance and texture of the paper, but also visually through the volume created by the gold tinting on the folds.
  An elegant object for a committee that works to promote of French art de vivre at the international level.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/hermes-colbert-menu_03.jpg
- /media/hermes-colbert-menu_02.jpg
- /media/hermes-colbert-menu_01.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Menu for a luncheon for the Comité Colbert, Hermès
translationKey: menu-dejeuner-comite-colbert
---
