---
artist: Geert van Beijeren & Adriaan van Ravesteijn
client: ""
cover: ""
date: 2020-11-10
description: |-
  Between September 1968 and November 1989 Geert van Beijeren & Adriaan van Ravesteijn published 156 bulletins of the influential art magazine Art & Project. The first magazines were merely announcements of upcoming exhibitions in the Art & Project gallery. Later editions more and more took the form of art objects.

  Custom-made plastic sleeves for the conservation and presentation of bulletins : Laurel Parker Book for Christophe Daviet-Thery.
designs:
- Sur mesure
draft: true
fonctions:
- Archiver
- Présenter
formes:
- Boîte éditeur
images: []
secteurs:
- Art contemporain
series: []
title: Art & Project galery
---
