---
artist: Barbara Bénédicte Penn
client: Barbara Bénédicte Penn
cover: /media/penn_boite01.jpg
date: 2021-06-15
description: "  \nOne-of-a kind box covered in Takeo paper, to hold digital prints
  on pages of an old herbarium. With a booklet by the artist, Barbara Bénédicte Penn.
  \ \n  \nHot stamping, design and fabrication Laurel Parker Book.  \n  \nPrints by
  Ryan Boatwright at Atelier Boba and Studio Bordas, restauration by Elsa Gravé."
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/penn_boite01.jpg
- /media/penn_boite02.jpg
- /media/penn_boite03.jpg
- /media/penn_boite04.jpg
- /media/penn_boite05.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Presentation box and booklet for Barbara Bénédicte Penn
translationKey: barbara-benedicte-penn
---
