---
artist: John Baldessari and William Carlos Williams
client: Éditions Xavier Barral
cover: /media/baldessari-poems_06.jpg
date: 2013-11-10
description: |-
  Poems, by John Baldessari and William Carlos Williams. Xavier Barral for the Éditions du Solstice, Paris 2013. Format 37 × 47 cm. Lithographic prints, Imprimerie Idem.
  Chemise with rounded spine and yapp edges, slipcase with meticulous finishing. Hot stamping. Series of 145.
  Box design : Laurel Parker and Xavier Barral.
  Box fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
- Coffret
images:
- /media/baldessari-poems_06.jpg
- /media/baldessari-poems_05.jpg
- /media/baldessari-poems_02.jpg
- /media/baldessari-poems_04.jpg
- /media/baldessari-poems_03.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Poems, by John Baldessari and William Carlos Williams
translationKey: poems-john-baldessari-willima-carlos-williams
---
