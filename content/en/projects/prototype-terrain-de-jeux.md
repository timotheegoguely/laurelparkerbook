---
artist: 'Jérôme Barbe, Franck Léonard, Fred Maillard'
client: Laurel Parker Edition
cover: /media/barbe-leonard-maillard-terrain-de-jeux_02.jpg
date: 2018-11-10T00:00:00.000Z
description: >-
  This project is a space of creation, reflection, and exchanges, using images
  from the exhibition _Prototype_ by Terrain de Jeux which took place in
  Montreuil in March 2018. The game of this book, in the form of an unbound
  section with a false page imposition, is the impossibility of putting the
  double-page spreads in order — a metaphor for the difficulty of showing
  diverse works in a group exhibition. Format 27 × 20,5 cm, 24 pages • Offset
  printing, conservation portfolio with hot stamping. • 100 ex • Laurel Parker
  Edition, 2018 • 25 € TTC
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
  - Chemise
images:
  - /media/barbe-leonard-maillard-terrain-de-jeux_02.jpg
  - /media/barbe-leonard-maillard-terrain-de-jeux_03.jpg
  - /media/barbe-leonard-maillard-terrain-de-jeux_01.jpg
  - /media/barbe-leonard-maillard-terrain-de-jeux_04.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: '"Prototype", by Terrain de Jeux'
translationKey: terrain-de-jeux
___mb_schema: /.mattrbld/schemas/projet.json
---

