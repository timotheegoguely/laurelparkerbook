---
title: Collection, by Emmanuelle Fructus
artist: Emmanuelle Fructus
client: Un Livre Une Image
cover: /media/fructus_unlivreuneimage-02.jpg
date: 2020-11-11
description: |-
  Screw post photo album containing 80 vintage recipes postcards by Emilie Bernard.
  Cover in embossed card stock, custom polyester viewing sleeves.
  One of a kind in a series of postcard archives.
designs:
- Sur mesure
fonctions:
- Archiver
- Présenter
formes:
- Portoflio
- Livre
images:
- /media/fructus_unlivreuneimage-01.jpg
- /media/fructus_unlivreuneimage-02.jpg
- /media/fructus_unlivreuneimage-03.jpg
secteurs:
- Archives
- Photo
- Art contemporain
series:
- Unique
shop: false
translationKey: collection-un-livre-une-image
---
