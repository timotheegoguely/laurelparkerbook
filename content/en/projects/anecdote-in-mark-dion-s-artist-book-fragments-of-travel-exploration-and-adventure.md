---
artist: Mark Dion
client: ""
cover: /media/dion_fragments-of-artistbook_03.jpg
date: 2017-11-10
description: |-
  Anecdote in Mark Dion’s artist book : Fragments of Travel Exploration and Adventure.
  Extract from the magazine The Shelf n°5, Laurel Parker, Binding artists' books, 2017.

  The original drawings were torn out from spiral-bound sketchbooks. It was thus decided to reproduce these features on the facsimiles.
designs:
- Sur mesure
draft: true
fonctions:
- Éditer
formes:
- Livre
images:
- /media/dion_fragments-of-artistbook_03.jpg
- /media/dion_fragments-of-artistbook_02.jpg
- /media/dion_fragments-of-artistbook_01.jpg
secteurs:
- Art contemporain
series:
- Multiple
title: 'Anecdote in Mark Dion’s artist book : Fragments of Travel Exploration and
  Adventure'
translationKey: anecdote-fragments-of-travel-exmploration
---
