---
artist: Claire-Lise Havet
client: Claire-Lise Havet
cover: /media/havet-pressbook_01.jpg
date: 2014-05-04
description: Portfolio book in grey bookcloth exterior and white cloth interior.
  Hot stamping. Two pockets inside the back cover for business cards and postcards.
designs:
- Semi-sur mesure
fonctions:
- Présenter
formes:
- Portoflio
images:
- /media/havet-pressbook_01.jpg
- /media/havet-pressbook_03.jpg
secteurs:
- Photo
series:
- Multiple
shop: false
title: Portfolio book in bookcloth
translationKey: pressbook-havet
---
