---
artist: Dove Allouche
client: Dove Allouche
cover: /media/allouche_tableaup_serie_08w.jpg
date: 2022-10-28
description: "Tableau périodique, 2022, Dove Allouche, édition de 3 / 1ea.  \nLa boite
  regroupe une image représentant un spectre de raies d'émissions pour chaque élément
  chimique composant le tableau périodique.  \nCoffrets en toile de couleur chartreuse,
  qualité conservation."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/allouche_tableaup_serie_08w.jpg
- /media/allouche_tableaup_serie_07w.jpg
- /media/allouche_tableaup_serie_02w.jpg
- /media/allouche_tableaup_serie_03w.jpg
- /media/allouche_tableaup_serie_01w.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Tableau périodique
translationKey: tableau-periodique-serie
---
