---
artist: Jean Larivière
client: Louis Vuitton – Studio Graphique
cover: /media/lariviere-coffret-04.jpg
date: 2011-05-02
description: "Portfolio Jean Larivière, with 20 large format photographs. Éditions
  Louis Vuitton. 30 copies + 5 A.P. Drawer-style box with a drawing by the artist
  hot-stamped on the top of the box. Archival quality box. \n\nDesign Graphique :
  Studio Graphique, Louis Vuitton    \nIris prints : Jules Maeght, atelier Arte.   \nBox
  design and fabrication : Laurel Parker Book."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Portfolio
- Coffret
images:
- /media/lariviere-coffret-04.jpg
- /media/lariviere-coffret-02.jpg
- /media/lariviere-coffret-03.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Portfolio Jean Larivière
translationKey: portfolio-jean-lariviere
---
