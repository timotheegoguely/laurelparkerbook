---
artist: Ying Lei
client: Ying Lei
cover: /media/lei-livre-diplome-01.jpg
date: 2015-11-11
description: This beautiful book was the diploma project of Ying Lei, Gobelins - École
  de l'image – in photography. Digital prints, hand-sewn, cloth case binding, insertion
  of a photo in the cover.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lei-livre-diplome-01.jpg
- /media/lei-livre-diplome-02.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: Diploma project for Ying Lei
translationKey: livre-ying-lei
---
