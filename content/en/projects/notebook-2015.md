---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-carnet_reliure_2014.jpg
date: 2014-06-03
description: 'Our end-of-the-year journal 2015: hand-dyed itajime paper, non-adhesive
  binding, embroidery thread, laser cutting, hot stamping. Series of 150 copies.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet_reliure_2014.jpg
- /media/lpb-carnet_reliure_2014-2.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: End-of-the-year Notebook 2014
translationKey: notebook-2014
---
