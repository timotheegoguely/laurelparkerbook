---
artist: Yona Friedman
client: mfc Michèle Didier
cover: /media/friedman2-1024x611.jpg
date: 2013-12-31T00:00:00.000Z
description: >-
  Artist book. Offset printing, laser cutting, japanese binding. Limited edition
  of 75 copies + 25 A.P, 2014. The drawings by the architect and designer Yona
  Friedman can be taken out of the book thanks to the contoured perforated laser
  cutting. Publisher : mfc Michèle Didier. Fabrication : Laurel Parker Book.
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/friedman2-1024x611.jpg
  - /media/friedman3web.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: false
title: '1001 nuits + 1 jour, by Yona Friedman'
translationKey: 1001-nuits-1-jour
___mb_schema: /.mattrbld/schemas/projet.json
---
Impression offset, découpes laser, reliure à la japonaise. 24 × 32,2 cm. Édition limitée à 75 exemplaires + 25 E.A.
