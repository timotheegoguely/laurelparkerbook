---
artist: Marie Benattar
client: Marie Benattar
cover: /media/benattar-pressbook_01.jpg
date: 2014-06-10
description: Portfolio book in leather with visible screw posts, the client’s signature
  has been reproduced in blind stamping. With ink jet prints on fine paper, and a
  pocket for business cards.
designs:
- Semi-sur mesure
fonctions:
- Présenter
formes:
- Portoflio
images:
- /media/benattar-pressbook_01.jpg
- /media/benattar-pressbook_04.jpg
- /media/benattar-pressbook_02.jpg
- /media/benattar-pressbook_03.jpg
secteurs:
- Photo
series:
- Unique
shop: false
title: Portfolio for Marie Benattar
translationKey: ""
---
