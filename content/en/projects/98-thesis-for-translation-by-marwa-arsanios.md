---
artist: Marwa Arsanios
client: Galerie Mor-Charpentier
cover: /media/arsanios-98-thesis-for-translation-03.jpg
date: 2014-11-10
description: 'One-of-a-kind binding. Each page of the original binding has been attached
  on a paper tabs, then bound by screw post, cover in pH neutral cloth. This book
  was a part of an exhibition and was originally bound simply with rings. We were
  contacted by the gallery to find a solution to conserve the work, while preserving
  the easy opening of the book. For the Galerie Mor-Charpentier. Design and fabrication
  : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Présenter
- Archiver
formes:
- Coffret
- Livre
images:
- /media/arsanios-98-thesis-for-translation-03.jpg
- /media/arsanios-98-thesis-for-translation-01.jpg
- /media/arsanios-98-thesis-for-translation-04.jpg
- /media/arsanios-98-thesis-for-translation-02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Unique
shop: false
title: 98 Thesis for Translation, by Marwa Arsanios
translationKey: 98-thesis-for-translation
---
