---
artist: Yann Sérandour
client: '&: Christophe Daviet-Thery'
cover: /media/serandour-sextodecimo-03.jpg
date: 2013-11-10
description: |
  Artist book by Yann Sérandour. An invitation by Christophe Daviet-Thery, Paris and the Cneai =, Chatou. Open edition.
  The basis for the work is a poster by the Italian publisher Corraini, featuring Bruno Munari’s famous photographic series "Seeking comfort in an uncomfortable chair", which was initially published in the art, design and architecture magazine Domus in 1944. By folding this poster, Yann Sérandour produced a 32-page printer’s signature, alternating printed images and empty spaces created by the back side of the poster.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
- Œuvre papier
images:
- /media/serandour-sextodecimo-03.jpg
- /media/serandour-sextodecimo-02.jpg
- /media/serandour-sextodecimo-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Sextodecimo
translationKey: sextodecimo
---
