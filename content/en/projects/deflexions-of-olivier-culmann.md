---
artist: Olivier Culmann
client: Picto Foundation
cover: /media/culmann-deflexions-01.jpg
date: 2017-11-11
description: "_DéfleXions, 1992 - 2008_, by Olivier Culmann (from the Tandance Floue
  collective), artist's edition produced with the support of Picto Foundation, sponsor
  of the Niépce Prize, as part of the prize awarded to Olivier Culmann by the Gens
  d'Images association. _DéfleXions, 1992 - 2008_ presents several cross-roads; attempts
  or temptations that have marked the photographer's journey since 1992; various paths
  he has ventured on. Some of them continue, others left dormant or abandoned forever
  ... The box contains seven series or sketches which have punctuated the work of
  Olivier Culmann. They show us some of the secrets of a photographer whose work has
  remained from the outset imprinted with research and questioning. \n\n_DéfleXions,
  1992 - 2008_ \"is an artist's object imagined by Olivier Culmann and Laurel Parker,
  with the support of the Picto Foundation. 12 copies, signed and numbered.\n\nPhotographs
  and text : Olivier Culmann  \n Printing : Elisabeth Hering, Vianney Drouin, Patrice
  Baron, Payram, Guillaume Weihmann, Pierric Paulian et Maxime Le Gall from Picto
  Laboratories.   \nBox fabrication : Laurel Parker Book."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Chemise
- Coffret
images:
- /media/culmann-deflexions-01.jpg
- /media/culmann-deflexions-02.jpg
- /media/culmann-deflexions-03.jpg
- /media/culmann-deflexions-04.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: DéfleXions, 1992-2008, by Olivier Culmann
translationKey: delfexions
---
