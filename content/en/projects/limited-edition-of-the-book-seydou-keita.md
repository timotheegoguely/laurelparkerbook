---
artist: Seydou Keïta
client: Éditions Louis Vuitton
cover: /media/keita-coffret-vuitton_02.jpg
date: 2012-09-16
description: "Limited edition of the book _Seydou Keïta, Photographs, Bamako, Mali,
  1949-1970_. Deluxe boxed set containing the book, published by Steidldangin, and
  a numbered and signed silver gelatin print produced by Box. Éditions Louis Vuitton,
  Paris, 2012, 50 copies + 10 A.P   \nDesign : Studio Graphique, Louis Vuitton.   \nFabrication
  : Laurel Parker Book."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/keita-coffret-vuitton_02.jpg
- /media/keita-coffret-vuitton_03.jpg
- /media/keita-coffret-vuitton_01.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Seydou Keïta, Photographs, Bamako, Mali 1949-1970
translationKey: seydou-keita
---
