---
title: Window box for Agnes Geoffray
artist: Agnes Geoffray
client: Agnes Geoffray
cover: /media/geoffray-boite-vitrine-01.jpg
date: 2021-04-15
description: Window-box presenting a photographic work on silk by the artist Agnes
  Geoffray. Two-pieces box in archival quality cloth and paper, and custom-fit plexiglas.
  Modeled after a specimen box.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/geoffray-boite-vitrine-01.jpg
- /media/geoffray-boite-vitrine-02.jpg
- /media/geoffray-boite-vitrine-03.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
translationKey: vitrine-geoffray
---
