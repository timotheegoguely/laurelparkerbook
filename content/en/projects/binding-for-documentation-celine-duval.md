---
artist: documentation céline duval
client: documentation céline duval
cover: /media/duval-la-revue-en-4-images_02.jpg
date: 2015-11-11
description: |-
  Binding of the complete collection of 60 issues of _La revue en 4 images_ (The journal of 4 images) of documentation céline duval,. [Each issue is attatched to a tab](https://www.doc-cd.net/publications/books/revue-en-4-images-reliee/ "documentation céline duval"), which allow the revue to be opened. Exposed stitching on the spine. For the archive of private collectors who are interested in preserving the whole collection in one volume. Design and fabrication : Laurel Parker Book.

  In the photos :
  Issue n°21 : Mathilde
designs:
- Sur mesure
fonctions:
- Archiver
- Éditer
formes:
- Livre
images:
- /media/duval-la-revue-en-4-images_02.jpg
- /media/duval-la-revue-en-4-images_07.jpg
- /media/duval-la-revue-en-4-images_06.jpg
- /media/duval-la-revue-en-4-images_01.jpg
- /media/duval-la-revue-en-4-images_03.jpg
- /media/duval-la-revue-en-4-images_05.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: La revue en 4 images, by documentation céline duval
translationKey: la-revue
---
