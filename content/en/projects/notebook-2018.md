---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-carnet2018-03.jpg
date: 2017-06-06
description: 'Our end-of-the-year notebook from 2017 : A photo album with 9 window
  mats holding images of our projects from throughout the year. Die cutting and hot
  stamping by our friends at Créanog. Card stock by Takeo and conservation papers.
  Gift wrapping in hand-folded itajime paper. Limited edition of 180 copies for our
  clients and collaborators. Design and fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Portfolio
- Livre
images:
- /media/lpb-carnet2018-03.jpg
- /media/lpb-carnet2018-01.jpg
- /media/lpb-carnet2018-02.jpg
- /media/lpb-carnet2018-04.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: End-of-the-year Notebook 2017
translationKey: notebook-2017
---
