---
actions:
- Relier
artist: Mark Dion
client: '&: Christophe Daviet-Thery'
cover: /media/livre_artiste_mark_dion_3.jpg
date: 2007-01-01
description: "“In this artist’s book, Mark Dion assumes the guise of an 18th or 19th
  century scientist who explores an unknown land (he kindly includes 2 maps of this
  terra incognita) and exhaustively documents the native flora and fauna. The book
  has been exquisitely constructed to convincingly carry out this fiction: pieces
  of paper are die cut and assembled and bound into the book to suggest an impromptu
  but thorough record of the explore’s discoveries and observations.The Iris prints
  appear to be convincing facsimile of Dion’s faux-scientific maquette.” \n\nArtist
  book, edition of 36 + 9 AP. Co-production of XN Éditions, &: Christophe Daviet-Thery,
  and Fabienne Leclerc In Situ gallery. Iris prints by Arteprint, Bruxelles. Case
  binding, quarterbound in red buffalo leather and Zerkall paper. Cloth pocket in
  back cover to hold extra prints and two mini-books bound in leather. Guiled cloth
  chemise."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/livre_artiste_mark_dion_3.jpg
- /media/livre-artistes-mark-dion_02.jpg
- /media/dion_fragments-of-artistbook_02.jpg
secteurs:
- Art contemporain
series:
- Multiple
title: Fragments of Travel Exploration and Adventure
---
Tirages numériques par Arteprint, Bruxelles. Reliure d’emboîtage, demi à coins, en buffle rouge et papier Zerkall. Pochette en toile dans le plat arrière avec quelques tirages et deux mini-livres reliés en buffle rouge. Chemise en toile et papier, dorée.