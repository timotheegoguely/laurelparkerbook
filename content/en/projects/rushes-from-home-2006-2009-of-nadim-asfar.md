---
artist: Nadim Asfar
client: Nadim Asfar
cover: /media/asfar-rushes-from-home-02.jpg
date: 2021-09-29
description: |-
  Photographs print on translucent paper. Texts are typed on typewriter on Japanese paper. Photographs explore the artist home neighborhood in Beyrouth. The screw post binding and conservation wrapper by Laurel Parker Book.

  This project is exhibited at the Tanit Gallery (Munich) on 2021.

  Reproductions : Nadim Asfar et Ava du Parc
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/asfar-rushes-from-home-04.jpg
- /media/asfar-rushes-from-home-01.jpg
- /media/asfar-rushes-from-home-05.jpg
- /media/asfar-rushes-from-home-03.jpg
- /media/asfar-rushes-from-home-02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Rushes from home 2006-2009, by Nadim Asfar
translationKey: Rushes-from-home
---
