---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-papeterie-carnetvoeux2009.jpg
date: 2009-06-09
description: 'Our end of the year journal 2009 : coptic binding, offset prints from
  the project "Inside the White Cube, édition fantôme" by Yann Sérandour. Series of
  20 copies. Design and fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-papeterie-carnetvoeux2009.jpg
- /media/lpb-papeterie-carnetvoeux2009-02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: End-of-the-year Notebook 2009
translationKey: notebook-2009
---
