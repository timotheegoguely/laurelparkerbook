---
artist: Yann Sérandour
client: Laurel Parker Edition
cover: /media/serandour_ftf_07.jpg
date: 2018-06-05T00:00:00.000Z
description: >-
  A classic conservation box - known in French as a « boîte à chasses » ( a term
  with a double meaning of « a box with excess board » and « a hunting box » ) -
  is covered in camouflage cloth and holds a group of eight real-sized
  reproductions (A3) of mounted herbarium specimens of common reeds collected
  around the world. Each mount is printed on Japanese paper that has been folded
  to fit into the B4-sized edition by using the broken lines of the plant.
  Format box 38 × 27 cm 8 • fine art prints on Japanese paper, camouflage cloth,
  archival boards • 15 exemplaires + 3 A.P • Laurel Parker Edition, 2018 • 2300
  € VAT included
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
  - Coffret
images:
  - /media/serandour_ftf_07.jpg
  - /media/serandour_ftf_01.jpg
  - /media/serandour_ftf_08.jpg
  - /media/serandour_ftf_05.jpg
  - /media/serandour_ftf_06.jpg
  - /media/serandour_ftf_09.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: Folded to fit by Yann Sérandour
translationKey: folded-to-fit
___mb_schema: /.mattrbld/schemas/projet.json
---

