---
title: St. Tropez
artist: André
client: Louis Vuitton – Studio Graphique
cover: /media/andre-st-tropez-01.jpg
date: 2010-10-31
description: "Portfolio box with 10 drawings by the artist André. Éditions Louis Vuitton.
  Prints by Imprimerie Arte - Maeght. This portfolio was produced in conjuction with
  the re-design of the Louis Vuitton St. Tropez shop.\nThis drawer-style box has an
  embroidered patch of the artist’s design set in a sunken well on the top of the
  box. Archival quality box, Italian cloth.\nGraphique design : Studio Graphique,
  Louis Vuitton \nBox design : Laurel Parker"
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/andre-st-tropez-01.jpg
- /media/andre-st-tropez-03.jpg
secteurs:
- Luxe
- Art contemporain
series:
- Multiple
translationKey: st-tropez
---
