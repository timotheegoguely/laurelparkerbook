---
artist: ""
client: Éditions Xavier Barral
cover: /media/exb-de-gonet-04.jpg
date: 2013-05-12
description: 'Binding in the style of Jean de Gonet, with exposed sewing on leather
  thongs laced into wooden covers. Spine "rapporté" in black calfskin. Documents of
  various formats (drawings, letters, photographs...) glued onto sewn tabs. Made following
  research on the bindings of de Gonet at the Kandinsky Library and at the Historical
  Library of the City of Paris. Client : Éditions Xavier Barral Book covers in sycamore
  wood : Maonia, Paris Design and fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/exb-de-gonet-04.jpg
- /media/exb-de-gonet-02.jpg
- /media/exb-de-gonet-01.jpg
- /media/exb-de-gonet-03.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: Binding for François Pinault
translationKey: reliure-francois-pinault
---
