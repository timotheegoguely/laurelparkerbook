---
artist: Laurel Parker et Paul Chamard
client: Laurel Parker Edition
cover: /media/lpb-inside-outside-05-1.jpg
date: 2020-11-11T00:00:00.000Z
description: >-
  Inside Outside #2, an installation created following our residency at the
  Villa Kujoyama in Kyoto, Japan. At the exhibition ! VIVA VILLA ! les vies
  minuscules, Octobre 24, 2020 - January 10, 2021 at the Collection Lambert,
  Avignon, France.


  A reconstruction of a traditional Japanese space, this piece is inspired by
  both by the dioramas and reconstructions in museums, as well as the sets of
  theater and film.


  Held together by fine wooden frames, each element is made of Japanese paper
  (washi), transformed using traditional techniques.


  Parts of our experience in Japan can be found in these fine and fragile
  objects: packaging, partitions, clothing, patterns inspired by the exterior...
designs:
  - Sur mesure
fonctions:
  - Exposer
formes:
  - Installation
images:
  - /media/lpb-inside-outside-05-1.jpg
  - /media/lpb-inside-outside-02.jpg
  - /media/lpb-inside-outside-01.jpg
  - /media/lpb-inside-outside-04.jpg
  - /media/lpb-inside-outside-03.jpg
  - /media/lpb-inside-outside-06.jpg
  - /media/lpb-inside-outside-07.jpg
  - /media/lpb-inside-outside-02-1.jpg
secteurs:
  - Art contemporain
series:
  - Unique
shop: null
title: 'Inside - Outside #2'
translationKey: inside-outside-2
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

