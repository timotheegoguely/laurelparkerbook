---
artist: Denis Boulze
client: ""
cover: /media/boulze_book_01.jpg
date: 2019-11-11
description: Portfolio book and slipcase in linen cloth with black hot stamping.
designs:
- Semi-sur mesure
fonctions:
- Présenter
formes:
- Portoflio
images:
- /media/boulze_book_03.jpg
- /media/boulze_book_02.jpg
- /media/boulze_book_04.jpg
- /media/boulze_book_01.jpg
secteurs:
- Photo
series:
- Unique
shop: false
title: Portfolio for Denis Boulze
translationKey: pressbook-denis-boulze
---
