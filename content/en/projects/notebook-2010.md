---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-carnet_2010.jpg
date: 2010-11-11
description: 'Our end of the year journal 2010 : conservation binding, fold-out cover
  with a drawing by the artist Franck Léonard reproduced in hot stamping. Series of
  90 copies. Design and fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet_2010_02.jpg
- /media/lpb-carnet_2010.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: End-of-the-year Notebook 2010
translationKey: notebook-2010
---
