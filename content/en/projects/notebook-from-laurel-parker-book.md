---
artist: Laurel Parker Book
client: ""
cover: /media/lpb-carnet-2013-01.jpg
date: 2020-11-11
description: 'Since we started our studio in 2008, we have created each year a journal
  for our clients - an end of the year gift. The idea is simple : try to mix design
  elements from different projects that we worked on throughout the year. This game
  pushes us to create interesting and unexpected objects.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet-2013-01.jpg
- /media/lpb-carnet-2016-02.jpg
- /media/lpb-carnet2018-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: End-of-the-year journal, Laurel Parker Book
translationKey: notebook-laurel-parker-book
---
