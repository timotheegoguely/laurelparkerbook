---
artist: Fabien Giraud
client: Rosascape
cover: /media/giraud-lelamort-01.jpg
date: 2011-11-10
description: A book inside a book. Artist book made up of fifteen fragments which
  are presented in the form of books. Each book is cut in the middle by a second book,
  _Metaxu_,  containing the text of a conversation between Fabien Giraud and Vincent
  Normand. Text of _Metaxu_ printed in letterpress from a composition in linotype
  at the Moulin de Got, colored pages cut from a backdrop used in a film. _Le La Mort_
  is catalog from an exhibition with photographs printed digitally. Binding by Laurel
  Parker Book.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/giraud-lelamort-01.jpg
- /media/giraud-lelamort-02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Le La Mort, by Fabien Giraud
translationKey: le-la-mort
---
