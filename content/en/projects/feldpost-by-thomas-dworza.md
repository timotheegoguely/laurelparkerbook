---
artist: Thomas Dworzak
client: Magnum Photos
cover: /media/magnum-feldpost-02.jpg
date: 2019-09-15
description: 'Feldpost was the name given to the German military postal system used
  before, and during, World War I. Thomas Dworzak’s project of the same name has seen
  the Magnum photographer, over the last seven years, create an annotated image for
  each day of the Great War – over 1,500 of them in total. These ‘postcards’, with
  their extensive explanatory notes written by Chris Bird, represent a vision of the
  war which conveys it’s truly global nature: a complex picture of the conflict that
  delves into its far-reaching impact, myriad complexities and peculiarities, as well
  as its present day reverberations.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/magnum-feldpost-02.jpg
- /media/magnum_feldpost_01.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Feldpost, by Thomas Dworzak
translationKey: feldpost
---
