---
artist: Lola Reboud
client: Lola Reboud
cover: /media/reboud-climatesii-04.jpg
date: 2018-11-11
description: |-
  Deluxe box for a limited edition of 10 copies : in shiny blue Fedrigoni card stock, to hold the book and an original signed print real gold leaf. Poursuite éditions, 2018.
  Design and fabrication : Laurel Parker Book.
designs:
- Semi-sur mesure
fonctions:
- Éditer
formes:
- Chemise
images:
- /media/reboud-climatesii-04.jpg
- /media/reboud-climatesii-02.jpg
- /media/reboud-climatesii-03.jpg
- /media/reboud-climatesii-01.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Les Climats II (Japon), by Lola Reboud
translationKey: les-climats
---
