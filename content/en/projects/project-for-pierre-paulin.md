---
artist: Pierre Paulin
client: Pierre Paulin
cover: /media/paulin-binding-03.jpg
date: 2017-11-11
description: |-
  Three artists books by Pierre Paulin. Purple calf leather, blind stamping. In conjunction with his exhibition _Boom boom, run run_ at the FRAC Île-de-France, curator Xavier Franceschi.

  Fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/paulin-binding-03.jpg
- /media/paulin-binding-02.jpg
- /media/paulin-binding-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: 1981, 1998,  2008, by Pierre Paulin
translationKey: 1981-pierre-paulin
---
