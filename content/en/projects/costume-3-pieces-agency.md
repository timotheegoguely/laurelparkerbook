---
artist: ""
client: Costume 3 Pièces
cover: /media/costumes3pieces-les-habilleurs-02.jpg
date: 2013-06-04
description: |-
  For the creative studio of the agency Costume 3 Pièces :

  A series of look books. Digital prints and coptic binding in turquoise blue linen thread. Uncovered cardboard and hot stamping. 50 copies.

  A series of press books. Digital prints. Blue cloth and fabric linings. Inside pocket, as in a three piece suite (Costume 3 pièces). Hot stamping in white. 20 books.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
- Livre
images:
- /media/costumes3pieces-les-habilleurs-02.jpg
- /media/costumes3pieces-les-habilleurs-03.jpg
- /media/costumes3pieces-les-habilleurs-05.jpg
- /media/costumes3pieces-les-habilleurs-04.jpg
- /media/costumes3pieces-portfolio-01.jpg
secteurs:
- Agence
series:
- Multiple
shop: false
title: Portfolio books, Costume 3 pièces agency
translationKey: costume-3-pieces
---
