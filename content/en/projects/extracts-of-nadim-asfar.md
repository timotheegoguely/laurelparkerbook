---
artist: Nadim Asfar
client: Nadim Asfar
cover: /media/asfar-livre-extraits-03.jpg
date: 2021-09-29
description: |-
  This photographic edition tells artist home neighborhood in Beyrouth. Prints are mounted on Japanese paper tabs. The sewing is visible, and the covers are « rapportés ». The book is protected with a conservation slipcase.

  This project has exhibited at the Tanit Gallery, Munich on 2021.

  Reproductions : Nadim Asfar & Ava du Parc
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/asfar-livre-extraits-03.jpg
- /media/asfar-livre-extraits-04.jpg
- /media/asfar-livre-extraits-02.jpg
- /media/asfar-livre-extraits-01.jpg
- /media/asfar-livre-extraits-05.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Extraits, by Nadim Asfar
translationKey: extraits-asfar
---
