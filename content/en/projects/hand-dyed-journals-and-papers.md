---
artist: ""
client: Calligrane
cover: /media/lpb-calligrane-itajime-01.jpg
date: 2003-11-11
description: Hand-dyed journals and papers. For the boutique Calligrane, Paris, 2003.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-calligrane-itajime-01.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Itajime collection for Calligrane
translationKey: calligrane-itajime
---
