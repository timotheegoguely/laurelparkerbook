---
artist: ""
client: ""
cover: /media/lpb-carnet_2012.jpg
date: 2012-11-11
description: 'Our end of the year journal 2012 : coptic binding, front cover in blue
  paper, back cover in black calfskin, interior in various papers, digital prints,
  and hot stamping. Series of 100 copies. Colophon : a direct adaptation of the colophon
  of "Mad Marginal / Archives" by Dora Garcia, graphic design by Jean-Claude Chianale.
  Design et fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet_2012.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: End-of-the-year Notebook 2012
translationKey: notebook-2012
---
