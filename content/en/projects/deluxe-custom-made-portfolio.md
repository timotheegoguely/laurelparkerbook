---
artist: ""
client: Studio KO
cover: /media/roi-maroc_coffret-01.jpg
date: 2014-05-04
description: 'Deluxe custom-made presentation boxes to hold the architectural plans
  for the interior design of a private palace in Morocco. Porfolio set in the cover.
  Format 50 × 65 cm. Black cloth, German laid paper, hot stamping in gold. Series
  of 3 deluxe presentation boxes and 20 portfolios. For Studio KO (Karl Fournier &
  Olivier Marty).'
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
- Coffret
images:
- /media/roi-maroc_coffret-01.jpg
- /media/ko-maroc-coffret-04.jpg
- /media/ko-maroc-coffret-02.jpg
- /media/ko-maroc-coffret-01.jpg
- /media/ko-maroc-coffret-0.jpg
- /media/ko-maroc-coffret-03.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Deluxe presentation for an interior design project
translationKey: studio-ko
---
