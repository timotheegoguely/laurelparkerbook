---
artist: Oscar Muñoz
client: Toluca Editions
cover: /media/munoz-inventario-03.jpg
date: 2014-11-10
description: |-
  Inventario, by Oscar Muñoz. Accordion file folder in Takeo paper to hold 10 carbon prints. With brass elements. An original and atypical form for a photography portfolio.
  Toluca Editions, Paris, 2014. 13 ex + 4 A.P
  Fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Œuvre papier
- Chemise
images:
- /media/munoz-inventario-03.jpg
- /media/munoz-inventario-01.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Inventario, by Oscar Muñoz
translationKey: inventario
---
