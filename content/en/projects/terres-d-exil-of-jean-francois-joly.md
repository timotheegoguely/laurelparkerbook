---
artist: Jean-François Joly
client: Wallguest
cover: /media/joly_terre-exil-01.jpg
date: 2016-11-11
description: |-
  Terres d’exil, portfolio of 5 gelatin silver prints by Jean-François Joly, limited edition of 10 copies. This portfolio was produced to coincide with the artist’s exhibition at the Maison Européenne de la Photographie.

  Album with mats, in the style of daguerreotype albums of the 19th century. Cover and pages in paper handmade by the Richard de Bas paper mill; internal structure made from museum-quality board. Title and colophon in hot stamping. Design and fabrication: Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/joly_terre-exil-01.jpg
- /media/joly_terre-exil-05.jpg
- /media/joly_terre-exil-03.jpg
- /media/joly_terre-exil-04.jpg
- /media/joly_terre-exil-02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: 'Terres d’exil '
translationKey: terres-exil
---
