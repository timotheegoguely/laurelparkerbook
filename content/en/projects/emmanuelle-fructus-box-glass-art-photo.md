---
title: '5M, 5N, 5B'
translationKey: fructusverre
description: >-
  Laminated Diamant glass, silver print inlaid between Eva film. Fabrication of
  the pedestal box in buckram cloth by&nbsp; Laurel Parker Book. Edition of 3.
date: '2022-12-11'
client: Emmanuelle Fructus
artist: Emmanuelle Fructus
series:
  - Unique
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Coffret
cover: /media/fructus-boite-verre-glass-photo-art-web-06.jpg
images:
  - /media/fructus-boite-verre-glass-photo-art-web-03.jpg
  - /media/fructus-boite-verre-glass-photo-art-web-04.jpg
  - /media/fructus-boite-verre-glass-photo-art-web-05.jpg
  - /media/fructus-boite-verre-glass-photo-art-web-02.jpg
  - /media/fructus-boite-verre-glass-photo-art-web-01.jpg
shop: null
foldedByHand: null
___mb_schema: /.mattrbld/schemas/projet.json
---

