---
title: AgBr
translationKey: agbr
description: >-
  Portfolio de présentation pour la série AgBr, 9 tirages argentiques grand
  format de Dove Allouche. Images tirées d'une des premières prises de vue de la
  mer Morte, conservée dans les collections du Mahj, une photographie de Félix
  Bonfils (1831-1885), "Palestine : mer Morte, 1877".
date: 2023-06-14
client: Dove Allouche
artist: Dove Allouche
series:
  - Unique
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Coffret
cover: /media/dove-allouche-portfolio-mermorte-01-web.jpg
images:
  - /media/dove-allouche-portfolio-mermorte-01-web.jpg
  - /media/dove-allouche-portfolio-mermorte-03-web.jpg
  - /media/dove-allouche-portfolio-mermorte-04-web.jpg
  - /media/dove-allouche-portfolio-mermorte-02-web.jpg
shop: false
___mb_schema: /.mattrbld/schemas/projet.json
---

