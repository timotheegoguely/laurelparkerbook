---
title: >-
  “Persuade everyone to see all the pictures that make you feel joyful”, de
  Tiane Doan na Champassak
translationKey: livreindigo
description: >-
  Etching ink applied to  45 ink jet prints of archive photos. Japanese binding
  by Laurel Parker Book. Support in plexiglas covered with wood by  Maonia.
  Japanese paper by Awagami. One of a kind book, 41,3 x 37 x 1,5 cm
date: 2024-02-02
client: Tiane Doan na Champassak
artist: Tiane Doan na Champassak
series:
  - Unique
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
cover: /media/tiane-indigo-book-08-site.jpg
images:
  - /media/tiane-indigo-book-08-site.jpg
  - /media/tiane-indigo-book-06-site.jpg
  - /media/tiane-indigo-book-04-site.jpg
  - /media/tiane-indigo-book-03-site.jpg
shop: false
foldedByHand: false
___mb_schema: /.mattrbld/schemas/projet.json
---

