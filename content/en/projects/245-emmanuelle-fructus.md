---
artist: Emmanuelle Fructus
client: Emmanuelle Fructus
cover: /media/fructus_245_10_-laurelparkerbook.JPG
date: 2021-10-25T00:00:00.000Z
description: >-
  245 is part of the tradition of fabric books for children and those of
  artists, such as Louise Bourgeois who cut, sewed and embroidered pieces from
  her wardrobe to make Ode to oblivion. The pages here are made from bed sheets
  purchased at flea markets, like the B&amp;W photos of the previous works of
  Emmanuelle Fructus. Sensitive surfaces testifying to reality, textiles and
  photo paper share the same relationship to materiality and signify time. Both
  allow a reappropriation of the work through gesture.  30,3 x 41,5 cm •
  Silkscreen printed in 6 colors on cotton and linen bed sheets, hand sewn,
  separate cover with hot stamping • 8 ex + 2 A.P. • Laurel Parker Edition, 2021
  •1250,00 € VAT included
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/245-fructus-lpbedition.jpeg
  - /media/245-fructus-lpbedition-2.jpeg
  - /media/245-fructus-lpbedition-4.jpeg
  - /media/245-fructus-lpbedition-3.jpeg
  - /media/245-fructus-lpbedition-5.jpeg
secteurs:
  - Art contemporain
  - Photo
series:
  - Multiple
shop: true
title: '245, Emmanuelle Fructus'
translationKey: 245-fructus
___mb_schema: /.mattrbld/schemas/projet.json
---

