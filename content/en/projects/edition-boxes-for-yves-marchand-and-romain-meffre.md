---
artist: Yves Marchand and Romain Meffre
client: Polka Galerie
cover: /media/vuitton-boite-polka_02.jpg
date: 2015-05-10
description: '12 limited edition boxes, each with 15 pigments prints by Yves Marchand
  and Romain Meffre, from their book _Fondation Louis Vuitton / Frank Gehry_ (éditions
  Skira). Published by Polka Galerie for the Fondation Louis Vuitton. Covered in white
  book cloth, with a platform covered in Zerkall blue laid paper. Hot stamping in
  hologram film. Fabrication : Laurel Parker Book'
designs:
- Semi-sur mesure
- Sur mesure
fonctions:
- Éditer
- Présenter
formes:
- Boîte d’éditeur
images:
- /media/vuitton-boite-polka_02.jpg
- /media/vuitton-boite-polka_03.jpg
- /media/vuitton-boite-polka_04.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Fondation Louis Vuitton / Frank Gehry, by Yves Marchand and Romain Meffre
translationKey: marchand-meffre
---
