---
artist: Robert Desnos
client: Bibliothèque Littéraire Jacques Doucet
cover: /media/desnos-les-sommeils_03.jpg
date: 2017-06-06
description: |-
  Box to hold _Les Sommeils_, original drawings and writings by Robert Desnos, for the collection of the Jacques Doucet Literary Library, Paris. Restauration of the documents by Atelier Coralie Barbe, restorer of books and works on paper. Presentation case with shelfs containing five archival boxes, materials conforming to museum standards.

  Design and fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Archiver
- Présenter
formes:
- Coffret
images:
- /media/desnos-les-sommeils_02.jpg
- /media/desnos-les-sommeils_04.jpg
- /media/desnos-les-sommeils_03.jpg
- /media/desnos-les-sommeils_01.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: Les Sommeils, by Robert Desnos
translationKey: les-sommeils-robert-desnos
---
