---
artist: Françoise Pétrovitch
client: Semiose Galerie
cover: /media/petrovitch-boite-conservation-02.jpg
date: 2012-05-13
description: Archival quality box for the presentation and archiving of drawings by
  Françoise Pétrovitch at the Semiose Gallery. Large format (60 × 90cm). Red library
  buckram cloth and mirror-effect hot stamping.
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/petrovitch-boite-conservation-02.jpg
- /media/petrovitch-boite-conservation-03.jpg
- /media/petrovitch-boite-conservation-01.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: Archival quality box for Françoise Pétrovitch drawings
translationKey: boite-dessin-petrovitch
---
