---
artist: Michel Journiac
client: Galerie Christophe Gaillard
cover: /media/journiac_24hours-02.jpg
date: 2018-11-11
description: 'Limited edition box set of 42 photographs _(Réalités & Phantasmes)_.
  Edition produced for the exhibition _Michel Journiac_ at the Galerie Christophe
  Gaillard. Edition of 10 copies + 2 HC + 2 AP. Black and white silver gelatin prints
  on Ilford fiber paper developed by Cadre en Seine Choi. Mats by Circad. Box design
  and fabrication, hot stamping on mats by Laurel Parker Book. Conception graphique
  by Camille Morin.'
designs:
- Sur mesure
fonctions:
- Archiver
- Présenter
- Éditer
formes:
- Coffret
images:
- /media/journiac_24hours-02.jpg
- /media/journiac_24hours-01.jpg
- /media/journiac_24hours-marquage.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: 24 Hours in the Life of an Ordinary Woman, by Michel Journiac
translationKey: 24h
---
