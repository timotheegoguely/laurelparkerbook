---
artist: Laurel Parker et Paul Chamard
client: null
cover: /media/lpb-kujoyama-02.jpg
date: 2019-11-11T00:00:00.000Z
description: >-
  Installation for the Nuit Blanche, Le Terminal, Kyoto, Japan, October 2019.
  Following the artist-in-residency of Laurel Parker and Paul Chamard at the
  Villa Kujoyama. Japanese paper (washi), Japanese cedar, real gold thread,
  printing in mica powder.


  If in the western world paper is a material for writing, in Japan, it becomes
  a partition which splits a space in two with the help of fine wooden frames.
  Laurel Parker and Paul Chamard question these uses by reflecting on the
  relationship between private and public space. Traditional printed patterns
  for the interior are inspired by external motifs from nature whose use is
  highly codified in Japan. What patterns can be taken from the urban landscape
  of a 21st century city like Kyoto ?
designs:
  - Sur mesure
fonctions:
  - Exposer
formes:
  - Œuvre papier
  - Installation
images:
  - /media/lpb-kujoyama-02.jpg
  - /media/lpb-kujoyama-03.jpg
  - /media/lpb-kujoyama-04.jpg
  - /media/lpb-kujoyama-07.jpg
  - /media/lpb-kujoyama-05.jpg
  - /media/lpb-kujoyama-01.jpg
  - /media/lpb-kujoyama-06.jpg
secteurs:
  - Art contemporain
series:
  - Unique
shop: null
title: Inside - Outside
translationKey: inside-outside
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

