---
artist: ""
client: Grafikmente
cover: /media/carrefour_press_book_3.jpg
date: 2017-02-06
description: |-
  Architects call on us for one-of-a-kind presentation boxes which can enhance their proposal as well as their dedication to the project.
  Box for Carrefour Property : a presentation box with 5 portfolios, silkscreen printing on each cover. Hot stamping on the top of the box.

  Graphic design : Arancha Vega for Grafikmente
  Fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
- Portoflio
images:
- /media/carrefour_press_book_3.jpg
- /media/carrefour_press_book_1.jpg
- /media/carrefour_press_book_11.jpg
- /media/carrefour_press_book_8.jpg
- /media/carrefour_press_book_5.jpg
- /media/carrefour_press_book_6.jpg
secteurs:
- Agence
series:
- Multiple
shop: false
title: Portfolios for architect agency
translationKey: monaco
---
