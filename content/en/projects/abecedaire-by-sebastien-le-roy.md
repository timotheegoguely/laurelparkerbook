---
artist: Sebastien Le Roy
client: Les Paroles Gelées
cover: /media/leroy_abecedaire_01.jpg
date: 2014-11-11
description: '300 copies + 30 special edition Slip case made from japanese card stock
  with a hot stamping. For the special edition, we used the same beautiful japanese
  card stock to make a box to hold the book and an original signed linoleum print.
  Prototype and fabrication : Laurel Parker Book'
designs:
- Semi-sur mesure
fonctions:
- Présenter
formes:
- Boîte éditeur
images:
- /media/leroy_abecedaire_01.jpg
- /media/leroy_abecedaire_05.jpg
- /media/leroy_abecedaire_04.jpg
- /media/leroy_abecedaire_03.jpg
- /media/leroy_abecedaire_02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Abécédaire, by Sebastien Le Roy
translationKey: abecedaire-sebastien-le-roy
---
