---
artist: null
client: Boucheron
cover: /media/boucheron-01.jpg
date: 2009-10-19T00:00:00.000Z
description: >-
  Album with mats for the presentation of drawings for the luxury house
  Boucheron. Insertion of the drawings by the top of the mats. Mats covered in
  satin cloth. Book covered in calfskin with a rounded spine.  Design and
  fabrication : Laurel Parker.
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Portfolio
images:
  - /media/boucheron-01.jpg
  - /media/boucheron-album-03.jpg
  - /media/boucheron-album-05.jpg
secteurs:
  - Luxe
series:
  - Multiple
shop: false
title: 'Presentation albums for Boucheron fine jewelry '
translationKey: album-boucheron
___mb_schema: /.mattrbld/schemas/projet.json
---

