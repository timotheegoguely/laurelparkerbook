---
artist: Marina Gadonneix
client: Marina Gadonneix
cover: /media/gadonneix-boite-presentation-01.jpg
date: 2013-01-06
description: 'A series of boxes for the artist Marina Gadonneix. Variation of colors
  and hot-stamping following a basic model. Box design and fabrication : Laurel Parker
  Book.'
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/gadonneix-boite-presentation-01.jpg
- /media/gadonneix-boite-presentation-02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Removed landscapes, Three saftey lights, Crash...
translationKey: removed-landscapes
---
