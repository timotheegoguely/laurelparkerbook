---
artist: Jean Royère
client: Galerie Jacques Lacoste
cover: /media/royere-album_lacoste_01.jpg
date: 2017-11-11
description: |-
  Facsimile of an album from the 1930’s with photographs of the furniture of Jean Royère, designer. Page and photo restauration by Atelier Coralie Barbe.

  We have reproduced the original structure of the album, while overcoming the technical defects to create a more durable structure. Dyed rafia, coated bookcloth,  tinted Japanese paper, and a rounded wooden spine made by the Maonia cabinetmakers.
  For Jacques Lacoste Gallery.
  Design and fabrication : Laurel Parker Book, 2017.
designs:
- Sur mesure
fonctions:
- Archiver
- Présenter
formes:
- Portfolio
- Livre
images:
- /media/royere-album_lacoste_01.jpg
- /media/royere-album_lacoste_03.jpg
- /media/royere-album-lacoste_02.jpg
secteurs:
- Archives
series:
- Multiple
shop: false
title: Facsimile of the furniture of Jean Royère
translationKey: album-jean-royere
---
