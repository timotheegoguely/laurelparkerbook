---
title: Du Glacier du Rhône au Lac Léman
translationKey: glacier
description: >-
  Conservation quality box with a system of dividers to hold an artist book, for
  the collection of the Médiathèque&nbsp;Valais. Corinne Vionnet asked us to
  design an object to hold her photo book in the form of an accordeon with pages
  of various widths. Conservation box covered in paper, digital UV printing on
  conservation quality paper. Design and fabrication&nbsp;: Laurel Parker Book
  •&nbsp;Typographic design :&nbsp;Marine Bigourie
date: '2023-04-05'
client: Médiathèque du Valais
artist: Corinne Vionnet
series:
  - Unique
secteurs:
  - Art contemporain
  - Photo
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Coffret
cover: /media/vionnet-glacier-lac-06-web.jpg
images:
  - /media/vionnet-glacier-lac-03-web.jpg
  - /media/vionnet-glacier-lac-01-web.jpg
  - /media/vionnet-glacier-lac-05-web.jpg
  - /media/vionnet-glacier-lac-04-web.jpg
  - /media/vionnet-glacier-lac-07-web.jpg
shop: false
___mb_schema: /.mattrbld/schemas/projet.json
---

