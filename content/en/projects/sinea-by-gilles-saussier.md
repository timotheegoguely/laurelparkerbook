---
artist: Gilles Saussier
client: Gilles Saussier
cover: /media/saussier_sinea_02.jpg
date: 2012-10-23
description: "Card stock folder for the artist book Sinea, by Gilles Saussier. The
  form of the flaps is inspired by the Endless Column by Constantin Brancusi, which
  is the subject of the book. Edition of 20 copies. Hot stamping in white.\nBook design
  and printing : Benoît Santiard and Gilles Saussier \nFolder design by Benoît Santiard
  and Gilles Saussier \nFolder fabrication : Laurel Parker Book."
designs: []
fonctions:
- Éditer
formes:
- Chemise
- Œuvre papier
images:
- /media/saussier_sinea_02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Sinea
translationKey: sinea
---
