---
artist: ""
client: ENSBA-École de Beaux-Arts de Paris
cover: /media/ensba_coffret-de-voyage_02.jpg
date: 2019-05-05
description: 'Conservation boxes for  _« coffrets à estampe »_ from the middle ages
  : treasures from the collection of the École de Beaux-Arts of Paris. Dating from
  the end of the 15th century, these boxes, made from beech wood covered in leather
  and encircled with metal, have a print inside the cover. These prints are rare wood
  block print incunables.'
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/ensba_coffret-de-voyage_04.jpg
- /media/ensba_coffret-de-voyage_02.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: Conservation boxes for « coffrets à estampe » from the 15th century
translationKey: coffret-estampe
---
