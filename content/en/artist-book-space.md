---
description: >-
  The mission of LPB gallery is to create a space allowing the public to see
  artists’ book objets and understand their specificity.
images:
  - /media/expo-paper-plant-specimens-washi-book-01.jpg
  - /media/expo-paper-plant-specimens-washi-book-06.jpg
  - /media/expo-paper-plant-specimens-washi-book-04.jpg
  - /media/expo-paper-plant-specimens-washi-book-03.jpg
  - /media/plant-specimens-laurel-parker-book-art-washi-site.jpg
  - /media/expo-paper-plant-specimens-washi-book-02.jpg
layout: page
menu:
  aside:
    weight: 3
slug: artist-book-space
title: Artist Book Space
translationKey: artist-book-space
___mb_schema: /.mattrbld/schemas/page.json
draft: null
---
Exhibition & Book Launch

***Plant Specimen***

Laurel Parker & Paul Chamard

Opening 12 janvier 2025

≈≈≈≈≈≈≈≈≈≈≈≈

In Japan, washi has been used as a textile for over a millennium. Among the objects made of washi, we find carpets, clothes, hats, umbrellas, water bottles, bowls and cups, lanterns, and even toys…

It is thanks to bast fibers — plant fibers extracted from the inner bark of local plants used to make paper pulp — that washi obtains the elasticity and strength necessary to be folded, crumpled, rolled, woven, manipulated and put in volume.

This exhibition of “specimens” explores the relationship between paper and plants.

≈≈≈≈≈≈≈≈≈≈≈≈

Our space is located in the new cultural district in Romainville, in the Komunuma group alongside the galleries Air de Paris, In Situ-Fabienne Leclerc, Galerie Sator, Jocelyn Wolff, and 22,48 m².

The mission of our gallery is to create a space allowing the public to see artists’ book objets and understand their specificity.

Our temporary exhibitions share a space with a book shop of artists’ books, and with the workshop and library of Laurel Parker Book (artists’ books, photo books, technical books…). This space is open everyone.

A program of conferences, workshops, and book launches completes our mission.

Gallery opening hours: Tuesday to Saturday, 10 am to 6 pm as well as Opening Sundays (check our Instagram for information).

[Inscription to our Newsletter for news and openings](http://eepurl.com/beg7pH)
