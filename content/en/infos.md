---
layout: infos
title: Infos
translationKey: infos
description: null
___mb_schema: /.mattrbld/schemas/infos.json
---
{{< col >}}

## About us

Laurel Parker Book is a hybrid space devoted to artists’ books, at the crossroads of design studio, fabrication workshop, and art gallery.

Since 2008, Laurel Parker Book has been designing books, boxes, and artists’ multiples for artists and publishers, as well as for agencies, luxury houses, institutions, and the private sector. Our craft binding workshop is dedicated to artistic creation, beautiful presentation, and the conservation of artistic and heritage works.

Since 2018, Laurel Parker Edition has been publishing contemporary art in multiples, always keeping in mind the book-object and the handmade object.

In 2020, the studio joined their colleagues at Komunuma, adding to their workshop space a gallery–bookshop–library : Artist Book Space.

Following their residency at the Villa Kujoyama in Kyoto, Laurel Parker Book has developed *Folded By Hand* : artworks and surfaces in washi (Japanese artisanal paper) using traditional treatments. These objects - a mixture of Japanese techniques and French savoir-faire - are intended for decoration and interior design.

{{< /col >}}

{{< col >}}

## Contact

For an appointment, send us an email or give us a call :

Tél : [+33 (0) 9 83 93 48 20](tel:+33983934820)  
Email : laurelparkerbook@gmail.com

Laurel Parker Book  
[43, rue de la Commune de Paris<br>93230 Romainville, France](https://g.page/laurelparkerbook?share)

Metro : Line 5, Bobigny Pantin Raymond Queneau  
Bus : 145, 147, 318  
Car : N3 by Paris, follow avenue Gaston Roussel  
Vélib’ : Station Gaston Roussel

---

## Social networks

[Facebook](https://fr-fr.facebook.com/pages/category/Community/Laurel-Parker-Book-636027673183719/), [Instagram](https://www.instagram.com/laurelparkerbook/)

---

## Newsletter

[Subscribe to our newsletter](https://laurelparkerbook.us10.list-manage.com/subscribe?u=a807097a566fbfb6c01068b6b&id=62464aa724 "subscribe form")

{{< /col >}}

{{< col >}}

## Team

Laurel Parker, founder and artistic director of the studio, studied painting and filmmaking the School of the Museum of Fine Arts, Boston (Diploma), and art history at Tufts University (BFA). She studied bookbinding and printing at the Center for Book Arts in New York. She was laureate of the Villa Kujoyama (Kyoto) in 2019 in duo with Paul Chamard. She was named Artisan of exception by the Fondation Remy Cointreau in 2019. Laurel gives workshops and conferences in different art and design schools in France.

Paul Chamard, head of the studio’s production, studied etching at École Estienne, where he earned his arts and crafts diploma, and at the HEAR in Strasbourg, where he earned his Master’s degree in the book object. He is responsible for the daily production of projects as well as the technicians who join us for large projects. Paul was laureate of the Villa Kujoyama in 2019 in duo with Laurel Parker.

Coline Boudier Ligault, production assistant, studied Art History at Strasbourg et earned her CAP de reliure (bookbinding certificate). She continued at ENSAV de la Cambre en Bruxelles earning a Bachelor of Book and Paper Design. In 2023, elle participated in the program Savoir-faire in transmission (BDMMA Paris) at the atelier Invenio Flory.

---

{{< list data="clients" title="Clients" >}}

{{< /col >}}

{{< col >}}

## Awards & Grants & Label

[Academic, Skills Academy : Paper, Fondation Hermès,](https://www.fondationdentreprisehermes.org/fr/projet/skills-academy-2025-papier) Laurel Parker (2025)

[Lauréats, Villa Kujoyama, Laurel Parker et Paul Chamard](https://www.villakujoyama.jp/resident/laurel-parker-paul-chamard/ "voir l'article") (2019)

[Artisan d’Exception](https://www.fondationremycointreau.com/nos_artisans/laurel-parker "voir l'article"), Fondation Rémy Cointreau (2019)

[Grands Ateliers de France](http://www.grandsateliersdefrance.com/fr/mobile/laurel-parker-book-233.html), member

[French Design by VIA](https://connectedby.lefrenchdesign.org/_/company?id=6601c4ecb8b1b7a79693f14b), adherent

Maison des Artistes, adherent

Grands Prix de la Création, finalist (2024, 2018, 2016)

Fabriqué à Paris, label (2019, 2018)

Grant, Paris Création : Ateliers de Paris Endowment Fund, Daniel and Nina Carasso Foundation (2014)

Studio grant, Semaest, Viaduc des Arts, Paris (2004)

---

## Fairs & Exhibitions

{{< time >}}2024{{< /time >}}

-   [Anthology](https://sinople.paris/galerie/) / Sinople, Paris
    

{{< time >}}2024{{< /time >}}

-   10 years of Arts & Crafts residencies at the Villa Kujoyama / Le Terminal, Kyoto
    

{{< time >}}2024{{< /time >}}

-   [Ligare](https://sinople.paris/galerie/) / Sinople, Paris
    

{{< time >}}2023{{< /time >}}

-   Biennale Émergence, Pantin
    

{{< time >}}2022{{< /time >}}

-   [Inside Outside / Musée de la Chasse et de la Nature, Paris](https://www.chassenature.org/rendez-vous/inside-outside)
    

{{< time >}}2022{{< /time >}}

-   Salon du livre rare, Grand Palais
    

{{< time >}}2021{{< /time >}}

-   Multiple Art Days
    

{{< time >}}2020{{< /time >}}

-   [Les vies minuscules / Festival Viva Villa / Collection Lambert](https://www.vivavilla.info/artistes/laurel-parker-et-paul-chamard/)
    

{{< time >}}2019{{< /time >}}

-   [La Nuit Blanche, Kyoto](https://www.villakujoyama.jp/nuit-blanche-kyoto-2019/)
    
-   Multiple Art Days
    
-   FLAT, Turin
    
-   Salon de Livres Rares & Objets d’Art, Grand Palais
    

{{< time >}}2018{{< /time >}}

-   Multiple Art Days
    
-   Salon de Livres Rares & Objets d’Art, Grand Palais
    

---

## Conferences

{{< time >}}2023{{< /time >}}

-   [Library Meeting : Laurel Parker / Maison Européenne de la Photographie / Paris](https://www.mep-fr.org/event/rencontre-bibliotheque-laurel-parker-book/)
    

{{< time >}}2022{{< /time >}}

-   Preparatory class, Ateliers de beaux-arts de la ville de Paris / Conference on our artistic journey and edition projects / Alexandra Fau, curator and art history professor
    

{{< time >}}2018{{< /time >}}

-   Viva Villa, Marseille / Conference on artists’ books / Federico Nicolao, festival curator
    

{{< time >}}2017{{< /time >}}

-   “[The client, the graphic designer and the printer](http://www.laab.fr/le-commanditaire-le-graphiste-et-limprimeur-2017/ "see the article")” / FAC Rennes 2 / an invitation of Jocelyn Cottencin & Yann Sérandour
    
-   “The secret life of photo books” / la Maison Doisneau de la photographie, Gentilly
    

{{< time >}}2015{{< /time >}}

-   “[Collaboration in the editions of artists’ books](https://www.cnap.fr/%C2%AB-editer-%C2%BB-formes-d%E2%80%99un-engagement "see the article")” / Cycle Éditer / ESAD d’Amiens
    

{{< time >}}2013{{< /time >}}

-   “Collaboration between the workshops of Laurel Parker Book and Coralie Barbe Restauration” / series Plexus / École Estienne
    

{{< time >}}2011{{< /time >}}

-   “Artists’ books projects in the studio of Laurel Parker Book” / Center for Book Arts, New York
    

---

## Jurys

{{< time >}}2021{{< /time >}}

-   [Jury of the Prix Elysée 2020-2022](https://prixelysee.ch/editions-precedentes/edition-2021/), 4<sup>th</sup> edition (Laurel Parker)
    

{{< time >}}2020{{< /time >}}

-   [Jury of the Paris Photo–Aperture Foundation Book Awards](https://www.parisphoto.com/fr-fr/programme/2020/photobook-awards-2020/laurel-parker.html "see the article"), 9e edition (Laurel Parker)
    

{{< time >}}2018{{< /time >}}

-   Jury, VAE (validation of earned experience) Master in photography, ENSP Arles (Laurel Parker)
    

---

## Teaching (Laurel Parker)

Université Rennes 2 / workshop edition 3<sup>rd</sup> year Licence professionnelle Design graphique, éditorial et multimédia (class of Yann Sérandour and Jocelyn Cottencin ), 2020–2025

ESADHaR · Le Havre / workshop edition 3<sup>rd</sup> year DNA (invited by Sonia Da Rocha), 2024

ESAD Valence / workshop “books with a system” in 3<sup>rd</sup> year graphic design (in the class of Lisa Bayle & Samuel Vermeil), 2022

EESAB Bretagne, Rennes / workshop edition 2<sup>nd</sup> year graphic design (class of Jérôme Saint-Loubert Bié, etc), 2011–2022

EMI École des métiers d’information / workshop edition photo book (invitation of Jean Larive), 2020–2023

Parsons Paris / presentation of book edition / AMT Art Media and Technology / (1<sup>st</sup> year class of Tamara Rosenblum), 2021

ESAM Caen / workshop edition 2<sup>nd</sup> year graphic design (class of Juanma Gomez) 2018–2021

Lycée Eugénie Coton / workshop edition en 2<sup>nd</sup> year communication (class of Yoan De Roeck), 2019

ESAM Caen / workshop edition in Master’s program book edition (class of Juanma Gomez), 2018

ENSA Nancy / workshop edition photo book with Marina Gadonneix and Remi Faucheaux / 2018

AA School of Architecture, visiting school Paris / workshop edition, 2017 & 2018

INP Institut national du patrimoine / workshop conditioning archival materials, 2017 & 2018

Pôle supérieur de design Nouvelle Aquitaine / Plastic practice and Mediation UE5, DSAA 2<sup>nd</sup> year design, 2016–2017

Pôle supérieur de design Nouvelle Aquitaine / workshop edition in 1<sup>st</sup> year DSAA (with Élisabeth Charvet & Mahaut Clément), 2017

F93 / workshop edition, dispositif ULIS at middle school Pasteur, Villemomble, Laurel Parker & Paul Chamard

Center for Book Arts, New York / instructor, bookbinding and paper arts, 2001–2011

École spéciale d’architecture, Paris / workshop edition, 2009

SVA School of Visual Arts, New York / workshop edition, 2001

Horizons New England Craft Program, Williamsburg, Massachusetts / workshop rbookbinding and paper arts, 1996–2001

Massachusetts College of Art, Boston / workshop edition, 1998

---

## Press

{{< time >}}2024{{< /time >}}

-   [“Composite Revue”](https://dumplingbooks.fr/composite/), by the Campus Fonderie de l'image, №5.
    

{{< time >}}2023{{< /time >}}

-   [“Tous les possibles du papier”](https://process.vision/article/laurel-parker-booktous-les-possibles-du-papier/), by Cyrille Jouanno, Process Magazine №36.
    

{{< time >}}2019{{< /time >}}

-   “[Les livres de Laurel Parker](/media/roven14_livres_laurelparker_vieville.pdf "see the article")”, by Camille Viéville, Roven №14.
    

{{< time >}}2018{{< /time >}}

-   “[L’importance de l’écrin…Dans sont atelier parisien, Laurel Parker conçoit des écrins d’exception](/media/intramuros-198-atelier-laurel-parker.pdf "see the article")”, by David Kabla, Intramuros №198.
    
-   “[Laurel Parker Book](/media/grabados-y-edicion-61-laurel-parker.pdf "see the article")”, Grabado y edicion №61.
    
-   “[Atelier Laurel Parker Book : le livre, un objet d’art](/media/connaissance-des-arts-laurel-parker-book.jpeg "see the article")”, Connaissance des arts, hors-série métiers d’art en France 2018.
    

{{< time >}}2015{{< /time >}}

-   “[Profession relieuse d’art](/media/cote_paris-37-relieur-art.pdf "see the article")”, by Amandine Schira, Côté Paris №37.
    

{{< time >}}2006{{< /time >}}

-   “[Laurel Parker : la reliure dans la peau](/media/elle-deco-160-reliure-laurel-parker.jpg "see the article")”, Elle Deco №160.
    

---

## Publications

{{< time >}}2022{{< /time >}}

-   *72 Saisons à la Villa Kujoyama* / Gallimard.
    

{{< time >}}2021{{< /time >}}

-   *Books : Art, Craft & Community* / London Centre for Book Arts, Ludion.
    

{{< time >}}2020{{< /time >}}

-   *Les vies minuscules* / Festival Viva Villa, Collection Lambert.
    

{{< time >}}2017{{< /time >}}

-   “[Pour la reliure de l’art](/media/the-shelf-05-reliure.pdf "see the article")” / The Shelf Journal, №5.
    

{{< time >}}2015{{< /time >}}

-   *Collectionner, conserver, exposer le graphisme : Entretiens autour du travail de Dieter Roth* / EESAB Rennes et le FRAC Bretagne.
    

---

## Legal Mentions

Design & code : [Timothée Goguely](https://timothee.goguely.com)

Typeface : [Mint Grotesk](https://www.futurefonts.xyz/loveletters/mint-grotesk), designed by [Love Letters](http://love-letters.be/).

{{< /col >}}
