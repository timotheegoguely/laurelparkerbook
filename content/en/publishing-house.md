---
description: >-
  Laurel Parker Edition publishes contemporary art in multiples, always with in
  mind the book-objet and the hand-crafted object. 
layout: shop
menu:
  aside:
    weight: 4
slug: publishing-house
title: Publishing house
translationKey: maison-edition
___mb_schema: /.mattrbld/schemas/page.json
---
Since 2018, Laurel Parker Edition publishes contemporary art in multiples, always with in mind the book-objet and the hand-crafted object.

Please contact us by email if you wish to acquire one of our editions.
