---
description: >-
  Laurel Parker Book is a studio that designs and makes bespoke books,
  presentation objects, and conservation objects. Since 2008, we have
  collaborated with artists, publishers, institutions, and luxury goods houses.
  Our work is artisanal and entirely handmade at our studio in Paris.
image: /media/_40B9172.jpg
title: Home
___mb_schema: /.mattrbld/schemas/home.json
---

