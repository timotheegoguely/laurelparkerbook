---
description: >-
  Laurel Parker Book designs books, boxes, and book objects for artists and
  publishers, as well as for agencies, luxury goods houses, institutions, and
  the private sector.
images:
  - /media/studio-creation-showroom-web.jpg
  - /media/rdv-atelier-3-site.jpg
  - /media/rdv-atelier-site.jpg
layout: page
menu:
  aside:
    weight: 1
slug: design-studio
title: Design studio
translationKey: studio-de-creation
___mb_schema: /.mattrbld/schemas/page.json
draft: null
---
Laurel Parker Book designs books, boxes, and book objects for artists and publishers, as well agencies, luxury goods houses, institutions, and the private sector.

The work is collaborative, in discussion with the client, the graphic designer, the printer, as well as suppliers and other craftsmen. Each project is carried from conception to fabrication, offering services including object design, graphic design, and the fabrication in our studio.

Our Materials Library includes a selection of thousands of swatches of papers, cardboards, leather, cloths, threads, trimmings, clasps, as well as examples of printing methods and other related techniques.

Our Showroom includes hundreds of projects and prototypes realized by our studio, which help visualize and design new projects.

Our in-house Book Library has over 600 books and book-objects which further nourish the design process.
