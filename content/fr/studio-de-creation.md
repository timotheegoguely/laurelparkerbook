---
description: >-
  Laurel Parker Book conçoit des livres, des boîtes et des objets-livres pour
  les artistes et les éditeurs, ainsi que pour les agences, les maisons de luxe,
  les institutions et le secteur privé.
images:
  - /media/studio-creation-showroom-web.jpg
  - /media/rdv-atelier-3-site.jpg
  - /media/rdv-atelier-site.jpg
layout: page
menu:
  aside:
    weight: 1
slug: studio-de-creation
title: Studio de création
translationKey: studio-de-creation
___mb_schema: /.mattrbld/schemas/page.json
draft: null
---
Laurel Parker Book conçoit des livres, des boîtes et des objets-livres pour les artistes et les éditeurs, ainsi que pour les agences, les maisons de luxe, les institutions et le secteur privé.

Le travail est collaboratif, en discussion avec le client, le graphiste, l’imprimeur, ainsi que les fournisseurs et d’autres artisans. Chaque projet est porté de l’idée à la fabrication, en proposant des services incluant le design d’objet, le design graphique, la préparation et suivi de production au sein de l’atelier.

La Bibliothèque de matériaux comprend une sélection de milliers d’échantillons de papiers, cartons, cuirs, tissus, fils, passementeries, fermoirs, ainsi que des exemples de méthodes d’impression et d’autres techniques liées.

Le Showroom comprend des centaines de projets et prototypes réalisés par le studio, qui aident à visualiser et concevoir de nouveaux projets.

La bibliothèque de livres in situ contient plus de 600 livres et objets-livres qui nourrissent davantage le processus de conception.
