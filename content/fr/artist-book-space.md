---
description: >-
  La mission de notre galerie est de créer un espace permettant au public de
  voir des livres d’artistes et comprendre leur spécificité.
images:
  - /media/expo-paper-plant-specimens-washi-book-01.jpg
  - /media/expo-paper-plant-specimens-washi-book-06.jpg
  - /media/expo-paper-plant-specimens-washi-book-04.jpg
  - /media/expo-paper-plant-specimens-washi-book-03.jpg
  - /media/plant-specimens-laurel-parker-book-art-washi-site.jpg
  - /media/expo-paper-plant-specimens-washi-book-02.jpg
layout: page
menu:
  aside:
    weight: 3
slug: artist-book-space
title: Artist Book Space
translationKey: artist-book-space
___mb_schema: /.mattrbld/schemas/page.json
draft: null
---
Exposition & lancement d’édition

***Plant Specimens***

Laurel Parker & Paul Chamard

Vernissage 12 janvier 2025

≈≈≈≈≈≈≈≈≈≈≈≈

Au Japon, le washi est utilisé comme un textile depuis plus d’un millénaire. Parmi les objets composés de washi, on trouve des tapis, vêtements, chapeaux, parapluies, gourdes, bols et tasses, lanternes, ou encore des jouets… C’est grâce aux fibres libériennes — fibres végétales extraites de l’écorce interne de plantes locales utilisées pour faire la pâte à papier — que le washi obtient l’élasticité et la résistance nécessaire pour être plié, froissé, roulé, tissé, manipulé et mis en volume. Cette exposition de « spécimens » explore la relation entre le papier et le végétal.

≈≈≈≈≈≈≈≈≈≈≈

Notre espace est situé à Komunuma, nouveau quartier culturel à Romainville, aux côtés des galeries Air de Paris, In Situ-Fabienne Leclerc, Galerie Sator, Jocelyn Wolff et 22,48 m².

La mission de notre galerie est de créer un lieu permettant au public de voir des livres d’artistes et de comprendre leurs spécificités. Nos expositions ponctuelles partagent un espace avec une librairie de livres d’artistes, l’atelier et la bibliothèque de Laurel Parker Book (livres d’artistes, livres photos, livres techniques). Ce lieu est ouvert à tous. Une programmation de conférences, workshop, et lancement de livres complète notre mission.

[Inscription Newsletter](http://eepurl.com/beg7pH) pour les vernissages et actualités.
