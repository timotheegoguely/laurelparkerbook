---
title: Folded By Hand
description: Papiers japonais façonnés à la main destinés à la décoration d'intérieur.
layout: folded-by-hand
menu:
  aside:
    weight: 5
slug: folded-by-hand
translationKey: folded-by-hand
images: null
___mb_schema: /.mattrbld/schemas/page.json
draft: null
---
Suite à leur résidence à la Villa Kujoyama à Kyoto, Laurel Parker Book développe *Folded by Hand* : œuvres et surfaces en washi (papier artisanal japonais) utilisant des traitements traditionnels. Ces objets – mélange de techniques japonaises et savoir-faire français – sont destinés à la décoration et à l’aménagement d’intérieur.

Folded by Hand est représenté par [Sinople Paris](https://sinople.paris/galerie/).
