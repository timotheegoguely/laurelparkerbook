---
description: >-
  Laurel Parker Book est un atelier artisanal de reliure dédié à la création
  artistique.
images:
  - /media/atelier-d-artisan-romainville-2024-site.jpg
  - /media/_40B9194.jpg
  - /media/atelier-laurel-parker-book-3.jpg
  - /media/vue_atelier_lpb_04.jpg
  - /media/atelier-laurel-parker-book-2.jpg
layout: page
menu:
  aside:
    weight: 2
slug: atelier-artisan
title: Atelier d’artisan
translationKey: atelier-artisan
___mb_schema: /.mattrbld/schemas/page.json
draft: null
---
Depuis 2008, Laurel Parker Book est un atelier artisanal de reliure dédié à la création artistique. Chaque projet est fabriqué à la main au sein de l’atelier, un travail sur mesure avec une attention particulière portée aux détails.

Le travail du papier, la connaissance des arts, des techniques, de l’évolution des formes et des structures historiques, tout cela permet des réalisations cohérentes et durables.

Les domaines de l'Art Contemporain, des archives et collections, du luxe, de la photographie et de l'édition apportent des projets innovants et enrichissent la créativité de l'atelier.

Projets sur rendez-vous, [contacter l'atelier par mail](mailto:laurelparkerbook@gmail.com)
