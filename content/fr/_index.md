---
description: >-
  Laurel Parker Book est un atelier de design et fabrication de livres, d’objets
  de présentation et de conservation sur mesure. Depuis 2008, nous collaborons
  avec des artistes, des éditeurs, des institutions, et des maisons de luxe.
  Notre travail est artisanal et entièrement fait à la main à notre atelier à
  Paris.
image: /media/_40B9172.jpg
title: Accueil
___mb_schema: /.mattrbld/schemas/home.json
---

