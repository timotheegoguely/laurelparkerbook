---
artist: Nathalie Grenier
client: Nathalie Grenier
cover: /media/grenier_leporello_01_w.jpg
date: 2022-10-12
description: "Leporello géant pour la présentation de 9 dessins originaux (encre et
  estampage).  \nStructure papier, impressions du titre et du colophon : jet d’encre
  UV."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/grenier_leporello_01_w.jpg
- /media/grenier_leporello_04_w.jpg
- /media/grenier_leporello_03_w.jpg
- /media/grenier_leporello_02_w.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: Soulévements
translationKey: soulevements
---
