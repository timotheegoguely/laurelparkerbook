---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-carnet-2016-01.jpg
date: 2016-11-11
description: 'Notre carnet fin d’année 2016 : couture main en chaînette, découpe à
  la forme et marquage à chaud hologramme, impression en sérigraphie. Édition de 150
  exemplaires. Design et fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet-2016-01.jpg
- /media/lpb-carnet-2016-02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carnet de fin d’année 2016
translationKey: notebook-2016
---
