---
artist: Joël Alain Dervaux
client: Joël Alain Dervaux
cover: /media/dervaux_allonges_02.jpg
date: 2018-11-11
description: "Album contenant 10 tirages sous maries-louise. Couverture en toile et
  plats rapportés, pages maries-louise en papier et carton de qualité archivage, marquage
  à chaud du titre et du colophon. Édition originale de 3 ex + 2 E.A., Paris, 2018.\n\nTirages
  :Fred Goyeau Photography  \nGraphisme : Marine Bigourie  \nDesign et reliure : Laurel
  Parker Book"
designs:
- Sur mesure
fonctions:
- Présenter
- Éditer
formes:
- Portfolio
- Livre
images:
- /media/dervaux_allonges_01.jpg
- /media/dervaux_allonges_06.jpg
- /media/dervaux_allonges_03.jpg
- /media/dervaux_allonges_04.jpg
- /media/dervaux_allonges_02.jpg
- /media/dervaux_allonges_05.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Allongés, de Joël Alain Dervaux
translationKey: allonges-joel-alain-dervaux
---
