---
artist: Caroline & Cyril Desroche
client: Delpire & Co
cover: /media/delpire-losangeles-standards5.jpg
date: 2021-11-04
description: "Coffret en papier sérigraphié, conçu dans notre atelier. Chaque boîte
  contient 15 tirages d'exception issu d'une sélection du livre Los Angeles Standards
  de Caroline & Cyril Desroche.\n\nLos Angeles Standards, coffret en édition limitée
  de 15 exemplaires + 3 EA.\n\nÉdité par Delpire&Co  \nEn collaboration avec Poursuite
  Edition  \nConception graphique de Grégoire Pujade Lauraine  \nTirages photos par
  Picto  \nFabrication et conception du coffret par Laurel Parker Book"
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/delpire-losangeles-standards2.jpg
- /media/delpire-losangeles-standards5.jpg
- /media/delpire-losangeles-standards.jpg
- /media/delpire-losangeles-standards3.jpg
- /media/delpire-losangeles-standards4.jpg
secteurs:
- Photo
series:
- Multiple
shop: false
title: Los Angeles Standards (coffret édition limitée) de Caroline & Cyril Desroche
translationKey: los-angeles-standards
---
