---
artist: Thomas Dworzak
client: Magnum Photos
cover: /media/magnum-feldpost-02.jpg
date: 2019-09-09
description: 'Feldpost était le nom donné au système postal militaire allemand utilisé
  au cours de la Première Guerre mondiale. Thomas Dworzak de l’agence Magnum a créé
  au cours des sept dernières années une image annotée pour chaque jour de la Grand
  Guerre - plus de 1500 images au total. Ces "cartes postales", avec leurs notes explicatives
  écrites par Chris Bird, représentent une vision de la guerre qui en résume le caractère
  véritablement mondial : une image complexe du conflit, ses multiples particularités,
  ainsi que ses répercussions actuelles.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/magnum_feldpost_01.jpg
- /media/magnum-feldpost-02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Feldpost, par Thomas Dworzak
translationKey: feldpost
---
