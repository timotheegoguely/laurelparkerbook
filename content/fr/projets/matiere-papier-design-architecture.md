---
title: Surfaces en washi transformé
translationKey: matiere-papier
description: >-
  Détails des œuvres et surfaces en washi (papier artisanal japonais) utilisant
  des traitements traditionnels. Ces objets – mélange de techniques japonaises
  et savoir-faire français – sont destinés à la décoration et aménagement
  d’intérieur.
date: '2023-01-11'
client: null
artist: Laurel Parker Book
series:
  - Prototype
secteurs:
  - Agence
designs:
  - Sur mesure
fonctions:
  - Exposer
formes:
  - Œuvre papier
cover: /media/carre-rose-1-site.jpg
images:
  - /media/carre-rose-1-site.jpg
  - /media/carre-rose-6-site.jpg
  - /media/cire-rose-5-site.jpg
  - /media/cire-rose-4-site.jpg
  - /media/gampi-vert-3-site.jpg
  - /media/momigami-marron-3-site.jpg
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

