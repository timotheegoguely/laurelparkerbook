---
artist: Babx
client: Babx
cover: /media/babx-pochettecd-03.jpg
date: 2015-09-20
description: "Coffret en 350 exemplaires pour une édition limitée d’un CD, contenant
  également un livret et 9 dessins. Structure en carte japonaise, marquage à chaud,
  impression au tampon et couture main.   \nDesign graphique : Yoan De Roeck   \nDessins
  : Laurent Allaire (Allx)   \nDesign et fabrication du coffret : Laurel Parker Book"
designs:
- Semi-sur mesure
fonctions:
- Éditer
formes:
- Boîte 2.0
- Chemise
images:
- /media/babx-pochettecd-03.jpg
- /media/babx-pochettecd-02.jpg
- /media/babx-pochettecd-01.jpg
- /media/babx-pochettecd-05.jpg
- /media/babx-pochettecd-04.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Cristal automatique n°1, de Babx
translationKey: cristal-automatique
---
