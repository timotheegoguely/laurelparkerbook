---
artist: Blexbolex
client: Orbis Pictus Club
cover: /media/blexbolex_fleur_oracle_04.jpg
date: 2016-11-11
description: 'Livre d’artiste avec 32 planches sérigraphiées, 200 exemplaires numérotés
  et signés. Reliure copte, fil de lin ciré. Editeur : Orbis Pictus Club, Imprimeur
  : Frédérique Déjean. Fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/blexbolex_fleur_oracle_04.jpg
- /media/blexbolex_fleur_oracle_02.jpg
- /media/blexbolex_fleur_oracle_03.jpg
- /media/blexbolex_fleur_oracle_01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: La Fleur Oracle, de Blexbolex
translationKey: fleur-oracle
---
