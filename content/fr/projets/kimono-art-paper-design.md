---
title: Kimono
translationKey: kimono-design
description: >-
  Le kimono, le vêtement traditionnel du japon, est créé par une série de
  rectangles. Ces morceaux sont décorés avant d’être cousus ensembles. Le corps
  est emballé dans la géométrie. L’itagime – l’art du pliage et teinture des
  textiles – est appliqué au papier pour le changer d’une matière plane
  bidimensionnelle en une nouvelle matière tridimensionnelle. Sa transformation
  est complétée par un cirage avec une teinture minérale qui apporte une couleur
  en référence aux teintes japonaises.
date: '2020-01-11'
client: null
artist: Laurel Parker
series:
  - Unique
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Exposer
formes:
  - Œuvre papier
cover: /media/kimono-rose-1-site.jpg
images:
  - /media/kimono-rose-1-site.jpg
  - /media/kimono-rose-2-site.jpg
  - /media/kimono-vert1-site.jpg
  - /media/kimono-vert2-site.jpg
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---
<p></p>
