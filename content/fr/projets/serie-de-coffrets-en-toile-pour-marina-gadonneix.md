---
artist: Marina Gadonneix
client: Marina Gadonneix
cover: /media/gadonneix-boite-presentation-01.jpg
date: 2013-05-12
description: 'Série de coffrets en toile pour l’artiste Marina Gadonneix. Variation
  de la couleur de la toile et du marquage à chaud suivant un modèle de base. Design
  de la boîte et fabrication : Laurel Parker Book.'
designs:
- Sur mesure
draft: true
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/gadonneix-boite-presentation-01.jpg
- /media/gadonneix-boite-presentation-02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Removed landscapes, Three saftey lights, Crash...
translationKey: removed-landscapes
---
