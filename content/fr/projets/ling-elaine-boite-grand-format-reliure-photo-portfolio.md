---
artist: Elaine Ling
client: ""
cover: /media/baobab_ling_02.jpg
date: 2015-11-11
description: |-
  Baobab, coffret pour une série de 11 tirages 53 × 68 cm de l’artiste Elaine Ling. Boite en toile sérigraphiée. Titre, texte et colophon tirés en sérigraphie sur un papier fait main par Frédérique Gironde de Ruscombe Mill à Margaux. Edité en 5 exemplaires, 2015.

  Tirages photographiques avec le procédé platine / paladium par Salto Ulbeek.
  Sérigraphies imprimées par le Orbis Pictus Club.
  Design de l’édition et fabrication du coffret : Laurel Parker Book.

  Photographies du coffret : Hermance Triay.
designs:
- Sur mesure
fonctions:
- Éditer
- Présenter
formes:
- Coffret
images:
- /media/baobab_ling_02.jpg
- /media/baobab_ling_03.jpg
- /media/baobab_ling_04.jpg
- /media/baobab_ling_01.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Baobab, d’Elaine Ling
translationKey: baobab-elaine-ling
---
