---
title: Du Glacier du Rhône au Lac Léman
translationKey: glacier
description: >-
  Un coffret de qualité&nbsp;conservation avec un système
  d'intercalaires&nbsp;pour conditionner un livre d'artiste dans&nbsp;la
  collection de la Médiathèque&nbsp;Valais. Corinne Vionnet nous a demandé
  de&nbsp;concevoir un objet pour contenir son livre photographique&nbsp;en
  forme de leporello avec des pages de formats variés. Coffret en papier,
  tirages textes et images en impression UV. Design et réalisation du
  coffret&nbsp;: Laurel Parker Book •&nbsp;Design typographique :&nbsp;Marine
  Bigourie
date: '2023-04-05'
client: Médiathèque du Valais
artist: Corinne Vionnet
series:
  - Unique
secteurs:
  - Art contemporain
  - Photo
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Coffret
cover: /media/vionnet-glacier-lac-06-web.jpg
images:
  - /media/vionnet-glacier-lac-03-web.jpg
  - /media/vionnet-glacier-lac-01-web.jpg
  - /media/vionnet-glacier-lac-05-web.jpg
  - /media/vionnet-glacier-lac-04-web.jpg
  - /media/vionnet-glacier-lac-07-web.jpg
shop: false
___mb_schema: /.mattrbld/schemas/projet.json
---
<p></p>
