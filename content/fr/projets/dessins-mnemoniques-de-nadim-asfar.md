---
artist: Nadim Asfar
client: Nadim Asfar
cover: /media/asfar-livre-artistes-onglets6.jpg
date: 2021-09-29
description: "Montage et couture sur onglets en papier japon, plats rapportés et étui
  de conservation. Impressions sur feuillets translucides, dessins d’Antoine Atallah.
  Texte à la machine à écrire de Nadim Asfar.\n\nCe projet est présenté en ce moment
  à la galerie Tanit, Munich :  \nImaginary Cities: chapter II  \nBeyrouth as project
  \ \nNadim Asfar en collaboration avec Antoine Atallah  \n24 sept – 20 nov 2021"
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/asfar-livre-artistes-onglets6.jpg
- /media/asfar-livre-artistes-onglets4.jpg
- /media/asfar-livre-artistes-onglets3.jpg
- /media/asfar-livre-artistes-onglets5.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Dessins Mnémoniques, de Nadim Asfar
translationKey: Dessins-mnémoniques
---
