---
artist: ""
client: Costume 3 Pièces
cover: /media/costumes3pieces-les-habilleurs-02.jpg
date: 2013-05-19
description: Une série de look books pour le studio de création de l’agence Costume
  3 Pièces. Tirages numériques et couture à la copte en fil de lin turquoise. Carton
  brute et marquage à chaud. 50 exemplaires. Une série de press books. Tirages numériques.
  Toile bleue et doublures en tissu. Pochette intérieure en clin d’œil aux costumes
  trois pièces. Marquage à chaud en blanc. 20 exemplaires.
designs:
- Sur mesure
fonctions:
- Éditer
- Présenter
formes:
- Portfolio
- Livre
images:
- /media/costumes3pieces-les-habilleurs-02.jpg
- /media/costumes3pieces-les-habilleurs-05.jpg
- /media/costumes3pieces-portfolio-01.jpg
- /media/costumes3pieces-les-habilleurs-03.jpg
- /media/costumes3pieces-les-habilleurs-04.jpg
secteurs:
- Agence
series:
- Multiple
shop: false
title: Look book et press book, Agence Costume 3 pièces
translationKey: costume-3-pieces
---
