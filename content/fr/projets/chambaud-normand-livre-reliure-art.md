---
artist: Etienne Chambaud, Vincent Normand
client: Paraguay Press
cover: /media/chambaud_contre-histoire-separation-01.jpg
date: 2007-10-29
description: |-
  Traduction dans l’espace du livre du film éponyme d’Etienne Chambaud et Vincent Normand qui lie le destin de la guillotine et du musée moderne autour du concept de « Musée décapité ». Le texte de la voix-off a été reproduit sous la forme d’un livre à tirage unique, puis photographié dans le dispositif de tournage du film, donnant lieu à un livre de photographies oscillant entre le statut de document textuel et celui de scène imaginaire.
  "Contre-Histoire de la Séparation (52 min, 2010) débute pendant la Terreur, en 1793, lors de l’apparition de la guillotine sur la scène révolutionnaire et de la création du musée moderne. Il se termine en 1977, année de la dernière décapitation par guillotine et de l’inauguration d’un nouveau prototype de musée, le Centre Pompidou. Il explore l’espace ouvert par le mode de découpe optique et politique inventé par la modernité, en retraçant la dissolution des fonctions du musée moderne et de la guillotine dans ce que les auteurs appellent le 'Musée décapité '". — Raphael Brunel, Les Cahiers de la Création Contemporaine
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/chambaud_contre-histoire-separation-01.jpg
- /media/chambaud_contre-histoire-separation-03.jpg
- /media/chambaud_contre-histoire-separation-02.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: Contre-Histoire de la Séparation
translationKey: contre-histoire
---
