---
artist: Klima Intérieurs (Léa Bigot et Sarah Espeute)
client: Laurel Parker Edition
cover: /media/klima-pages2_01.jpg
date: 2018-09-17T00:00:00.000Z
description: >-
  La suite de Klima Pages #1..._#2, Somptueuses Résidences_, est un catalogue
  imaginaire présentat quatre habitations de vacances situées quelque part dans
  le Sud. 30,5 × 23,5 cm, 90 pages. Impression numérique par Media Graphic /
  Compagnons du Sagittaire, Rennes. Édition limitée à 150 exemplaires numérotés
  dont 20 exemplaires spéciaux avec une clef et un porte-clef bas-relief signé
  Klima Intérieurs. Format 30,5 x 23,5 cm, 90 pages • Impression numérique • 150
  ex + 20 ex spéciaux avec une clef et porte-clef bas-relief signé Klima •
  Laurel Parker Edition, 2018 • 35 € / 70 € TTC
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/klima-pages2_01.jpg
  - /media/klima-pages2_03.jpg
  - /media/klima-pages2_04.jpg
  - /media/klima-pages2_02.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: 'Klima Pages #2, par Klima Intérieurs'
translationKey: klima-pages
___mb_schema: /.mattrbld/schemas/projet.json
---

