---
title: 'Folded by Hand : gamme Shoji'
translationKey: fbh shoji
description: >-
  <em>Folded by Hand : </em>surfaces en washi (papier artisanal japonais)
  utilisant des traitements traditionnels. Ces objets – mélange de techniques
  japonaises et savoir-faire français – sont destinés à la décoration et
  aménagement d’intérieur. Série&nbsp;<em>Shoji </em>en<em> blanc, rose ciré et
  verdigris ciré.</em>
date: '2024-05-11'
client: Laurel Parker Book
artist: Folded by Hand
series:
  - Multiple
secteurs: null
designs:
  - Sur mesure
fonctions:
  - Exposer
formes:
  - Œuvre papier
cover: /media/fbh-paper-design-shoji-rectangle-2-web.jpg
images:
  - /media/fbh-paper-design-shoji-rectangle-web.jpg
  - /media/fbh-paper-design-wax-rose-web.jpg
  - /media/fbh-paper-design-wax-verdigris-web.jpg
  - null
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

