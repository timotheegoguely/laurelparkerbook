---
artist: Sebastien Le Roy
client: Les Paroles Gelées
cover: /media/leroy_abecedaire_01.jpg
date: 2014-11-11
description: "Deux solutions pour une édition de livres éditée par Les Paroles Gelées.
  Un étui simple en carte avec un marquage à chaud pour l’édition d'un 300 exemplaires.
  Une chemise avec un rabat pour contenir le livre et un tirage en linogravure signé
  par l'artiste. 300 exemplaires + 30 exemplaires de tête. Papier Takeo.  \nPrototypage
  et fabrication : Laurel Parker Book."
designs:
- Semi-sur mesure
fonctions:
- Présenter
formes:
- Chemise
images:
- /media/leroy_abecedaire_01.jpg
- /media/leroy_abecedaire_05.jpg
- /media/leroy_abecedaire_04.jpg
- /media/leroy_abecedaire_03.jpg
- /media/leroy_abecedaire_02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Abécédaire, de Sebastien Le Roy
translationKey: abecedaire-sebastien-le-roy
---
