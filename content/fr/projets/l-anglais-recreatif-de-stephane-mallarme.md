---
artist: Stéphane Mallarmé
client: Bibliothèque Littéraire Jacques Doucet
cover: /media/mallarme-boite-conservation_02.jpg
date: 2013-09-15
description: 'Coffret pour un jeu créé par Stéphane Mallarmé, collection de la Bibliothèque
  Littéraire Jacques Doucet. Restauration des éléments composant le jeu de Mallarmé
  par Coralie Barbe, restauratrice de livres et d’œuvres sur papier. Coffret à tiroirs,
  conforme aux normes muséographiques, design et fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/mallarme-boite-conservation_02.jpg
- /media/mallarme-boite-conservation_01.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: L'Anglais Récréatif de Stéphane Mallarmé
translationKey: stephane-mallarme
---
