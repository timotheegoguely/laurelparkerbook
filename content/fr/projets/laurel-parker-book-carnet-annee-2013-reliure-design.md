---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-carnet-2013-04.jpg
date: 2013-11-11
description: 'Notre carnet fin d’année 2013 : reliure à vis, pages dépliantes en papier
  jaune, couvertures en papier fait main, et marquage à chaud en bleu nuit. Édition
  de 150 exemplaires. Design et fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet-2013-02.jpg
- /media/lpb-carnet-2013-04.jpg
- /media/lpb-carnet-2013-03.jpg
- /media/lpb-carnet-2013-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carnet de fin d’année 2013
translationKey: ""
---
