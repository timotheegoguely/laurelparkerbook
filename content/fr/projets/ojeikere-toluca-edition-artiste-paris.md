---
artist: J.D. Okhai Ojeikere
client: Toluca Editions
cover: /media/ojeikere-afrokhoi-sapiens_01.jpg
date: 2014-11-10T00:00:00.000Z
description: >-
  5 photographies originales, tirées sur papier baryté, dans un coffret de luxe.
  Textes d’Abdourahman Waberi. Boîtiers en bois d’ébène par Edward Barber et Jay
  Osgerby (pas dans la photo). Design graphique d’Olivier Andreotti. Pochettes
  et papier de conservation avec marquages à chaud, Laurel Parker Book. Édition
  limitée, 20 exemplaires + 4 A.P
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
  - Chemise
images:
  - /media/ojeikere-afrokhoi-sapiens_01.jpg
  - /media/ojeikere-afrokhoi-sapiens_02.jpg
  - /media/ojeikere-afrokhoi-sapiens_03.jpg
secteurs:
  - Photo
  - Art contemporain
series:
  - Multiple
shop: false
title: 'Afrokhoï Sapiens, de J.D. Okhai Ojeikere'
translationKey: afrokhoi-sapiens
___mb_schema: /.mattrbld/schemas/projet.json
---

