---
artist: Gilles Saussier
client: Gilles Saussier
cover: /media/saussier_sinea_02.jpg
date: 2012-10-29
description: |-
  Chemise en carte pour le livre d’artiste "Sinea" de Gilles Saussier. Carte forte avec marquage à chaud. Le design des rabats reprend la forme du module de la colonne sans fin de Brancusi, le sujet du livre. 20 exemplaires.
  Design et impression du livre : Benoît Santiard et Gilles Saussier
  Design de la chemise : Benoît Santiard et Gilles Saussier
  Fabrication de la chemise : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Chemise
- Œuvre papier
images:
- /media/saussier_sinea_02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Sinea
translationKey: sinea
---
