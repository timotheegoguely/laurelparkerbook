---
artist: Yves Marchand et Romain Meffre
client: Polka Galerie
cover: /media/vuitton-boite-polka_02.jpg
date: 2015-06-09
description: 'Coffret en édition limitée à 12 exemplaires, contenant 15 tirages pigmentaires
  des photographes Yves Marchand et Romain Meffre, de leur livre _Fondation Louis
  Vuitton / Frank Gehry_ (éditions Skira). Édité par Polka Galerie pour la Fondation
  Louis Vuitton. En toile blanche, avec du papier bleu allemand fait à la cuve. Marquage
  à chaud de couleur hologramme. Fabrication : Laurel Parker Book.'
designs:
- Semi-sur mesure
fonctions:
- Éditer
- Présenter
formes:
- Boîte d’éditeur
images:
- /media/vuitton-boite-polka_02.jpg
- /media/vuitton-boite-polka_03.jpg
- /media/vuitton-boite-polka_04.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Fondation Louis Vuitton / Frank Gehry, par Yves Marchand et Romain Meffre
translationKey: marchand-meffre
---
