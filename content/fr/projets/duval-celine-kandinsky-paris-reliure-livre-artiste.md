---
artist: documentation céline duval
client: documentation céline duval
cover: /media/celine-duval-sur-plan_03.jpg
date: 2016-11-11
description: "Livre unique créé pendant une conférence au Centre Pompidou en janvier
  2013. Reliure en spirale avec cartes postales et un étui avec un marquage à chaud.
  Logo conçu par la graphiste Myriam Barchechat à partir d’une police de caractères
  dessinée par Josef Albers dans les années 1920 au Bauhaus. \n\nDans son ouvrage,
  Céline Duval propose une lecture très personnelle des photographies de famille de
  son peintre préféré Wassily Kandinsky (qu’elle avait découvert dans un album à la
  bibliothèque Kandinsky au Centre Pompidou). Cette lecture s’appuie sur les enseignements
  que le peintre développe dans son ouvrage philosophique _Point et ligne sur plan_,
  qui décrit et définit les éléments géométriques présents dans la peinture. \n\nReliure
  unique de l’édition de 2013 des Éditions Semiose, avec cartes postales intercalées
  entre les pages et tenues dans des pochettes polyester reliées avec une nouvelle
  spirale."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
- Livre
images:
- /media/celine-duval-sur-plan_02.jpg
- /media/celine-duval-sur-plan_04.jpg
- /media/celine-duval-sur-plan_03.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Cœur, point et ligne sur plan (livre unique)
translationKey: coeur-point
---
