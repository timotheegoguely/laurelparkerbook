---
artist: Olivier Culmann
client: Picto Foundation
cover: /media/culmann-deflexions-01.jpg
date: 2017-11-11
description: "_DéfleXions, 1992 – 2008_, de Olivier Culmann (du collectif Tandance
  Floue), édition d’artiste réalisée avec le soutien de la [Picto Foundation](https://www.pictofoundation.fr/prix-niepce-2017/
  \"Picto Foundation\"), mécène du Prix Niépce, dans le cadre du prix décerné à Olivier
  Culmann par l’association Gens d’Images en 2017.\n\n_DéfleXions, 1992 – 2008_ présente
  des pistes de traverse. Des tentatives ou tentations qui ont jalonné le parcours
  du photographe depuis 1992. Des chemins divers sur lesquels il s’est aventuré. Certains
  d’entre eux seront prolongés, d’autres laissés momentanément en sommeil ou abandonnés
  à jamais…\n\nLa boîte contient sept séries, sept esquisses qui ont ponctué l’œuvre
  d’Olivier Culmann. Elles nous montrent quelques secrets d’un photographe dont l’œuvre
  reste depuis l’origine empreinte de recherche et de questionnement.\n\n_DéfleXions,
  1992 – 2008 »_ est un objet d’artiste imaginé par Olivier Culmann et Laurel Parker,
  avec le soutien de Picto Foundation.12 exemplaires, signés et numérotés. \n\nPhotographies
  et les textes : Olivier Culmann  \nSuivi du projet et la réalisation des tirages
  : Elisabeth Hering, Vianney Drouin, Patrice Baron, Payram, Guillaume Weihmann, Pierric
  Paulian et Maxime Le Gall du laboratoire Picto. Réalisation des coffrets : Laurel
  Parker Book."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Chemise
- Coffret
images:
- /media/culmann-deflexions-01.jpg
- /media/culmann-deflexions-02.jpg
- /media/culmann-deflexions-03.jpg
- /media/culmann-deflexions-04.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: DéfleXions, 1992-2008, de Olivier Culmann
translationKey: delfexions
---
