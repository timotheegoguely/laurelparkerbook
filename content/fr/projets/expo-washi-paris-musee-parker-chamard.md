---
title: Inside Outside au Musée de la Chasse et de la Nature
translationKey: museenature
description: "À l’occasion des 39e Journées européennes du patrimoine et de la Paris Design Week 2022, le Musée de la Chasse et de la Nature et la Villa Kujoyama, partenaires depuis 2016, invitent le public à venir découvrir le projet de recherche que Laurel Parker et Paul Chamard ont développé pendant leur résidence à Kyoto autour du papier japonais, le washi, inscrit au patrimoine mondial de l’UNESCO. Étonnant trompe l’œil, chaque élément de l’installation «\_Inside Outside\_» – kimono, tatami, packaging etc., est fabriqué en washi transformé selon des techniques traditionnelles. Ces objets d’interprétation nous invitent à interroger l’usage contemporain du washi devenu l’un des matériaux favoris des architectes, artisans d’art, artistes et designers."
date: 2022-09-18T00:00:00.000Z
client: null
artist: Laurel Parker et Paul Chamard
series:
  - Unique
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Exposer
formes:
  - Installation
  - Œuvre papier
cover: /media/parker-chamard-musee01-site.jpg
images:
  - /media/parker-chamard-musee01-site.jpg
  - /media/parkerchamardmusee01.jpg
  - /media/parker-chamard-musee02-site.jpg
  - /media/parker-chamard-musee03-site.jpg
  - /media/parker-chamard-musee05-site.jpg
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

