---
artist: 'Jérôme Barbe, Franck Léonard, Fred Maillard'
client: Laurel Parker Edition
cover: /media/barbe-leonard-maillard-terrain-de-jeux_02.jpg
date: 2018-11-11T00:00:00.000Z
description: >-
  Ce projet est un espace de création, de réflexion et d’échanges, avec les
  images de l’exposition Prototype de Terrain de Jeux qui s’est tenue à
  Montreuil en Mars 2018. Le jeu de ce livre en cahier non-relié, avec une
  fausse imposition de pages, est l’impossibilité de mettre en ordre les images
  en double-page — une métaphore pour la difficulté d'exposer des œuvres
  diverses dans une exposition collective. Format 27 × 20,5 cm, 24 pages •
  Impression offset, pochette de conservation avec marquages à chaud • 100 ex •
  Laurel Parker Edition, 2018 • 25 € TTC
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Chemise
  - Livre
images:
  - /media/barbe-leonard-maillard-terrain-de-jeux_03.jpg
  - /media/barbe-leonard-maillard-terrain-de-jeux_04.jpg
  - /media/barbe-leonard-maillard-terrain-de-jeux_01.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: '"Prototype", Terrain de Jeux'
translationKey: terrain-de-jeux
___mb_schema: /.mattrbld/schemas/projet.json
---

