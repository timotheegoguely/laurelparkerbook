---
artist: Luc Delahaye
client: Luc Delahaye
cover: /media/delahaye-97-figurants-01.jpg
date: 2013-11-10
description: Livre d’artiste avec tirages numériques.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/delahaye-97-figurants-01.jpg
- /media/delahaye-97-figurants-02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: 97 figurants de Luc Delahaye
translationKey: 97-figurants
---
