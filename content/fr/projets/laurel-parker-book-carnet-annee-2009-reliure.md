---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-papeterie-carnetvoeux2009-02.jpg
date: 2009-11-11
description: 'Notre carnet fin d’année 2009 : reliure à la copte, tirages offset du
  projet "Inside the White Cube, édition fantôme" de Yann Sérandour. Édition de 20
  exemplaires. Design et fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-papeterie-carnetvoeux2009.jpg
- /media/lpb-papeterie-carnetvoeux2009-02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carnet de fin d’année 2009
translationKey: notebook-2009
---
