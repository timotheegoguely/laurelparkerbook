---
title: 'Folded by Hand : Gampi Rectangles'
translationKey: fbh gampi
description: >-
  <em>Folded by Hand : </em>surfaces en washi (papier artisanal japonais)
  utilisant des traitements traditionnels. Ces objets – mélange de techniques
  japonaises et savoir-faire français – sont destinés à la décoration et
  aménagement d’intérieur. Série <em>Gampi Rectangles</em> en matcha, sugi et
  natural.
date: '2024-05-12'
client: Laurel Parker Book
artist: Folded by Hand
series:
  - Multiple
secteurs:
  - Agence
designs:
  - Sur-mesure
fonctions:
  - Exposer
formes:
  - Œuvre papier
cover: /media/fbh-paper-design-gampi-matcha-2-web.jpg
images:
  - /media/fbh-paper-design-gampi-matcha-web.jpg
  - /media/fbh-paper-design-gampi-sugi-web.jpg
  - /media/fbh-paper-design-gampi-natural-web.jpg
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

