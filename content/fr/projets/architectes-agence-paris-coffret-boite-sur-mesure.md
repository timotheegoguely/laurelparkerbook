---
artist: null
client: Grafikmente
cover: /media/carrefour_press_book_3.jpg
date: 2017-05-07T00:00:00.000Z
description: >-
  Plusieurs architectes font appel à nous pour des coffrets de présentation. Ces
  objects uniques mettent en valeur leur travail et leur implication dans un
  projet.


  Coffret Carrefour, boite de présentation et 5 pressbooks, couvertures
  sérigraphiées. Appel d'offre pour le design et construction d'un centre
  commercial à Monaco.


  Conception graphique : Arancha Vega pour Grafikmente

  Fabrication : Laurel Parker Book.
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Coffret
  - Portfolio
images:
  - /media/carrefour_press_book_3.jpg
  - /media/carrefour_press_book_1.jpg
  - /media/carrefour_press_book_11.jpg
  - /media/carrefour_press_book_8.jpg
  - /media/carrefour_press_book_5.jpg
  - /media/carrefour_press_book_6.jpg
secteurs:
  - Agence
series:
  - Multiple
shop: false
title: Coffrets pour architectes
translationKey: monaco
___mb_schema: /.mattrbld/schemas/projet.json
---

