---
artist: Ivars Gravlejs
client: Maya Mikelsone
cover: /media/gravlejs-nelson-goodman_01.jpg
date: 2010-11-10
description: '"My Newspaper" de l’artiste Ivars Gravlejs se retrouve incrusté dans
  une réédition limitée de "L’Art en théorie et en action" de Nelson Goodman.  Conception
  et design par Maya Mikelsone pour son projet de Master de l’école du Magasin- CNAC
  de Grenoble, 2010. Design graphique : Thomas Berthou .'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/gravlejs-nelson-goodman_01.jpg
- /media/gravlejs-nelson-goodman_02.jpg
secteurs:
- Art contemporain
series:
- Multiple
title: My Newspaper, de Ivars Gravlejs
translationKey: my-newspaper
---
