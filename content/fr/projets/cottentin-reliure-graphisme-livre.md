---
artist: Jocelyn Cottencin
client: Jocelyn Cottencin
cover: /media/cottencin_reliure_01.jpg
date: 2019-09-15
description: Cette édition est une relecture d’un projet vidéo réalisé à l’IUT de
  Roubaix. Reliure cousue, dos carré, toile sérigraphiée.
designs: []
fonctions:
- Éditer
formes:
- Livre
images:
- /media/cottencin_reliure_03.jpg
- /media/cottencin_reliure_01.jpg
- /media/cottencin_reliure_02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Chronique d’un automne, les formes du travail, de Jocelyn Cottencin
translationKey: jocelyn-cottencin
---
