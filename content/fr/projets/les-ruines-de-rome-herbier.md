---
artist: Ludovic Michaux
client: Ludovic Michaux
cover: /media/boite_conservation_anatstylose_michaux_3.jpg
date: 2008-09-21
description: Coffrets de conservation avec 58 tirages argentiques de Ludovic Michaux
  du livre Anastylose, Rome, XIII av. J.-C., 1942. Farce archéologique en deux actes
  et un aparté, publié par Fage éditions et l’Académie de France à Rome. Trois boîtes
  à charnière dans un coffret avec étagères. Toile buckram, carton musée, papiers
  pH neutre. Marquages à sec. Pour l’acquisition en 2008 par la Bibliothèque Nationale
  de France, Département des Estampes et de la Photographie.
designs:
- Sur mesure
fonctions:
- Présenter
- Archiver
formes:
- Coffret
images:
- /media/boite_conservation_anatstylose_michaux_3.jpg
- /media/boite_conservation_anatstylose_michaux_4.jpg
- /media/boite_conservation_anatstylose_michaux_6.jpg
secteurs:
- Archives
- Photo
series:
- Unique
shop: false
title: Anastylose, de Ludovic Michaux
translationKey: anastylose
---
