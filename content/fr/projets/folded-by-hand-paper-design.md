---
title: Folded by Hand nuancier
translationKey: fbh nuancier
description: >-
  <em>Folded by Hand : </em>surfaces en washi (papier artisanal japonais)
  utilisant des traitements traditionnels. Ces objets – mélange de techniques
  japonaises et savoir-faire français – sont destinés à la décoration et
  aménagement d’intérieur.
date: '2024-05-13'
client: Laurel Parker Book
artist: Folded By Hand
series: null
secteurs:
  - Agence
designs:
  - Sur mesure
fonctions:
  - Exposer
formes:
  - Œuvre papier
cover: /media/fbh-paper-design-02-web.jpg
images:
  - /media/fbh-paper-design-02-web.jpg
  - /media/fbh-paper-design-01-web.jpg
shop: null
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

