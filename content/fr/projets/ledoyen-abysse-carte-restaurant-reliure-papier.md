---
artist: ""
client: Grafikmente
cover: /media/ledoyen_menu_01.jpg
date: 2019-05-05
description: |-
  Carte de restaurant développée pour l’Abysse, nouveau restaurant trois étoiles au Pavillon Ledoyen.
  Structure en origami pour tenir et changer souvent le menu. Couverture en papier japonais transformé à la main avec la technique itajime.
  Direction artistique Arancha Vega, Agence Grafikmente. Design Laurel Parker.
designs: []
fonctions:
- Présenter
formes:
- Œuvre papier
images:
- /media/ledoyen_menu_01.jpg
- /media/ledoyen_menu_02.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Carte de restaurant, l’Abysse, Pavillon Ledoyen
translationKey: abysse-ledoyen
---
