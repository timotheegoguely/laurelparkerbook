---
artist: Emmanuelle Fructrus
client: null
cover: /media/fructus_245_10_-laurelparkerbook.JPG
date: 2021-10-25T00:00:00.000Z
description: >-
  245 s'inscrit dans la tradition du livre en tissu pour enfant et ceux
  d'artistes, telle Louise Bourgeois qui a découpé, cousu, brodé sa garde-robe
  pour fabriquer _l'Ode à l'oubli_. Les pages sont ici réalisées à partir de
  draps chinés au même titre que les photos N&amp;B des précédents tableaux de
  l'artiste. Surfaces sensibles témoignant du réel, le textile et le papier
  photo partagent un même rapport à la matérialité et signifient le temps. Tous
  deux permettent une réappropriation de l'œuvre par le geste.  30,3 x 41,5 cm
  •Sérigraphie 6 couleurs sur draps de lit en coton et lin, couture main,
  couverture détachée avec marquage à chaud • 8 ex + 2 A.P. • Laurel Parker
  Edition, 2021 •1250,00 € TTC
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/245-fructus-lpbedition.jpeg
  - /media/245-fructus-lpbedition-2.jpeg
  - /media/245-fructus-lpbedition-4.jpeg
  - /media/245-fructus-lpbedition-3.jpeg
  - /media/245-fructus-lpbedition-5.jpeg
secteurs:
  - Art contemporain
  - Photo
series:
  - Multiple
shop: true
title: '245, Emmanuelle Fructus'
translationKey: 245-fructus
___mb_schema: /.mattrbld/schemas/projet.json
---

