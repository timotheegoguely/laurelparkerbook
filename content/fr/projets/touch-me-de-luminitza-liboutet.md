---
artist: Luminitza Liboutet
client: Un Livre Une Image
cover: /media/liboutet-touch-me-02.jpg
date: 2009-11-10
description: 'Série de photos de l’artiste roumaine. Design : Emmanuelle Fructus
  et Laurel Parker.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Boîte d’éditeur
images:
- /media/liboutet-touch-me-01.jpg
- /media/liboutet-touch-me-02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
title: Touch Me
translationKey: touch-me
---
