---
artist: Marie Benattar
client: Marie Benattar
cover: /media/benattar-pressbook_01.jpg
date: 2014-09-28T00:00:00.000Z
description: >-
  Press book en cuir. Vis apparentes, avec marquage à sec de la signature de la
  cliente, des tirages numériques sur papier fine art, et une pochette pour des
  cartes de visite.

  Fabrication : Laurel Parker Book.
designs:
  - Semi-sur mesure
fonctions:
  - Présenter
formes:
  - Portfolio
images:
  - /media/benattar-pressbook_01.jpg
  - /media/benattar-pressbook_04.jpg
  - /media/benattar-pressbook_02.jpg
  - /media/benattar-pressbook_03.jpg
secteurs:
  - Photo
series:
  - Unique
shop: false
title: Press book de Marie Benattar
translationKey: /content/fr/projets/press-book-de-marie-benattar.md
___mb_schema: /.mattrbld/schemas/projet.json
---

