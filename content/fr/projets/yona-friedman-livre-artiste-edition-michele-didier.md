---
artist: Yona Friedman
client: mfc Michèle Didier
cover: /media/friedman2-1024x611.jpg
date: 2014-01-01
description: 'Livre d’artiste. Impression offset, découpes laser, reliure à la japonaise.
  Édition limitée à 75 exemplaires + 25 E.A. Les dessins de l’architecte et designer
  Yona Friedman sont détachables grâce à la découpe laser. Edition : mfc Michèle Didier.
  Fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/friedman2-1024x611.jpg
- /media/friedman3web.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: 1001 nuits + 1 jour, de Yona Friedman
translationKey: 1001-nuits-1-jour
---
Impression offset, découpes laser, reliure à la japonaise. 24 × 32,2 cm. Édition limitée à 75 exemplaires + 25 E.A.