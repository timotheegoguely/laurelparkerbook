---
artist: Marina Gadonneix
client: ""
cover: /media/gadonneix-boite-presentation-01.jpg
date: 2013-01-05
description: Une série de boîtes pour l’artiste Marina Gadonneix. Variation de couleurs
  de toile et de marquage à chaud suivant un modèle.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/gadonneix-boite-presentation-01.jpg
- /media/gadonneix-boite-presentation-02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Removed landscapes, Three saftey lights, Crash...
translationKey: removed-landscapes
---
