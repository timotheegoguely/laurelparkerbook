---
artist: Franck Hurley
client: Salto Ulbeek
cover: /media/hurley-endurance-01.jpg
date: 2021-01-06
description: "L’exploration de l’Antarctique par Shackleton à bord de l’Endurance
  entre 1914 et 1917 fut documentée par le photographe Frank Hurley. Les négatifs
  réalisés lors de cette odyssée ont été restaurés, réédités et augmentés de tirages
  inédits pour offrir ce merveilleux projet initié par Salto Ulbeek en collaboration
  avec The Royal Geographical Society et The Scott Polar Research Institute, Cambridge.
  \  \nCe coffret regroupe un livre, deux livrets cousus et 68 tirages, le tout imprimé
  en platine-palladium. Ces différents éléments s’organisent autour d’un plateau amovible
  qui structure l’exploration du coffret. La toile et les rubans tissent des liens
  avec les vêtements des marins et la couleur chaude des tirages platines.  \nEdition
  et tirages, Salto Ulbeek\n\n  \nDesign du coffret, fabrication et marquage à chaud
  réalisés par Laurel Parker Book"
designs:
- Sur mesure
fonctions:
- Présenter
- Éditer
formes:
- Coffret
images:
- /media/hurley-endurance-20.jpg
- /media/hurley-endurance-07.jpg
- /media/hurley-endurance-06.jpg
- /media/hurley-endurance-05.jpg
- /media/hurley-endurance-04.jpg
- /media/hurley-endurance-03.jpg
- /media/hurley-endurance-01.jpg
secteurs:
- Photo
series:
- Multiple
shop: false
title: Shackleton's Endurance
translationKey: shackleton
---
