---
artist: Denis Felix
client: Denis Félix
cover: /media/felix-portfolio-02.jpg
date: 2014-09-21
description: |-
  Coffret de présentation pour le photographe Denis Félix avec échancrure pour format A2. En toile enduite enduite gris clair, marquage à chaud sur une étiquette incrustée.

  Design et fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/felix-portfolio-02.jpg
- /media/felix-portfolio-04.jpg
- /media/felix-portfolio-03.jpg
- /media/felix-portfolio-01.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Coffret de présentation pour Denis Felix
translationKey: denis-felix
---
