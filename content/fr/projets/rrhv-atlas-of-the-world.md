---
title: Atlas of the world RRHV
translationKey: atlas
description: >-
  Un exemplaire de Philips Atlas of the World est réorganisé et relié en
  fonction des schémas de déplacement des réfugiés du monde entier. Le livre
  comprend deux textes de Vahid Davar : ‘Nassim’s Testament’, un poème épique
  sur le voyage d’asile onirique de deux amis de l’Iran au Royaume-Uni, et
  l’essai ‘A Chimerical World with a Nomenclature of its Own’ qui est un
  commentaire sur le poème. Les textes sont placés sur des pages en couvertures
  de survie dorées. La version persane originale de ‘Nassim’s Testament’ est
  inclue dans une découpe au centre du livre. Une série d’images tirées sur
  papier japonais font écho à l’histoire des réfugiés. Édition de 10 copies et 4
  EA. Produit par In Situ - Fabienne Leclerc.
date: '2023-06-01'
client: null
artist: 'Ramin Haerizadeh, Rokni Haerizadeh, Hesam Rahmanian & Vahid Davar'
series:
  - Multiple
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
cover: /media/rrh-atlas-03-site-laurelparkerbook.jpg
images:
  - /media/rrh-atlas-03-site-laurelparkerbook.jpg
  - /media/rrh-atlas-07-web.jpg
  - /media/rrh-atlas-09-web.jpg
  - /media/rrh-atlas-11-site-laurelparkerbook.jpg
  - /media/rrh-atlas-13-web.jpg
  - /media/rrh-atlas-20-web.jpg
shop: false
___mb_schema: /.mattrbld/schemas/projet.json
---

