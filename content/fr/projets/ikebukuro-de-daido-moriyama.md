---
artist: Daido Moriyama
client: Le Bal
cover: /media/moriyama-ikebukuro-01.jpg
date: 2013-11-11
description: "Édition realisée au Print Show organisé au Bal le 9 juillet 2013. Chaque
  visiteur a été invité à créer leur propre livre à partir de 50 images de Daido Moriyama.
  Chaque livre a été assemblé et relié sur place. Série limitée à 200 exemplaires.\n\nConception
  par Pierre Hourquet & Sébastian Hau  \nDesign graphique par Antoine Seiter  \nImpression
  en riso par Après Midi Lab  \nReliure (dos collé carré manuel) par Laurel Parker
  et Paul Chamard"
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/moriyama-ikebukuro-01.jpg
- /media/moriyama-ikebukuro-02.jpg
- /media/moriyama-ikebukuro-03.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Ikebukuro, de Daido Moriyama
translationKey: ikebukuro
---
