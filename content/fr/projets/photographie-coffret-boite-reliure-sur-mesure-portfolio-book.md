---
artist: Sara White Wilson
client: Sara White Wilson
cover: /media/wilson-coffret-presentation-01.jpg
date: 2015-09-21
description: 'Coffret en veau noir avec 8 dossiers de photographies. Dossiers en toile
  doublée de papier pH neutre. Titres marqués à chaud. Avec un cadre intérieur pour
  une carte de présentation. Design et fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
- Coffret
images:
- /media/wilson-coffret-presentation-01.jpg
- /media/wilson-coffret-presentation-02.jpg
- /media/wilson-coffret-presentation-04.jpg
- /media/wilson-coffret-presentation-03.jpg
secteurs:
- Photo
series:
- Unique
shop: false
title: Portfolio de Sara White Wilson
translationKey: portfolio-white-wilson
---
