---
artist: Emmanuelle Fructus
client: Un Livre Une Image
cover: /media/fructus_unlivreuneimage-02.jpg
date: 2020-11-11T00:00:00.000Z
description: >-
  Album à vis contenant 80 cartes postales des recettes d’Emilie Bernard.
  Couverture en papier gaufré, pochette de conservation sur-mesure. Exemplaire
  unique dans une série de cartes postales originales.
designs:
  - Sur mesure
fonctions:
  - Archiver
  - Présenter
formes:
  - Livre
images:
  - /media/fructus_unlivreuneimage-01.jpg
  - /media/fructus_unlivreuneimage-02.jpg
  - /media/fructus_unlivreuneimage-03.jpg
secteurs:
  - Art contemporain
  - Archives
  - Photo
series:
  - Unique
shop: false
title: 'Collection, Un livre - une image'
translationKey: collection-un-livre-une-image
___mb_schema: /.mattrbld/schemas/projet.json
---

