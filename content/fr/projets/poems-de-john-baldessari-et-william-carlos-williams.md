---
artist: John Baldessari et William Carlos Williams
client: Éditions Xavier Barral
cover: /media/baldessari-poems_06.jpg
date: 2013-11-10
description: 'Commandé par Xavier Barral pour les Éditions du Solstice, un club d’édition
  limitée. Format 37 × 47 cm. Chemise à dos arrondi et étui hautes finitions. Marquages
  à chaud. Série de 145 exemplaires. Impressions lithographiques : Imprimerie Idem.
  Design du coffret : Laurel Parker et Xavier Barral. Fabrication du coffret : Laurel
  Parker Book.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Portfolio
- Coffret
images:
- /media/baldessari-poems_06.jpg
- /media/baldessari-poems_05.jpg
- /media/baldessari-poems_02.jpg
- /media/baldessari-poems_04.jpg
- /media/baldessari-poems_03.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Poems, de John Baldessari et William Carlos Williams
translationKey: poems-john-baldessari-willima-carlos-williams
---
