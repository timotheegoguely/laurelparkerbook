---
artist: Pierre Paulin
client: Pierre Paulin
cover: /media/paulin-binding-03.jpg
date: 2017-11-11
description: Livres d'artiste de Pierre Paulin. Veau violet, marquage à froid, 2017.
  Pour son exposition Boom boom, run run, au FRAC Île-de-France, commissaire de l’exposition
  Xavier Franceschi.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/paulin-binding-03.jpg
- /media/paulin-binding-02.jpg
- /media/paulin-white-02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: 1981, 1998,  2008, de Pierre Paulin
translationKey: 1981-pierre-paulin
---
