---
artist: Frédérique Fleurance
client: Fréderique Fleurance
cover: /media/fleurance-boite-presentation-02.jpg
date: 2014-11-10T00:00:00.000Z
description: >-
  Coffret de présentation pour l’artiste plasticienne Frédérique Fleurance.
  Boîte A4 recouverte de papier japonais couleur kraft, marquage à sec, rivets
  nickelés et un élastique pour la fermeture, 2014. Design : d’après des dessins
  de Frédérique Fleurance. Fabrication : Laurel Parker Book.
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Coffret
images:
  - /media/fleurance-boite-presentation-02.jpg
  - /media/fleurance-boite-presentation-04.jpg
  - /media/fleurance-boite-presentation-03.jpg
  - /media/fleurance-boite-presentation-01.jpg
secteurs:
  - Art contemporain
series:
  - Unique
shop: false
title: Coffret de présentation pour Frédérique Fleurance
translationKey: frederique-fleurence
___mb_schema: /.mattrbld/schemas/projet.json
---

