---
artist: Laurel Parker & Paul Chamard
client: Laurel Parker Book
cover: /media/lpb-carnet2018-03.jpg
date: 2017-05-07
description: 'Notre carnet fin d’année 2017 : Un album de 9 maries-louises contenant
  les aperçus de nos projets de l’année. Découpe à la forme et marquage à chaud par
  nos amis chez Créanog, papier Takeo et papier de conservation, emballage en papier
  plié style itajime. Édition de 180 exemplaires numérotés pour nos clients et collaborateurs.
  Design and fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Portfolio
- Livre
images:
- /media/lpb-carnet2018-03.jpg
- /media/lpb-carnet2018-01.jpg
- /media/lpb-carnet2018-02.jpg
- /media/lpb-carnet2018-04.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carnet de fin d’année 2017
translationKey: notebook-2017
---
