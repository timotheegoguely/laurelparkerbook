---
artist: Galerie Art & Project
client: ""
cover: ""
date: 2020-11-10
description: |-
  De 1968 à 1989, la galerie Art & Project édita un bulletin éponyme dans lequel elle proposait à de nombreux artistes de s'exprimer au travers d’une simple feuille de papier. Les premiers numéros faisaient office de carton d’invitation d’expositions à venir mais, rapidement, les artistes ont su se l’approprier pour en faire des objets d’art à part entière. La majorité des bulletins accueillait simplement du texte et des dessins mais le support servit aussi pour des reproductions photographiques (noir et blanc et couleur), des partitions musicales ou même des pliages.

  Création de pochettes sur mesure pour présenter et conserver les bulletins :
  Laurel Parker Book pour l’éditeur Christophe Daviet-Thery.
designs: []
draft: true
fonctions:
- Présenter
- Archiver
formes:
- Boîte éditeur
images: []
secteurs:
- Archives
- Art contemporain
series: []
title: Galerie Art & Project
---
