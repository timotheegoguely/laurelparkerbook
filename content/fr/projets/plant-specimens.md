---
title: Plant Specimens
translationKey: plantspecimens
description: >-
  A selection of plant fibers assembled into sheets in Japan and then
  transformed by hand in France. Plant Specimens, Laurel Parker &amp; Paul
  Chamard. Laurel Parker Editon, Romainville, 2025. Edition of 10 ex + 2 A.P.
  Diverse washi papers, red cedar wood, screw post binding.
date: '2025-01-09'
client: Laurel Parker Edition
artist: Laurel Parker & Paul Chamard
series:
  - Multiple
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
cover: /media/plant-specimens-laurel-parker-book-art-washi-site.jpg
images:
  - /media/plant-specimens-laurel-parker-book-art-washi-site.jpg
shop: true
foldedByHand: null
___mb_schema: /.mattrbld/schemas/projet.json
---

