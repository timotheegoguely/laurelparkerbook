---
title: '5M, 5N, 5B'
translationKey: fructusverre
description: >-
  Verre Diamant feuilleté, tirages argentiques incrustés entre deux films Eva.
  Réalisation de la boîte-socle en toile buckram. Edition de 3 coffrets uniques.
date: '2022-12-05'
client: Emmanuelle Fructus
artist: Emmanuelle Fructus
series:
  - Unique
  - Multiple
secteurs:
  - Photo
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Coffret
cover: /media/fructus-boite-verre-glass-photo-art-web-06.jpg
images:
  - /media/fructus-boite-verre-glass-photo-art-web-06.jpg
  - /media/fructus-boite-verre-glass-photo-art-web-03.jpg
  - /media/fructus-boite-verre-glass-photo-art-web-04.jpg
  - /media/fructus-boite-verre-glass-photo-art-web-05.jpg
  - /media/fructus-boite-verre-glass-photo-art-web-02.jpg
  - /media/fructus-boite-verre-glass-photo-art-web-01.jpg
shop: null
foldedByHand: null
___mb_schema: /.mattrbld/schemas/projet.json
---

