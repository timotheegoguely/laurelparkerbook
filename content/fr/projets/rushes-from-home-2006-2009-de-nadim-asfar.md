---
artist: Nadim Asfar
client: Nadim Asfar
cover: /media/asfar-rushes-from-home-02.jpg
date: 2021-09-29
description: |-
  Livre photographique imprimé sur feuillets translucides. Les textes sont écrits à la machine à écrire sur papier japon. La série est prise dans le quartier de résidence de l'artiste à Beyrouth. La reliure à vis et la chemise de conservation sont fabriquées à l'atelier Laurel Parker Book.

  Ce projet est présenté à la galerie Tanit (Munich) de septembre à novembre 2021.

  Reproductions : Nadim Asfar et Ava du Parc
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/asfar-rushes-from-home-04.jpg
- /media/asfar-rushes-from-home-01.jpg
- /media/asfar-rushes-from-home-05.jpg
- /media/asfar-rushes-from-home-03.jpg
- /media/asfar-rushes-from-home-02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Unique
shop: false
title: Rushes from home 2006-2009, de Nadim Asfar
translationKey: Rushes-from-home
---
