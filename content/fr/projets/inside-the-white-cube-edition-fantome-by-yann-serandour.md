---
artist: Yann Sérandour
client: '&: Christophe Daviet-Thery'
cover: /media/serandour_inside_1.jpg
date: 2009-09-20
description: |-
  Boîte "Édition fantôme" en édition limitée pour contenir les versions anglaise et française de [Inside the White Cube, Édition palimpseste](http://www.jslb.fr/index.php?/book-design/yann-serandour-itwc-edition-fantome/ "JSLB") de Yann Sérandour.  "Designed after Jack W. Stauffacher’s design for Brian O’Doherty _Inside the White Cube_, using Adrian Frutiger’s Méridien typeface. The inside faces of the case are made of a life size printed photograph of the cover of this book and of the cover of the issue of the magazine _Artforum_ in which the first part of Brian O’Doherty essay was first published, giving clues to the book’s layout."

  Design par Jérôme Saint-Loubert Bié. Boîte en toile Cialux avec marquage à chaud, par Laurel Parker. Impression offset par Media Graphic / Les Compagnons du Sagittaire, Rennes. Édition de 50 exemplaires.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Boîte d’éditeur
images:
- /media/serandour_inside_1.jpg
- /media/serandour_inside_2.jpg
- /media/serandour_inside_3.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Inside the White Cube, Édition Fantôme
translationKey: inside-the-white-cube
---
