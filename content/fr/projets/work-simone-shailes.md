---
artist: Simone Shailes
client: Simone Shailes
cover: /media/shailes-book-02.jpg
date: 2021-02-04
description: "Simone Shailes portfolio, 10 livrets aux couvertures colorées et coutures
  assorties dans un étui en toile. Boîte de présentation pour échantillons de matières.
  Impression quadri-numérique, marquage à chaud du titre.\n\n  \nDesign : Simone Shailes\n\nConseils
  et fabrication : Laurel Parker Book"
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
images:
- /media/shailes-book-02.jpg
- /media/shailes-book-09.jpg
- /media/shailes-book-04.jpg
- /media/shailes-book-10.jpg
- /media/shailes-book-08.jpg
- /media/shailes-book-05.jpg
- /media/shailes-book-01.jpg
- /media/shailes-book-06.jpg
secteurs:
- Photo
- Luxe
series:
- Unique
shop: false
title: Work, Simone Shailes
translationKey: work-shailes
---
