---
artist: Laurel Parker & Paul Chamard
client: Laurel Parker Edition
cover: /media/lpb-inside-outside-05.jpg
date: 2020-11-11T00:00:00.000Z
description: >-
  Inside Outside #2, une installation créée suite à notre résidence à la Villa
  Kujoyama. En exposition au festival ! VIVA VILLA ! les vies minuscules,
  Collection Lambert, Avignon, du 24 octobre 2020 au 10 janvier 2021.


  Il s’agit de la reconstitution d’un espace traditionnel japonais, inspiré à la
  fois par les mises en scène de musées et sites patrimoniaux, mais aussi celles
  de théâtres et de films.


  Porté par de fines armatures de bois, chaque élément est composé de papier
  japonais (washi), transformé selon des techniques traditionnelles.


  Une partie de notre expérience au Japon se retrouve dans ces objets fins et
  fragiles : emballages, cloisons, vêtement, motifs inspirés de l’extérieur…
designs:
  - Sur mesure
fonctions:
  - Exposer
formes:
  - Installation
images:
  - /media/lpb-inside-outside-05-1.jpg
  - /media/lpb-inside-outside-02.jpg
  - /media/lpb-inside-outside-01.jpg
  - /media/lpb-inside-outside-04.jpg
  - /media/lpb-inside-outside-03.jpg
  - /media/lpb-inside-outside-06.jpg
  - /media/lpb-inside-outside-07.jpg
  - /media/lpb-inside-outside-02-1.jpg
secteurs:
  - Art contemporain
series:
  - Unique
shop: null
title: 'Inside Outside #2'
translationKey: inside-outside-2
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

