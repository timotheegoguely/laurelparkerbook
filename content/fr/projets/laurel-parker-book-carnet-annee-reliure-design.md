---
artist: Laurel Parker Book
client: ""
cover: /media/lpb-carnet-2013-01.jpg
date: 2020-11-11
description: 'Depuis le début de notre atelier, nous créons un carnet de vœux, un
  cadeau de fin d’année pour nos clients. Le jeu est simple : essayer de mélanger
  des éléments graphiques de divers projets sur lesquels nous avons travaillé tout
  au long de l’année. Ce jeu nous incite à créer des objets étonnants et inattendus.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet-2013-01.jpg
- /media/lpb-carnet-2016-02.jpg
- /media/lpb-carnet2018-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: 'Carnet fin d’année, Laurel Parker Book '
translationKey: notebook-laurel-parker-book
---
