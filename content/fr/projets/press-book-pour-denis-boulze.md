---
artist: Denis Boulze
client: Denis Boulze
cover: /media/boulze_book_01.jpg
date: 2019-11-11T00:00:00.000Z
description: Press book et étui en toile de lin avec marquage à chaud noir.
designs:
  - Semi-sur mesure
fonctions:
  - Présenter
formes:
  - Portfolio
images:
  - /media/boulze_book_01.jpg
  - /media/boulze_book_03.jpg
  - /media/boulze_book_02.jpg
  - /media/boulze_book_04.jpg
secteurs:
  - Photo
series:
  - Unique
shop: false
title: Press book de Denis Boulze
translationKey: pressbook-denis-boulze
___mb_schema: /.mattrbld/schemas/projet.json
---

