---
artist: Nico Therin
client: Nico Therin
cover: /media/therin_book_02.jpg
date: 2019-11-12T00:00:00.000Z
description: >-
  Press book en toile buckram pour l’intérieur et l’extérieur, marquage à chaud
  d’un logo, étui en toile. Fabrication : Laurel Parker Book.
designs:
  - Semi-sur mesure
fonctions:
  - Présenter
formes:
  - Portfolio
images:
  - /media/therin_book_02.jpg
  - /media/therin_book_01.jpg
  - /media/therin_book_03.jpg
secteurs:
  - Photo
  - Art contemporain
series:
  - Multiple
shop: false
title: Press book de Nico Therin
translationKey: pressbook-nico-therin
___mb_schema: /.mattrbld/schemas/projet.json
---

