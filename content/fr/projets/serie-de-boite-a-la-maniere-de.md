---
artist: ""
client: Bibliothèque Littéraire Jacques Doucet
cover: /media/adler-boite-doucet_01.jpg
date: 2014-05-05
description: |-
  Série de boîte « à la manière de » Rose Adler pour la bibliothèque littéraire Jacques Doucet. Dans les années 1920s et 1930s Rose Adler a réalisé des chemises à rabats en parchemin et papier bois japonais qui ont été abimés par le temps. Les nouvelles boîtes de conservations préservent au maximum l’esthétique et les matériaux utilisés par Rose Adler ainsi que la conservation des documents.

  Projet réalisé en partenariat avec l’atelier Coralie Barbe, restauratrice du patrimoine.
  Fabrication : Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/adler-boite-doucet_01.jpg
- /media/adler-boite-doucet_04.jpg
- /media/adler-boite-doucet_02.jpg
secteurs:
- Archives
series:
- Multiple
shop: false
title: Boîtes Rose Adler
translationKey: facsimile-rose-adler
---
