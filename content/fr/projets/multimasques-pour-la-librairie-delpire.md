---
artist: ""
client: Delpire & Co
cover: /media/delpire-multimasques-tigre.jpg
date: 2021-06-15
description: "Multimasques pour la librairie Delpire.  \nMontage des masques Tigre
  et Zèbre contenus dans les archives des éditions Delpire.  \nExposés pour l'ouverture
  de la librairie Delpire."
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Œuvre papier
images:
- /media/delpire-multimasques-tigre.jpg
- /media/delpire-multimasques-zebre.jpg
- /media/delpire-multimasques1.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: Multimasques pour la librairie Delpire & Co
translationKey: multimasques-delpire
---
