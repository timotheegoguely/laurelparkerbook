---
artist: Yann Sérandour
client: '&: Christophe Daviet-Thery'
cover: /media/serandour-open-cube-01.jpg
date: 2010-01-05
description: Cette boîte proposée sous la forme d’une édition ouverte numérotée peut
  contenir jusqu’à douze exemplaires du livre de Sol LeWitt, Incomplete Open Cubes
  (The John Weber Gallery, New York, 1974). Son possesseur est invité à compléter
  la présente édition en y ajoutant les exemplaires du livre de Sol LeWitt qu’il aura
  pu collecter. Livret imprimé par Les Compagnons du Sagittaire, Rennes. Boîte en
  carton qualité archivage recouvert du papier couché.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/serandour-open-cube-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Incomplete Open Cubes, par Yann Sérandour
translationKey: incomplete-open-cubes
---
