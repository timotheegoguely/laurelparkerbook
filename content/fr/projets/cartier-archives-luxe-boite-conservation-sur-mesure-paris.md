---
artist: null
client: Cartier
cover: /media/cartier_boite-plume_01.jpg
date: 2018-09-16T00:00:00.000Z
description: >-
  Boîtes de conservation pour la collection historique de la maison Cartier.
  _Plumes_, coffret avec compartiments sur mesure et plateaux amovibles qui
  rassemble plusieurs boites présentant des plumes de martin-pêcheur sous leur
  forme naturelle et transformée. Ces éléments font partie de la collection
  historique de la maison Cartier. Nous sommes heureux de collaborer
  régulièrement avec les archives de la maison Cartier et contribué à conserver
  les albums photographiques de Jacques Cartier, les dessins et registres qui
  sont un témoignage important pour l’histoire de cette maison. Nos boîtes sont
  en pH neutre et composées de matériaux qualité archivage dans un respect total
  des normes muséales.


  Design et fabrication : Laurel Parker Book.
designs:
  - Sur mesure
fonctions:
  - Archiver
formes:
  - Coffret
images:
  - /media/cartier_boite-plume_01.jpg
  - /media/cartier_boite-plume_02.jpg
secteurs:
  - Archives
  - Luxe
series:
  - Unique
shop: false
title: 'Boîtes de conservation, Cartier'
translationKey: boite-cartier
___mb_schema: /.mattrbld/schemas/projet.json
---

