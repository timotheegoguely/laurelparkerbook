---
artist: Victoria Viennet
client: Victoria Viennet
cover: /media/viennet-pressbook-03.jpg
date: 2015-11-11T00:00:00.000Z
description: >-
  Press book en toile avec marquage à chaud couleur cuivre pour le diplôme de
  Victoria Viennet à l’école Icart Photo, 2015.
designs:
  - Semi-sur mesure
fonctions:
  - Présenter
formes:
  - Portfolio
images:
  - /media/viennet-pressbook-03.jpg
  - /media/viennet-pressbook-02.jpg
  - /media/viennet-pressbook-01.jpg
secteurs:
  - Photo
series:
  - Unique
shop: false
title: Press book de diplôme pour Victoria Viennet
translationKey: pressbook-victoria-viennet
___mb_schema: /.mattrbld/schemas/projet.json
---

