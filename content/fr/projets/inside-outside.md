---
artist: Laurel Parker et Paul Chamard
client: Laurel Parker Edition
cover: /media/lpb-kujoyama-02.jpg
date: 2019-11-11T00:00:00.000Z
description: >-
  Installation à la Nuit Blanche, Le Terminal, Kyoto, Japon, octobre 2019. Suite
  à la residence de Laurel Parker et Paul Chamard à la Villa Kujoyama. Papier
  japonais (washi), bois de cèdre japonais, fil or, impression en poudre de
  mica.


  Si en Occident le papier est une matière d’écriture, il devient au Japon une
  cloison qui scinde l’espace en deux à l’aide de fines armatures en bois.
  Laurel Parker et Paul Chamard ont interrogé ces utilisations par une réflexion
  autour du rapport entre espace privé et public. Les imprimés des intérieurs
  sont inspirés de motifs extérieurs issus de la nature dont l’utilisation est
  très codifiée au Japon. Quels motifs peuvent être extraits du paysage urbain
  d’une ville du XXIème siècle comme Kyoto ?
designs:
  - Sur mesure
fonctions:
  - Exposer
formes:
  - Œuvre papier
  - Installation
images:
  - /media/lpb-kujoyama-02.jpg
  - /media/lpb-kujoyama-04.jpg
  - /media/lpb-kujoyama-03.jpg
  - /media/lpb-kujoyama-07.jpg
  - /media/lpb-kujoyama-05.jpg
  - /media/lpb-kujoyama-01.jpg
  - /media/lpb-kujoyama-06.jpg
secteurs:
  - Art contemporain
series:
  - Unique
shop: null
title: Inside - Outside
translationKey: inside-outside
foldedByHand: true
___mb_schema: /.mattrbld/schemas/projet.json
---

