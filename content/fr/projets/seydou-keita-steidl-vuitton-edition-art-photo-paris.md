---
title: 'Seydou Keïta, Photographs, Bamako, Mali 1949-1970'
artist: Seydou Keïta
client: Éditions Louis Vuitton
cover: /media/keita-coffret-vuitton_02.jpg
date: 2012-09-17T00:00:00.000Z
description: >-
  Édition limitée du livre _Seydou Keïta, Photographs, Bamako, Mali, 1949-1970_
  (Steidl-dangin). Coffret de luxe contenant le livre et un tirage argentique
  numéroté produit par le studio Box. Éditions Louis Vuitton, Paris, 2012, 50
  exemplaires + 10 A.P 


  Design : Studio Graphique, Louis Vuitton. Fabrication : Laurel Parker Book.
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Coffret
images:
  - /media/keita-coffret-vuitton_02.jpg
  - /media/keita-coffret-vuitton_03.jpg
  - /media/keita-coffret-vuitton_01.jpg
secteurs:
  - Photo
  - Art contemporain
series:
  - Multiple
shop: false
translationKey: seydou-keita
___mb_schema: /.mattrbld/schemas/projet.json
---

