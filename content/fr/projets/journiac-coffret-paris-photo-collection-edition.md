---
artist: Michel Journiac
client: Galerie Christophe Gaillard
cover: /media/journiac_24hours-02.jpg
date: 2018-11-11T00:00:00.000Z
description: >-
  Coffret en édition limitée de 42 photographies (Réalités & Phantasmes) sous
  marie-louis. Édition réalisée dans le cadre de l’exposition de Michel Journiac
  à la Galerie Christophe Gaillard. Édition de 10 ex + 2 HC + 2 AP.


  Tirages barytés noir & blanc sur papier Ilford développés à la main par Cadre
  en Seine Choi. Passe-partouts par Circad. Design et fabrication du coffret,
  marquage à chaud sur passe-partouts, Laurel Parker Book. Conception graphique
  par Camille Morin.
designs:
  - Sur mesure
fonctions:
  - Archiver
  - Présenter
  - Éditer
formes:
  - Coffret
images:
  - /media/journiac_24hours-02.jpg
  - /media/journiac_24hours-01.jpg
  - /media/journiac_24hours-marquage.jpg
secteurs:
  - Photo
  - Art contemporain
series:
  - Multiple
shop: false
title: '24 Heures dans la vie d’une femme ordinaire, de Michel Journiac'
translationKey: 24h
___mb_schema: /.mattrbld/schemas/projet.json
---

