---
artist: documentation céline duval
client: documentation céline duval
cover: /media/duval-la-revue-en-4-images_02.jpg
date: 2015-11-11
description: 'Reliure de l’ensemble des 60 numéros de _La revue en 4 images_ de documentation
  céline duval. [Chaque revue est montée sur onglet](https://www.doc-cd.net/publications/books/revue-en-4-images-reliee/
  "documentation céline duval"), ce qui permet leur ouverture, et cousue sur ruban.
  Les éléments de reliure visibles font partie du design de l’objet. Il s’agit d’un
  livre pour des institutions et des collections privées qui souhaitent archiver et
  présenter cette belle revue dans son intégralité. Design et fabricaction : Laurel
  Parker Book.'
designs:
- Sur mesure
fonctions:
- Archiver
- Éditer
formes:
- Livre
images:
- /media/duval-la-revue-en-4-images_02.jpg
- /media/duval-la-revue-en-4-images_07.jpg
- /media/duval-la-revue-en-4-images_06.jpg
- /media/duval-la-revue-en-4-images_01.jpg
- /media/duval-la-revue-en-4-images_03.jpg
- /media/duval-la-revue-en-4-images_05.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: La revue en 4 images, de documentation céline duval
translationKey: la-revue
---
