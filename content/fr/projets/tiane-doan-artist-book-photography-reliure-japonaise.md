---
title: >-
  “Persuade everyone to see all the pictures that make you feel joyful”, de
  Tiane Doan na Champassak
translationKey: livreindigo
description: >-
  Encre de gravure appliquée sur 45 impressions jet d'encre d'archives. Reliure
  japonaise par Laurel Parker Book. Support en plexiglas recouvert de bois par
  Maonia. Papier japonais Awagami. Exemplaire unique 41,3 x 37 x 1,5 cm
date: 2024-02-28
client: Tiane Doan na Champassak
artist: Tiane
series:
  - Unique
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
cover: /media/tiane-indigo-book-08-site.jpg
images:
  - /media/tiane-indigo-book-08-site.jpg
  - /media/tiane-indigo-book-06-site.jpg
  - /media/tiane-indigo-book-04-site.jpg
  - /media/tiane-indigo-book-03-site.jpg
shop: false
foldedByHand: false
___mb_schema: /.mattrbld/schemas/projet.json
---

