---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-carnet_2010.jpg
date: 2010-11-11
description: 'Notre carnet fin d’année 2010 : couture de conservation, couverture
  dépliante avec un dessin de l’artiste Franck Léonard en marquage à chaud. Édition
  de 90 exemplaires. Design et fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet_2010_02.jpg
- /media/lpb-carnet_2010.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carnet de fin d’année 2010
translationKey: notebook-2010
---
