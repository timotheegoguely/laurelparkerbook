---
artist: Amélie Ambroise
client: Amélie Ambroise
cover: /media/ambroise_portfolio_01.jpg
date: 2009-11-11
description: Rose gourmande d’Amélie Ambroise, étudiante à Icart Photo en 2009. Tirages
  photo en marie-louise en papier, dessins originaux de l’artiste sur les rectos,
  reliure sur onglet en accordéon, tissu coton, chemise en toile avec pochoir et coussins.
  Design, pochoir, dessins, coussins, et recherches graphiques de la cliente. Reliure
  par Laurel Parker. Jugé meilleur book de sa promotion.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/ambroise_portfolio_01.jpg
- /media/ambroise_portfolio_02.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Livre de diplôme d’Amélie Ambroise
translationKey: livre-amelie-ambroise
---
