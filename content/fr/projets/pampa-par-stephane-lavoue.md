---
artist: Stéphane Lavoué
client: Picto Foundation
cover: /media/lavoue_pampa_02.jpg
date: 2018-11-11
description: 'Une série de livres d’artiste réalisé dans le cadre du Prix Niépce 2018.
  Tirages numériques sur papier japonais, Picto Laboratoire. Reliure sur onglets,
  plats rapportés. Gardes en papier marbré, toile italienne. Avec le soutien de la
  Picto Foundation, mécène du Prix Niépce, et les Gens d’images. Design d’objet et
  design graphique : Laurel Parker '
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lavoue_pampa_02.jpg
- /media/lavoue_pampa_01.jpg
- /media/lavoue_pampa_03.jpg
- /media/_40B9194.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Pampa de Stéphane Lavoué
translationKey: pampa
---
