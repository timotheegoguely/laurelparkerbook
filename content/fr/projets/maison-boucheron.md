---
artist: null
client: Boucheron
cover: /media/boucheron-01.jpg
date: 2009-10-20T00:00:00.000Z
description: >-
  Albums marie-louise pour la présentation des dessins sur calque de la maison
  Boucheron. Système d’insertion des dessins par le haut, pages en carte
  recouverte de toile, extérieur en plein veau gaufré avec dos arrondi. Design
  de la structure et fabrication : Laurel Parker.
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Portfolio
images:
  - /media/boucheron-01.jpg
  - /media/boucheron-02.jpg
  - /media/boucheron-album-05.jpg
  - /media/boucheron7.jpg
  - /media/boucheron17.jpg
secteurs:
  - Luxe
series:
  - Multiple
shop: false
title: 'Album de présentation, haute joaillerie, Boucheron '
translationKey: album-boucheron
___mb_schema: /.mattrbld/schemas/projet.json
---

