---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-carnet_2012.jpg
date: 2012-11-11
description: 'Notre carnet fin d’année 2012 : couture copte, première de couverture
  en papier bleu, quatrième de couverture en veau noir, cahiers en papiers divers,
  tirages numériques, et marquages à chaud. Édition de 100 exemplaires. Colophon :
  une adaptation directe du colophon de "Mad Marginal / Archives" de Dora Garcia,
  design graphique par Jean-Claude Chianale. Design et fabrication : Laurel Parker
  Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet_2012.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carnet de fin d’année 2012
translationKey: notebook-2012
---
