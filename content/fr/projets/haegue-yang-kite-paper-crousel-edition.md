---
title: Mesmerizing Kite – Acrobat in Butterfly-Bat Grip
translationKey: kite
description: >-
  Hanji, papier millimétré sur cerf-volant coréen traditionnel, bobine de pin,
  corde de cerf-volant, composants en érable. Édition de 50 + 5AP. Boîte
  comprenant un couvercle, un plateau amovible qui contient le cerf-volant et un
  compartiment caché qui contient le support. Toile de coton glacée et
  sérigraphie. Conception &amp; Fabrication du coffret : Laurel Parker Book.
  Production : Galerie Chantal Crousel. Photos : Jiayun Deng — Galerie Chantal
  Crousel.
date: '2022-12-23'
client: Galerie Chantal Crousel
artist: Haegue Yang
series:
  - Multiple
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Coffret
cover: /media/haegue-yang-kite-paper-crousel-01.jpg
images:
  - /media/haegue-yang-kite-paper-crousel-01.jpg
  - /media/haegue-yang-kite-paper-crousel-04.jpg
  - /media/haegue-yang-kite-paper-crousel-05.jpg
  - /media/haegue-yang-kite-paper-crousel-02.jpg
  - /media/haegue-yang-kite-paper-crousel-03.jpg
shop: null
foldedByHand: null
___mb_schema: /.mattrbld/schemas/projet.json
---

