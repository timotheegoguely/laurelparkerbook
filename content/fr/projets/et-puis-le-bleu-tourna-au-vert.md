---
artist: Yann Sérandour
client: Laurel Parker Edition
cover: /media/serandour_bleu_08_w.jpg
date: 2022-09-30T00:00:00.000Z
description: >-
  Cette longue frise colorée est composée par l’alignement d’une collection de
  retirages successifs du même ouvrage de Sartre : _Esquisse d’une théorie des
  émotions_, publié aux éditions Hermann. Les teintes des couvertures ont été
  ordonnées dans l’ordre chronologique des différentes réimpressions du livre
  entre 1960 et 1995. Virant curieusement du bleu au vert, elle suit la
  dégradation progressive du design original de Frutiger et retrace par ses
  marques d’usage et de lecture la réception du texte de Sartre au fil du temps.
  Leporello, 66 pages, 25 × 19 cm (fermé), 1254 cm déployé, mise en pages Marine
  Bigourie, impression Média Graphic, Rennes • 15 exemplaires + 4 HC • Laurel
  Parker Edition, 2022 •  1254,00 € TTC
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/serandour_bleu_03_w.jpg
  - /media/serandour_bleu_02_w.jpg
  - /media/serandour_bleu_08_w.jpg
  - /media/serandour_bleu_06_w.jpg
  - /media/serandour_bleu_02_atgregory_copitet.jpg
  - /media/serandour_bleu_11_w.jpg
  - /media/serandour_bleu_01_atgregory_copitet.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: Et puis le bleu tourna au vert
translationKey: yann-emotions
___mb_schema: /.mattrbld/schemas/projet.json
---

