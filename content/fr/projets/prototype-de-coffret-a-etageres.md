---
artist: Laurel Parker Book
client: Royal Mansour
cover: /media/royal-mansour-coffret-etageres-02.jpg
date: 2014-06-03
description: 'Coffret contenant une édition de trois livres pour le Royal Mansour
  à Marrakech. Design et fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/royal-mansour-coffret-etageres-02.jpg
- /media/royal-mansour-coffret-etageres-01.jpg
secteurs:
- Luxe
series:
- Prototype
shop: false
title: Prototype de coffret à tiroirs
translationKey: coffret-royal-mansour
---
