---
artist: null
client: Studio KO
cover: /media/roi-maroc_coffret-01.jpg
date: 2014-05-04T00:00:00.000Z
description: >-
  Coffrets de luxe sur mesure pour tirages 50 × 65 cm. Planches d’architecte
  d’intérieur du Studio KO (Karl Fournier & Olivier Marty), pour un palais privé
  au Maroc. Incrustation d’un portfolio dans le couvercle. Toile noire, papier
  vergé allemand et marquage à chaud en or. Série de 3 coffrets et 20
  portfolios.
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Coffret
  - Portfolio
images:
  - /media/roi-maroc_coffret-01.jpg
  - /media/ko-maroc-coffret-04.jpg
  - /media/ko-maroc-coffret-02.jpg
  - /media/ko-maroc-coffret-01.jpg
  - /media/ko-maroc-coffret-0.jpg
  - /media/ko-maroc-coffret-03.jpg
secteurs:
  - Luxe
series:
  - Multiple
shop: false
title: Coffret de luxe pour un architecte d'intérieur
translationKey: studio-ko
___mb_schema: /.mattrbld/schemas/projet.json
---

