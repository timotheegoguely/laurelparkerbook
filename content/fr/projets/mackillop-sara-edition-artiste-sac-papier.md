---
artist: Sara MacKillop
client: Laurel Parker Edition
cover: /media/mackillop-bag-01.jpg
date: 2018-10-18T00:00:00.000Z
description: >-
  Nous avons invité l’artiste anglaise Sara MacKillop à dessiner un sac de
  caisse pour notre stand au Salon MAD 2018. Suite à ses livres d’artiste IKEA
  Kitchens, IKEA Baths, et IKEA Wardrobes, Sara MacKillop a créé un sac sur le
  même thème. Format 41 × 32 cm • Impression sérigraphie 2 couleurs recto/verso
  • Laurel Parker Edition, 2018 • 9,99 € TTC
designs:
  - Semi-sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
images:
  - /media/mackillop-bag-01.jpg
  - /media/mackillop-bag-02.jpg
  - /media/mackillop-bag-serigraphy.jpg
  - /media/mackillop-bag-serigraphy-02.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: 'Bag, de Sara MacKillop'
translationKey: bag-sara-mackillop
___mb_schema: /.mattrbld/schemas/projet.json
---

