---
artist: ""
client: ENSBA-École de Beaux-Arts de Paris
cover: /media/ensba-boite-a-seaux-02.jpg
date: 2020-11-11
description: |-
  Boîte de conservation sur mesure en toile buckram et comblage plastazote de qualité archivage.
  Réalisée pour le conditionnement d’une archive historique de l’École de Beaux-Arts de Paris : l’ordonnance de fondation de l’Académie royale de peinture et de sculpture, en parchemin munie de nombreux sceaux.

  Alexandre Leducq, conservateur en charge des manuscrits et imprimés anciens.
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/ensba-boite-a-seaux-01.jpg
- /media/ensba-boite-a-seaux-02.jpg
- /media/ensba-boite-a-seaux-03.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: Boîte de conservation pour documents en parchemin
translationKey: boîte-sceaux
---
