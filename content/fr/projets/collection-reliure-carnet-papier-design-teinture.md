---
artist: ""
client: Calligrane
cover: /media/lpb-calligrane-itajime-01.jpg
date: 2003-11-11
description: Collection de carnets et papiers teintés à la main pour la boutique Calligrane,
  Paris, 2003
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-calligrane-itajime-01.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Collection itajimé pour Calligrane
translationKey: calligrane-itajime
---
