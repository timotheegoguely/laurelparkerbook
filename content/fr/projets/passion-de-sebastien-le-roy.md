---
artist: Sébastien Le Roy
client: Les Paroles Gelées
cover: /media/roy-passion_01.jpg
date: 2016-11-11
description: 'Livre d’artiste. Impression de 44 gravures originales sur lino. Le
  système de reliure, qui date du Moyen-Age, est sans colle et permet une très bonne
  ouverture du livre. Carte japonaise de Takeo. Design et fabrication : Laurel Parker
  Book.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/roy-passion_01.jpg
- /media/roy-passion_03.jpg
- /media/roy-passion_02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Passion, de Sébastien Le Roy
translationKey: passion-sebastien-le-roy
---
