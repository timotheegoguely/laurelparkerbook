---
artist: Koenraad Dedobbeleer
client: '&: Christophe Daviet-Thery'
cover: /media/dedobbeleer-space-02.jpg
date: 2011-10-31
description: "Livre d’artiste qui peut avoir une lecture de livre (page par page)
  ou d’un espace (debout en volume).\n\"Une approche visuelle de l’univers artistique
  de Koenraad Dedobbeleer fait de formes reprises d’objets du quotidien, de détails
  architecturaux et de modèles sculpturaux (livre d’artiste). Ces visuels d’objets
  créent un réseau imbriqué de combinaisons et de mouvements sans cesse renouvelés.
  Œuvre d’art à part entière, cette édition démontre que ces modulations ne s'effectuent
  jamais de manière évidente mais qu’au contraire, l’artiste crée un lien par lequel
  les formes s'interconnectent.\"— les presses du réel\nLeporello. 200 exemplaires.
  \nTirages offset par Cultura, Belgique. \nReliure par Laurel Parker Book."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/dedobbeleer-space-02.jpg
- /media/dedobbeleer-space-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
title: Space Has No Meaning Outside of Time
translationKey: space-has-no-meaning-outside-of-time
---
