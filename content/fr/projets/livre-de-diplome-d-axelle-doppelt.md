---
artist: Axelle Doppelt
client: Axelle Doppelt
cover: /media/doppelt-pop-up-01.jpg
date: 2015-11-11
description: Livre de diplôme d’Axelle Doppelt, ESAD Penninghen, 2015. Pop-ups de
  l'étudiante. Mise en place du design de la reliure pour un livre pop-up, montage
  des pages, emboîtage avec plats de couverture en plexi.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/doppelt-pop-up-01.jpg
- /media/doppelt-pop-up-02.jpg
- /media/doppelt-pop-up-03.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: Livre de diplôme d’Axelle Doppelt
translationKey: livre-axelle-doppelt
---
