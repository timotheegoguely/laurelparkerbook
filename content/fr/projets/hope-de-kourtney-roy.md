---
artist: Kourtney Roy
client: Galerie Catherine et André Hug
cover: /media/kourtney_roy_hug_02.jpg
date: 2017-11-11
description: 'Coffret en papiers gaufrés (Colorplan) marquage à chaud rouge. Le coffret
  reprend l’univers et les couleurs de l’artiste. Design et fabrication : Laurel
  Parker Book.'
designs:
- Sur mesure
fonctions:
- Éditer
- Présenter
formes:
- Coffret
images:
- /media/kourtney_roy_hug_02.jpg
- /media/kourtney_roy_hug_03.jpg
- /media/kourtney_roy_hug_01.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Hope, de Kourtney Roy
translationKey: hope
---
