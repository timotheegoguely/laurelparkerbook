---
artist: Andreas Varnavides
client: Andreas Varnavides
cover: /media/andreas_camerasolitude_01.jpg
date: 2019-11-11T00:00:00.000Z
description: >-
  Edition du photographe Andreas Varnavides. Boîte recouverte de la toile avec
  impression numérique UV. Édition de 30 + 5 épreuves d’artiste. Impressions
  pigmentaires sur papier Hahnemuehle.


  Design graphique : Marine Bigourie  

  Fabrication : Laurel Parker Book
designs:
  - Semi-sur mesure
fonctions:
  - Éditer
formes:
  - Boîte d’éditeur
images:
  - /media/andreas_camerasolitude_03.jpg
  - /media/andreas_camerasolitude_01.jpg
secteurs:
  - Art contemporain
  - Photo
series:
  - Multiple
shop: false
title: 'Camera Solitude, d’Andreas Varnavides'
translationKey: camera-solitude
___mb_schema: /.mattrbld/schemas/projet.json
---

