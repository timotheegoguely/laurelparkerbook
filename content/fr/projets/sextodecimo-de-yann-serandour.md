---
artist: Yann Sérandour
client: '&: Christophe Daviet-Thery'
cover: /media/serandour-sextodecimo-03.jpg
date: 2013-11-10
description: Livre d'artiste. Édité par Christophe Daviet-Thery et le Cneai=, Chatou.
  Édition ouverte. Partant de l’affiche de l’éditeur italien Corraini, reproduisant
  la célèbre série de photographies de Bruno Munari Recherche de confort dans un fauteuil
  inconfortable, initialement publiées dans le magazine Domus en 1944, Yann Sérandour
  a réalisé une brochure, en repliant l’affiche sous forme d’un cahier de 32 pages,
  alternant images imprimées et espaces vides (le verso de l’affiche).
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
- Œuvre papier
images:
- /media/serandour-sextodecimo-03.jpg
- /media/serandour-sextodecimo-02.jpg
- /media/serandour-sextodecimo-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Sextodecimo
translationKey: sextodecimo
---
