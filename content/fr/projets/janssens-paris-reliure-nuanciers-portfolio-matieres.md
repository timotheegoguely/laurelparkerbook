---
artist: ""
client: Philippine Janssens
cover: /media/janssens_-classeur-02.jpg
date: 2019-11-11
description: |-
  Classeurs de présentation pour la boutique Philippine Janssens, Paris.
  Toile dorée, anneaux teintés couleur laiton, marquage à chaud.
  Fabrication et recherche matières : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
images:
- /media/janssens_-classeur-02.jpg
- /media/janssens_-classeur-01.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Classeurs de présentation pour Philippine Janssens
translationKey: janssens
---
