---
artist: Yann Sérandour
client: Laurel Parker Edition
cover: /media/serandour_ftf_07.jpg
date: 2018-06-05T00:00:00.000Z
description: >-
  Une « boite à chasses » recouverte d’une toile camouflage réunit un ensemble
  de huit planches d’herbiers reproduites à taille réelle (A3) représentant des
  spécimens de roseaux communs collectés à travers le monde. Chaque planche
  réimprimée sur un papier japon a été pliée pour rentrer dans le format B4 de
  l’édition en suivant les lignes brisées de la plante. Format boîte 38 × 27 cm
  8 • tirages jet d’encre pigmentaires, tissu camouflage, carton d’archivage •
  15 exemplaires + 3 A.P • Laurel Parker Edition, 2018 • 2300 € TTC
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
  - Coffret
images:
  - /media/serandour_ftf_07.jpg
  - /media/serandour_ftf_01.jpg
  - /media/serandour_ftf_08.jpg
  - /media/serandour_ftf_05.jpg
  - /media/serandour_ftf_06.jpg
  - /media/serandour_ftf_09.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: 'Folded to fit, par Yann Sérandour'
translationKey: folded-to-fit
___mb_schema: /.mattrbld/schemas/projet.json
---

