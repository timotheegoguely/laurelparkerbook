---
title: 'Miroirs Noirs RVB CMJ NB '
translationKey: miroirs-noirs
description: >-
  Ensemble de 8 miroirs noirs. Verre convexe float argenté noir de fumée en
  vis-à-vis de tissus et soies colorés. Étui en carton de conservation,
  recouvert de toile Buckram.
date: '2022-11-01'
client: Emmanuelle Fructus
artist: Emmanuelle Fructus
series:
  - Unique
secteurs:
  - Photo
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Coffret
cover: /media/fructus_miroir_noir_01_w.jpg
images:
  - /media/fructus_miroir_noir_01_w.jpg
  - /media/fructus_miroir_noir_04_w.jpg
  - /media/fructus_miroir_noir_09_w.jpg
  - /media/miroir_noir_at_copitet_02_w.jpg
  - /media/miroir_noir_at_copitet_01_w.jpg
shop: null
foldedByHand: null
___mb_schema: /.mattrbld/schemas/projet.json
---

