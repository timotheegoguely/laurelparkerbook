---
artist: Robert Desnos
client: Bibliothèque Littéraire Jacques Doucet
cover: /media/desnos-les-sommeils_03.jpg
date: 2017-06-06
description: |-
  Coffret pour _Les Sommeils,_ dessins et écrits de Robert Desnos, collection de la Bibliothèque Littéraire Jacques Doucet. Restauration des dessins originaux par l’atelier Coralie Barbe, restauratrice de livres et d’œuvres sur papier. Coffret à étagères contenant cinq boites, le tout conforme aux normes muséographiques.

  Design et fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/desnos-les-sommeils_02.jpg
- /media/desnos-les-sommeils_04.jpg
- /media/desnos-les-sommeils_03.jpg
- /media/desnos-les-sommeils_01.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: Les Sommeils de Robert Desnos
translationKey: les-sommeils-robert-desnos
---
