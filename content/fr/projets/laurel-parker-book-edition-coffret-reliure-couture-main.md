---
artist: ""
client: Laurel Parker Book
cover: /media/collection-boxes-01.jpg
date: 2020-11-11
description: "Développé à l’atelier, ce coffret présente quatre types de conditionnement
  à usage des archives et des collections. Chaque boite de conservation est taillée
  sur-mesure, composée de matériaux neutres et développée pour suivre une logique
  de collection.\n\nCes modèles ont été conçus pour différents documents : \n\nBoîte
  Déméter – pour des volumes lourds et fragiles   \nBoîte Daphné – pour des séries
  de minuscules et de petits formats   \nPortfolio Artémis – pour des feuillets volants
  et documents fins   \nÉtui Gaïa – adapté à chaque format\n\nCoffret à destination
  des conservateurs, _contactez-nous pour en bénéficier_."
designs:
- Semi-sur mesure
fonctions:
- Archiver
formes:
- Portfolio
- Chemise
- Coffret
images:
- /media/collection-boxes-01.jpg
secteurs:
- Archives
series:
- Multiple
shop: false
title: Collection Case
translationKey: collection-case
---
