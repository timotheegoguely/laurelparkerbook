---
artist: Marwa Arsanios
client: Galerie Mor-Charpentier
cover: /media/arsanios-98-thesis-for-translation-03.jpg
date: 2014-11-10T00:00:00.000Z
description: >-
  Livre unique. Pages collées sur onglets et tenues par des vis, toile de
  qualité archivage, 2014. Présenté lors d’une exposition simplement relié par
  des anneaux, il a fallu trouver une solution pour préserver ce livre d’artiste
  tout en gardant une manipulation aisée. Pour la galerie Mor-Charpentier.
  Design et fabrication : Laurel Parker Book.
designs:
  - Sur mesure
fonctions:
  - Éditer
  - Archiver
formes:
  - Coffret
  - Livre
images:
  - /media/arsanios-98-thesis-for-translation-03.jpg
  - /media/arsanios-98-thesis-for-translation-01.jpg
  - /media/arsanios-98-thesis-for-translation-04.jpg
  - /media/arsanios-98-thesis-for-translation-02.jpg
secteurs:
  - Art contemporain
series:
  - Unique
shop: false
title: '98 Thesis for Translation, de Marwa Arsanios'
translationKey: 98-thesis-for-translation
___mb_schema: /.mattrbld/schemas/projet.json
---

