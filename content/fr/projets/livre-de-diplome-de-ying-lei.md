---
artist: Ying Lei
client: Ying Lei
cover: /media/lei-livre-diplome-01.jpg
date: 2015-11-11
description: Livre de diplôme en photo aux Gobelins - École de l'image. Tirages numériques,
  couture sur ruban, emboîtage toile, incrustation d’une image en cuvette dans la
  couverture.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lei-livre-diplome-01.jpg
- /media/lei-livre-diplome-02.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: Livre de diplôme de Ying Lei
translationKey: livre-ying-lei
---
