---
artist: Agnes Geoffray
client: Agnes Geoffray
cover: /media/geoffray-boite-vitrine-01.jpg
date: 2021-04-15
description: Nouvelle boîte vitrine présentant une œuvre sur soie de l’artiste Agnes
  Geoffray. Boîte à couvercle en toile et papier de qualité archivage, plexiglas sur
  mesure. Sur le modèle d’une boite de specimen
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/geoffray-boite-vitrine-01.jpg
- /media/geoffray-boite-vitrine-02.jpg
- /media/geoffray-boite-vitrine-03.jpg
secteurs:
- Art contemporain
series:
- Unique
shop: false
title: Boîte vitrine pour Agnes Geoffray
translationKey: vitrine-geoffray
---
