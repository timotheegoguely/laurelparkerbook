---
artist: Laurel Parker Book
client: Philippine Janssens
cover: /media/janssens_pressbook_03.jpg
date: 2018-11-11T00:00:00.000Z
description: >-
  Un ensemble de pressbooks et coffrets pour la boutique Philippine Janssens rue
  du Faubourg Saint-Honoré et son espace au Bon Marché. Cuir rose, toile enduite
  couleur laiton, marquage à chaud, rubans doré et rose, intercalaire doublé en
  toile enduite laiton, 2018. Fabrication et recherche matière : Laurel Parker
  Book.
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Coffret
  - Portfolio
images:
  - /media/janssens_pressbook_03.jpg
  - /media/janssens_pressbook_01.jpg
  - /media/janssens_pressbook_05.jpg
  - /media/janssens_pressbook_02.jpg
  - /media/janssens_pressbook_04.jpg
  - /media/janssens_pressbook_06.jpg
secteurs:
  - Luxe
series:
  - Multiple
shop: false
title: 'Portfolios pour boutiques Philippine Janssens '
translationKey: portfolio-philippine-janssens
___mb_schema: /.mattrbld/schemas/projet.json
---

