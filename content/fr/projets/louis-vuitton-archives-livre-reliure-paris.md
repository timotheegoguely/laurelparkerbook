---
artist: ""
client: Louis Vuitton – Studio Graphique
cover: /media/vuitton-reliure-coptes_01.jpg
date: 2016-09-18
description: 'Reliures coptes des documents pour les archives de Louis Vuitton. Commande
  du Studio Graphique, Louis Vuitton. Fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Livre
images:
- /media/vuitton-reliure-coptes_01.jpg
secteurs:
- Archives
- Luxe
series:
- Multiple
shop: false
title: Reliures pour les archives de Louis Vuitton
translationKey: archives-vuitton
---
