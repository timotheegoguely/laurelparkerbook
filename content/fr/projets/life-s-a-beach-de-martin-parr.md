---
artist: Martin Parr
client: Éditions Xavier Barral
cover: /media/parr-prototype-02.jpg
date: 2012-10-23
description: 'Prototypes pour l’édition _Life’s a Beach_ de Martin Parr. Co-édition
  d’Aperture Foundation et les Éditions Xavier Barral. Album photo avec une reliure
  à la japonais de 4 trous. Photos insérées dans des fentes. Design : Xavier Barral
  Graphiste : Aurélie Vitoux . Prototypage, création des maquettes et vidéos pour
  la fabrication industrielle : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/parr-prototype-02.jpg
- /media/parr-prototype-01.jpg
- /media/parr-prototype-03.jpg
- /media/parr-prototype-05.jpg
- /media/parr-prototype-04.jpg
secteurs:
- Photo
- Art contemporain
series:
- Prototype
shop: false
title: Life's a Beach, de Martin Parr
translationKey: lifes-a-beach
---
