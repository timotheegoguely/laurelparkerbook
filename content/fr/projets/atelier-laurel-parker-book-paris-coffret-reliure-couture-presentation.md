---
artist: null
client: Laurel Parker Book
cover: /media/lpb-book-shoe-02.jpg
date: 2014-11-10T00:00:00.000Z
description: >-
  Notre "book shoe" : un coffret pour présenter une trentaine de coutures pour
  brochures de 1, 2, et 3 cahiers. Inspiré d’un « étui d’attente » développé par
  des conservateurs et restaurateurs de livre américains, le Book Shoe permet de
  mettre en avant notre savoir-faire et notre créativité. Un couvercle a été
  ajouté pour réaliser un objet fini et transportable. Design et fabrication :
  Laurel Parker Book.
designs:
  - Semi-sur mesure
fonctions:
  - Éditer
  - Présenter
formes:
  - Coffret
  - Livre
images:
  - /media/lpb-book-shoe-02.jpg
  - /media/lpb-book-shoe-04.jpg
  - /media/lpb-book-shoe-05.jpg
  - /media/lpb-book-shoe-03.jpg
  - /media/lpb-book-shoe-01.jpg
secteurs:
  - Agence
  - Luxe
series:
  - Unique
shop: null
title: Book shoe
translationKey: book-shoe
foldedByHand: null
___mb_schema: /.mattrbld/schemas/projet.json
---

