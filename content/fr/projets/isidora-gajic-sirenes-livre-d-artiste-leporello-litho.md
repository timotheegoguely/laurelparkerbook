---
title: Sirènes
translationKey: sirenes
description: >-
  Sirènes, livre d'artiste d'Isidora Gajic, édition limitée à 35 exemplaires + 5
  AP. Impressions lithographiques par Atelier Bruno Robbe (Belgique). Reliure
  accordéon et étui par Laurel Parker Book. Publié par L'éditeur du dimanche.
date: '2023-11-11'
client: l'éditeur du dimanche
artist: Isidora Gajic
series:
  - Multiple
secteurs:
  - Art contemporain
  - Photo
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
cover: /media/isidora-gajic-sirenes-artistbook-art-photo-edition-05-w.jpg
images:
  - /media/isidora-gajic-sirenes-artistbook-art-photo-edition-05-w.jpg
  - /media/isidora-gajic-sirenes-artistbook-art-photo-edition-02-w.jpg
  - /media/isidora-gajic-sirenes-artistbook-art-photo-edition-01-w.jpg
  - /media/isidora-gajic-sirenes-artistbook-art-photo-edition-03-w.jpg
  - /media/isidora-gajic-sirenes-artistbook-art-photo-edition-04-w.jpg
shop: null
foldedByHand: null
___mb_schema: /.mattrbld/schemas/projet.json
---

