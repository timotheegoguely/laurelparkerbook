---
artist: Clément Lemaire
client: Clément Lemaire
cover: /media/lemaire_boite-presentation_01.jpg
date: 2017-11-11
description: |-
  Certains photographes viennent nous voir pour réaliser des boîtes de présentation sur-mesure. Nous sommes ravis de produire un objet en relation avec l’univers de chacun.

  Pour Clément Lemaire, un coffret de présentation à couvercle, échancrure sur les deux plateaux. Deux toiles enduites couleur « martin-pêcheur » et vert électrique, marquage à chaud, 2017.

  Design et fabrication : Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/lemaire_boite-presentation_01.jpg
- /media/lemaire_boite-presentation_02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Unique
shop: false
title: Coffret de présentation pour Clément Lemaire
translationKey: clement-lemaire
---
