---
artist: ""
client: F93
cover: /media/f93_carte_2019_01.jpg
date: 2019-02-04
description: 'Depuis 2017, notre atelier est invité pour faire le design et la fabrication
  des cartes de voeux de la Fondation 93 à Montreuil. Chaque projet met en valeur
  le papier en le transformant et en jouant sur le logo de la F93 (dessiné par Gaël
  Hugo, One More Studio). En 2019, le logo a été découpé à laser et brodé à la main.
  300 exemples. Design et fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Œuvre papier
images:
- /media/f93_carte_2019_01.jpg
- /media/f93_carte_2019_03.jpg
- /media/f93_logo_web.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carte de vœux 2019, F93
translationKey: F93-2019
---
