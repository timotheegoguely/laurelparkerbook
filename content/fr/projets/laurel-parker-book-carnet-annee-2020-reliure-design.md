---
artist: Laurel Parker Book
client: ""
cover: /media/0088.jpg
date: 2021-03-08
description: "Nous vous présentons Endurance, notre carnet de fin d’année, designé
  et fabriqué par nos soins pour les clients et amis de l’atelier. Chaque élément
  graphique est lié à un projet de commande durant l’année. Les références des travaux
  sont listées en marquage à chaud dans le colophon. Sur la couverture, le petit bonhomme
  détachable symbolise cette année 2020 un peu particulière : restons debout face
  aux difficultés actuelles.   \n  \nInspirations : Emmanuelle Fructus et les photos
  du voyage de l’Endurance par Frank Hurley."
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/0088.jpg
- /media/0092.jpg
- /media/0053.jpg
- /media/0068.jpg
- /media/0066.jpg
- /media/0067.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carnet fin d’année 2020
translationKey: carnet-2020
---
