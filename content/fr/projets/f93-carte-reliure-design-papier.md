---
artist: ""
client: F93
cover: /media/f93_carte_2017_01.jpg
date: 2017-05-07
description: Depuis 2017, notre atelier est invité pour faire le design et la fabrication
  des cartes de voeux de la Fondation 93 à Montreuil. Chaque projet met en valeur
  le papier en le transformant et en jouant sur le logo de la F93 (dessiné par Gaël
  Hugo). Papier japonais plié à la main, impression sérigraphie en blanc mat par Frédéric
  Déjean de l’Orbis Pictus Club. Série de 100.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Œuvre papier
images:
- /media/f93_carte_2017_01.jpg
- /media/f93_carte_2017_02.jpg
- /media/f93_logo_web.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carte de voeux 2017, F93
translationKey: F93-2017
---
