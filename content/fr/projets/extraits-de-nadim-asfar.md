---
artist: Nadim Asfar
client: Nadim Asfar
cover: /media/asfar-livre-extraits-03.jpg
date: 2021-09-29
description: |-
  Édition photographique sur le quartier de résidence de l'artiste à Beyrouth. Le montage et la reliure (couture sur onglets en papier japon) ont été réalisés à l'atelier Laurel Parker Book. Le livre est protégé par un étui en papier sur mesure.

  Ce projet fut présenté à la galerie Tanit (Munich) de septembre à novembre 2021.

  Reproductions : Nadim Asfar & Ava du Parc
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
images:
- /media/asfar-livre-extraits-03.jpg
- /media/asfar-livre-extraits-04.jpg
- /media/asfar-livre-extraits-02.jpg
- /media/asfar-livre-extraits-01.jpg
- /media/asfar-livre-extraits-05.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Extraits de Nadim Asfar
translationKey: extraits-asfar
---
