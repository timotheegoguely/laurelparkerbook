---
artist: ""
client: Calligrane
cover: /media/lpb-carnet2018-05.jpg
date: 2015-07-09
description: Collection de papeterie de luxe. Carnets faits main avec des couvertures
  réalisées avec notre technique de teinture à la main.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet2018-05.jpg
- /media/lpb-papeterie_motif_02.jpg
- /media/lpb-papeterie_motif_03.jpg
- /media/lpb-papeterie_motif_01.jpg
secteurs:
- Luxe
series:
- Multiple
shop: false
title: Papeterie de luxe
translationKey: papeterie-itajime
---
