---
artist: ""
client: Créanog
cover: /media/guerlain_boite-ruche-03.jpg
date: 2013-06-04
description: |-
  Boîte unique pour la présentation d’un parfum, Ruche impériale de Guerlain.
  Design du coffret et gaufrages : Créanog.
  Fabrication du coffret : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/guerlain_boite-ruche-03.jpg
- /media/guerlain_boite-ruche-01.jpg
- /media/guerlain_boite-ruche-04.jpg
- /media/guerlain_boite-ruche-02.jpg
secteurs:
- Luxe
series:
- Unique
shop: false
title: Ruche impériale de Guerlain
translationKey: ruche-imperiale
---
