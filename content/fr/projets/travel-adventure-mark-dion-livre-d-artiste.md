---
artist: Marc Dion
client: null
cover: /media/dion_fragments-of-artistbook_03.jpg
date: 2017-11-10T00:00:00.000Z
description: >-
  Anecdote au sujet du livre d’artiste Fragments of Travel Exploration and
  Adventure, de Marc Dion.

  Extrait du magazine The Shelf n°5, Laurel Parker, Pour la reliure de l’art,
  2017.


  Les dessins originaux comportaient des perforations et des déchirures, car ils
  avaient été détachés de carnets à spirales et de blocs-notes perforés : il a
  été décidé de reproduire ces éléments sur les fac-similés.
designs:
  - Sur mesure
draft: true
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/dion_fragments-of-artistbook_03.jpg
  - /media/dion_fragments-of-artistbook_02.jpg
  - /media/dion_fragments-of-artistbook_01.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: false
title: >-
  Anecdote au sujet du livre d’artiste Fragments of Travel Exploration and
  Adventure, de Marc Dion
translationKey: anecdote-fragments-of-travel-exmploration
___mb_schema: /.mattrbld/schemas/projet.json
---

