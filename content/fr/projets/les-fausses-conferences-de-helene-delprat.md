---
artist: Hélène Delprat
client: Galerie Christophe Gaillard
cover: /media/delprat_fausses-conferences-04.jpg
date: 2017-06-06
description: |-
  En lien avec son exposition monographique I Did It My Way à la Maison Rouge, nous avons réalisé avec Hélène Delprat et Camille Morin de la Galerie Christophe Gaillard une série de Video Box. Design graphique Camille Morin. Séries de 5 exemplaires + 1 épreuve d’artiste. Design et fabrication Laurel Parker Book.

  Les (Fausses) Conférences, le coffret prend la forme d’une diapositive 35mm avec une image transparente en noir et blanc. Carton museum, tirage numérique sur mylar, marquage à chaud noir.
  -W.O.R.K.S & D.A.Y.S, Hélène Delprat a imaginé un agenda personnalisé, avec une dorure sur tranche et une reliure en spirale, pour contenir le DVD. Tirages numériques sur papier Munken Polar. Série de 5 exemplaires + 1 épreuve d’artiste.
  -Hi-Han Song Remix, peinture de l’artiste imprimée sur papier gaufré. Impression numérique, marquage à chaud noir sur papier Chromolux, carton museum.
  -Comment j’ai inventé Versailles, papier japonais plié à la main, carton museum, marquage à sec.
  -Les chants du guerrier couvert de cendres, une image d’une tête cagoulée est dessinée par des perforations brillantes à travers un velours de porc.
designs:
- Sur mesure
fonctions:
- Présenter
- Éditer
formes:
- Boîte 2.0
images:
- /media/delprat_fausses-conferences-04.jpg
- /media/delprat_fausses-conferences-02.jpg
- /media/delprat_dvd-box_01.jpg
- /media/delprat_fausses-conferences-01.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Video Box de Hélène Delprat
translationKey: video-box-helene-delprat
---
