---
artist: ""
client: F93
cover: /media/f93_carte_2019-02.jpg
date: 2018-02-05
description: 'Depuis 2017, notre atelier est invité pour faire le design et la fabrication
  des cartes de voeux de la Fondation 93 à Montreuil. Chaque projet met en valeur
  le papier en le transformant et en jouant sur le logo de la F93 (dessiné par Gaël
  Hugo). En 2018, le design inspiré par un calendrier d’avent, qui dévoile l’année
  de la création de la F93. Impression sérigraphie en noir et jaune. Série de 100.
  Design et fabrication : Laurel Parker Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Œuvre papier
images:
- /media/f93_carte_2019-02.jpg
- /media/f93_logo_web.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carte de vœux 2018, F93
translationKey: F93-2018
---
