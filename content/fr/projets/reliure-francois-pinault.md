---
artist: null
client: Éditions Xavier Barral
cover: /media/exb-de-gonet-04.jpg
date: 2013-09-15T00:00:00.000Z
description: >-
  Reliure à la façon de Jean de Gonet, avec couture apparente sur bandes de cuir
  qui passent dans des plats en bois. Dos rapporté en veau noir. Documents de
  formats variés (dessins, lettres, photographies...) collés sur onglets. Fait
  d’après des recherches autour des reliures de Jean de Gonet à la Bibliothèque
  Kandinsky et à la Bibliothèque historique de la ville de Paris. Commande des
  Éditions Xavier Barral pour la famille Pinault. Plats en bois de sycamore :
  Maonia Design et fabrication : Laurel Parker Book
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/exb-de-gonet-04.jpg
  - /media/exb-de-gonet-02.jpg
  - /media/exb-de-gonet-01.jpg
  - /media/exb-de-gonet-03.jpg
secteurs:
  - Art contemporain
series:
  - Unique
shop: false
title: Reliure unique pour François Pinault
translationKey: reliure-francois-pinault
___mb_schema: /.mattrbld/schemas/projet.json
---

