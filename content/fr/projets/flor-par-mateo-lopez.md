---
artist: Mateo López
client: '&: Christophe Daviet-Thery'
cover: /media/lopez_flor_01.jpg
date: 2019-09-22
description: "«_Flor (Fleur)_ fait partie d'une série de sculptures pliantes, j'aime
  imaginer des objets d'art qui traitent de l'espace, ont une certaine portabilité,
  s'élargissent et se contractent. Je m'intéresse également à la relation physique
  entre notre corps et l'objet du livre. Flor est une invitation à s'engager, bouger
  et activer, un jeu d'abstraction qui évoque la floraison. \"—Mateo López\n\nPages
  articulées en carton recouvert de toile buckram 25 ex + 5E.A.  \nFabrication : Laurel
  Parker Book"
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lopez_flor_01.jpg
- /media/lopez_flor_02.jpg
- /media/lopez_flor_03.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Flor, par Mateo Lopez
translationKey: flor
---
