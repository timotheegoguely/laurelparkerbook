---
artist: Brodbeck & Barbuat
client: Brodbeck & Barbuat
cover: /media/brodbeck_barbuat_book_03.jpg
date: 2021-01-07
description: "Portfolio du duo Brodbeck & de Barbuat. Ensemble de trois objets en
  toile composé d’un coffret, d’un press-book à vis et d’une boite 2.0. Le coffret
  regroupe un livret, maintenu par deux élastiques, et un emplacement sur-mesure accueillant
  un ordinateur et une sélection de tirages papier. Le pressbook à vis dispose d’une
  pochette en toile où se glisse un livret. La boite 2.0 protège et apporte une présence
  physique au support numérique (ici clé usb). Sur chacun des objets, le titre est
  inscrit en marquage à chaud imprimé à notre atelier. \nDesign et fabrication réalisés
  par Laurel Parker Book"
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Portfolio
images:
- /media/brodbeck_barbuat_book_03.jpg
- /media/brodbeck_barbuat_book_01.jpg
- /media/brodbeck_barbuat_book_02.jpg
- /media/brodbeck_barbuat_book_05.jpg
- /media/brodbeck_barbuat_book_04.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Portfolio du duo Brodbeck & de Barbuat
translationKey: portfolio-brodbeck-barbuat
---
