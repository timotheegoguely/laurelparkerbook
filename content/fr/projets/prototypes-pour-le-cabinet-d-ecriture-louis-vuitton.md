---
artist: ""
client: Louis Vuitton – Cabinet d’Écriture
cover: /media/vuitton-carnet-ecriture-02.jpg
date: 2013-01-30
description: |-
  Prototypes pour le Cabinet d’Écriture, Louis Vuitton, Paris. Un coffret de reliures qui ont marqué l’histoire de l’écriture et du livre:

  Reliure à la japonaise en agneau doublé du papier japon teinté à la main. Reliure à la Lyonnaise en pleine peau de chèvre doublée de papier, avec coutures en fil de lin teinté à la main. Reliure à la copte en peau d’agneau et gardes en chèvre velours, avec couture en fil de lin teinté à la main.

  Recherches de matières, raffinement du produit, étude de la mise en série, et création des prototypes finis.
  Design et fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/vuitton-carnet-ecriture-02.jpg
- /media/vuitton-carnet-ecriture-03.jpg
- /media/vuitton-carnet-ecriture-01.jpg
- /media/vuitton-carnet-ecriture-05.jpg
- /media/vuitton-carnet-ecriture-04.jpg
secteurs:
- Luxe
series:
- Prototype
shop: false
title: Prototypes pour le Cabinet d’Écriture, Louis Vuitton
translationKey: prototype-louis-vuitton
---
