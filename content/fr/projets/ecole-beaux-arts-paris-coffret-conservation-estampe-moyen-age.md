---
artist: ""
client: ENSBA-École de Beaux-Arts de Paris
cover: /media/ensba_coffret-de-voyage_02.jpg
date: 2019-06-11
description: Boîtes de conservation pour des « coffrets à estampe » du Moyen Âge,
  trésors de la collection de l’École des Beaux-Arts de Paris. Datés de la fin du
  XVe siècle, ces coffrets en bois de hêtre, gainés de cuir et cerclés de métal, portent
  une estampe au revers de leur couvercle. Les estampes sont de rares incunables imprimées
  en xylographie.
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/ensba_coffret-de-voyage_04.jpg
- /media/ensba_coffret-de-voyage_02.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: Boîtes de conservation pour des « coffrets à estampe » du Moyen Âge
translationKey: coffret-estampe
---
