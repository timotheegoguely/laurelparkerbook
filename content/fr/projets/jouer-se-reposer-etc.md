---
artist: Emmanuelle Fructus
client: Un Livre Une Image
cover: /media/fructus_album-un-livre-une-image-02.jpg
date: 2012-09-16
description: Albums pour une collection de photographies anonymes. Papier qualité
  archivage. Photographies insérées dans des coins découpés dans la page. Marquage
  à chaud sur le dos amovible. Étui de protection.
designs:
- Sur mesure
fonctions:
- Archiver
- Présenter
formes:
- Portfolio
- Livre
images:
- /media/fructus_album-un-livre-une-image-02.jpg
- /media/fructus_album-un-livre-une-image-01.jpg
secteurs:
- Archives
- Photo
series:
- Multiple
shop: false
title: Jouer, se reposer etc ...
translationKey: albums-fructus
---
