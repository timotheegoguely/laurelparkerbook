---
artist: Laurel Parker Book
client: Laurel Parker Book
cover: /media/lpb-reliure_en_valise-04.jpg
date: 2018-08-15
description: 'Un hommage à la _Boîte-en-valise_ de Marcel Duchamp. Sept reliures d’art
  dans un coffret. _Reliure-en-valise_ met en avant ce qui nous tient à cœur, l’exigence
  de la technique, le choix pointu des matériaux et l’aspect historique et patrimonial
  de chaque objet. Veau blanc, veau noir, toile de lin, cartons japonais, papiers
  allemands, fil de lin ciré, bouton en nacre et crochets en os. Une invitation d’Eric
  Sébastien Faure-Lagorce pour une exposition itinérante à l’Institut Français. Commissaire
  d’exposition : Éric Sébastien Faure-Lagorce. Design et fabrication : Laurel Parker
  Book.'
designs:
- Sur mesure
fonctions:
- Présenter
- Éditer
formes:
- Livre
- Coffret
images:
- /media/lpb-reliure_en_valise-04.jpg
- /media/lpb-reliure_en_valise-01.jpg
- /media/lpb-reliure_en_valise-02.jpg
- /media/lpb-reliure_en_valise-03.jpg
secteurs:
- Luxe
- Art contemporain
series:
- Unique
shop: false
title: Reliure en valise
translationKey: reliure-en-valise
---
