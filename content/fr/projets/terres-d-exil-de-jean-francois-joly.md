---
artist: Jean-François Joly
client: Wallguest
cover: /media/joly_terre-exil-01.jpg
date: 2016-11-11
description: |-
  Un portfolio regroupant 5 tirages argentique, limitée à 10 exemplaires. Ce projet a été réalisé parallèlement à l’exposition de Jean-François Joly à la Maison Européenne de la Photographie en mai 2016. Album marie-louise dans le style des albums de daguerréotypes du XIXème siècle. Couverture et pages en papier Richard de Bas fait main sur structure en carton sans acide. Marquages à chaud du titre et du colophon.

  Design et fabrication : Laurel Parker Book.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/joly_terre-exil-01.jpg
- /media/joly_terre-exil-05.jpg
- /media/joly_terre-exil-03.jpg
- /media/joly_terre-exil-04.jpg
- /media/joly_terre-exil-02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Terres d’exil
translationKey: terres-exil
---
