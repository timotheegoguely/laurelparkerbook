---
artist: Laurence Leblanc
client: Picto Foundation
cover: /media/leblanc-dargile-04.jpg
date: 2016-11-11
description: "_D'argile_, livre d’artiste réalisé avec le soutien de la [Picto Foundation](https://www.pictofoundation.fr/prix-niepce/
  \"Picto Foundation\"), mécène du Prix Niépce, dans le cadre du prix décerné à Laurence
  Leblanc par l’association Gens d’Images en 2016.\n\nCe livre d’artiste a été conçu
  autour de la série « D’ARGILE ». La série est le résultat d’un travail photographique
  minutieux, prenant pour sujet de petites figurines en glaise modelées pour un film
  de Rithy Panh témoignant des atrocités du génocide cambodgien.\n\nLaurence Leblanc
  utilise le principe de renversement d’échelle et de sérialité pour faire de ces
  visages plus petits que l’ongle du pouce de la main, de véritables portraits. Les
  54 tirages noirs et blancs que l’on retrouve dans l’objet entrent étrangement en
  résonance avec l’histoire de la photographie anthropométrique que les khmers rouges
  ont terriblement su exploiter à des fins d’identification et de répression.\n\nPourtant,
  pour interroger la valeur d’une vie unique et singulière dans cette atrocité du
  nombre, Laurence Leblanc ne cède pas au systématisme qui caractérise les _mug_ _shots_
  et se joue d’un dispositif photographique renouvelée. L’apparition d’une main humaine
  démesurée ou la vision de deux figurines semblant se soutenir l’une l’autre sont
  autant d’images saillantes qui interrogent différemment. Toujours chez Laurence
  Leblanc, de cette dissonance affleure le sens.\n\nPour ce projet, nous avons adapté
  un prototype déjà conçu par l’artiste. Pour essayer de garder l’authenticité de
  l’objet originel, nous avons utilisé deux types de papiers. Cela donne un effet
  grisé sur les tranches, en rappel du scotch et de la colle utilisés par l’artiste.\n\nDesign
  : Laurence Leblanc avec Laurel Parker   \nImpressions : Picto   \nFabrication :
  Laurel Parker Book"
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/leblanc-dargile-04.jpg
- /media/leblanc-dargile-03.jpg
- /media/leblanc-dargile-02.jpg
- /media/leblanc-dargile-01.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: D’argile, de Laurence Leblanc
translationKey: dargile
---
