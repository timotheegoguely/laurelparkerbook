---
artist: Françoise Pétrovitch
client: Laurel Parker Edition
cover: /media/petrovitch_oiseaux-01.jpg
date: 2020-09-15T00:00:00.000Z
description: >-
  Pendant le confinement, nous avons suivi sur Instagram les dessins que
  Françoise Pétrovitch faisait tous les jours des oiseaux qu’elle regardait à
  travers sa fenêtre. Nous avons contacté Françoise pour lui proposer de faire
  une édition autour de ces oiseaux, une centaine en tout ! Le résultat de notre
  collaboration est un coffret à la fois boîte de spécimen quand il est fermé,
  et mini-théâtre quand il est ouvert. Les oiseaux dans chaque coffret – sept
  dessins originaux – peuvent être mis en scène, comme des fleurs dans un
  ikebana, l’art Japonais d’arrangement floral. On regarde cet arrangement
  d’oiseaux vivants et morts à travers un morceau de verre soufflé à la bouche,
  ajoutant les irrégularités des fenêtres dans les vielles maisons. L’édition
  est limitée à 17 jeux d'oiseaux uniques (Set A à Q). Chaque set comporte une
  ménagerie de 7 dessins originaux d’oiseaux rassemblés par l’artiste, dans un
  coffret à fenêtre fabriqué à la main. Laurel Parker Edition, 2020 • Prix sur
  demande
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
  - Coffret
images:
  - /media/petrovitch_oiseaux-01.jpg
  - /media/petrovitch_oiseaux-08.jpg
  - /media/petrovitch_oiseaux-04.jpg
  - /media/petrovitch_oiseaux-03.jpg
  - /media/petrovitch_oiseaux-05.jpg
  - /media/petrovitch_oiseaux-06.jpg
  - /media/petrovitch_oiseaux-02.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: Regarder les oiseaux
translationKey: regarder-les-oiseaux
___mb_schema: /.mattrbld/schemas/projet.json
---

