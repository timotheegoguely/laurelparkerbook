---
artist: Françoise Pétrovitch
client: Semiose Galerie
cover: /media/petrovitch-boite-conservation-02.jpg
date: 2012-09-16
description: Boîte qualité archivage pour présenter et stocker des dessins de Françoise
  Pétrovitch à la galerie Semiose. Très grand format de 60 × 90 cm. Toile buckram
  rouge et marquage à chaud couleur miroir.
designs:
- Sur mesure
fonctions:
- Archiver
formes:
- Coffret
images:
- /media/petrovitch-boite-conservation-02.jpg
- /media/petrovitch-boite-conservation-01.jpg
- /media/petrovitch-boite-conservation-03.jpg
secteurs:
- Archives
series:
- Unique
shop: false
title: Boîte des dessins de Françoise Pétrovitch
translationKey: boite-dessin-petrovitch
---
