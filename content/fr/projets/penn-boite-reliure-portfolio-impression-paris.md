---
artist: Barbara Bénédicte Penn
client: Barbara Bénédicte Penn
cover: /media/penn_boite01.jpg
date: 2021-06-15
description: "Boîte unique en papier pour des tirages numériques sur feuilles d’herbier
  et un livret de l’artiste Barbara Bénédicte Penn.  \n  \nMarquage à chaud, design
  et fabrication par Laurel Parker Book  \n  \nTirages par Ryan Boatright (Atelier
  Boba) et restauration des feuilles d’herbier par Elsa Gravé."
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Coffret
images:
- /media/penn_boite01.jpg
- /media/penn_boite02.jpg
- /media/penn_boite03.jpg
- /media/penn_boite04.jpg
- /media/penn_boite05.jpg
secteurs:
- Photo
- Art contemporain
series:
- Unique
shop: false
title: Boîte de présentation et livret pour Barbara Bénédicte Penn
translationKey: barbara-benedicte-penn
---
