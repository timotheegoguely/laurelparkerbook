---
artist: Lola Reboud
client: Lola Reboud
cover: /media/reboud-climatesii-04.jpg
date: 2018-11-11
description: |-
  Coffret en carte bleu pour l’édition limitée comprenant le livre _Les Climats II (Japon)_ édité par Poursuite Éditions, accompagné d’un tirage signé avec ajout de feuille d’or, 10 exemplaires, 2018. Design et fabrication : Laurel Parker Book

  "Les Climats II (Japon), premier livre de Lola Reboud, a pour fil conducteur la relation que nous entretenons aux Climats – milieux humains, et où la géographie des territoires, comme le cycle des saisons, est aussi importante que les individus. Suite à des échanges avec le volcanologue Patrick Allard, chercheur à l’Institut du Globe de Paris, Lola Reboud s’est rendue dans la région du Kyushu photographier les laboratoires d’observations volcaniques et les sites de Sakurajima et Aso San, ainsi qu’à Beppu, où la géothermie est particulièrement visible. Cet ensemble photographique forme un récit où se mêle une figure humaine en regard de son environnement. La géothermie qui caractérise l’archipel Nippon est aussi au cœur du sujet."
designs:
- Semi-sur mesure
fonctions:
- Éditer
formes:
- Chemise
images:
- /media/reboud-climatesii-04.jpg
- /media/reboud-climatesii-02.jpg
- /media/reboud-climatesii-03.jpg
- /media/reboud-climatesii-01.jpg
secteurs:
- Photo
- Art contemporain
series:
- Multiple
shop: false
title: Les Climats II (Japon), de Lola Reboud
translationKey: les-climats
---
