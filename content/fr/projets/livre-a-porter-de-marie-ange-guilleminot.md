---
artist: Marie-Ange Guilleminot
client: Marie-Ange Guilleminot
cover: /media/guilleminot-toucher-voir_01.jpg
date: 2015-11-11
description: 'Livre d’artiste en deux versions : en accordéon et relié à la Japonaise.
  Pour le projet _Toucher-voir,_ qui a été confié à Marie-Ange Guilleminot par Olivier
  Saillard, directeur du Palais Galliera - Musée de la mode de la Ville de Paris.
  Ce projet, destiné à tous mais principalement aux aveugles et aux mal voyants, a
  été réalisé grâce au soutien de The Conny-Maeva Charitable Foundation et l’association
  Le Livre de l’aveugle. Design Graphique et pilotage du projet : Élise Gay et Kévin
  Donnot. Impression en gaufrage : Le Livre de l’Aveugle. Reliure : Laurel Parker
  Book'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/guilleminot-toucher-voir_01.jpg
- /media/guilleminot-toucher-voir_02.jpg
- /media/guilleminot-toucher-voir_03.jpg
- /media/guilleminot-toucher-voir_04.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Livre-à-porter, de Marie-Ange Guilleminot
translationKey: livre-marie-ange-guilleminot
---
