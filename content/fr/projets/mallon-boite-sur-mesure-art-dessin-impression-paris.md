---
artist: François Mallon
client: François Mallon
cover: /media/mallon-for-100-drawings_02.jpg
date: 2014-05-11
description: 'Coffret de présentation en toile avec deux zones de marquage à chaud
  et une chemise enveloppant une centaine de dessins. Pour l’artiste François Mallon.
  Design : Antoine Delage de Luget. Design et fabrication : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Éditer
- Présenter
formes:
- Coffret
images:
- /media/mallon-for-100-drawings_02.jpg
- /media/mallon-for-100-drawings_03.jpg
- /media/mallon-for-100-drawings_01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: In Memoriam...100 dessins, de François Mallon
translationKey: francois-mallon
---
