---
artist: André
client: Louis Vuitton – Studio Graphique
cover: /media/andre-st-tropez-01.jpg
date: 2010-09-26
description: "Portfolio avec 10 dessins de l’artiste André. Éditions Louis Vuitton.
  Tirages par  l’Imprimerie Arte - Maeght. Ce portfolio a été produit pour la ré-ouverture
  de la boutique Louis Vuitton à St. Tropez .\nCette boîte tiroir a une broderie incrustée
  dans le haut du coffret. Qualité archivage, en toile italienne. \nDesign graphique
  : Studio Graphique, Louis Vuitton\nDesign du coffret : Laurel Parker"
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Coffret
images:
- /media/andre-st-tropez-01.jpg
- /media/andre-st-tropez-03.jpg
secteurs:
- Luxe
- Art contemporain
series:
- Multiple
title: St. Tropez
translationKey: st-tropez
---
