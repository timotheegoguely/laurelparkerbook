---
actions:
  - Relier
artist: Mark Dion
client: '&: Christophe Daviet-Thery'
cover: /media/livre_artiste_mark_dion_3.jpg
date: 2007-01-01T00:00:00.000Z
description: >-
  "Dans ce livre d’artiste, Mark Dion se fait passer pour un scientifique du 18e
  ou 19e siècle qui explore une terre inconnue (il inclut gentiment 2 cartes de
  cette terra incognita) et documente de manière exhaustive la flore et la faune
  indigènes. Le livre a été superbement construit pour réaliser cette fiction de
  manière convaincante: des morceaux de papier sont découpés et assemblés et
  reliés dans le livre pour suggérer un compte rendu impromptu mais complet des
  découvertes et des observations de l'exploration. Les gravures Iris semblent
  être un fac-similé convaincant de la maquette faux-scientifique de Dion. "


  Reliure d’emboîtage, demi à coins, en buffle rouge et papier Zerkall. Pochette
  en toile dans le plat arrière avec quelques tirages et deux mini-livres reliés
  en buffle rouge. Chemise en toile et papier, dorée. 36 exemplaires + 9 E.A.
  Co-production de XN Éditions, Christophe Daviet-Thery Livres d’artistes, et In
  Situ galerie. Tirages numériques par Arteprint, Bruxelles. 
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/livre_artiste_mark_dion_3.jpg
  - /media/livre-artistes-mark-dion_02.jpg
  - /media/dion_fragments-of-artistbook_02.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
title: 'Fragments of Travel, Exploration and Adventure'
translationKey: /content/fr/projets/fragments-of-travel-exploration-and-adventure.md
shop: false
___mb_schema: /.mattrbld/schemas/projet.json
---
Tirages numériques par Arteprint, Bruxelles. Reliure d’emboîtage, demi à coins, en buffle rouge et papier Zerkall. Pochette en toile dans le plat arrière avec quelques tirages et deux mini-livres reliés en buffle rouge. Chemise en toile et papier, dorée.
