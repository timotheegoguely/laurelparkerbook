---
artist: Jean Larivière
client: Louis Vuitton – Studio Graphique
cover: /media/lariviere-coffret-04.jpg
date: 2011-09-18T00:00:00.000Z
description: >-
  Portfolio pour 20 tirages au format 53 × 65 cm. Éditions Louis Vuitton. 50
  exemplaires + 10 A.P. Boîte tiroir avec un marquage à chaud d’un dessin de
  l’artiste sur le haut du coffret. Coffret de qualité archivage. 


  Design graphique : Studio Graphique, Louis Vuitton.   

  Tirages : Jules Maeght, atelier Arte.   

  Design de l’objet et fabrication : Laurel Parker Book.
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Coffret
  - Portfolio
images:
  - /media/lariviere-coffret-04.jpg
  - /media/lariviere-coffret-02.jpg
  - /media/lariviere-coffret-03.jpg
secteurs:
  - Photo
  - Art contemporain
series:
  - Multiple
shop: false
title: Portfolio Jean Larivière
translationKey: portfolio-jean-lariviere
___mb_schema: /.mattrbld/schemas/projet.json
---

