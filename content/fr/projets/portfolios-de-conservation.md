---
artist: null
client: ENSBA-École de Beaux-Arts de Paris
cover: /media/lpb_portfolios-conservation_02.jpg
date: 2020-11-11T00:00:00.000Z
description: >-
  Portfolios de conservation en toile buckram et pochette en papier de qualité
  archivage. Réalisés pour le conditionnement des incunables de petits formats
  et minuscules de la collection Masson pour l’École de Beaux-Arts de Paris.
designs:
  - Semi-sur mesure
fonctions:
  - Archiver
formes:
  - Portfolio
images:
  - /media/lpb_portfolios-conservation_02.jpg
  - /media/lpb_portfolios-conservation_01.jpg
  - /media/lpb_portfolios-conservation_03.jpg
  - /media/lpb_portfolios-conservation_04.jpg
secteurs:
  - Archives
series:
  - Multiple
shop: false
title: Portfolios de conservation
translationKey: portfolio-de-conservation
___mb_schema: /.mattrbld/schemas/projet.json
---

