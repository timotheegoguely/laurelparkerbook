---
artist: Caroline & Hughes Dubois
client: Luna Light Agency
cover: /media/dubois-borobudur-03.jpg
date: 2017-06-06
description: Portfolio de 9 tirages platine-palladium. Impression typographique par
  l’Imprimerie Nationale. Design et fabrication du coffret de luxe par Laurel Parker
  Book.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Portfolio
- Coffret
images:
- /media/dubois-borobudur-03.jpg
- /media/dubois-borobudur-01.jpg
- /media/dubois-borobudur-02.jpg
secteurs:
- Art contemporain
- Photo
series:
- Multiple
shop: false
title: Borobudur Under the Full Moon, par Caroline & Hughes Dubois
translationKey: borobudur
---
