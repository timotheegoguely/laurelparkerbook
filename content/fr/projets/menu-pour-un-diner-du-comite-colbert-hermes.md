---
artist: ""
client: Hermès
cover: /media/hermes-colbert-menu_03.jpg
date: 2017-11-11
description: Menu pour un déjeuner du Comité Colbert organisé par Hermès, 2017. Papier
  japonais plié, teinture à la main de couleur or, fil doré. L’objet fait appel au
  sens du toucher pour apprécier l’aspect du papier et sa texture. Mais aussi de la
  vue par les effets de volumes du pliage et la teinte or. Un objet raffiné pour un
  comité qui travaille à la promotion de l’art de vivre français.
designs:
- Sur mesure
fonctions:
- Éditer
- Présenter
formes:
- Livre
images:
- /media/hermes-colbert-menu_03-1.jpg
- /media/hermes-colbert-menu_02.jpg
- /media/hermes-colbert-menu_01.jpg
secteurs:
- Luxe
series:
- Multiple
title: Menu pour un déjeuner du Comité Colbert
translationKey: menu-dejeuner-comite-colbert
---
