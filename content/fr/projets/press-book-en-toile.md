---
artist: Claire-Lise Havet
client: Claire-Lise Havet
cover: /media/havet-pressbook_01.jpg
date: 2015-09-27T00:00:00.000Z
description: >-
  Press book en toile pour l’intérieur et l’extérieur. Marquage à chaud d’un
  logo. Deux pochettes moulées sur la garde arrière.
designs:
  - Semi-sur mesure
fonctions:
  - Présenter
formes:
  - Portfolio
images:
  - /media/havet-pressbook_01.jpg
  - /media/havet-pressbook_03.jpg
secteurs:
  - Photo
series:
  - Multiple
shop: false
title: Press book de Claire-Lise Havet
translationKey: pressbook-havet
___mb_schema: /.mattrbld/schemas/projet.json
---

