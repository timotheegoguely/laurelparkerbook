---
title: Tableau Périodique Unique
translationKey: tableau
description: >-
  96 photographies représentant le spectre d'émission atomique des éléments du
  tableau périodique, classés par ordre alphabétique. Quatre coffrets de
  qualité&nbsp;conservation avec des photographies tenues dans des pochettes
  marie-louise coupées à la main.
date: '2023-05-14'
client: Dove Allouche
artist: Dove Allouche
series:
  - Unique
secteurs:
  - Art contemporain
designs:
  - Sur mesure
fonctions:
  - Présenter
  - Archiver
  - Éditer
formes:
  - Coffret
cover: /media/allouche-tpu2-01-web.jpg
images:
  - /media/allouche-tpu2-07-web.jpg
  - /media/allouche-tpu2-01-web.jpg
  - /media/allouche-tpu2-02-web.jpg
  - /media/allouche-tpu2-06-web.jpg
  - /media/allouche-tpu2-05-web.jpg
  - /media/allouche-tpu2-04-web.jpg
shop: false
___mb_schema: /.mattrbld/schemas/projet.json
---

