---
artist: Sara MacKillop
client: Laurel Parker Edition
cover: /media/mackillop_swatchbook_01.jpg
date: 2019-11-11T00:00:00.000Z
description: >-
  SWATCHBOOK est un livre d’artiste pour fêter les 10 ans d’auto-édition de
  l’artiste britannique Sara MacKillop. Les échantillons de matières et de
  couleurs déclinés dans Swatchbook font références aux sujets de prédilection
  de Sara MacKillop tout en évoquant “Presage: Paris Fashion Forecast”, un
  trimestriel célèbre de la haute couture de 1961 à 1987. Ce dossier avec 4
  dépliants / paravents peut être vu soit comme un livre soit démonté pour créer
  un espace d’exposition en volume. Tirages sérigraphie, numérique, et marquage
  à chaud sur papiers Colorplan, avec éléments fabriqués par l’artiste, de la
  papeterie, des stylos, des autocollants, et des pages de catalogue Ikea. 31 x
  22 x 6 cm fermé • Impressions numérique, sérigraphie, marquage à chaud, mixed
  media • 15 ex + 3 AP • Laurel Parker Edition, 2018 • 980,00€ TTC
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
  - Livre
images:
  - /media/mackillop_swatchbook_01.jpg
  - /media/mackillop_swatchbook_03.jpg
  - /media/mackillop_swatchbook_02.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: SWATCHBOOK
translationKey: swatchbook
___mb_schema: /.mattrbld/schemas/projet.json
---

