---
artist: Fabien Giraud
client: Rosascape
cover: /media/giraud-lelamort-01.jpg
date: 2011-11-10
description: _Le La Mort_ est une œuvre constituée de quinze fragments qui se présentent
  sous la forme de livres. Chaque ouvrage est coupé en son milieu par un second livre
  _Metaxu_ contenant le texte d'une conversation entre Fabien Giraud et Vincent Normand.
  Texte de _Metaxu_ composé en lintotype et imprimé par le Moulin de Got. 15 exemplaires.
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/giraud-lelamort-02.jpg
- /media/giraud-lelamort-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: 'Le La Mort, de Fabien Giraud '
translationKey: le-la-mort
---
