---
artist: Oscar Muñoz
client: Toluca Editions
cover: /media/munoz-inventario-03.jpg
date: 2014-11-10T00:00:00.000Z
description: >-
  Livre d’artiste sur le modèle d’un trieur à soufflets en papier japonais avec
  10 tirages à la poussière de charbon. Eléments en laiton. Une forme atypique
  et originale pour un portfolio de photographies. Toluca Editions, 13 ex + 4
  A.P. Fabrication : Laurel Parker Book.
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Œuvre papier
  - Chemise
images:
  - /media/munoz-inventario-03.jpg
  - /media/munoz-inventario-01.jpg
secteurs:
  - Photo
  - Art contemporain
series:
  - Multiple
shop: false
title: 'Inventario, d’Oscar Muñoz'
translationKey: inventario
___mb_schema: /.mattrbld/schemas/projet.json
---

