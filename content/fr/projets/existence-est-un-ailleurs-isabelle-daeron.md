---
artist: Isabelle Daëron
client: Laurel Parker Edition
cover: /media/existence_daeron_ed_lpb_01_w.jpg
date: 2022-04-26T00:00:00.000Z
description: >-
  Le travail de la designer Isabelle Daëron commence par le dessin - l'esquisse
  - qui continue à vivre, une fois le projet abouti, dans le trait formé par un
  cordon, dans le motif d'une aquarelle peinte, dans la texture du granit
  coloré. Ces lieux et objets pleins de joie de vivre doivent leur existence à
  un « ailleurs », l'imaginaire et la pratique plastique de l'artiste. 21 x 28
  cm, 36 pages • Impression offset, papiers Colorplan avec marquages à chaud 3
  couleurs, couture main avec fil coton vert • 200 ex • Laurel Parker Edition,
  2022 • 20 € TTC
designs:
  - Sur mesure
fonctions:
  - Éditer
formes:
  - Livre
images:
  - /media/existence_daeron_ed_lpb_00_w.jpg
  - /media/existence_daeron_ed_lpb_03_w.jpg
  - /media/existence_daeron_ed_lpb_05_w.jpg
  - /media/existence_daeron_ed_lpb_02_w.jpg
secteurs:
  - Art contemporain
series:
  - Multiple
shop: true
title: L'existence est un ailleurs
translationKey: existence
___mb_schema: /.mattrbld/schemas/projet.json
---

