---
artist: Emmanuelle Fructus
client: Un Livre Une Image
cover: /media/hommeinfidele2.jpg
date: 2008-11-09
description: 'Livre d’artiste réalisé à partir de tirages originaux de la collection
  de photos anonymes de l’artiste. Conception : Emmanuelle Fructus et Laurel Parker.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/hommeinfidele1.jpg
- /media/hommeinfidele2.jpg
secteurs:
- Art contemporain
- Photo
series:
- Unique
shop: false
title: Un homme infidèle, d'Emmanuelle Fructus
translationKey: un-homme-infidele
---
