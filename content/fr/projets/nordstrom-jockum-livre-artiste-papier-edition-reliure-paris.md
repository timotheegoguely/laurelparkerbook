---
artist: Jockum Nordström
client: '&: Christophe Daviet-Thery'
cover: /media/nordstrom-to-jockum-01.jpg
date: 2011-11-10
description: "Coffret contenant 7 planches de pop-ups de l'artiste Jockum Nordström.
  Christophe Daviet-Thery et XN Éditions. Édition de 25 exemplaires + 6 AP et 4 HC.
  Boîte à couvercle en toile buckram, marquage à chaud extérieur et intérieur.  \nIngénieur
  de papier, Marie-Victoria Garrido.  \nBoîte design et fabrication : Laurel Parker
  Book."
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Boîte d’éditeur
images:
- /media/nordstrom-to-jockum-01.jpg
- /media/nordstrom-to-jockum-02.jpg
- /media/nordstrom-to-jockum-03.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: By and To Jockum, de Jockum Nordström
translationKey: jockum-nordstrom
---
