---
artist: null
client: ENSBA-École de Beaux-Arts de Paris
cover: /media/ensba-boite-conservation-04.jpg
date: 2020-11-11T00:00:00.000Z
description: >-
  Boîtes de conservation pour la Collection de l’Ecole Nationale Supérieure des
  Beaux-Arts de Paris. Toile bicolore, carton musée, papier taupe pH neutre,
  mousse plastazote. Nous travaillons en collaboration avec le conservateur en
  charge des manuscrits et imprimés anciens. Nous réalisons des boîtes qualité
  archivage pour leurs collections. Elles doivent être solides pour préserver
  des documents lourds contenant souvent des plats en bois et des éléments
  métalliques.


  Design et fabrication : Laurel Parker Book
designs:
  - Sur mesure
fonctions:
  - Archiver
formes:
  - Portfolio
  - Boîte d’éditeur
  - Coffret
images:
  - /media/ensba-boite-conservation-04.jpg
  - /media/ensba-boite-conservation-01.jpg
  - /media/ensba-boite-conservation-02.jpg
  - /media/ensba-boite-conservation-03.jpg
secteurs:
  - Archives
series:
  - Unique
shop: false
title: Boîtes de conservation pour l’ENSBA Paris
translationKey: boites-conservation-ensba
___mb_schema: /.mattrbld/schemas/projet.json
---

