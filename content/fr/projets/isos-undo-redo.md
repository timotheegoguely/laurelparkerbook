---
artist: 'Undo-Redo, Atelier 3'
client: Undo-Redo
cover: /media/peri-fabrique-isos_02.jpg
date: 2014-11-10T00:00:00.000Z
description: >-
  _ISOs_, pour _Péri’Fabrique_ pendant les D’Days 2014, exposition au Musée des
  Arts Décoratifs, Paris. ISOs est une affiche tissée, œuvre expérimentale et
  novatrice née de la rencontre entre le studio Undo-Redo, les tisserands d’art
  de l’Atelier 3 et l’atelier Laurel Parker Book. Papiers japonais Takeo.
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Œuvre papier
  - Installation
images:
  - /media/peri-fabrique-isos_02.jpg
  - /media/peri-fabrique-isos_04.jpg
  - /media/peri-fabrique-isos_06.jpg
  - /media/peri-fabrique-isos_03.jpg
  - /media/peri-fabrique-isos_01.jpg
secteurs:
  - Art contemporain
series:
  - Unique
shop: false
title: ISOs
translationKey: iso
___mb_schema: /.mattrbld/schemas/projet.json
---

