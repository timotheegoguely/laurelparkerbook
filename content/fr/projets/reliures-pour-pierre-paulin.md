---
artist: Pierre Paulin
client: Pierre Paulin
cover: /media/paulin-white-01.jpg
date: 2019-11-11
description: 'Livre d’artiste. Cuir blanc, marquage à sec, 2019. Design de l’artiste.
  Reliure : Laurel Parker Book.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/paulin-white-01.jpg
- /media/paulin-white-04.jpg
- /media/paulin-white-02.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Look
translationKey: look-pierre-paulin
---
