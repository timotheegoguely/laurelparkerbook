---
artist: ""
client: Laurel Parker Book
cover: /media/lpb-carnet_reliure_2014.jpg
date: 2014-06-10
description: 'Notre carnet fin d’année 2014 : papier teinté à la main, couture de
  deux cahiers sans colle, découpe laser, marquage à chaud. Édition de 150 exemplaires.'
designs:
- Sur mesure
fonctions:
- Éditer
formes:
- Livre
images:
- /media/lpb-carnet_reliure_2014.jpg
- /media/lpb-carnet_reliure_2014-2.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Carnet de fin d’année 2014
translationKey: notebook-2014
---
