---
artist: null
client: Chaumet
cover: /media/chaumet_reliure_bleu_02.jpg
date: 2017-11-11T00:00:00.000Z
description: >-
  Reliure montée sur onglets, toile bleu, papier irisé, papier japonais
  mitsumata, marquage à chaud. 51 exemplaires, 2017.


  La maison Chaumet présente ses collections de haute joaillerie sous forme de
  dessins regroupés dans une reliure. La qualité des matières choisies est
  primordiale, comme les onglets en papier japonais mitsumata. Ce beau papier
  est fait main par Shinichiro Abe, le petit fils de Eishiro Abe, le premier
  artisan du papier à être nommé Trésor Vivant au Japon.


  Directrice artistique du projet : Hélène Nounou.

  Marquage à chaud : Créanog.

  Fabrication : Laurel Parker Book.
designs:
  - Sur mesure
fonctions:
  - Présenter
formes:
  - Livre
images:
  - /media/chaumet_reliure_bleu_02.jpg
  - /media/chaumet_reliure_bleu_03.jpg
  - /media/chaumet_reliure_bleu_01.jpg
  - /media/chaumet_reliure_02.jpg
secteurs:
  - Luxe
series:
  - Multiple
shop: false
title: 'Album de présentation, haute joaillerie, Chaumet'
translationKey: album-chaumet
___mb_schema: /.mattrbld/schemas/projet.json
---

