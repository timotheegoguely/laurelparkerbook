---
artist: Emmanuelle Fructus
client: Un Livre Une Image
cover: /media/fructus-51-01.jpg
date: 2019-11-11T00:00:00.000Z
description: >-
  Coffret de présentation se transformant en socle d’exposition. Toile Buckram,
  plastazote et marquage à chaud sur le fond du coffret.Présenté pour la
  première fois à l’exposition EXTRAordinaire : regards photographiques sur le
  quotidien, Institut pour la photographie, Lille.


  Création des personnages : Emmanuelle Fructus

  Fabrication du coffret : Laurel Parker Book  

  Design : Paul Chamard
designs:
  - Sur mesure
fonctions:
  - Archiver
  - Éditer
formes:
  - Coffret
images:
  - /media/fructus-51-01.jpg
  - /media/fructus-51-02.jpg
  - /media/fructus-51-03.jpg
secteurs:
  - Photo
  - Art contemporain
  - Archives
series:
  - Unique
shop: null
title: '51, d’Emmanuelle Fructus'
translationKey: '51'
___mb_schema: /.mattrbld/schemas/projet.json
foldedByHand: null
---

