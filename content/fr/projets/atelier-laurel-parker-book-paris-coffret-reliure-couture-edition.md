---
artist: null
client: Laurel Parker Book
cover: /media/bookcase_02.jpg
date: 2019-09-15T00:00:00.000Z
description: >-
  Un coffret contenant 15 coutures pour brochures de 1, 2 ou 3 cahiers.

  Créé comme un outil de présentation pour les graphistes et les directeurs
  artistiques, le bookcase donne un aperçu de coutures manuelles utilisées pour
  raffiner un projet de brochures ou de carnets. Un double étui permet de
  transporter et présenter cette collection.

  Design et fabrication : Laurel Parker Book

  Disponible dans notre boutique en ligne.
designs:
  - Semi-sur mesure
fonctions:
  - Archiver
  - Présenter
formes:
  - Chemise
  - Œuvre papier
  - Livre
images:
  - /media/bookcase.jpg
secteurs:
  - Luxe
  - Agence
  - Archives
series:
  - Multiple
shop: null
title: Bookcase
translationKey: bookcase
foldedByHand: null
___mb_schema: /.mattrbld/schemas/projet.json
---

