---
artist: Jean Royère
client: Galerie Jacques Lacoste
cover: /media/royere-album_lacoste_01.jpg
date: 2017-11-11T00:00:00.000Z
description: >-
  Série d'albums fac-similé des années 1930 comprenant des photographies du
  mobilier dessiné par Jean Royère. Restauration des pages par l’atelier Coralie
  Barbe.


  Nous avons reproduit la structure originale de l’album tout en palliant à ses
  défauts techniques pour lui apporter une plus grande solidité. Rafia tissé
  teinté, toile enduite, papier japonais, dos arrondi en bois par Maonia. Pour
  la galerie Jacques Lacoste. Design et fabrication : Laurel Parker Book
designs:
  - Sur mesure
fonctions:
  - Archiver
  - Présenter
formes:
  - Portfolio
  - Livre
images:
  - /media/royere-album_lacoste_01.jpg
  - /media/royere-album_lacoste_03.jpg
  - /media/royere-album-lacoste_02.jpg
secteurs:
  - Archives
series:
  - Multiple
shop: false
title: Album fac-similé du mobilier de Jean Royère
translationKey: album-jean-royere
___mb_schema: /.mattrbld/schemas/projet.json
---

