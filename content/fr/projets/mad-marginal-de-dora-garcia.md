---
artist: Dora Garcia
client: Rosascape
cover: /media/garcia-mad-marginal-02.jpg
date: 2011-11-10
description: |-
  Livre d’artiste avec des facsimilies des archvies du projet Mad Marginal, présenté à la Biennale de Venise, pavillon espagnol, 2011. 48 exemplaires. Boîte d’archives, système d’anneaux, en papier et toile. Marquage à chaud du colophon sur la couverture en 4 versions différentes. Design graphique et design des pochettes : Jean-Claude Chianale. Design de la boîte et fabrication : Laurel Parker Book.

  "Le livre d’artiste MAD MARGINAL s’inscrit en parallèle du projet éponyme sur lequel Dora García travaille depuis 2009. Sous la forme d’une boîte d’archive, ce livre d’artiste rassemble 35 documents ayant nourris la recherche de Dora García pendant ces deux ans. Ces documents sont ici reproduits à l’identique sous la forme de facsimilés. Traitant de la marginalité comme d’une position artistique et politique, Dora García s’intéresse aux différentes significations qu’une telle position peut recouvrir ainsi qu’à la contradiction et à la beauté de l’artiste comme figure marginale."
designs:
- Sur mesure
fonctions:
- Archiver
- Éditer
formes:
- Boîte d’éditeur
images:
- /media/garcia-mad-marginal-02.jpg
- /media/garcia-mad-marginal-01.jpg
secteurs:
- Art contemporain
series:
- Multiple
shop: false
title: Mad Marginal, Les archives, de Dora Garcia
translationKey: 'mad-marginal-dora-garcia '
---
