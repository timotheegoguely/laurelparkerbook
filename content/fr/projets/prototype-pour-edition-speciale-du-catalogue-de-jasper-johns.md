---
artist: Jasper Johns
client: Wildenstein Institute
cover: /media/johns-coffret-01.jpg
date: 2017-08-31
description: |-
  Prototype pour l’édition spéciale du Catalogue raisonné des peintures de Jasper Johns.
  Cinq reliures en matières diverses (veau écossais, toile de lin japonaise, toile italienne). Gardes en papier fait main par Ruscombe Mill, Margaux. Impressions en typo sur les pages volantes. Coffret en chêne noir des marais massif, finition brute, réalisé par Hervé Morin de l’atelier Maonia, Paris.
  Design et reliure par Laurel Parker Book
designs:
- Sur mesure
fonctions:
- Présenter
formes:
- Livre
- Coffret
images:
- /media/johns-coffret-01.jpg
secteurs:
- Archives
- Art contemporain
series:
- Prototype
shop: false
title: Prototype du Catalogue raisonné de Jasper Johns
translationKey: prototype-catalogue-raisonne-jasper-johns
---
