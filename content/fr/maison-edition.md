---
description: >-
  Laurel Parker Edition produit de l’art contemporain en multiple, toujours en
  pensant à l’objet-livre et l’objet artisanal.
layout: shop
menu:
  aside:
    weight: 4
slug: maison-edition
title: Maison d’édition
translationKey: maison-edition
___mb_schema: /.mattrbld/schemas/page.json
---
Depuis 2018, Laurel Parker Edition produit de l’art contemporain en multiple, toujours en pensant à l’objet-livre et l’objet artisanal.

Merci de nous contacter par mail concernant la disponibilité de nos éditions.
