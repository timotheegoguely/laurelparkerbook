---
categories:
- Reliure
- Livre d’artiste
- Expositions
cover: /media/paulin-binding-03.jpg
date: 2017-11-30
description: ""
title: '1981, 1998, 2008 : Pierre Paulin au Plateau'
---
Trois reliures uniques pour une exposition de l’artiste Pierre Paulin.

[**Pierre Paulin –**](https://www.fraciledefrance.com/pierre-paulin-boom-boom-run-run/) _Boom boom, run run  
_21.09 – 17.12.17 au Frac Île-de-France  
Commissaire de l’exposition : Xavier Franceschi

![](/media/paulin-binding-02.jpg)

« Le frac île-de-france présente au plateau la première exposition monographique consacrée à Pierre Paulin. Essentiellement composée de nouvelles productions, _Boom boom, run run_ déploie l’intérêt de l’artiste pour le langage, dont il multiplie ici les formes d’apparition, depuis la poésie, l’essai et la traduction, jusqu’au prêt-à-porter. »

![](/media/paulin-binding-03.jpg)