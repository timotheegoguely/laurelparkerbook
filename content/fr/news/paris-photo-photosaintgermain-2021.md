---
categories:
- Salon
cover: /media/photo-saint-germain-bandeau.jpg
date: 2021-11-09
description: Paris Photo & PhotoSaintGermain 2021
title: Paris Photo & PhotoSaintGermain 2021
translationKey: photo-saint-germain
---
À l'occasion du **salon Paris Photo et PhotoSaintGermain**, Laurel Parker Book vous propose de suivre l'atelier :

![](/media/photo-saint-germain-bandeau.jpg)

**- Hors les murs -**

**À la Galerie Christophe Gaillard** pour présenter _245_, d'Emmanuelle Fructus, dernière édition de Laurel Parker Edition parcours Rive Droite, PhotoSaintGermain

_245_ s'inscrit dans la tradition du livre en tissu pour enfant et ceux d'artistes, telle Louise Bourgeois qui a découpé, cousu, brodé sa garde-robe pour fabriquer _l'Ode à l'oubli_. Les pages sont ici réalisées à partir de draps chinés au même titre que les photos N&B des précédents tableaux. Surfaces sensibles témoignant du réel, le textile et le papier photo partagent un même rapport à la matérialité et signifient le temps. Tous deux permettent une réappropriation de l'œuvre par le geste.

![](/media/245-fructus-lpbedition-5.jpeg)

**Au salon Paris Photo** pour la présentation du coffret _Los Angeles Standards Portfolio_ de Caroline et Cyril Desroche sur le stand de Delpire & Co (stand SE12).

![](/media/delpire-losangeles-standards.jpg)

**- Galerie -**

Nous exposons actuellement le travail photographique d'Emmanuelle Fructus.  
  
_"Une image n'est jamais univoque. Si l'on ne peut résister à la proposition de cette artiste qui entretient un rapport vital et tactile à la photographie, c’est, que d'une singularité documentaire elle tisse un récit collectif. Il y a une quinzaine d'années ont émergé des silhouettes découpées dans du papier blanc issu de vracs d’images anciennes. L'atelier de Emmanuelle Fructus tient dans une boîte à chocolat : "je travaille sur les vies minuscules" dit-elle, faisant référence au livre de Pierre Michon. Elle découpe, colle, compose. Elle conjugue le temps et l'espace pour créer des tableaux uniques à partir d'une matière toujours originale, soustraite à un corpus de photographies anonymes. Cette fois, le principe des matrices et de la découpe au laser - qui ont prolongé le geste manuel de l'artiste lorsqu'il fallut découper plusieurs milliers de personnages - ont initié un tout autre projet avec sa complice de longue date, Laurel Parker. Il est alors question d'espace, de colorimétrie et de volume."_ extrait du texte de Natalie Amae, commissaire de l'exposition.

![](/media/rvb-cmj-com-photo-gregory-copitet.jpg)

**- Librairie -**   
  
**Plein de nouveautés en lien avec l'exposition** en cours sont arrivés dans notre librairie. Vous retrouverez :

\- _Books. Art, Craft & Community_, London Center for Book Art, 2021  
\- _Atlas des Régions Naturelles, vol 1 (ARN)_, Eric Tabuchi et Nelly Monnier, co-édition Poursuite et GwinZegal, 2021  
\- _C M Y,_ Hamada Yuji, 2015  
\- _Indexing Water, Notes on Representation Vol.9,_ Irene Kopelman, 2017  
\- _Labyrinth_, Olaf Nicolai et Jan Wenzel, édition Rollo Press, 2015  
\- _Patterns,_ Karel Martens, 2021  
\- The Shelf Journal n°3, n°4, n°5

Et bien d'autres livres à redécouvrir !

![](/media/selection2-books-rvb-cmj.jpg)