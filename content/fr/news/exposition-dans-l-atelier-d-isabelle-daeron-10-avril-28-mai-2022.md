---
categories:
- Livre d’artiste
- Expositions
cover: /media/atelier_daeron_expo_02.jpg
date: 2022-04-09
description: Exposition Dans l'atelier d'Isabelle Daëron
title: 'Exposition et livre : Dans l''atelier d''Isabelle Daëron (10 avril - 28 mai
  2022)'
translationKey: expo-daeron
---
Laurel Parker Book est ravi de présenter le travail d'Isabelle Daëron.

Accrochée sous la forme d'une galaxie, Isabelle Daëron nous livre une partie de son univers. Des dessins originaux, des prototypes de formes et de matières, des plans de recherches et des objets témoignent de ses derniers projets. L'exposition est réalisée en parallèle de la biennale de design de St Etienne.

![](/media/atelier_daeron_expo_01.jpg)

Une nouvelle édition limitée a été réalisée pour l'occasion !

L'existence est un ailleurs, livre d'Isabelle Daëron. 200ex, Laurel Parker Edition, 2022.

![](/media/existence_daeron_ed_lpb_05_w.jpg)

“Elle aimait la mer, la nature, le dessin. Au début, elle se destinait plutôt à l’architecture. Finalement, elle a choisi le design, qu’elle a apprisà Nantes, à Reims, puis à Paris, où elle est sortie en 2009 diplômée de l’Ensci (École nationale supérieure de création industrielle). Avec, déjà, un point de vue bien à elle. « J’étais assez critique par rapport au design. Concevoir des tables ou des chaises ne m’intéressait pas. Je voyais l’enjeu esthétique, mais je n’arrivais pas à y adhérer. » Pendant ses études, elle avait lu un texte expliquant que le design a pour mission de « contribuer à l’habitabilité du monde ». Alors elle a remonté l’origine de ce mot. « Il a été inventé par l’écrivain Louis-Sébastien Mercier (1740-1814), à propos de la possibilité d’habiter d’autres planètes. Ensuite, la littérature s’en est emparée. On le trouve chez Jules Verne, par exemple. Dans les années 1920-1930, il apparaît dans des études sur l’insalubrité des villes. Au début, le mot « habitable » contenait toujours une utopie, exprimait un projet de société, mais, peu à peu, la technique a pris le dessus. Dans les années 1960, « les sciences de l’habitabilité » ont été réduites à une liste de normes à appli-quer. Toutes les belles idées de départ ont débouché sur une logique consistant à reproduire toujours le même habitat, à imposer à tous la même manière de vivre, à créer un climat artificiel quelle que soit la région dans laquelle on habite. Je n’avais pas envie d’y contribuer. » Isabelle Daëron explore donc d’autres pistes. Pour elle, rendre le monde plus habitable, c’est, par exemple, redonner sa place à l’eau dans la ville. Elle s’est beaucoup intéressée au réseau d’eau non potable de Paris, construit au XIXe siècle, à l’époque d’Haussmann, par l’ingénieur Eugène Belgrand (1810-1878). La capitale française est l’une des rares villes au monde à disposer d’un tel sytème, qui permet de nettoyer rues et trottoirs et d’irriguer parcs et jardins, mais il est sous-utilisé. Pour lui redonner de l’intérêt, Isabelle Daëron a imaginé une bouche de rafraîchissement qui humidifie l’air en répandant de l’eau sur le sol, en cas de forte chaleur. Un prototype, nommé Aéro-Seine, a été construit dans le 20e arrondissement. La designer a ainsi appris à travailler avec les multiples services municipaux : le Pavillon de l’arsenal, centre parisien d’urbanisme et d’architecture, la Direction de la propreté et de l’eau, celle de la voirie et des déplacements, celle des parcs, jardins, squares et espaces verts... Quand elle parvient à « faire se parler entre eux des métiers qui ont chacun leur langage », elle jubile."

— Propos de Xavier de Jarcy, extrait du Télérama 3711 du 27/02/2021

[consulter le livret de l'exposition](/media/expo-daeron-livret-web.pdf)

[consulter le dossier de presse](/media/press_release_lpb_isabelle_daeron.pdf)

Lien vers son studio de design : [Studio Idaë](https://www.studioidae.com/en/)

![](/media/existence_daeron_ed_lpb_02_w.jpg)