---
categories:
- Livre d’artiste
- Expositions
cover: /media/rvb-cmj-com-photo-gregory-copitet.jpg
date: 2021-10-28
description: Exposition RVB-CMJ
title: 'Exposition et livre : RVB-CMJ d''Emmanuelle Fructus (30 oct - 22 déc 2021)'
translationKey: rvb-cmj-fructus
---
Laurel Parker Book est ravi de présenter le travail d'Emmanuelle Fructus.

![](/media/rvb-cmj-com-photo-gregory-copitet.jpg)

“Une image n'est jamais univoque. Si l'on ne peut résister à la proposition de cette artiste qui entretient un rapport vital et tactile à la photographie, c’est, que d'une singularité documentaire elle tisse un récit collectif.

Il y a une quinzaine d'années ont émergé des silhouettes découpées dans du papier blanc issu de vracs d’images anciennes. L'atelier de Emmanuelle Fructus tient dans une boîte à chocolat : "je travaille sur les vies minuscules" dit-elle, faisant référence au livre de Pierre Michon. Elle découpe, colle, compose. Elle conjugue le temps et l'espace pour créer des tableaux uniques à partir d'une matière toujours originale, soustraite à un corpus de photographies anonymes. Cette fois, le principe des matrices et de la découpe au laser - qui ont prolongé le geste manuel de l'artiste lorsqu'il fallut découper plusieurs milliers de personnages - ont initié un tout autre projet avec sa complice de longue date, Laurel Parker. Il est alors question d'espace, de colorimétrie et de volume..." [read more](/media/press_release_lpb_rvb_cmj_emmanuelle_fructus.pdf)

— Nathalie Amae, commissaire de l'exposition

[consulter le livret de l'exposition](/media/press_release_lpb_rvb_cmj_emmanuelle_fructus.pdf)

[consulter le dossier de presse](/media/press_release_lpb_isabelle_daeron.pdf "dossier_de_presse")

crédit photos de l'exposition © Gregory Copitet

![](/media/245-fructus-lpbedition-5.jpeg)

À l'occasion de l'exposition, Laurel Parker Edition réalise un nouveau projet avec l'artiste, Emmanuelle Fructus.

_245_ s'inscrit dans la tradition du livre en tissu pour enfant et ceux d'artistes, telle Louise Bourgeois qui a découpé, cousu, brodé sa garde-robe pour fabriquer _l'Ode à l'oubli_. Les pages sont ici réalisées à partir de draps chinés au même titre que les photos N&B des précédents tableaux. Surfaces sensibles témoignant du réel, le textile et le papier photo partagent un même rapport à la matérialité et signifient le temps. Tous deux permettent une réappropriation de l'œuvre par le geste.

![](/media/245-fructus-lpbedition.jpeg)  
_245_, Emmanuelle Fructus, 2021. Laurel Parker Edition, Romainville. 8 ex + 2 A.P.