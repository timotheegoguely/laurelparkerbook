---
categories:
- Livre d’artiste
- Conférence
cover: /media/semaine-thematique2015.jpg
date: 2015-04-17
description: ""
title: '« Éditer » ? Formes d’un engagement : conférence à Amiens'
translationKey: ""
---
Laurel Parker a été invité à l’ESAD d’Amiens par le graphiste-artiste Jean-Claude Chianale (de la [Bibliothèque Périphérique](http://www.jeanclaude-design.com/mp2013/)) pour participer à la semaine thématique _«_ [_Éditer » ? Formes d’un engagement_](http://www.esad-amiens.fr/semaine-thematique-du-31-mars-au-02-avril-2015/5693)_._ Cette conférence de trois jours a accueilli des personnalités diverses du monde de l’édition de livre : Élodie Boyer des Éditions Non-Standard, Frédéric Teschner des Éditions Naïca, Alexandre Laumonier des Éditions Zones Sensibles, Pauline Nunez des Éditions Ypsilon, Morgane Rébulard du Shelf Journal, l’historien du design Tony Côme, l’artiste de livres pop-up Lise Bonneau, Alessandro Ludovico critique de media et chef éditeur de Neural magazine, entre autres. Voici un [lien au programme](http://www.esad-amiens.fr/wp-content/uploads/2015/03/ESAD-20150402-PROG-07P.pdf) de la semaine thématique.

![](/media/semaine-thematique2015.jpg)

Dans sa présentation _« Livres d’artiste, un travail de collaboration »_, Laurel Parker traite de la nature collaborative de la production d’éditions de livres d’artiste en petite série. Quelques projets ont été exposés, du concept à la réalisation, en citant l’importance de la collaboration entre l’éditeur, l’artiste, le graphiste, l’imprimeur, et le relieur. Les livres qui ont été présentés proviennent des éditions Christophe Daviet-Thery, Toluca Editions, mfc Michèle Didier, les Éditions Xavier Barral, les éditions Rosascape, et des éditions auto-produites par des artistes.

Un grand merci à Jean-Claude Chianale et à l’ESAD d’Amiens pour cette expérience enrichissante.