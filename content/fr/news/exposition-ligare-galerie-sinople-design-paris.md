---
draft: false
title: 'EXPOSITION LIGARE A LA GALERIE SINOPLE, PARIS (5 SEPT AU 5 OCT 2024)'
translationKey: ligare
description: >-
  En deux actes de la Paris Design Week à la Paris Fashion Week, SINOPLE
  présente « Ligare », une installation entre architecture, design, art et mode.
date: '2024-11-14'
categories:
  - exposition
cover: /media/sinople-galerie-paris-exposition-ligare-05.jpg
___mb_schema: /.mattrbld/schemas/news.json
---
Du 5 septembre au 5 octobre 2024, en deux actes de la Paris Design Week à la Paris Fashion Week, SINOPLE présente « Ligare », une installation entre architecture, design, art et mode.

Réunies par leurs explorations et métissages culturels autour de trois fibres naturelles, du Japon au Ghana, Laurel Parker Book, Inès Bressand Studio et Miyouki Nakajima dressent un panorama artistique ou fonctionnel des usages innovants et contemporains du papier washi, de la soie et d’une graminée africaine.

![](/media/sinople-galerie-paris-exposition-ligare-01.jpg)

Réunies par leurs explorations et métissages culturels autour de trois fibres naturelles, du Japon au Ghana, Laurel Parker Book, Inès Bressand Studio et Miyouki Nakajima dressent un panorama artistique ou fonctionnel des usages innovants et contemporains du papier washi, de la soie et d’une graminée africaine.

![](/media/sinople-galerie-paris-exposition-ligare-04.jpg)

Ces matériaux rencontrés au cours de leurs résidences, ou ces techniques de mise en œuvre de la fibre apprises en autodidacte leur permettent d’ennoblir et de donner forme à un matériau simple. Il en résulte des productions atypiques, contemporaines, qui portent un regard neuf sur des savoir-faire traditionnels. La fibre se fait alors lien entre des pratiques ancestrales et hybrides, entre les cultures dont elles sont originaires et celles de ces créatrices.

![](/media/sinople-galerie-paris-exposition-ligare-02.jpg)

Le papier devient surface ou volume pour l’architecture, l’herbe se meut en mobilier ou accessoires de mode tandis que la soie s’abandonne à la sculpture ou à la composition abstraite. Dans cet ensemble, l’ombre et la lumière composent avec les reflets et les lignes des matériaux.

Sinople, Hôtel de Retz 9, rue Charlot Paris IIIèm

05 sept. – 05 oct. mar. – sam. 12:00 – 19:00

Vernissage le 5 septembre de 18:00 à 21:00

![](/media/sinople-galerie-paris-exposition-ligare-03.jpg)
