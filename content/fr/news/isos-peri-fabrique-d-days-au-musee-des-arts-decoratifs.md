---
categories:
- Projet d’artiste
- Expositions
cover: /media/peri-fabrique-isos_02.jpg
date: 2014-05-19
description: ""
title: ISOs – Péri’Fabrique, D’Days au Musée des Arts Décoratifs
translationKey: ""
---
du 20 au 25 mai, Musée des Arts Décoratifs; Hall des Maréchaux _du dossier de presse de Undo-Redo :_

![](/media/peri-fabrique-isos_04.jpg)

![](/media/peri-fabrique-isos_02.jpg)

_ISOs_, une œuvre expérimentale et novatrice née de la rencontre entre Undo-Redo (le duo de graphistes Nicola et Teresa Aguzzi), et les tisserands des tapisseries d’art l’Atelier 3 (Frédérique Bachellerie et Peter Schönwald).

Renouant avec l’émulation des faubourgs, le programme de co-création Péri’Fabrique finance chaque année la création de pièces uniques par un artisan d’art et un designer. Associé à l’Atelier 3, tisserands des tapisseries d’art, le studio de création graphique Undo-Redo a crée «ISOs».

##### _«_ ISOs » est une affiche tissée.

Le dialogue entre les matériaux utilisés – du tissu en papier et du papier à l’aspect du tissu – et les techniques employées – tissage sur métier et reliure – naît du désir de rapprocher le savoir faire du tissage à celui du graphisme et de l’édition en combinant leurs langages respectifs.

Nous avons emprunté à l’univers de l’édition l’échelle du format de papier A (standard international ISO 216), pour créer un module à décliner. La progression de ce module, qui rétrécit proportionnellement dans sa taille au fur et à mesure que son ton augmente dans la gamme chromatique, est le motif de la tapisserie.

Le module de départ, de la taille d’une feuille A0, correspond à la partie tissée par l’Atelier 3. Ce rectangle monochrome d’1 m2 présente une trame très épaisse, réalisée avec des bandes de papier roulées à la main et des fils en coton.

Reliée à ce bloc uni dans sa partie inférieure, une mosaïque chromatique reconstitue le format A0. Elle est réalisée avec 8 feuilles de papier de la Maison Takeo, papetier japonais d’exception. La texture du papier, très marquée dans les premiers modules à l’instar d’une trame en tissu, s’attenue au fur et à mesure, jusqu’à devenir totalement lisse dans le dernier module.

L’assemblage minutieux des éléments a été réalisé selon la technique de reliure de livres d’art par l’atelier **Laurel Parker Book**, maître dans ce domaine. Les modules, reliés au fil de chaîne, sont volontairement espacés entre eux pour créer une tapisserie mouvante où l’air et la lumière circulent librement.

![](/media/peri-fabrique-isos_01.jpg)

©Marco Illuminati

![](/media/peri-fabrique-isos_06.jpg)

©Marco Illuminati

![](/media/isos-expo-marco_illuminati.jpg)

Nicola Aguzzi du studio Undo-Redo et Peter Schönwald de l’Atelier 3 au vernissage au Musée des Arts Décoratifs. ©Marco Illuminati