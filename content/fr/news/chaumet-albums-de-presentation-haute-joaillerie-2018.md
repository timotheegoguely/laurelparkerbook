---
categories:
- Press book
- Reliure
- Album photo
cover: /media/chaumet_les-mondes-01.jpg
date: 2018-04-24
description: ""
title: Chaumet, albums de présentation, haute joaillerie 2018
---
Nous avons recroisé la route de la maison Chaumet pour réaliser leurs reliures de présentation. Ces albums permettent à la maison Chaumet de présenter sa nouvelle collection de haute joaillerie sous forme de dessins.

![](/media/chaumet_les-mondes-01.jpg)

Ce projet permet de travailler en collaboration avec Hélène Nounou, avec qui nous ciblons les nouveaux matériaux en rapport avec l’identité graphique de Chaumet.

![](/media/chaumet_les-mondes-04.jpg)

Nous avons gardé la même structure que l’année dernière, une reliure montée sur onglet, pour profiter de son ouverture exceptionnelle, ce qui rend la manipulation plus aisée et met en valeur les gouachés des bijoux. Une nouvelle fois, notre choix s’est porté sur le papier japonais mitsumata, accompagné cette année d’une toile et de papiers couleur grège.

La qualité du marquage à chaud se révèle de manière subtile avec ces associations colorées.

![](/media/chaumet_reliure_02.jpg)

Collection de haute joaillerie Chaumet, reliure sur onglets.  
Toile grège, papier grège, montage sur onglet en papier japonais, marquage à chaud, 10 exemplaires, 2018.

Conception graphique : Hélène Nounou.  
Marquage à chaud : Créanog.  
Fabrication : Laurel Parker Book