---
categories:
- Reliure
- Livre d’artiste
cover: /media/joly_terre-exil-01.jpg
date: 2016-04-30
description: ""
title: Terres d’exil, de Jean-François Joly
translationKey: ""
---
_Terres d’exil_ est une édition regroupant 5 photographies de [Jean-François Joly ](http://jfjoly.com/en)et limitée à 10 exemplaires. Commandé par [Wallguest](https://wallguest.com/), ce projet a été réalisé parallèlement à l’exposition de Jean-François Joly à la [Maison Européenne de la Photographie](http://www.mep-fr.org/evenement/jean-francois-joly-terres-dexil/) en mai 2016.  
Ce livre fonctionne à la manière d’un album de photographie style XIXe siècle : les tirages sont glissés dans des marie-louise. L’ensemble est réalisé en papier Richard de Bas fait main sur une structure en carton sans acide.

![](/media/joly_terre-exil-05.jpg)

![](/media/joly_terre-exil-03.jpg)

![](/media/joly_terre-exil-02.jpg)