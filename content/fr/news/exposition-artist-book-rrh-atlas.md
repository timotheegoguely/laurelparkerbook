---
draft: null
title: Exposition Atlas of the World RRHV (14 mai au 28 juillet 2023)
translationKey: expoatlas
description: >-
  Exposition du livre d'artiste Atlas of the World RRHV. Des artistes Rokni et
  Ramin Haerizadeh, et Hesam Rahmanian. Textes de Vahid Davar.
date: '2023-07-28'
categories:
  - expositions
cover: /media/nkn-4436-hd-photo-gregory-copitet-site.jpg
___mb_schema: /.mattrbld/schemas/news.json
---
Un exemplaire de Philip’s Atlas of the World est réorganisé et relié en fonction des schémas de déplacement des réfugiés du monde entier. Le livre comprend deux textes de Vahid Davar : ‘Nassim’s Testament’, un poème épique sur le voyage d’asile onirique de deux amis de l’Iran au Royaume-Uni, et l’essai ‘A Chimerical World with a Nomenclature of its Own’ qui est un commentaire sur le poème. Les textes sont placés sur des pages en couvertures de survie dorées. La version persane originale de ‘Nassim’s Testament’ est inclue dans une découpe au centre du livre. Une série d’images tirées sur papier japonais font écho à l’histoire des réfugiés.

![](/media/nkn-4436-hd-photo-gregory-copitet-site.jpg)

Rokni Haerizadeh (n. 1978, Téhéran), Ramin Haerizadeh (n. 1975, Téhéran), et Hesam Rahmanian (n. 1980, Knoxville) vivent et travaillent ensemble depuis 2009. Leur oeuvre est le fruit de l’énergie engendrée par des relations en constante évolution : entre eux, dans leur travail et dans leur environnement. De nouveaux collaborateurs, matériaux et événements vont et viennent dans cet espace. Leurs peintures, vidéos, objets et livres offrent juste ce qu’il faut pour piquer la curiosité sur les conditions de leur création.

Vahid Davar (n. 1981, Shiraz) vit en Écosse où il est chercheur doctorant en études persanes à l’université de St Andrews. Il est auteur de deux volumes de poésie très appréciés en Iran. Il est lauréat du prix Farzaneh 2022 du meilleur article sur la littérature persane.

![](/media/nkn-4433-web-photo-gregory-copitet-site.jpg)

Édition de 10 + 4EA produite par la galerie In Situ - Fabienne Leclerc

Design et reliure par Laurel Parker Book

Design graphique par Marine Bigourie

[Lien vers la fiche Projet de l’édition](https://www.laurelparkerbook.com/fr/projets/rrhv-atlas-of-the-world/)

crédit photos de l’exposition © Gregory Copitet

![](/media/nkn-4425-web-photo-gregory-copitet-site.jpg)
