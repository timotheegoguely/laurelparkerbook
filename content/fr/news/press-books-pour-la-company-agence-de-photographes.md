---
categories:
- Press book
cover: /media/la-compagny-press_book_cuir_01.jpg
date: 2014-12-01
description: ""
title: Press books pour La Company, agence de photographes
translationKey: ""
---
[La Company](http://www.lacompany.net/accueil.html), une agence de photographes et producteurs, est un client de longue date pour qui on a beaucoup d’admiration et de respect. Nos premiers press books fabriqués pour eux datent de 2009, et ils sont toujours en service !

![](/media/la-compagny-press_book_cuir_01.jpg)

Chaque photographe représenté par La Company a un press book d’une couleur différente, mais toujours de la même gamme de cuir vachette, traité pour l’usage intensif. Un joli marquage en argent « miroir » est discret et classe. Nous adorons les nouvelles couleurs, choisies en fonction du travail des artistes représentés dedans. Le choix de matières et couleurs montre le soin pris par l’agence dans l’approche de leur métier.

![](/media/la-compagny-press_book_cuir_02.jpg)