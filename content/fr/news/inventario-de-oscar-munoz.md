---
categories:
- Livre d’artiste
cover: /media/munoz-inventario-03.jpg
date: 2014-10-02
description: ""
title: Inventario, de Oscar Muñoz
translationKey: ""
---
Un nouveau projet de livre d’artiste pour les éditions [Toluca](http://www.tolucaeditions.com/). Ocscar Muñoz, artiste contemporain colombien, vient de cloturer son exposition au Jeu de Paume. Cet objet prend la forme d’un trieur à soufflet, objet de bureau, pour donner une forme atypique au rangement de ses photographies. Son travail artistique parle de l’apparition et de la disparition de l’image, ainsi que de la mémoire. _Inventario_ est une édition de 17 exemplaires en papiers japonais, avec des éléments en laiton.

![](/media/munoz-inventario-01.jpg)

Le montage a été fait entièrement à la main. On adore ce projet et cet artiste !

![](/media/munoz-inventario-02.jpg)