---
categories:
- Expositions
cover: /media/42x60_plan_web.jpg
date: 2022-02-19
description: 42x60, Displays Art Work in Public Spaces
title: 42 x 60, Displays Art Work in Public Spaces
translationKey: 42x60expo
---
**42 x 60, Displays Art Work in Public Spaces**  
Exposition du 20 février au 03 avril 2022  
Vernissage le 20 février de 14h à 18h

42 x 60 est invité à exposer simultanément à Laurel Parker Book et dans le quartier culturel de Romainville : les galeries de Komunuma, les réserves du FRAC, Parsons School, la Fondation Fiminco, Quai 36, Après Midi Lab et Coutume Café.

Une nouvelle affiche de Sara MacKillop est éditée pour l'occasion !

[Visualiser le livret de l'exposition](https://www.laurelparkerbook.com/fr//media/livret-site.pdf)

[Lien vers 42x60.com](https://42x60.com/ "42x60")

![](/media/42x60saramackillop.jpg)![](/media/42x60_rue_12_web.jpg)

Depuis seize ans, 42x60 appose chaque année, dans l’espace public à Paris, trois mille exemplaires d’une nouvelle affiche au format A2, spécialement conçue par un artiste.

Cette activité a commencé en septembre 2006, avec une composition photographique créée par l’artiste wallon Patrick Everaert, connu pour son travail de recyclage des images de l’imaginaire indirect, qu’il compose à partie de prélèvements dans des sources médiatiques dont, de préférence, il ne comprend pas la langue.

Ces affiches sont apparues à un rythme irrégulier dans une quinzaine de quartiers parisiens, parfois à l’étranger. Plusieurs affiches arborent des corps fragmentés, stoppés net dans un mouvement arrêté, à l’image du bras en perspective frontale de Georges Tony Stoll (2007), des combattants en action de Danielle Gutman Hopenblum (2016) ou des mains et des pieds en fleurs de Toru Nagahama (2009). Les nus sont récurrents.

Nombreuses sont les images faisant écho à l’architecture, proposant un autre espace qui trouerait les murs de l’espace urbain qu’elles recouvrent, à l’instar des coins de table de Michel Bousquet (2019) ou de Saara Ekström (2010) ; sur la plage de Susanna Hesselberg (2007) repose face contre sable un Icare/albatros, Exilé sur le sol au milieu des huées. Recomposés, ces espaces sont souvent savants et d’une géométrie complexe, à l’image de l’origami de triangles et de disques de Franziska Holstein (2015).

42x60 n’est pas loquace. Les mots n’y sont pas proscrits, mais demeurent rares. Ils sont plutôt réservés aux vidéos publiées en parallèle sur le site internet éponyme, prononcés avec infiniment de délicatesse par les artistes eux-mêmes : Je peux partir de rien, s’il y a la lumière, assure par exemple Dries Segers en octobre 2018.

Perpétué par Carole Chichet et Malvina Silberman, l’art du placardage est vieux comme le monde ; au Moyen-Âge les disputationes – ces joutes verbales sur un point de doctrine religieuse – trouvaient ainsi un écho au sein de la communauté, d’autant plus large qu’ils abandonnaient le latin pour se fondre dans la langue vulgaire. Longtemps, l’affichage fut réservé aux paroles royales ou religieuses, puis vinrent les harangues, la propagande, la réclame. L’affiche de ville, ou bilboquet, connut alors un tel essor qu’on alla jusqu’à la qualifier de reine de la ville.

C’est l’orgueil de l’art, d’être inutile. Ce qui ne signifie nullement qu’il ne sert à rien. En venant à notre rencontre sans que nous les sollicitions, les affiches semées dans la métropole par 42x60 me font penser aux alliés décrits par l’écrivain anthropologue Carlos Castaneda, ces entités inorganiques protectrices qui parfois miraculeusement se matérialisent, nous sollicitent par leur seule présence mais, si nous ne les remarquons pas, s’évanouissent à jamais sans laisser aucune trace de leur manifestation terrestre : les négligeant nous n’aurions plus, alors, à nous en prendre qu’à nous-mêmes.

**- propos de Stéphane Corréard**

![](/media/42x60_rue_01_web.jpg)

![](/media/42x60_affiches_all.jpg)

Les artistes exposés :

Gilles Berquet, Michel Bousquet , Marjolijn De Wit, John Divola , Jeanne Dunning, Saara Ekström, Patrick Everaert, Dorothee Golz, Danielle Gutman Hopenblum, Susanna Hesselberg, Franziska Holstein, Paul Kooiker, Sara MacKillop, Toru Nagahama, Paul Pouvreau, Peter Puklus, Dries Segers, Yann Serandour, Jana Sterbak, Georges Tony Stoll, Clare Strand, Ruth Van Beek et Jeanine Woollard.