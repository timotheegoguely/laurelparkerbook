---
categories:
- Boîte de présentation
cover: /media/FFleurance5web.jpg
date: 2014-03-15
description: ""
title: Pas juste une boîte A4
---
[Frédérique Fleurance](http://www.frederique-fleurance.fr/), artiste plasticienne, est venue chez nous avec une idée très précise de ce qu’elle cherchait. Elle voulait présenter des images de son travail sur feuilles A4, un format facile et pas cher à imprimer. Elle avait même un dessin préparatoire de sa boîte de rêve.

![](/media/fleurance-boite-presentation-02.jpg)

Boîte recouverte de papier japonais couleur kraft, marquage à sec, rivets nickelés, et élastique.

![](/media/fleurance-croquis-01.jpg)