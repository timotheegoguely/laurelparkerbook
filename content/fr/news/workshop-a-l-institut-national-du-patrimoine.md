---
categories:
- Enseignement
- Workshop
cover: /media/workshop_inp_conditionnement_3.jpg
date: 2017-06-29
description: ""
title: Workshop à l’Institut National du Patrimoine
translationKey: ""
---
Laurel Parker a été invitée par l’Institut National du Patrimoine pour faire un workshop au sujet du conditionnement des œuvres pour les étudiants de leur département restauration à Aubervilliers.

![](/media/workshop_inp_conditionnement_1.jpg)

L’INP est un établissement français d’enseignement supérieur du ministère de la Culture. Il a pour mission la formation initiale des conservateurs du patrimoine de l’État, de la fonction publique territoriale et de la ville de Paris ainsi que la formation des restaurateurs du patrimoine habilités à travailler sur les collectons publiques.

Les étudiants ont appris les étapes pour réaliser le conditionnement d’un objet : constater le besoin pour la conservation de l’objet ; décider quel type de conditionnement convient au besoin ; mesurer l’objet ; choisir les matières de fabrication les mieux adaptées ; fabriquer l’objet.

![](/media/workshop_inp_conditionnement_3.jpg)

Après la fabrication d’une première boîte de conservation pour un objet simple à conditionner (un livre), ils sont passés à des objets plus difficiles comme les suivants :

\- des plaques de gravure en cuivre + des tirages sur papier chiffon  
\- plusieurs jeux de cartes, chacun dans un compartiment  
\- des photographies sur plaques de verre  
\- une peinture sur métal restaurée  
\- un jeu de société avec plusieurs éléments