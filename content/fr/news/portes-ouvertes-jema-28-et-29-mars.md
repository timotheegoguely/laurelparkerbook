---
categories:
- Portes ouvertes
cover: /media/jema-cocarde.jpg
date: 2015-03-17
description: ""
title: Portes Ouvertes – JEMA – 28 et 29 mars
translationKey: ""
---
Bientôt le printemps! Nous ouvrons nos portes pour les JEMA : Journées Européennes des Métiers d’Arts. C’est le moment pour venir nous voir, découvrir notre travail, et discuter de vos projets.

**Les 28 et 29 mars, du 11h à 19h**

![](/media/jema-cocarde.jpg)

Dans notre batîment, les _Ateliers M1D_, vous pourrez voir nos nouveautés et découvrir le travail de nos voisins :  
**Ateliers M1D**  
_Laurel Parker Book,_ relieur  
_Judith Bourdin, de novembre_, ennoblisseur textile  
_Pièces Uniques_, bijoutier en métaux précieux  
_Stéphane Ruchaud_, photographe illustrateur  
_Janaïna Milheiro_, plumassier et ennoblisseur textile  
_Atelier Frémy_, imprimeur en sérigraphie  
_Vous pouvez dormir dans la grange,_ sacs

![](/media/jema-atelier_lpbook-01.jpg)

![](/media/jema-atelierprimo2.jpg)![](/media/jema-atelierprimo3.jpg)