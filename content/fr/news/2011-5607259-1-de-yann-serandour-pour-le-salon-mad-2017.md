---
categories:
- Salon
- Projet d’artiste
- Livre d’artiste
cover: /media/atelier_vue_01.jpg
date: 2017-05-28
description: ""
title: 2011/5607259-1 de Yann Sérandour pour le Salon MAD 2017
---
Nous avons été invité pour collaborer à nouveau avec notre ami [Yann Sérandour](http://www.rearsound.net) sur une édition spécialement conçue pour le Salon MAD 2017 [à la Maison Rouge](http://multipleartdays.com) : _2011/5607249-1_.

_« À l’occasion de la troisième édition du salon MAD, Yann Sérandour a imprimé un compte à rebours de 5607249 à 1 sur une ramette de 500 feuilles. 5607240 est le nombre par lequel l’œuvre programmatique de Roman Opalka 1965/1 – ∞ s’acheva en 2011. Générée par ordinateur en moins d’une seconde, cette transcription typographique en constitue tout autant la rétrospective et le catalogue inversés. » – dossier de presse du Salon MAD_

Notre intervention a été de trouver une manière de contenir les 500 feuilles. Yann Sérandour aimait bien notre emballage de carnet de fin d’année 2014 – un _furoshiki_ en toile de reliure en coton enduit. Après plusieurs tests, nous avons adapté le _furoshiki_ pour qu’il ressemble à une enveloppe similaire aux emballages de ramettes de papier de bureau.

**Yann Sérandour**  
 **2011/5607249-1**

**Impression numérique sur ramette de 499 feuilles A4**  
 **Munken Print White 18 115g**  
 **Seveso Canvas Extra R Fantasia 160g/m2**  
 **Fiicher .txt généré par Éric Sérandour le 02/11/2012 à 14h33**

**12 exemplaires numérotés et signés (+2 E.A. + 2 H.C.)**  
 **Édition MAD, Paris, 2017**

IMG

Yann Sérandour au Salon MAD avec l’œuvre _2011/5607249-1_ exposé sur le stand du CNEAI.[__](http://laurelparkerbook-blog.com/wp-content/uploads/2017/09/serandour_salon_mad_2017_1w.jpg)

IMG

L’œuvre _2011/5607249-1_ sur le stand du CNEAI au Salon MAD.[](http://laurelparkerbook-blog.com/wp-content/uploads/2017/09/serandour_salon_mad_2017_2w.jpg)

IMG

L’œuvre _2011/5607249-1_ exposée sur le mur au Salon MAD 2017.[](http://laurelparkerbook-blog.com/wp-content/uploads/2017/09/serandour_salon_mad_2017_1w.jpg)