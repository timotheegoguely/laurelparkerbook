---
categories:
- Livre d’artiste
cover: ""
date: 2014-09-27
description: ""
draft: true
title: La genèse d’une idée
---
Voilà la genèse d’une idée…le petit Jean en train de jouer donne le principe central pour un livre d’artiste. L’éditrice Michèle Didier est venue vers nous avec l’idée des dessins de Yona Friedman reliés sur des pages détachables. Grâce à Jean, nous avons eu l’idée de faire une découpe laser autour des dessins, qui à la fois garde la forme orginale de chaque dessin, et laisse les traces du dessin sur chaque page.

Le montage des tirages sur le mur de la galerie Michèle Didier imite la façon dont les dessins originaux étaient montés sur le mur chez l’artiste.

1001 nuits + 1 jour de Yona Friedman, éditions [mfc Michèle Didier](http://www.micheledidier.com/index.php/gb/artists/mfc-yona-friedman/1001-nuits-et-1-jour.html).