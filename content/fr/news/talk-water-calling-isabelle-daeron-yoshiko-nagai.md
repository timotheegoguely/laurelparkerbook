---
draft: null
title: 'Talk “Thinking by Drawing” d''Isabelle Daëron & Yoshiko Nagai '
translationKey: talkwatercalling
description: >-
  Talk autour de l'histoire de l'eau à Kyoto et de l'édition Water Calling
  d'Isabelle Daëron, en collaboration avec Yoshiko Nagai.
date: '2023-09-09'
categories: null
cover: /media/water-calling-site.jpg
___mb_schema: /.mattrbld/schemas/news.json
---
Talk : “Thinking by Drawing” Isabelle Daëron & Yoshiko Nagai samedi 9 septembre 2023 à 15h30

![](/media/daeron-water-calling-talk2.jpg)

Isabelle Daëron a été en résidence à Kyoto pour mener des recherches sur le terrain pour ce projet. Avec une carte de Kyoto et des informations sur sa topograpie en main, elle a étudié le débit de l’eau en utilisant la méthode “thinking by drawing” (penser en dessinant). Lors de cette conférence, elle expliqera comment elle a intégré ses recherches sur l’eau à Kyoto, complété ses dessins et les a compilés dans le livre Water Calling en étroite collaboration avec la commisaire Yoshiko Nagai.

![](/media/portrait-isabelle-yoshiko-web.jpg)
