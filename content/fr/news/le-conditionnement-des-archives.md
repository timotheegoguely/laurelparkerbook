---
categories:
- Boîte de conservation
cover: /media/patiala-boite-conservation-02.jpg
date: 2018-02-28
description: ""
title: Le conditionnement des archives
---
Au fil des années, nous avons plaisir à retrouver les archives de la maison Cartier et les collections de l’Ecole Nationale Supérieure des Beaux-Arts. Ces deux archives souhaitent réaliser des boîtes de conservation pour remédier à certaines de leurs contraintes. Focus sur deux projets de conservation.

![](/media/patiala-boite-conservation-02.jpg)

Pour les archives de Cartier, réalisation d’une grande boite pour conserver le portfolio « Patiala » et des tirages photographiques grands formats.  
Cette boite de conservation a été réalisée autour du portfolio, elle comprend une épaisseur de cales pour ranger de nouvelles planches d’images qui ne sont pas comprises dans le portfolio.

![](/media/patiala-boite-conservation-01.jpg)

Pour l’Ecole Nationale des Beaux Arts, la fabrication de boîte de conservation permet de protéger les incunables de la collection Masson. Ces livres ont traversés le temps durant plus de 500 ans. Ils ont parfois une forme atypique comme celui-ci qui ressemble à un ballon de rugby. La prise des mesures est importante pour réaliser la boite adéquate.

![](/media/ensba-boite-conservation-01-1.jpg)

![](/media/ensba-boite-conservation-02-1.jpg)