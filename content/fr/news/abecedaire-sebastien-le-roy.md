---
categories:
- Coffret
- Livre d’artiste
- Projet d’artiste
cover: /media/livre_artiste_abecedaire_1web.jpg
date: 2015-01-27
description: ""
images:
- /media/livre_artiste_abecedaire_1web.jpg
- /media/livre_artiste_abecedaire_2web.jpg
- /media/livre_artiste_abecedaire_detail.web_.jpg
- /media/livre_artiste_abecedaire_luxe1web.jpg
- /media/livre_artiste_abecedaire_luxe2web.jpg
- /media/livre_artiste_abecedaire_luxe3web.jpg
title: Abécédaire de Sébastien Le Roy, deux objets pour une édition
---
![](/media/livre_artiste_abecedaire_1web.jpg "Abécédaire de Sébastien Le Roy. Éditions Les Paroles Gelées, 2014.")

Notre dernier travail de 2014 était un projet pour la nouvelle maison d’édition Les Paroles Gelées. Les éditeurs sont venus nous voir avec leur première édition du livre, L’Abécédaire de Sébastien Le Roy, déjà imprimée et reliée par l’imprimerie Trace.  Le livre contient 58 linogravures originales, dans une édition de 300 exemplaires sur papier Old Mill, et 30 exemplaires de tête imprimés sur papier vélin d’Arches accompagnés d’une gravure inédite et signée par l’artiste.

L’édition avait déjà été reliée à la japonaise, avec une couverture en papier. Les éditeurs cherchaient un bel étui, simple, et dans le même esprit que le livre. Le seul hic était de trouver une solution de design pour les deux éditions :  l’édition courante et l’édition de tête.

Pour l’édition de 300 exemplaires, nous avons proposé un étui en carte repliée, avec un découpage à la forme et un montage manuel, pour un objet simple à fabriquer. L’étui en carte doublée donne une rigidité au livre, tout en restant un objet en papier. Il laisse visible la couture au dos du livre.

La carte est un beau papier japonais, couleur kraft, pour faire sortir la beauté du papier du livre, sans ajouter une couleur avec trop de caractère. Un marquage à chaud en noir mat reprend le même dessin que sur la couverture du livre. L’ensemble est chic mais discret.

![](/media/livre_artiste_abecedaire_2web.jpg "Abécédaire de Sébastien Le Roy, édition courante.")

Pour l’édition de tête, nous avons gardé la même forme d’étui en carte repliée, la même matière, et le même marquage, mais nous avons transformé l’objet en coffret. Un abattant qui ferme le coffret se déplie pour dévoiler la gravure.

![](/media/livre_artiste_abecedaire_luxe1web.jpg "Abécédaire de Sébastien Le Roy, édition de tête avec une gravure signée")

![](/media/livre_artiste_abecedaire_luxe2web.jpg "Abécédaire de Sébastien Le Roy, édition de tête ouverte.")

![](/media/livre_artiste_abecedaire_luxe3web.jpg "Abécédaire de Sébastien Le Roy, édition de tête.")

Nous remercions Les Paroles Gelées et Sébastien Le Roy pour avoir travaillé avec nous sur leur première édition.

_Abécédaire_, de Sébastien Le Roy Éditions Les Paroles Gelées, Paris, 2014. 300 exemplaires + 30 exemplaires de tête