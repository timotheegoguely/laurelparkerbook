---
categories:
- Press book
- Coffret
- Boîte de présentation
cover: /media/carrefour_press_book_3.jpg
date: 2018-01-31
description: ""
title: Books de présentation pour un projet à Monaco
translationKey: ""
---
Nous réalisons des objets de présentation pour les cabinets d’architecture. Ces outils leurs permettent de présenter leurs idées aux clients et de gagner des appels à projets.

![](/media/carrefour_press_book_3.jpg)

De nombreux éléments doivent être agencés pour ce genre de projet comme des plans, clef usb, dossiers de différents formats, logos… Autant d’astuces à trouver pour les regrouper de manière cohérente et esthétique.

C’est pour réaliser un coffret sur mesure que le studio [**Grafikmente**](http://www.grafikmente.fr/) nous a contacté. Les nombreux éléments composant le coffret n’étaient pas encore définis au lancement de la fabrication, il nous a fallu concevoir des structures pouvant recevoir des documents de dernière minute.

![](/media/carrefour_press_book_11.jpg)

Nous sommes parti sur des press-books à vis et couvertures indépendantes pour garder cette souplesse. Des pochettes en troisième de couverture des press-books ont accueillis des dossiers supplémentaires et trois éléments ont été incrustés dans le couvercle du coffret.

Nous avons collaboré avec les graphistes du studio Grafikmente pour préparer tous les visuels sérigraphiés sur toile et le façonnage des tirages.

![](/media/carrefour_press_book_5.jpg)