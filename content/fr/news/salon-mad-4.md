---
categories:
- Salon
cover: ""
date: 2018-09-24
description: ""
draft: true
title: Salon MAD 4
---
Le Salon MAD à la Monnaie de Paris a été notre premier salon comme éditeur. Voici des images du salon et de nos préparations.

IMG

Sur le stand : Léa Hussenot, Juliette Amigues (notre stagiaire de la Cambre), Paul Chamard, et Laurel Parker.

IMG

IMG

Nos premières éditions : Folded to Fit par Yann Sérandour, Prototype par Terrain de Jeux, Klima Pages #2 par Klima Intérieurs, Bag par Sara MacKillop, et notre Bookcase : a Tool for Designers.

IMG

Laurel Parker montre le nouveau Bookhouse, avec dessins de Klima Intérieurs, à Jacqueline Martin.

IMG

Signature du livre Prototype de Terrain de Jeux : Jérôme Barbe, Franck Léonard, et Fred Maillard.

IMG

IMG

IMG

Yann Sérandour en train de plier des tirages pour son œuvre « Folded to Fit ».

IMG

Laurel Parker avec notre ami Eric Sébastien Faure-Lagorce.

IMG

IMG

Nos super stagiaires à l’installation de notre image trompe-l’œil : Sam Bouffandeau de l’École supérieure d’Art et de Design d’Angers (ESAD) et Juliette Amigues, de l’École Nationale Supérieure des arts visuels de la Cambre, à Bruxelles.

IMG

Pour fabriquer le décor de notre stand, Paul Chamard a créé une image trompe-l’œil de notre showroom à partir d’une vingtaine d’images. Nous avons fait des tirages numériques sur du papier « dos bleu » chez Picto.

IMG

IMG

Un peu de bois, un peu de colle double-face, et beaucoup de patience…

IMG

IMG

De vraies étagères, peintes par Léa Hussenot…

IMG

La cimaise avec le vrai showroom derrière.

IMG

IMG

IMG

Une bière après les signatures avec nos amis Terrain de Jeux, et nos amis de l’atelier m.u.r.r. Vincent Busson et Ariel Fleizsbein.