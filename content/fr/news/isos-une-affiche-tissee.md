---
categories:
- Livre d’artiste
- Expositions
cover: /media/isos-tissage-atelier-01.jpg
date: 2014-05-18
description: ""
title: ISOs – une affiche tissée
translationKey: ""
---
_ISOs_, un œuvre concue pour le Péri’Fabrique pendant les D’Days, expositon du Musée des Arts Décoratifs.

![](/media/peri-fabrique-isos_06.jpg)

©Marco Illuminati

« Renouant avec l’émulation des faubourgs, le programme de co-création Péri’Fabrique finance chaque année la création de pièces uniques par un artisan d’art et un designer. Associé à l’Atelier 3, tisserands des tapisseries d’art, le studio de création graphique Undo-Redoa crée « ISOs »…

L’assemblage minutieux des éléments a été réalisé selon la technique de reliure de livres d’art par l’atelier **Laurel Parker Book**, maître dans ce domaine.  
Les modules, reliés au fil de chaîne, sont volontairement espacés entre eux pour créer une tapisserie mouvante où l’air et la lumière circulent librement. »

![](/media/peri-fabrique-isos_03.jpg)

![](/media/matiere_isos5web.jpg)

![](/media/isos-tissage-atelier-01.jpg)

![](/media/isos-tissage-atelier-02.jpg)