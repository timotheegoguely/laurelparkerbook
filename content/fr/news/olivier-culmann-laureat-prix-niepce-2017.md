---
categories:
- Boîte de présentation
- Livre d’artiste
cover: /media/culmann-deflexions-01.jpg
date: 2017-11-01
description: ""
title: Olivier Culmann, lauréat Prix Niépce 2017
translationKey: ""
---
[Olivier Culmann](http://tendancefloue.net/wp-content/uploads/test//2017/04/Dossier-dartiste-Olivier-Culmann_Prix-Niépce-2017.pdf), lauréat du Prix Niépce 2017, a imaginé avec Laurel Parker Book et [Picto Foundation](http://www.picto.fr/2017/decouvrez-lobjet-dartiste-dolivier-culmann/) une boite rassemblant sept séries, sept esquisses qui ont ponctué son parcours. Extraites de l’intimité de leur écrin, elles nous montrent les secrets d’un photographe dont l’œuvre reste depuis l’origine empreinte de recherche et de questionnement.

![](/media/culmann-loop.gif)

Nous avons commencé la phase de prototypage cet été avec le lauréat Olivier Culmann, Vincent Marcilhacy, directeur de Picto Fondation, et Élisabeth Hering, fabricant de tirage d’exposition du laboratoire Picto et chef de production des tirages du projet Niépce. Suite à des premières réunions à l’atelier de l’artiste (Tendence Floue) et chez Picto, nous nous sommes réunis à notre atelier pour la présentation du premier prototype.

![](/media/culmann-work-01.jpg)

Olivier Culmann, lauréat du Prix Niépce 2017

![](/media/culmann-work-02.jpg)

Olivier Culmann, Laurel Parker et Vincent Marcilhacy directeur de Picto Foundation

L’objet d’artiste d’Olivier Culmann présente des pistes de traverse. Des tentatives ou tentations qui ont jalonné le parcours du photographe depuis 1992. Des chemins divers sur lesquels il s’est aventuré. Certains d’entre eux seront prolongés, d’autres laissés momentanément en sommeil ou abandonnés à jamais…

Cet objet d’artiste a été réalisé avec le soutien de Picto Foundation en collaboration avec Laurel Parker Book, dans le cadre du [Prix Niépce Gens d’Images](https://gensdimages.com/2017/05/19/olivier-culmann-laureat-du-prix-niepce-2017/) décerné au photographe en 2017.  
Il s’agit d’une édition de 12 exemplaires signés et numérotés.

![](/media/culmann-deflexions-01.jpg)

L’édition inclut des tirages UV sur Dibond Alu brossé, des tirages UV sous plexi 3mm, des tirages argentiques d’après négatifs à l’agrandisseur, des tirages couleur RC sur papier Fuji, et des tirages jet d’encre sur des papiers divers de Hahnemühle FineArt.

_DéfleXions, 1992 – 2008_  
Olivier Culmann

Édition d’artiste réalisée avec le soutien de Picto Foundation, mécène du Prix Niépce, dans le cadre du prix décerné à Olivier Culmann par l’association Gens d’Images en 2017.

12 exemplaires, signés et numérotés, ont été fabriqués de cette édition d’artiste.

Tous les matériaux sont de qualité archivage.  
Pour la conception : Olivier Culmann, Laurel Parker et Vincent Marcilhacy.  
Pour les photographies et les textes : Olivier Culmann  
Pour le suivi du projet et la réalisation des tirages : Elisabeth Hering, Vianney Drouin, Patrice Baron, Payram, Guillaume Weihmann, Pierric Paulian et Maxime Le Gall du laboratoire Picto.  
Pour la réalisation des coffrets : Laurel Parker Book.

Avec le soutien d’Hahnemühle FineArt.

Olivier Culmann fait partie du collectif [Tendance Floue](http://tendancefloue.net).