---
draft: null
title: 'Inside Outside, exposition au Musée de la Chasse et de la Nature'
translationKey: expomuseechasse
description: >-
  Exposition de l'installation Inside Outside en Washi, au musée de la Chasse et
  de la Nature, bar à papier et rencontre avec les artistes Laurel Parker et
  Paul Chamard.
date: '2022-09-18'
categories:
  - expositions
cover: /media/parker-chamard-musee01-site.jpg
___mb_schema: /.mattrbld/schemas/news.json
---
![](/media/parkerchamardmusee01.jpg)

A l’occasion des 39e Journées européennes du patrimoine et de la Paris Design Week 2022, le Musée de la Chasse et de la Nature et la Villa Kujoyama, partenaires depuis 2016, invitent le public à venir découvrir le projet de recherche que Laurel Parker et Paul Chamard ont développé pendant leur résidence à Kyoto autour du papier japonais, le washi, inscrit au patrimoine mondial de l’UNESCO. Étonnant trompe l’œil, chaque élément de l’installation « Inside Outside » -kimono, tatami, packaging etc., est fabriqué en washi transformé selon des techniques traditionnelles. Ces objets d’interprétation nous invitent à interroger l’usage contemporain du washi devenu l’un des matériaux favoris des architectes, artisans d’art, artistes et designers.

![](/media/parker-chamard-musee05-site.jpg)

![](/media/parker-chamard-musee04-site.jpg)

![](/media/parker-chamard-musee02-site.jpg)

![](/media/parker-chamard-musee03-site.jpg)

![](/media/parker-chamard-musee01-site.jpg)
