---
categories:
- Non classé
cover: /media/books-london2.jpg
date: 2021-11-02
description: article Books. Art, Craft & Community
title: Article dans Books. Art, Craft & Community
translationKey: London-book-arts
---
Nous sommes heureux de faire partie du livre ‘Books. Art, Craft & Community’ ! De nombreux relieurs, imprimeurs, éditeurs et designers amoureux du papier et des livres partagent leur passion dans ce bel ouvrage édité par le London Book Arts et Ludion Publishers.

![](/media/books-london.jpg)

![](/media/books-london2.jpg)

![](/media/book-art-and-craft.jpg)