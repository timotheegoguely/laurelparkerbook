---
categories:
- Salon
cover: /media/mad-salon-2021.jpg
date: 2021-09-11
description: Salon MAD 6 - 2021
title: Let's go MAD
translationKey: salon-mad6
---
Si vous avez manqué le salon MAD 6 ce week-end (10-11-12 sept), voici un petit résumé :

Entourée de Paul Chamard, Agathe Ruelland Remy et Anissa Pasques-Faraday, nous présentions l’atelier-galerie-librairie Laurel Parker Book et ses éditions.

![](/media/mad-salon-2021.jpg)

![](/media/mad6-2021.jpg)

Pendant 3 jours, les visiteurs ont découvert sur notre stand des œuvres multiples éditées par Laurel Parker Édition. Nous avons également pris le temps d’expliquer notre travail (et passion) de design et fabrication au sein de l’atelier.

Merci à l’équipe du salon [Multiple Art Days](http://multipleartdays.fr/) pour l’organisation de cet événement.

« Let’s go MAD »

![](/media/mad-laurel-agathe-salon2021.jpg)