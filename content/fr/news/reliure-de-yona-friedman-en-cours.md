---
categories:
- Livre d’artiste
cover: /media/friedman_reliure-02.jpg
date: 2014-09-29
description: ""
title: Reliure de Yona Friedman en cours
translationKey: ""
---
Pour le projet de livre d’artiste de Yona Friedman, la reliure est à la fois basique et exigeante. La pré-perforation des pages nous permet faire une couture propre et d’équerre, ce qui était très important pour un livre aussi epuré.

1001 nuits + 1 jour de Yona Friedman, éditions [mfc Michèle Didier](http://www.micheledidier.com/index.php/gb/artists/mfc-yona-friedman/1001-nuits-et-1-jour.html).

![](/media/friedman_reliure-01.jpg)

![](/media/friedman_reliure-02.jpg)