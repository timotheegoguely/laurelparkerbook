---
title: '1001 nuits + 1 jour, de Yona Friedman'
description: null
categories:
  - Reliure
  - Livre d’artiste
cover: /media/friedman-1001nuits_02.jpg
date: 2014-09-30T00:00:00.000Z
___mb_schema: /.mattrbld/schemas/news.json
---
Ce livre d’artiste est un recueil de dessins réalisés par l’architecte et designer Yona Friedman. Les reproductions sont détachables grâce à une découpe laser en pointillés suivant le contour de l'image. Reliure à la japonaise.  
Édité par [mfc Michèle Didier](https://www.micheledidier.com/) à 75 exemplaires + 25 épreuves d’artiste.

![](/media/friedman-1001nuits_02.jpg)

![](/media/friedman-1001nuits_03.jpg)
