---
categories:
- Reliure
- Boîte de présentation
cover: /media/lpb-book-shoe-02.jpg
date: 2014-10-02
description: ""
title: Book shoe
translationKey: ""
---
Qu’est ce qu’un book shoe ? Littéralement, c’est une chaussure pour un livre. Ce concept a été créé par des conservateurs et restaurateurs de livres aux États-Unis pour faire une sorte d’étui ouvert afin de protéger les livres en attendant la fabrication d’une boîte de conservation.

![](/media/lpb-book-shoe-02.jpg)

Notre joli book shoe cubique s’inspire de leur design, et nous avons ajouté un couvercle pour en faire un objet fini. C’est une boîte pour la présentation des différentes reliures de brochures.  
Une poignée en ruban de lin et des fermetures en os rendent la boîte transportable. Parfait pour nos rendez-vous à l’extérieur !

![](/media/lpb-book-shoe-01.jpg)