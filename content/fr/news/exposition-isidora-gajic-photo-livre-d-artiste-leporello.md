---
draft: null
title: >-
  Exposition Isidora Gajic - Sirènes & l'éditeur du dimanche (4 novembre au 22
  décembre 2023) 
translationKey: expogajic
description: >-
  Exposition d'Isidora Gajic et l'éditeur du dimanche chez Laurel Parker Book.
  Art, photographie, éditions d'artiste.
date: '2023-12-22'
categories:
  - expositions
cover: /media/gajic-expo-03-site.jpg
___mb_schema: /.mattrbld/schemas/news.json
---
Nous sommes heureux de présenter le travail d’Isidora Gajic et de l’éditeur du dimanche, représentées tout d’abord par l’oeuvre collaborative à laquelle nous avons contribuée : l’édition du leporello Sirènes. Des éditions de tirages sur soie accompagnent l’édition. L’ensemble des livres de l’éditeur du dimanche sont disponibles à la consultation.

![](/media/gajic-expo-03-site.jpg)

![](/media/gajic-expo-01-site.jpg)

![](/media/gajic-expo-02-site.jpg)

crédit photos de l’exposition © Gregory Copitet
