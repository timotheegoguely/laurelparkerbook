---
categories:
- Non classé
cover: /media/laurelparkerbook-com.jpg
date: 2021-01-20
description: ""
title: Nouveau site web
translationKey: new-website
---
En cherchant des idées pour notre nouveau site web, nous avons commencé par lister plusieurs sites que nous aimons. En tête de liste se trouve le site web de nos amis [The Shelf Company](http://company.theshelf.fr/), éditeur du [_Shelf Journal_](http://journal.theshelf.fr/). Également sur notre liste de favoris se trouve le site de [Change is good](https://changeisgood.fr/), le studio de design graphique collaboratif de Rik Bas Backer et José Soares. Je suis fan de leur travail depuis que j’ai reçu en cadeau _Écrits_ et _Le cinéma de Michelangelo Antonioni_, d’Alain Bonfand (éditions Images Modernes), deux beaux livres conçus par le duo.

J’ai été surprise de découvrir que leurs deux sites web avaient été développés par la même personne : [Timothée Goguely](https://timothee.goguely.com/). Une piste toute tracée pour trouver notre développeur web ! De plus, Timothée est également designer et s’engage dans une démarche d’éco-conception, développant des solutions web sobres et accessibles.

Lors de notre première rencontre, Timothée m’a demandé de lui envoyer des visuels qui m’inspirent et nourrissent mon travail. Parmi les choses que j’ai envoyées, il y a une image du canon Van de Graaf de la construction de pages, tel qu’il apparaît dans _La forme du livre_ de Jan Tschichold.

![](/media/van-de-graaf-canon.png "Canon de Van de Graaf")

Cela a inspiré Timothée pour créer le design de notre site à partir d’une grille fixe de 9×9 qui s’adapte aux différentes tailles d’écrans. Il est structuré en trois menus principaux : projets, infos et actualités (articles sur l’environnement du studio). Il présente également les trois activités de l’atelier : reliure à la main, éditeur et galerie-librairie, le tout bilingue français / anglais.

![](/media/laurelparkerbook-com-safari-big-sur-light.png "Page d’accueil de notre nouveau site web")

Un soin particulier a été porté au choix et l’usage de la typographie : tous les textes sont composés en [Mint Grotesk](https://www.futurefonts.xyz/loveletters/mint-grotesk) Regular et Medium, une police de caractères dessinée par la fonderie belge [Love Letters](http://love-letters.be/), à la fois rigoureuse et joyeuse, contemporaine et enracinée dans l’histoire.

Enfin, l’usage d’un blanc cassé en arrière-plan et d’une gamme de gris foncés légèrement bleutés pour les textes adouci le contraste et apporte un confort de lecture, tout en rappelant l’univers du papier qui nous est cher.