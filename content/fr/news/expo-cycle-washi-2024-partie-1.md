---
draft: null
title: Exposition Cycle Washi 1 (14 janvier au 23 février 2024)
translationKey: expowashi1
description: >-
  Cycle d'exposition sur le washi, papier artisanal du Japon, chez Laurel Parker
  Book à Romainville.
date: '2024-02-28'
categories:
  - Expositions
cover: /media/washic1-01-news.jpg
___mb_schema: /.mattrbld/schemas/news.json
---
Le Washi – le papier traditionnel du Japon – a été inscrit il y a 10 ans par l’UNESCO sur la liste du patrimoine culturel immatériel de l’humanité. Fabriqué à partir de fibres de trois plantes – le kozo, le mitsumata et le gampi – le washi est utilisé depuis plus de mille ans non seulement pour l’écriture, la peinture et l’imprimerie, mais aussi pour la fabrication d’objets : vêtements, fils pour tissage, contenants, lanternes, parapluies, jouets, et sur les cloisons et portes pour définir l’espace intérieur. Le washi est utilisé à des fins cérémonielles telles que l’emballage de cadeaux et dans les temples et sanctuaires pour les rituels chamaniques.

Nous célébrerons ces 10 ans tout au long de l’année avec des conférences, des expositions et de nouveaux projets de conception et d’édition.

![](/media/washic1-04-news.jpg)

Un premier accrochage dans notre espace présente une partie de notre travail post-résidence de la Villa Kujoyama. De nombreux ouvrages de références sélectionnés dans notre bibliothèque sont également disponibles à la consultation.

![](/media/washic1-03-news.jpg)

Le “bar à papier” conçu à l’occasion de notre présentation au Musée de la Chasse et de la Nature est une invitation au public pour toucher les matières.

![](/media/washic1-01-news.jpg)

![](/media/washic1-05-news.jpg)
