---
draft: null
title: Exposition Cycle Washi 2 (3 mars au 17 mai 2024)
translationKey: washi2
description: null
date: '2024-03-03'
categories:
  - expositions
cover: /media/washic2-02-site.jpg
___mb_schema: /.mattrbld/schemas/news.json
---
Pour le Cycle Washi : N°2, nous vous invitons à découvrir les washi colorés.

Et également, le dimanche 3 mars de 15h à 16h : Table ronde avec : Sumiko Oé-Gottini, Aurore Thibout, Paul Chamard & Laurel Parker.

![](/media/washic2-04-site.jpg "De nombreux nuanciers présentés")

En exposant nos nuanciers de Washi colorés, nous présentons les différentes manières de teinter le papier. Directement dans la pulpe par des pigments naturels, provenant de minéraux ou de végétaux, ou ajouté sur la feuille au pinceau.

À travers ces couleurs de synthèse ou naturelles… chaque morceaux de washi nous transmet les teintes imaginées par les artisans japonais.

![](/media/washic2-01-site.jpg "Accrochage des œuvres d'Aurore Thibout lors du vernissage")

En parallèle des papiers, nous présentons les créations textile de la plasticienne Aurore Thibout lors du vernissage. Composées en partie de washi sous la forme de fil à tisser.

![](/media/washic2-02-site.jpg "Vue de l'exposition Washi, cycle 2 : Couleur")

![](/media/washic2-03-site.jpg "Seconde œuvre d'Aurore Thibout présentée")
