---
draft: null
title: Acquisition Regarder les oiseaux par le Musée de la Chasse et de la Nature
translationKey: acquisitionpetrovitch
description: null
date: '2024-05-17'
categories:
  - expositions
  - projet d'artiste
  - livre d'artiste
cover: /media/oiseaux-musee-11-web.jpg
___mb_schema: /.mattrbld/schemas/news.json
---
![](/media/oiseaux-musee-02-web.jpg "Françoise Pétrovitch et Laurel Parker à l'installation de l'œuvre au Musée de la Chasse et de la Nature")

C’est avec une grande émotion que nous annonçons l’acquisition et l’installation de ***Regarder les oiseaux*** de Françoise Pétrovitch au Musée de la Chasse et de la Nature. Nous remercions la Fondation François Sommer, Christine Germain-Donnat (ancienne directrice du musée), Rémy Provendier-Commenne (responsable des collections du musée), et Sumiko Oé-Gottini (conseillère artistique) qui ont fait de ce rêve une réalité.

![](/media/oiseaux-musee-04-web.jpg "Françoise Pétrovitch à l'installation de l'œuvre Regarder les oiseaux")

“L’action, ou le geste, est devenu le titre de cette pièce, qui a été imaginée pendant le confinement. De mars à mai 2020, confinée dans sa maison en Normandie, Françoise Pétrovitch lève les yeux et observe les oiseaux dans le ciel à travers sa fenêtre. C’est un moment de liberté. C’est un instant de poésie qui fait vagabonder l’esprit. Face au paysage naturel verdoyant : un regard, une fenêtre sur un mur, un bout du ciel et les oiseaux qui volent. Ce sont ces éléments les plus simples qui activent notre imaginaire. Françoise Pétrovitch ne dessine pas les oiseaux mais elle les pose dans l’air. Dans ses tableaux, nous pouvons voir à la fois la vibration imperceptible des ailes qui précède l’envol et la tension invisible dans l’air qui s’apprête à l’accueillir. Laurel Parker édition a souhaité rendre palpable dans une enveloppe matérielle sensible, cet état transitoire délicat des oiseaux dans la nature que l’artiste a d’abord cristallisé sur papier. Il s’agit d’un livre-objet, qui invite à regarder les oiseaux. Lorsque l’on ouvre la boîte, le couvercle devient fenêtre et le fonds se transforme en toile, bord de la fenêtre ou branche où se posent les oiseaux. La fenêtre est réalisée avec du verre soufflé à la bouche. Le verre est une matière vivante. Il continue à couler petit à petit dans une échelle de temps hors de portée pour l’œil humain. Les micros-bulles, imperfections subtiles, nous relient à la fenêtre d’une maison séculaire qui respire. Le cadre de la fenêtre en papier gaufré bleu-gris reflète quant à lui, l’humeur changeante du ciel printanier. Les oiseaux sont tous d’une forme et d’une couleur différente. Ils sont sept. Il est tentant de les prendre un par un et de les disposer sur la toile. Ce jeu de composition s’inspire de l’Ikébana, l’art floral japonais qui consiste à mettre en scène les fleurs en rapport avec l’espace pour y exprimer un paysage et la saisonnalité. Une fois le rituel de l’Ikébana terminé, les oiseaux regagnent la boîte. Le rangement prolonge le moment de plaisir comme si nous assistions à la fin d’une pièce de théâtre. Le rideau tombe, mais nous emporterons avec nous, la rémanence des scènes vécues.” — *propos de Sumiko Oé-Gottini*

![](/media/oiseaux-musee-12w-web.jpg)

![](/media/oiseaux-musee-11-web.jpg)

![](/media/oiseaux-musee-13-web.jpg)
