---
categories:
- Reliure
- Papeterie
cover: /media/ledoyen_menu_02.jpg
date: 2017-02-14
description: ""
title: Transformation du papier japonais par le pli
translationKey: ""
---
**Menu pour un dîner du Comité Colbert organisé par Hermes**

Le Comité Colbert, qui a comme mission de rassembler les maisons françaises de luxe et les institutions culturelles, travaille à la promotion de l’art de vivre français. Nous avons été invité par le service d’éditions chez Hermès afin de créer un menu pour un dîner spécial.

![](/media/ledoyen_menu_01.jpg)

Nous avons créé une reliure avec des papiers japonais pliés à la main et teintés de couleur or, les couvertures du menu restent souples. La couture est réaliser avec un fil or, qui ajoute une touche raffinée à l’ensemble, évoquant comme un rappel la teinte or présente sur chacun des plis.. Ce projet fait appel aux sens aussi bien dans son contenu que dans le travail de nos couvertures. Avec le toucher de la main on peut apprécier la texture si particulière du papier plié. Avec l’oeil, on peut admirer les effets de volumes crées par les plis, souligné par la teinte or.

![](/media/ledoyen_menu_02.jpg)

**Carte de voeux 2017 pour la F93**

Nos amis de la [F93](http://www.f93.fr/) à Montreuil nous ont contactés pour créer leur carte de voeux 2017. Nous avions déjà travaillé avec la F93 et leur graphiste Gaël Hugo pour fabriquer leur carte de voeux 2015.

![](/media/f93_carte_2017_01.jpg)

Cette année, nous nous sommes inspirés de leur nouveau logo, créé par Gaël Hugo, qui est un jeu de carrés. Utilisant le même procédé et le même papier que pour le projet d’Hermès, nous avons créé une carte souple et délicate, dont les plis formant des carrés apparaissent une fois le livret étendu. Le message, dans leur nouvelle typographie, est imprimé en sérigraphie en blanc mat par Frédéric Déjean de l’Orbis Pictus Club à Maisons Alfort.

Une petite couture en fil argent métallique se défait facilement pour ouvrir le livret et lire le message. L’affiche fonctionne à plat sur une table ou suspendue pour bénéficier de la transparence du papier.

![](/media/f93_logo_web.jpg)

Nouveau logo du F93 par [Gaël Hugo, One More Studio, Paris](http://www.onemore-studio.com/).