---
categories:
- Reliure
- Livre d’artiste
cover: /media/arsanios-98-thesis-for-translation-03.jpg
date: 2014-10-31
description: ""
title: 98 Thesis for Translation part 1, de Marwa Arsanios
---
La galerie Mor-Charpentier nous a contacté pour relier un livre d’artiste de Marwa Arsanios. Composé de deux anneaux et de pages perforées, le livre avait été présenté à une exposition : les visiteurs étaient invités à intervenir sur les espaces vides à côté des images et des textes imprimés pour y mettre leur « traduction ». Les anneaux, une méthode rapide et peu onéreuse pour relier un livre, étaient bien adaptés pour l’exposition mais pas pour la conservation de cette œuvre. Dans le temps, les pages auraient été abimées par les anneaux et le livre aurait été difficile à manipuler.

![](/media/arsanios-98-thesis-for-translation-03.jpg)

Nous avons préservé les pages entières et collé un onglet sur chacune. Les onglets sont ensuite reliés avec des vis dans une couverture recouverte d’une toile de qualité conservation.  
Le livre fonctionne sans crainte d’être abimé, et il est facile à tenir dans la main et à feuilleter.

![](/media/arsanios-98-thesis-for-translation-02.jpg)

[Lien vers la galerie Mor-Charpentier](http://mor-charpentier.com/)

[Lien ver le site de l’artiste Marwa Arsanios](http://www.marwaarsanios.info/)