---
categories:
- Reliure
- Press book
cover: /media/roux-livre-diplome-02.jpg
date: 2015-03-17
description: ""
title: Les books de diplôme
translationKey: ""
---
Nos clients-étudiants viennent d’écoles diverses, de formations et de disciplines différentes : photo, graphisme, architecture, mode, beaux-arts. Chaque année c’est un moment de découverte pour nous… des projets avec des idées fraîches, pleins d’amour et de passion, avec un oeil sur l’avenir. Voici quelques projets exceptionnels qui sont passés par l’atelier ces dernières années.

![](/media/roux-livre-diplome-01.jpg)

![](/media/roux-livre-diplome-02.jpg)

Ci dessus, _Fictitious Feasts_, de **Charles Roux,** étudiant à Icart Photo en 2014. Tirages photos collés sur onglets, couture sur rubans, reliure en cuir avec marquage à chaud. Photo de l’artiste. Prix du meilleur book de sa promotion.

![](/media/chen-livre-diplome_01.jpg)

![](/media/chen-livre-diplome_02.jpg)

_Mademoiselle S,_ de Yen-chun Chen, Icart Photo. Tirages numériques, reliure à la japonaise.

![](/media/artemise-livre-diplome-02.jpg)

![](/media/artemise-livre-diplome-01.jpg)

![](/media/artemise-livre-diplome-03.jpg)

_Colorful fragments of the absence_, d’**Artemis Pyrpilis**, étudiante à Icart Photo en 2010. Tirages numériques, double-pages imprimées et pliées sur la tranche (à la japonaise), couture « Singer » à la main, couverture en toile avec marquage à chaud en deux couleurs. Design de l’artiste.  
Photos : Artemis Pyrpilis

![](/media/gravlejs-nelson-goodman_02.jpg)

**Maya Mikelsone**, étudiante en 2010 à l’École du Magasin, formation professionnelle aux pratiques curatoriales. Une exposition se retrouve dans un livre : un dialogue entre _L’Art en théorie et en action_ de Nelson Goodman et les photographies de l’artiste Ivars Gravlejs. 10 exemplaires.

_L’Attente,_ de **Guilherme Do Carmo**, étudiant à Icart Photo en 2010. Ses tirages, sur papier fine art, sont présentés dans des pages marie-louise, avec des charnières en toile. Prix du meilleur book de sa promotion.  
Photos du livre : Guilherme Do Carmo

![](/media/ambroise_portfolio_01.jpg)

![](/media/ambroise_portfolio_02.jpg)

_Rose Gourmande_, le livre de diplôme d’**Amélie Amboise**, étudiante à Icart Photo en 2009. Amélie est venue avec le design déjà conçu : un livre composé de photos dans des pages marie-louise, et de dessins originaux sur les rectos des pages. Elle nous a apporté le tissu à carreaux, qu’elle a ensuite utilisé pour fabriquer des gardes-coussin pour la chemise. Nous avons fait une reliure sur onglet en accordéon. Marquage en pochoir fait par l’artiste. Prix du meilleur book de sa promotion. Nous gardons un très bon souvenir de cette collaboration avec une jeune femme très douée et très motivée.  
Photos : Amélie Ambroise

![](/media/ratane-livre-diplome_1.jpg)

![](/media/ratane-livre-diplome_2.jpg)

Un très beau projet, le portrait d’une femme atteinte d’un cancer du sein. _Céline_, livre de diplôme  
d’**Isabelle Ratane**, étudiante à Icart Photo en 2013. Reliure à la japonaise, tirages numériques.

![](/media/essayan-livre-diplome_1.jpg)

_Hors Saison_, de Lydie Essayan, Université de Vincennes Paris VIII. Tirages photos sur onglets, couture main, charnières en toile.