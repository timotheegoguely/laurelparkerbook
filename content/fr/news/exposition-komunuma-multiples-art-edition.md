---
draft: null
title: Exposition Komunuma Multiples (9 sept–29 oct 2022)
description: >-
  Exposition collective de multiples d'artistes provenant des galeries d'art
  contemporain de komunuma, à Romainville.
categories:
  - Expositions
cover: /media/expo-komunuma-multiples-01-w-gregory-copitet.jpg
translationKey: expo-komunuma-multiples
date: '2022-10-29'
___mb_schema: /.mattrbld/schemas/news.json
---
[KOMUNUMA](https://komunuma.com) : Komunuma signifie « communauté » ou « commune » en Espéranto, la langue du partage. C’est le nom adopté par quatre galeries d’art et une association d’artistes qui s’installent à Romainville à l’automne 2019, auxquelles s’ajoutent deux nouvelles galeries en 2020 et 2021, au sein d’un ancien site industriel du XXe siècle. Le site — un campus de près de 11000 m2 — a été réhabilité par le groupe Fiminco et restauré par l’agence d’architectes FREAKS. Sur le même site, la Fondation Fiminco accueille une résidence d’artistes et organise des expositions avec des artistes émergents. Le Frac Île de France/Les Réserves ouvre ses portes en 2022. Komunuma est : [AIR DE PARIS](http://www.airdeparis.com/), [GALERIE SATOR](https://galeriesator.com/), [GALERIE JOCELYN WOLFF](https://www.galeriewolff.com/), [IN SITU FABIENNE LECLERC](http://www.insituparis.fr/), [LAUREL PARKER BOOK](https://www.laurelparkerbook.com/fr/news/1/01/exposition-komunuma-multiples-9-sept-29-oct-2022/laurelparkerbook.com "retour accueil"), ET [GALERIE MAËLLE](http://www.maellegalerie.com/).

![](/media/expo-komunuma-multiples-06-w-gregory-copitet.jpg "Guy de Cointet /Air de Paris")

AIR DE PARIS : GUY DE COINTET, Sophie Rummel drawings Multiple de 150 / 1974 / offset La codification, le hiéroglyphe, la typographie représentent chez Cointet, dont l’oeuvre est fortement marquée par un intérêt pour les procédés de langage et notamment les techniques d’écriture de Raymond Roussel, un processus d’élaboration du mot mis en image, et de la couleur mise en forme. Ses dessins et livres attestent de son goût du mystère, des langages codés et de la cryptographie.

![](/media/expo-komunuma-multiples-04-w-gregory-copitet.jpg "Gabriel Léger / Galerie Sator")

GALERIE SATOR : GABRIEL LEGER, Vent Clair Série de 55 / 2016 / feuille d’aluminium, tubes, cire Vent clair est le titre d’un film fantôme d’Andreï Tarkovsky, qui n’existe que sous forme écrite — le cinéaste n’ayant pu le réaliser. Le texte de ce scénario a été retapé à la machine à écrire sur un long rouleau d’aluminium, puis coupé en pages (A4) placées ensuite dans des contenants scellés. Il n’y a pas d’images auxquelles se rattacher, mais seulement des mots. D’un film qu’on ne verra jamais, demeure un scénario, caché dans une boîte que l’on choisit d’ouvrir ou pas. La pièce conçue se veut donc à l’image de ce film qui n’existe pas, et dont l’absence correspond étrangement à l’histoire même : en suspens.

![](/media/expo-komunuma-multiples-01-w-gregory-copitet.jpg)

GALERIE JOCELYN WOLFF : WILLIAM ANASTASI, Without Title (Subway Drawing) Multiple n° 11 de 26 / 2000 / aquatinte William Anastasi’s so-called blind drawings investigate the relationship between chance and mark making. His first Subway Drawings, such as this, were made on rides to and from daily chess games with the experimental music composer John Cage in the 1970s. With a pencil held in each hand, Anastasi closed his eyes and allowed the vibrations of the train to move the pencils across paper.

![](/media/expo-komunuma-multiples-08-w-gregory-copitet.jpg)

LAUREL PARKER BOOK : YANN SÉRANDOUR, Et puis le bleu tourna au vert Multiple de 15 / 2022 / Laurel Parker Edition / leporello Cette longue frise colorée est composée par l’alignement d’une collection de retirages successifs du même ouvrage de Sartre : Esquisse d’une théorie des émotions publié aux éditions Hermann. Les teintes des couvertures ont été ordonnées dans l’ordre chronologique des différentes réimpressions du livre entre 1960 et 1995. Virant curieusement du bleu au vert, elle suit la dégradation progressive du design original de Frutiger et retrace par ses marques d’usage et de lecture la réception du texte de Sartre au fil du temps.

MAËLLE GALERIE : FLORYAN VARENNES, Archa Insula Multiple n° 5 de 70 / 2017 / foulard en soie, Maison Vianay Archa Insula reprend et tire la forme d’une oeuvre matricielle, Eva insula, une photographie sur laquelle apparaît un ensemble de plus de trois cents capsules d’insuline -liées au diabète - équivalent à une année de traitement. Assemblées en une composition sphériques, les aiguilles sont dirigées vers l’extérieur, l’objet photographie puis imprime sur un foulard en soie portatif, forme un blason alvéolée bleu clair.

![](/media/expo-komunuma-multiples-03-w-gregory-copitet.jpg "Mark Dion / In situ Fabienne Leclerc")

IN SITU FABIENNE LECLERC : MARK DION, Snakestone Multiple n° 2 de 12 / 2014 / Salon Verlag & Edition /sérigraphie et moulage plâtre peint manuellement, dans une boîte en carton noir MARK DION, Emanations of the Earth Multiple n° 11 de 16 / 2017 / Salon Verlag & Edition / sérigraphie et 4 moulages plâtre peints manuellement, dans une boîte Connu pour ses installations complexes inspirées des Wunderkammern comme des laboratoires scientifiques, Mark Dion s’intéresse particulièrement au rapport que l’homme entretient à la Nature à travers la construction du savoir et des discours scientifiques ayant cours depuis l’Antiquité. Ses projets se parent souvent des atours de l’expédition naturaliste, archéologique, impliquant parfois la figure de l’artiste imitant par la tenue et les gestes l’explorateur, le biochimiste, le détective ou l’archéologue.

![](/media/expo-komunuma-multiples-02-w-gregory-copitet.jpg "Mark Dion / In situ Fabienne Leclerc")

crédit photos de l’exposition © Gregory Copitet
