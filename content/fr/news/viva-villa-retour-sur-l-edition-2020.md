---
categories:
- Presse
cover: /media/viva-villa-01.png
date: 2021-04-05
description: ""
draft: true
title: ¡Viva Villa! - Retour sur l'édition 2020
translationKey: viva-villa
---
¡Viva Villa! - Retour sur l'édition 2020

Laurel Parker et Paul Chamard sont Lauréats de la Villa Kujoyama (Kyoto 2019) et participent au festival des résidences d'artistes Viva Villa, à Avignon (24 octobre 2020 - 14 mars 2021). Dans l'exposition Les vies minuscules, ils présentent l'installation Inside Outside #2, fruit de leurs recherches et de leur travail de résidents.

Le festival Viva Villa revient sur ces résidences d'artistes (Casa de Velázquez, Villa Kujoyama et Villa Medicis) dans une vidéo faite d'entretiens et de présentations des projets exposés dans Les Vies minuscules.

_(Laurel Parker et Paul Chamard, 3:00 min)_

[viva-villa-festival.mp4](/media/viva-villa-festival.mp4 "viva-villa-festival.mp4")