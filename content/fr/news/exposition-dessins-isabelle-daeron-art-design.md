---
draft: null
title: 'Exposition Water Calling, Isabelle Daëron (8 septembre au 27 octobre 2023)'
translationKey: expowatercalling
description: >-
  Exposition d'Isabelle Daëron chez Laurel Parker Book. Editions d'artistes,
  illustrations, objets et inspirations du Japon.
date: '2023-10-27'
categories:
  - expositions
cover: /media/daeron-water-calling-01.jpg
___mb_schema: /.mattrbld/schemas/news.json
---
![](/media/daeron-water-calling-01.jpg)

Water Calling est la seconde exposition d’Isabelle Daëron chez Laurel Parker Book. De nombreuses éditions sont présentes. Livres d’inspirations provenant du Japon, objets, dessins originaux, livres d’artiste, et une sélection présentant le travail éditorial de Yoshiko Nagai et de Saicoro (Hideyuki Saito). Une fresque murale dessinée au feutre nous plonge dans l’univers d’Isabelle Daëron…

![](/media/daeron-water-calling-03.jpg)

Water Calling est un livre conceptuel qui utilise des dessins d’Isabelle Daëron pour illustrer le mécanisme de l’eau souterraine qui coule abondamment sous Kyoto et comment les gens ont coexisté avec elle.Impression offset, marquage à chaud, avec une jaquette-affiche. Concept et dessins d’Isabelle Daëron Concept et texte de Yoshiko Nagai Design de Hideyuki Saito Saicoro, Tokyo, 2023.

![](/media/daeron-water-calling-02.jpg)

crédit photos de l’exposition © Gregory Copitet
