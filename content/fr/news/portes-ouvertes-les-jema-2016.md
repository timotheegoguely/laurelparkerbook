---
categories:
- Portes ouvertes
cover: /media/jema_2016.jpg
date: 2016-03-27
description: ""
title: Portes ouvertes – Les JEMA 2016
translationKey: ""
---
Nous ouvrons nos portes pour les JEMA : Journées Européennes des Métiers d’Arts.  
C’est le moment de venir nous voir, découvrir notre travail, et discuter de vos projets.

![](/media/jema_2016.jpg)

**Les 2 et 3 avril, de 11h à 19h**

Au plaisir de vous y voir,  
Laurel et Paul

![](/media/jema-atelier_lpbook-01.jpg)