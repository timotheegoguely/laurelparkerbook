---
categories:
- Coffret
- Boîte de présentation
cover: /media/vuitton-boite-polka_02.jpg
date: 2015-05-17
description: ""
title: Yves Marchand et Romain Meffre, coffret pour la Fondation Louis Vuitton
translationKey: blog-marchand-meffr
---
Les artistes Yves Marchand et Romain Meffre ont été choisis par la Fondation Louis Vuitton pour photographier le nouveau bâtiment dessiné par Frank Gehry afin de réaliser une exposition et un livre : _Fondation Louis Vuitton / Frank Gehry_, des éditions Skira, en collaboration avec la Fondation Louis Vuitton et Polka Galerie.

![](/media/vuitton-boite-polka_02.jpg)

Un coffret contenant 15 tirages pigmentaires de ces photographies a été édité pour l’occasion.

Ces deux artistes sont connus pour leur travail sur les ruines de Détroit et sur l’île abandonnée de Gunkanjima, les deux projets édités par les éditions [Steidl](https://steidl.de/SRP-0010195161.html?s=meffre).

Pour apprécier le travail d’Yves Marchand et Romain Meffre, voir le livre à la [librairie de la Fondation Louis Vuitton](https://librairie.fondationlouisvuitton.fr/fr/editions-de-la-fondation/fondation-louis-vuitton-frank-gehry-version-bilingue-anglais-francais/18.html) ou visiter [leur site](http://www.marchandmeffre.com/).

![](/media/vuitton-boite-polka_03.jpg)

![](/media/vuitton-boite-polka_04.jpg)

_Fondation Louis Vuitton / Frank Gehry_  
_Yves Marchand et Romain Meffre_  
Coffret en édition limitée à 12 exemplaires  
En toile blanche, avec du papier bleu allemand fait à la cuve, et un marquage à chaud.  
Client : Polka Galerie