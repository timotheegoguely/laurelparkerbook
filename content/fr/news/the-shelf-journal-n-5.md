---
categories:
- Presse
cover: /media/theshelfjournal5.jpg
date: 2017-11-30
description: ""
title: The Shelf Journal N°5
translationKey: ""
---
Dans ce numéro / In this issue : Peter Saville, Ed Ruscha, Laurel Parker, Tauba Auerbach,  
Jean-Jacques Rullier, Daniel Canty, Le Feu Follet, Plethora Magazine

![](/media/theshelfjournal5.jpg)

Les livres s’écrivent, se conçoivent, s’exposent et se répandent; les livres inspirent. /  
Books are written, designed, exhibited and spread around; books inspire us.

En vente à Paris à :

* [**0fr.**](https://www.facebook.com/0fr-238409536196250/)
* [**Amalivre**](http://www.amalivre.fr/)
* [**Artazart**](http://artazart.com/)
* [**Comme un roman**](https://www.comme-un-roman.com/)
* [**Flammarion Centre Pompidou**](https://boutique.centrepompidou.fr/fr/librairie/c1/1/)
* [**Galerie Perrotin**](https://www.perrotin.com/fr)
* [**Galignani**](http://www.galignani.fr/histoire-de-la-librairie-galignani.php)
* [**La Régulière**](https://www.facebook.com/lareguliere/)
* [**Le Monte en l’air**](https://montenlair.wordpress.com/)
* [**Les Arpenteurs**](https://www.facebook.com/librairie-Les-Arpenteurs-108071795890460/)
* [**Librairie du Jeu de Paume**](http://www.librairiejeudepaume.org/)
* [**Librairie Erasmus**](https://www.erasmus.fr/main/public/Login.aspx)
* [**Musée d’Art Moderne de la ville de Paris**](http://www.mam.paris.fr/fr/librairie)
* [**Palais de Tokyo**](http://www.palaisdetokyo.com/fr/page/la-librairie-du-palais)
* [**Section 7 Books**](http://section7books.com/)
* [**Yvon Lambert**](http://shop.yvon-lambert.com/)

Aussi en vente sur le site du Shelf Journal : [**http://journal.theshelf.fr/**](http://journal.theshelf.fr/ "http://journal.theshelf.fr/")