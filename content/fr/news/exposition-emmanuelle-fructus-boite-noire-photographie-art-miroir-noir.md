---
draft: null
title: 'Exposition Boite Noire, Emmanuelle Fructus (15 janvier au 3 mars 2023)'
translationKey: expo boite noire
description: >-
  Exposition d'Emmanuelle Fructus, oeuvres récentes liées à l'histoire de la
  photographie, la photographie anonyme et le miroir noir ou miroir de Lorrain. 
date: '2023-03-03'
categories:
  - expositions
cover: /media/boite-noire-fructus-03.jpg
___mb_schema: /.mattrbld/schemas/news.json
---
![](/media/boite-noire-fructus-01.jpg)

La boîte d’archivage a pour fonction la conservation de documents. Une fois ouverte, la matière contenue devient accessible et vivante. L’archive est une représentation d’une réalité passée, accompagnée de nombreuses incertitudes.

Depuis plusieurs années, Emmanuelle Fructus interroge l’archive et ses modes de consultation. Elle crée des boîtes qui érigent la photographie anonyme en sculpture. Tout document, lorsqu’il est placé dans une boîte, se consulte à plat, et sa prise en main libère le sens de sa lecture. Emmanuelle Fructus place volontairement les images photographiques à la verticale, les inscrivant dans un autre espace de lecture. Cette mise en forme de l’archive relève du classement, elle énonce un nouveau corpus. Majoritairement trouvés dans des rebuts de vieux papiers sur les brocantes, ces matériaux pauvres sont décrochés de leur contexte originel. Toutes ces petites figures minuscules, anonymes, ces fragments de papiers oubliés à priori sans importance, ces images déclassées, ces reliquats de l’histoire convoquent le silence. Moment suspendu où la photographie demeure une image impénétrable et secrète. Sa face visible est signifiante et son hors-champ nous aveugle.

En 2022, Emmanuelle Fructus rencontre le miroir noir. Cette surface sombre et réfléchissante la conduit à interroger de nouveau l’infini photographique - 1001 étant le titre de sa première oeuvre. Dans la chambre noire, un positif ou un négatif trop insolé devient noir ou inversement blanc. Pour elle, le miroir noir matérialise la surface sensible constitutive du médium photographique et induit la sédimentation des images. Il reflète la réalité, il ne peut rien enregistrer ; il signifie l’absence. Il précède l’invention de la photographie et figure cette volonté de retenir une trace du réel sans y parvenir.

Toute photographie est une transposition, voire une abstraction. Toute réalité est alors réduite à deux dimensions et à un rapport de valeurs. « Vers 1839, d’aucuns ont cru que l’invention du daguerréotype avait permis de vaincre la désillusion provoquée par le reflet éphémère, fugitif et labile du miroir, en fixant l’image reflétée. À telle enseigne, le daguerréotype constituait « le miroir de l’objet » pour Eugène Delacroix, un « miroir doué de mémoire » selon Oliver Wendell Holmes, un miroir qui a « gardé l’empreinte de tous les objets qui s’y sont reflétés » pour Jules Janin » (Citation du texte écrit par Arnaud Maillet à l’occasion de la création en 2022 de Miroirs noirs RVB JMC NB par Emmanuelle Fructus).

L’Histoire est un agencement provisoire et invite aux renouvellements des pensées. Le miroir noir est comme la photographie, une image fragile qui se décroche sans cesse de sa réalité pour mieux réapparaître. Tous deux sont des espaces de projections, de séparations, de souvenirs et de réincarnations. Ces surfaces sensibles évoquent de manière différente l’oubli et jouent de leurs forces mémorielles.La boîte noire serait alors cet espace de conservation des écarts entre les reflets, le visible et l’invisible, la lumière et le vide. Emmanuelle Fructus tente d’inscrire son expérience de l’archive et de la photographie, d’en évoquer les gestes spécifiques, de suggérer les déplacements du temps et du regard.

![](/media/boite-noire-fructus-03.jpg)

![](/media/boite-noire-fructus-02.jpg)

crédit photos de l’exposition © Gregory Copitet
