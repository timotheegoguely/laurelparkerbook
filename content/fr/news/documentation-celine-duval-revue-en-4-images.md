---
categories:
- Reliure
- Livre d’artiste
cover: /media/duval-la-revue-en-4-images_02.jpg
date: 2015-09-10
description: ""
title: Documentation céline duval – revue en 4 images
translationKey: ""
---
Nous avons collaboré avec l’artiste Céline Duval sur une nouvelle œuvre qui relie l’ensemble des 60 numéros de _La revue en 4 images_. Chaque revue est montée sur onglet, ce qui permet leur ouverture, et cousue sur ruban. Les éléments de reliure visibles font partie du design de l’objet. Il s’agit d’un livre pour des institutions et des collections privées qui souhaitent archiver et présenter cette belle revue dans son integralité.

![](/media/duval-la-revue-en-4-images_02.jpg)

Photographies :  
Ouverture des revues  
n°21 : mathilde  
n°40 : là-bas, co-édition avec Julie C. Fortier

[index des revues](http://www.doc-cd.net/Pdf/Revues-cd.pdf) / [documentation céline duval](http://www.doc-cd.net/)

![](/media/duval-la-revue-en-4-images_01.jpg)

![](/media/duval-la-revue-en-4-images_07.jpg)