---
categories:
- Expositions
cover: /media/capture-d-ecran-2021-10-11-a-11-09-41.png
date: 2021-10-15
description: projection-film-Arakawa
title: Projection du film d'Arakawa le 16 oct 2021
translationKey: projection-arakawa
---
Dans le cadre de l'exposition _Playground Studies_ proposée par Vincent Romagny, **la galerie Laurel Parker Book vous invite à la projection du film d'Arakawa, _For Example (A Critique of Never)_, le 16 octobre 2021 à 14h et 16h.**

“ Filmé en 1971 et présenté pour la première fois au public au Whitney Museum en 1972, _For Example (A critique of Never)_ suit le parcours et le comportement erratique d’un enfant des rues ivre dans le Lower East Side à New York, et notamment sur une aire de jeux. Regardant ces scènes filmées, aujourd’hui difficilement imaginables et même difficilement soutenables, l’artiste, avec avec sa compagne la poétesse Madeline Gins, affirme en 1974 dans une publication éponyme que 'le vertige est son médium'. L’enfant est alors perçu comme un 'paratonnerre' et ses gestes comme autant d’interrogations métaphysiques. Il est la figure exemplaire de la recherche des deux artistes conceptuels sur l’origine de la signification. Aujourd’hui, l’œuvre pose la question des limites de la romantisation de l’enfant en situation de vulnérabilité qui empêche de percevoir sa détresse.”

\- Vincent Romagny -

![](/media/capture-d-ecran-2021-10-11-a-11-09-41.png)_For Example (A critique of Never)_ de Arakawa (1971), Arakawa, capture d'image à 22:08 min

_For Example (A Critique of Never)_ de Arakawa (1971)  
Film 16 mm transféré en dvd, n&b, 95 min.  
Filmé en 1971 et présenté pour la première fois au public au Whitney Museum en 1972, _For Example (A critique of Never)_ de Arakawa est un film expérimental qui suit le parcours et le comportement erratique d'un enfant des rues sur une aire de jeux new yorkaise.  
  
_Crédits : Courtesy of Reversible Destiny Foundation and Arakawa + Gins Tokyo office_