---
categories:
- Non classé
cover: /media/dsc_0188.jpeg
date: 2020-12-31
description: ""
title: Bienvenue à notre nouvelle apprentie 2021
translationKey: agathe-apprentie
---
2021 commence déjà très bien !! Nous sommes ravis d’annoncer qu’Agathe Ruelland Rémy rejoindra notre équipe pour 2021 en tant que designer graphique et apprentie en fabrication dans notre nouveau studio Komunuma.

![](/media/dsc_0188.jpeg)

![](/media/agathe-atelier-birthday.jpg)

Agathe est Lauréate du Prix de perfectionnement aux métiers d’art, Savoir-faire en transmission ! Elle a obtenu son DNA en option design graphique (avec mention photographie) et son DNSEP (Master) en design graphique mention édition de l’ESAM Caen-Cherbourg. Elle a fait un semestre Erasmus en master à KMD, École d’art et design à Bergen, en Norvège. Ella a fait un stage chez nous en 2020 et a travaillé sur des projets très divers : Regarder les oiseaux (Françoise Pétrovitch), elle a ciré notre kimono pour Inside Outside #2 (notre expo à la Viva Villa, Collection Lambert), elle a aidé avec notre nouveau site web, et surtout notre déménagement à Komunuma, etc etc. Nous tenons à remercier les mécènes : la Fondation Rémy Cointreau, la Fondation Michelle et Antoine Riboud, l’agence Re-active dirigée par Gilles Muller, et les Fonds pour les Ateliers de Paris, ainsi que Françoise Seince, l’INMA, et la Chambre de métiers et de l’artisanat de Paris.

![](/media/agathe-atelier-oiseaux.jpg)