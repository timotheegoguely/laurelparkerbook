---
categories:
- Press book
cover: /media/flores-pressbook-cuir_02.jpg
date: 2014-09-02
description: ""
title: Press book en cuir, tirages numériques
translationKey: ""
---
Pour la photographe [Marie Flores](http://marie-flores.com/), nous avons trouvé des matières aux couleurs à la fois neutres et élégantes. Le veau de couleur taupe est chic et contemporain. Le marquage à sec, c’est-à-dire sans couleur, est discret. Les tirages numériques reliés directement dans le book, sans feuillet plastique, rendent l’objet plus proche d’un livre que d’un classeur. Des images double-pages, à fonds perdus, mettent le lecteur directement dans le monde du magazine imprimé, qui est la destination d’un travail photographique éditorial.

![](/media/flores-pressbook-cuir_01.jpg)

![](/media/flores-pressbook-cuir_02.jpg)