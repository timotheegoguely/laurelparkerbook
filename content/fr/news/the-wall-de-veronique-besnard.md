---
categories:
- Prototype
- Livre d’artiste
- Boîte d’éditeur
cover: /media/besnard_the_wall-01.jpg
date: 2018-04-24
description: ""
title: The Wall de Véronique Besnard
---
Nous avons réalisé une boîte pour l’artiste photographe [**Véronique Besnard**](https://www.veroniquebesnard.com/books) en janvier 2018. Véronique Besnard souhaitait un coffret pour sa série de livres d’artiste _The Wall_, formant une trilogie : _L’arène, Le présage, Le jardin_.

![](/media/besnard_the_wall-01.jpg)

Il s’agit d’une boîte en toile blanche avec un marquage à chaud noir.

Nous sommes fiers d’apprendre que Véronique Besnard fait partie des sélectionnés pour le prix First Book Award qui récompense un jeune photographe n’ayant pas encore été publié par une maison d’édition au Photo London.

![](/media/besnard_the_wall-02.jpg)