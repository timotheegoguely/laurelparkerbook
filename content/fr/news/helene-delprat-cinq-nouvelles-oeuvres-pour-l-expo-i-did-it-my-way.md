---
categories:
- Boîte d’éditeur
- Reliure
- Projet d’artiste
cover: /media/delprat_fausses-conferences-04.jpg
date: 2017-09-06
description: ""
title: Hélène Delprat – cinq nouvelles œuvres pour l’expo « I Did It My Way »
translationKey: ""
---
Pour son exposition monographique [**_I Did It My Way_**](http://lamaisonrouge.org/Maison%20Rouge/documents/mrDPfrHDelprat9250.pdf) actuellement présentée à la Maison Rouge, nous avons collaboré avec l’artiste Hélène Delprat et la Galerie Christophe Gaillard pour la création de cinq nouveaux coffrets en édition limitée.

« Dix ans après la projection dans le vestibule de W.O.R.K.S & D.A.Y.S, Hélène Delprat a pensé spécialement pour les espaces de La maison rouge, l’expositon _I Did It My Way._ Des miroirs et des films sombres, des immenses peintures aux titres hilarants, des voix de cinéma, des dessins radiophoniques, des têtes d’oiseaux, des photocopies, Louis XIV, _Judex_ de Georges Franju ou bien encore le curieux rituel de la tonsure… » [_du dossier de presse de la Maison Rouge_](http://lamaisonrouge.org/Maison%20Rouge/documents/mrDPfrHDelprat9250.pdf)

![](/media/delprat_fausses-conferences-04.jpg)

C’est en parallèle des préparations pour cette exposition monographique que nous avons collaboré avec [Hélène Delprat](http://www.helenedelprat.com) et Camille Morin de la [Galerie Christophe Gaillard](http://galeriegaillard.com/) pour créer 5 nouvelles éditions limitées. Ces nouveaux projets s’inscrivent dans la continuité des DVD Boxes que nous avons déjà réalisées ces dernières années.

Le coffret pour _Hi-Han Song Remix_, ringard et coloré, reprend une peinture de l’artiste imprimée sur papier gaufré. Impression numérique, marquage à chaud noir sur papier Chromolux, carton museum. Série de 5 exemplaires + 1 épreuve d’artiste.

![](/media/delprat_dvd-box_01.jpg)

Pour la vidéo _Les (Fausses) Conférence_s, le coffret prend la forme d’une diapositive 35mm avec une image transparente en noir et blanc. Carton museum, tirage numérique sur mylar, marquage à chaud noir. Série de 5 exemplaires + 1 épreuve d’artiste.

![](/media/delprat_fausses-conferences-02.jpg)

C’est en référence à la perruque de Louis XIV, créée par l’artiste en papier plié pour son film _Comment j’ai inventé Versailles_, que nous avons conçu l’extérieur du quatrième coffret. Papier japonais plié à la main, carton museum, marquage à sec. Design graphique Camille Morin. Série de 5 exemplaires + 1 épreuve d’artiste.

![](/media/delprat_fausses-conferences-03.jpg)

Sur la couverture du coffret _Les chants du guerrier couvert de cendres_, une image d’une tête cagoulée est dessinée par des perforations brillantes à travers un velours de porc. Série de 5 exemplaires + 1 épreuve d’artiste.

![](/media/delprat_fausses-conferences-01.jpg)

Pour _W.O.R.K.S & D.A.Y.S_, Hélène Delprat a imaginé un agenda personnalisé, avec une dorure sur tranche et une reliure en spirale, pour contenir le DVD. Tirages numériques sur papier Munken Polar. Design graphique Camille Morin. Série de 5 exemplaires + 1 épreuve d’artiste.