---
categories:
- Expositions
cover: /media/calendar-houses-mackillop-01-bis.jpg
date: 2021-03-13
description: ""
title: Exposition Calendar Houses de Sara MacKillop (14 mars - 23 mai 2021)
translationKey: ""
---
Nous sommes heureux de vous accueillir le **dimanche 14 mars** pour **le vernissage** de notre **exposition Calendar Houses** de **Sara MacKillop, de 14h à 18h.**

Pour celles et ceux qui ne seraient pas disponible, l'exposition se déroule **du 14 mars au 23 mai 2021** à la galerie **Laurel Parker Book**.

![](/media/photo-com-calendar-houses.jpg)

![](/media/calendar-houses-mackillop-01-bis.jpg)

Présentation de Calendar Houses par **Grégorio MAGNANI** :

"En cohérence avec son utilisation récurrente de la papeterie en tant que support artistique, Sara MacKillop présente de nouvelles sculptures qui évoquent le contexte particulier de leur création. Fabriquées en 2020, pendant le confinement, elles emploient une sélection de range-revues en carton et de calendriers muraux pour façonner une série de maquettes semi-architecturales, chacune suggérant le croquis d’une maison de vacances individuelle ou jumelée. Une fois regroupées, les sculptures forment d’ingénieux prototypes et leurs différentes options, pour des maisons prêtes à construire dans une luxueuse station de montagne.

Créées dans les limites de l’appartement de MacKillop, simplement conçues grâce à des objets disponibles dans sa papeterie locale et démontables en kit pour faciliter l’expédition, les sculptures proposent une approche décontractée et ludique de la création artistique. L’intérêt récurrent de l’artiste envers les langages visuels, via lesquels les entreprises qui commercialisent les fournitures de bureau tentent d’attirer leurs clients vers un article en particulier, comme par exemple un range-revue en carton rose ou un calendrier mural décoré de chevaux sauvages, est exploité pour créer une architecture de jeux et de loisirs.

Mais c’est justement par leur caractère ludique, que ces sculptures faussement architecturales offrent une réflexion au sujet d’un paradoxe fondamental de notre époque : l’exigence selon laquelle notre espace de travail et notre espace de loisirs soient entièrement séparés, et néanmoins parfaitement imbriqués l’un dans l’autre.

Les séjours au ski auxquels se réfèrent les sculptures sont une, sinon la destination de loisirs par excellence de nos jours. Elle promet luxe, solitude douillette, liberté au grand air, et une propreté impeccable. Cela représente un mode de vie qui est fondamentalement différent de notre vie professionnelle. Pourtant, comme le suggère avec ironie le travail de MacKillop, l’espace de loisirs et l’espace de travail ne sont pas si faciles à séparer. Ces petits bâtiments sont constitués à partir de matériel professionnel, le seul loisir qui les caractérise est celui inscrit dans les calendriers qui rythment la vie des travailleurs. Les murs carrelés ignorent l’inclinaison du toit, et permettent au dossier de transparaître, offrant à ces maisons un ersatz de solidité et des codes-barres apparent."

[Visualiser le PDF de présentation de l'exposition](/media/pdf-web-calendar_houses_expo-14march.pdf "présentation-expo-MacKillop-2021")

![](/media/mackillop-edition-expo-2021.jpg)

horaires galerie :

mardi - samedi

14h - 18h

Laurel Parker Book, membre de Komunuma

43 rue de la Commune de Paris

93230 Romainville