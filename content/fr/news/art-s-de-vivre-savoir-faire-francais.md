---
categories:
- Reliure
- Expositions
- Coffret de luxe
- Coffret
cover: /media/lpb-reliure_en_valise-01.jpg
date: 2017-05-03
description: ""
title: « Art(s) de vivre, savoir-faire français »
---
**Salon Révélation  
Biennale Internationale Métiers d’Art et Création  
3 au 8 mai 2017 au Grand Palais  
une exposition itinérante produite par l’Institut Français  
commissariat : Eric Sébastien Faure-Lagorce**

![](/media/lpb-reliure_en_valise-01.jpg)

Par-delà l’image de savoir-faire exceptionnels, les métiers d’art français ont contribué à la création et à la diffusion d’un art de vivre en mouvement permanent dont l’apogée aux XVIIème et XVIIIème siècles imprègne la culture française et son aura internationale.

Cette exposition explore l’excellence, l’élégance et le raffinement du XXIème siècle, tel qu’imaginés et façonnés par vingt ateliers et artisans d’art contemporains. Au rythme de grands chapitres qui illustrent un art de vivre si singulier et dans le dédale de pièces imaginaires, leurs productions révèlent leur attachement à l’histoire et à la tradition, leur capacité à s’en affranchir et leur aptitude à imaginer des objets d’exception, des objets d’art(s) de vivre. Certes séculaires et constitutifs d’un patrimoine matériel et immatériel, les métiers d’art sont d’abord un véritable vivier de création et d’innovation pour les domaines de la décoration, du luminaire, de la gastronomie, du livre, de la mode, du parfum ou encore du jardin.

![](/media/lpb-reliure_en_valise-04.jpg)

_L’Institut français est l’opérateur de l’action culturelle extérieure de la France. Placé sous la tutelle du ministère des Affaires étrangères et du Développement international et du ministère de la Culture et de la Communication, il travaille en étroite relation avec le réseau culturel français à l’étranger, constitué de 98 Institut français et 137 antennes répartis dans le monde, et de plus de 800 Alliances françaises dans le monde présentes dans 134 pays._

_Cette exposition est présentée au Grand Palais avec le concours du salon Révélations._

![](/media/lpb-reliure_en_valise-03.jpg)

_Reliure en valise_, un hommage à la _Boîte-en-valise_ de Marcel Duchamp. Sept reliures d’art dans un coffret. Veau blanc, veau noir, toile de lin, cartons japonais, papiers allemands, fil de lin ciré, bouton en nacre et crochets en os. Une invitation d’Eric Sébastien Faure-Lagorce pour une exposition itinérante produite par l’Institut Français.