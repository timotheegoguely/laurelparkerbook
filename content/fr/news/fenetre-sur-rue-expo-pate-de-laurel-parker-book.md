---
categories:
- Expositions
cover: /media/lpb-expo_container_02-1.jpg
date: 2014-11-26
description: ""
title: 'Fenêtre sur rue : expo « Pâte » de Laurel Parker Book'
translationKey: ""
---
Les objets exposés, ainsi que leurs socles et cadres, sont tous fabriqués en papier et carton.

Ces deux matières premières, faites de pâte végétale, peuvent être utilisées pour créer des supports extrêmement divers. Souple ou rigide, fin ou épais, imprimé ou teinté, brut ou recouvert, fait main ou à la machine, le papier se déguise et se transforme comme nulle autre matière première.

![](/media/lpb-expo_container_02.jpg)

![](/media/lpb-expo_container_01.jpg)

#### Informations pratiques

Découvrez l’exposition de Laurel Parker Book  
dans le Zzz au 19 rue Neuve-Tolbiac  
Du 24 au 30 novembre 2014

[Site des Berges, ville de Paris](http://lesberges.paris.fr/agenda/laurel-parker-book/)