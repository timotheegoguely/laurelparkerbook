---
categories:
- Coffret de luxe
- Coffret
cover: /media/baobab_ling_02.jpg
date: 2015-11-12
description: ""
title: Baobab, Tree of Generations d’Elaine Ling
translationKey: ""
---
La photographe canadienne [Elaine Ling](http://www.elaineling.com/) a documenté des arbres baobab sur le continent africain avec une chambre photographique 4×5 et de la pellicule Polaroid Type 55 P/N. Ces images incroyables sont tirées en grand format par Georges Charlier à l’atelier [Salto Ulbeek](http://amanasalto.com/fr/) en Belgique avec le procédé _platine / paladium_. Le résultat est magnifique.

![](/media/baobab_ling_02.jpg)

Laurel Parker Book a conçu le design de cette édition de luxe, travaillant avec l’artiste à toutes les étapes du projet. Le coffret a été fabriqué à la main à notre atelier à Paris. Les textes et la couverture ont été imprimés en sérigraphie par le [Orbis Pictus Club](http://www.orbispictusclub.fr/), à Maisons Alfort. Le design typographique a été fait par [Yoan De Roeck](http://yoanderoeck.com/). Le papier de texte a été fabriqué à la main par Frédéric Gironde de [Ruscombe Mill](http://www.ruscombepaper.com/fr/index.html) à Margaux. C’est une belle collaboration entre plusieurs artisans et artistes.

11 tirages de format 53 x 68 cm, édition de 5 exemplaires  
_Photos du coffret ©_ [_Hermance Triay_](https://hermancetriayphotographe.wordpress.com)_._

![](/media/baobab_ling_03.jpg)

![](/media/baobab_ling_01.jpg)