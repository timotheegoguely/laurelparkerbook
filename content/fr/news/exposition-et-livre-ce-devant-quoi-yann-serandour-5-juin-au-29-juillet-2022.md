---
categories:
  - Reliure
  - Livre d’artiste
  - Expositions
cover: /media/serandour_expo_laurel_parker_book_web1.jpg
date: 2022-06-04T00:00:00.000Z
description: exposition yann sérandour
title: >-
  Exposition et livre : Ce devant quoi, Yann Sérandour (5 juin au 29 juillet
  2022)
translationKey: cedevantquoi
draft: null
___mb_schema: /.mattrbld/schemas/news.json
---
![](/media/expo_serandour_01_w_copitet.jpg)

![](/media/expo_serandour_02_w_copitet.jpg)

Ce leporello reproduit, sous la forme d’une longue frise colorée reliée en accordéon, 66 couvertures du même ouvrage de Jean-Paul Sartre, Esquisse d’une théorie des émotions, publié aux édition Hermann. Les teintes des couvertures reproduites ont été ordonnées dans l’ordre chronologique des différentes réimpressions du livre entre 1960 et 1995. Virant curieusement du bleu au vert, elle suit la dégradation progressive du design original d’Adrian Frutiger et retrace par ses marques d’usage la réception du livre de Sartre au fil du temps. Sa couverture est illustrée par un dessin de Geneviève Asse, connue pour son usage très singulier des nuances de bleu dans des peintures qui constituent une recherche inlassable sur l’espace, la lumière, la transparence. Si la source du « bleu Asse » reste inconnue, les nuances produites par les couvertures des livres imprimées entre 1960 et 1965 évoque d’une manière tout à fait fortuite sa palette.

Paru pour la première fois en 1939, le texte de Sartre prône une approche phénoménologique des émotions. Il constitue la partie liminaire d’un traité de psychologie phénoménologique, La psyché, jamais publié. Dans cet ouvrage de formation, Sartre s’oppose l’idée que les émotions ne seraient que des manifestations déclenchées par notre corps et échappant entièrement à notre volonté. L’émotion est une transformation du monde. Face à l’impossibilité de transformer le monde, la conscience modifierait l’image de celui-ci afin de transformer illusoirement une réalité insupportable. « Dans l’émotion, c’est le corps qui, dirigé par la conscience, change ses rapports au monde pour que le monde change ses qualités. » écrit-il.

Sous l’apparence d’un nuancier coloré marqué par le passage du temps, ce leporello déployé au mur relie de nombreuses histoires et figures du monde de l’art et de l’édition. La couverture du livre édité chez Hermann a été conçue par le typographe suisse Adrian Frutiger (1928–2015), directeur artistique de la maison d’édition Hermann entre 1957 et 1967, à partir d’un dessin de nature morte que Geneviève Asse (1923–2021) offrit à Pierre Berès (1913–2008) pour illustrer ce texte de Sartre. Considéré comme l’un des plus importants libraires de la seconde moitié du vingtième siècle, ce collectionneur et marchand de livres anciens et de manuscrits littéraires racheta la maison d’édition scientifique Hermann en 1956 et l’ouvrit au domaine des arts en s’associant à l’historien d’art André Chastel. Il fit appel à Frutiger pour moderniser l’image de la maison. Poursuivant cet objectif, la collection « l’Esprit et la main » dans laquelle ce titre de Sartre fut réédité republie des textes philosophiques ou scientifiques en les associant aux illustrations des artistes du moment.

Yann Sérandour  
Et puis le bleu tourna au vert  
Leporello, 66 pages, 25 × 19 cm (fermé), 1254 cm (ouvert)  
Laurel Parker Edition, Romainville, France, 2022  
Graphisme Marine Bigourie  
Impression Média Graphic, Rennes  
Édition limitée à 15 copies + 4 HC signés et numérotés

crédit photos de l’exposition © Gregory Copitet

![](/media/serandour_bleu_03_w.jpg)

![](/media/serandour_bleu_02_w.jpg)
