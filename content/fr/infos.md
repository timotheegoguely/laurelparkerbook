---
layout: infos
title: Infos
translationKey: infos
description: null
___mb_schema: /.mattrbld/schemas/infos.json
---
{{< col >}}

## À propos

Laurel Parker Book est un lieu hybride consacré aux livres d’artistes, au croisement du studio de design, de l’atelier de fabrication, et de la galerie d'art.

Depuis 2008, Laurel Parker Book conçoit des livres, des boîtes et des multiples pour les artistes et les éditeurs, ainsi que pour les agences, les maisons de luxe, les institutions et le secteur privé. Notre atelier artisanal de reliure est dédié à la création artistique, à la belle présentation, et la conservation des œuvres artistiques et patrimoniales.

Depuis 2018, Laurel Parker Edition publie de l’art contemporain en multiple, toujours en pensant à l’objet-livre et l’objet artisanal.

En 2020, l'atelier s'est installé aux côtés de leurs confrères de Komunuma en ajoutant au sein de leur atelier un espace galerie – librairie – bibliothèque : Artist Book Space.

Suite à leur résidence à la Villa Kujoyama à Kyoto, Laurel Parker Book développe *Folded By Hand* : œuvres et surfaces en washi (papier artisanal Japonais) utilisant des traitements traditionnels. Ces objets - mélange de techniques japonaises et savoir-faire français - sont destinés à la décoration et aménagement d'intérieur.

{{< /col >}}

{{< col >}}

## Demande de rendez-vous

Afin de proposer des solutions de design, de formes et de matières pour les projets de commande, et pour chaque nouveau client, nous réalisons une consultation d'une heure à notre studio.

---

## Contact

Pour prendre rendez-vous, envoyez un message par email ou téléphonez au numéro suivant :

Tél : [+33 (0) 9 83 93 48 20](tel:+33983934820)  
Email : laurelparkerbook@gmail.com

Laurel Parker Book  
[43, rue de la Commune de Paris<br>93230 Romainville, France](https://g.page/laurelparkerbook?share)

Métro : Ligne 5, Bobigny Pantin Raymond Queneau  
Bus : 145, 147, 318  
Vélib’ : Station Gaston Roussel

---

## Réseaux sociaux

[Facebook](https://fr-fr.facebook.com/pages/category/Community/Laurel-Parker-Book-636027673183719/), [Instagram](https://www.instagram.com/laurelparkerbook/)

---

## Newsletter

[S’inscrire à notre newsletter](http://eepurl.com/beg7pH)

{{< /col >}}

{{< col >}}

## Équipe

Laurel Parker, fondatrice et directrice artistique de l’atelier, a étudié la peinture et le film au School of the Museum of Fine Arts, Boston, où elle a obtenu son diplôme des Beaux-Arts, et à l’Université de Tufts, pour son Bachelor of Fine Arts. Elle a continué ses études de reliure et d’impression au Center for Book Arts à New York. Elle a été lauréate à la Villa Kujoyama en 2019 en binôme avec Paul Chamard. Elle est nommée Artisan d’exception par la Fondation Remy Cointreau. Laurel donne des workshops et des conférences dans différentes écoles d’art et de design en France.

Paul Chamard, chef de production de l’atelier, a étudié la gravure à l’École Estienne et obtenu un Master en option objet-livre à la HEAR de Strasbourg. Il est responsable de la fabrication quotidienne des projets à l’atelier ainsi que des techniciens qui y travaillent. Paul a été lauréat à la Villa Kujoyama en 2019 en binôme avec Laurel Parker.

Coline Boudier Ligault, assistante de production, a étudié l'Histoire de l'Art à Strasbourg et obtenu un CAP de reliure. Elle a continué à l'ENSAV de la Cambre à Bruxelles en Bachelor Design du livre et du papier. En 2023, elle a participé au prix Savoir-faire en transmission du BDMMA avec l'atelier Invenio Flory.

---

{{< list data="clients" title="Clients" >}}

{{< /col >}}

{{< col >}}

## Prix & Soutiens & Label

[Académicienne, Académie des savoir-faire : Papier, Fondation Hermès,](https://www.fondationdentreprisehermes.org/fr/projet/skills-academy-2025-papier) Laurel Parker (2025)

[Lauréats, Villa Kujoyama, Laurel Parker et Paul Chamard](https://www.villakujoyama.jp/resident/laurel-parker-paul-chamard/ "voir l'article") (2019)

[Artisan d’Exception](https://www.fondationremycointreau.com/nos_artisans/laurel-parker "voir l'article"), Fondation Rémy Cointreau (2019)

[Grands Ateliers de France](http://www.grandsateliersdefrance.com/fr/mobile/laurel-parker-book-233.html), membre

[French Design by VIA](https://connectedby.lefrenchdesign.org/_/company?id=6601c4ecb8b1b7a79693f14b), adhérant

La Maison des Artistes, adhérante

Grands Prix de la Création, finaliste (2024, 2018, 2016)

Fabriqué à Paris, label (2019, 2018)

Bourse, Paris Création : Fonds de dotation des Ateliers de Paris, la Fondation Daniel et Nina Carasso (2014)

Bourse d’atelier, Semaest, Viaduc des Arts, Paris (2004)

---

## Salons & Expositions

{{< time >}}2024{{< /time >}}

-   [Anthology](https://sinople.paris/galerie/) / Sinople, Paris
    

{{< time >}}2024{{< /time >}}

-   10 ans de résidence métiers d'art à la Villa Kujoyama / Le Terminal, Kyoto
    

{{< time >}}2024{{< /time >}}

-   [Ligare](https://sinople.paris/galerie/) / Sinople, Paris
    

{{< time >}}2023{{< /time >}}

-   Biennale Émergence, Pantin
    

{{< time >}}2022{{< /time >}}

-   [Inside Outside / Musée de la Chasse et de la Nature, Paris](https://www.chassenature.org/rendez-vous/inside-outside)
    

{{< time >}}2022{{< /time >}}

-   Salon du livre rare, Grand Palais
    

{{< time >}}2021{{< /time >}}

-   Multiple Art Days
    

{{< time >}}2020{{< /time >}}

-   [Les vies minuscules / Festival Viva Villa / Collection Lambert](https://www.vivavilla.info/artistes/laurel-parker-et-paul-chamard/)
    

{{< time >}}2019{{< /time >}}

-   [La Nuit Blanche, Kyoto](https://www.villakujoyama.jp/nuit-blanche-kyoto-2019/)
    
-   Multiple Art Days
    
-   FLAT, Turin
    
-   Salon de Livres Rares & Objets d’Art, Grand Palais
    

{{< time >}}2018{{< /time >}}

-   Multiple Art Days
    
-   Salon de Livres Rares & Objets d’Art, Grand Palais
    

---

## Conférences

{{< time >}}2023{{< /time >}}

-   [Rencontre bibliothèque : Laurel Parker / Maison Européenne de la Photographie / Paris](https://www.mep-fr.org/event/rencontre-bibliotheque-laurel-parker-book/)
    

{{< time >}}2022{{< /time >}}

-   Classe prépa, Ateliers de beaux-arts de la ville de Paris / Conférence sur notre parcours artistique et des projets d’édition / Alexandra Fau, commissaire d’exposition et enseignante en histoire de l’art
    

{{< time >}}2018{{< /time >}}

-   Viva Villa, Marseille / Conférence sur des livres d’artistes / Federico Nicolao, commissaire du festival
    

{{< time >}}2017{{< /time >}}

-   “[Le commanditaire, le graphiste et l’imprimeur](http://www.laab.fr/le-commanditaire-le-graphiste-et-limprimeur-2017/ "voir l'article")” / FAC Rennes 2 / une invitation de Jocelyn Cottencin et Yann Sérandour
    
-   “La vie secrète des livres de photographe” / la Maison Doisneau de la photographie, Gentilly
    

{{< time >}}2015{{< /time >}}

-   “[Collaboration dans des éditions de livres d’artiste](https://www.cnap.fr/%C2%AB-editer-%C2%BB-formes-d%E2%80%99un-engagement "voir l'article")” / Cycle Éditer / ESAD d’Amiens
    

{{< time >}}2013{{< /time >}}

-   “Collaboration entre les ateliers Laurel Parker Book et Coralie Barbe Restauration” / série Plexus / École Estienne
    

{{< time >}}2011{{< /time >}}

-   “Projets de livres d’artistes de l’atelier Laurel Parker Book” / Center for Book Arts, New York
    

---

## Jurys

{{< time >}}2021{{< /time >}}

-   [Jury du Prix Elysée 2020-2022](https://prixelysee.ch/editions-precedentes/edition-2021/), 4<sup>e</sup> édition (Laurel Parker)
    

{{< time >}}2020{{< /time >}}

-   [Jury du Prix du Livre Paris Photo-Aperture Foundation](https://www.parisphoto.com/fr-fr/programme/2020/photobook-awards-2020/laurel-parker.html "voir l'article"), 9<sup>e</sup> édition (Laurel Parker)
    

{{< time >}}2018{{< /time >}}

-   Jury, VAE master en photographie, ENSP Arles (Laurel Parker)
    

---

## Pédagogie (Laurel Parker)

Université Rennes 2 / workshop édition en 3<sup>e</sup> année Licence professionnelle Design graphique, éditorial et multimédia (dans le cours de Yann Sérandour et Jocelyn Cottencin ), 2020–2025

ESADHaR · Le Havre / workshop édition en 3<sup>e</sup> année DNA (invitation de Sonia Da Rocha), 2023-2024

ESAD Valence / workshop books with a system en 3<sup>e</sup> année design graphique (invitation de Lisa Bayle & Samuel Vermeil), 2022

EESAB Bretagne, Rennes / workshop d’édition en 2<sup>e</sup> année design graphique (dans le cours de Jérôme Saint-Loubert Bié, etc), 2011–2022

EMI École des métiers d’information / workshop d’édition de livre photo (invitation de Jean Larive), 2020–2023

Parsons Paris / présentation d’édition de livre / AMT Art Media and Technology / (dans le cours de Tamara Rosenblum), 2021

ESAM Caen / workshop d’édition en 2<sup>e</sup> année design graphique (dans le cours de Juanma Gomez), 2018–2021

Lycée Eugénie Coton / workshop édition en 2<sup>e</sup> année communication (cours de Yoan De Roeck), 2019

ESAM Caen / workshop d’édition en Master d’édition (dans le cours de Juanma Gomez), 2018

ENSA Nancy / workshop édition de livre photo avec Marina Gadonneix et Remi Faucheaux / 2018

AA School of Architecture, visiting school Paris / workshop édition, 2017 & 2018

INP Institut national du patrimoine / workshop conditionnement des œuvres d’archivage, 2017 & 2018

Pôle supérieur de design Nouvelle Aquitaine / enseignante en Pratique Plastique et Médiation UE5, DSAA 2<sup>e</sup> année, 2016–2017

Pôle supérieur de design Nouvelle Aquitaine / workshop édition en 1<sup>re</sup> année DSAA (avec Élisabeth Charvet et Mahaut Clément), 2017

F93 / workshop édition, dispositif ULIS au collège Pasteur, Villemomble, Laurel Parker & Paul Chamard

Center for Book Arts, New York / enseignante reliure et arts du papier, 2001–2011

École spéciale d’architecture, Paris / workshop édition, 2009

SVA School of Visual Arts, New York / workshop édition, 2001

Horizons New England Craft Program, Williamsburg, Massachusetts / workshop reliure et arts du papier, 1996–2001

Massachusetts College of Art, Boston / workshop édition, 1998

---

## Presse

{{< time >}}2024{{< /time >}}

-   [“Composite Revue”](https://dumplingbooks.fr/composite/), par le Campus Fonderie de l'image, №5.
    

{{< time >}}2023{{< /time >}}

-   [“Tous les possibles du papier”](https://process.vision/article/laurel-parker-booktous-les-possibles-du-papier/), par Cyrille Jouanno, Process Magazine №36
    

{{< time >}}2019{{< /time >}}

-   “[Les livres de Laurel Parker](/media/roven14_livres_laurelparker_vieville.pdf "voir l'article")”, par Camille Viéville, Roven №14.
    

{{< time >}}2018{{< /time >}}

-   “[L’importance de l’écrin…Dans sont atelier parisien, Laurel Parker conçoit des écrins d’exception](/media/intramuros-198-atelier-laurel-parker.pdf "voir l'article")”, de David Kabla, Intramuros №198.
    
-   “[Laurel Parker Book](/media/grabados-y-edicion-61-laurel-parker.pdf "voir l'article")”, Grabado y edicion №61.
    
-   “[Atelier Laurel Parker Book : le livre, un objet d’art](/media/connaissance-des-arts-laurel-parker-book.jpeg "voir l'article")”, Connaissance des arts, hors-série métiers d’art en France 2018.
    

{{< time >}}2015{{< /time >}}

-   “[Profession relieuse d’art](/media/cote_paris-37-relieur-art.pdf "voir l'article")”, Amandine Schira, Côté Paris №37.
    

{{< time >}}2006{{< /time >}}

-   “[Laurel Parker : la reliure dans la peau](/media/elle-deco-160-reliure-laurel-parker.jpg "voir l'article")”, Elle Deco №160.
    

---

## Publications

{{< time >}}2022{{< /time >}}

-   *72 Saisons à la Villa Kujoyama* / Gallimard
    

{{< time >}}2021{{< /time >}}

-   *Books : Art, Craft & Community* / London Centre for Book Arts, Ludion.
    

{{< time >}}2020{{< /time >}}

-   *Les vies minuscules* / Festival Viva Villa, Collection Lambert.
    

{{< time >}}2017{{< /time >}}

-   “[Pour la reliure de l’art](/media/the-shelf-05-reliure.pdf "the shelf laurel parker")”, The Shelf Journal, №5
    

{{< time >}}2015{{< /time >}}

-   *Collectionner, conserver, exposer le graphisme : Entretiens autour du travail de Dieter Roth*, EESAB Rennes et le FRAC Bretagne
    

---

## Mentions légales

Design & développement web : [Timothée Goguely](https://timothee.goguely.com)

Caractère typographique : [Mint Grotesk](https://www.futurefonts.xyz/loveletters/mint-grotesk), dessiné par [Love Letters](http://love-letters.be/).

{{< /col >}}
